#!/bin/bash

# forces script to trap on error
set -e

compass compile ./apps/client -e production --force
