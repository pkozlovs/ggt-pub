#!/bin/bash

# forces script to trap on error
set -e

function apt_add_repo {
  add-apt-repository ppa:"$1" -y
}

function apt_update {
  sudo apt-get -y update >/dev/null 2>/dev/null
}

echo "==> dependency: git"
apt_add_repo git-core/ppa; set +e
apt_update; set -e
apt-get install git -y

echo "==> dependency: ruby"
apt_add_repo brightbox/ruby-ng; set +e
apt_update; set -e
sudo apt-get install ruby2.3 ruby2.3-dev -y

echo "==> dependency: nodejs"
npm cache clean -f
npm install -g n
n 7.2.1
ln -sf /usr/local/n/versions/node/7.2.1/bin/node /usr/bin/node
npm install npm@latest -g

echo "OK"
