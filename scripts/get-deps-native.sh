#!/bin/bash

# forces script to trap on error
set -e

DIR_ERL="./bin/erlang"

# insert additional dependencies here
declare -a deps=(
  "git"
  "software-properties-common"
  "build-essential"
  "ncurses-dev"
  "openjdk-8-jdk"
  "unixodbc-dev"
  "g++"
  "libssl-dev"
  "curl"
  "inotify-tools"
  "ruby"
)

# install dependencies one at a time, saying "yes" to all of them taking up space.
for dependency in "${deps[@]}"
do
  echo "==> dependency: $dependency"
  apt-get -y install $dependency
done

echo "==> dependency: node"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "==> dependency: kerl"
curl -fsSL https://raw.githubusercontent.com/kerl/kerl/master/kerl > $DIR_ERL/kerl
chmod +x $DIR_ERL/kerl >/dev/null

echo "==> dependency: rebar3"
curl -fsSL https://s3.amazonaws.com/rebar3/rebar3 > $DIR_ERL/rebar3
chmod +x $DIR_ERL/rebar3 >/dev/null
cp $DIR_ERL/rebar3 /usr/local/rebar3 >/dev/null

echo "==> dependency: mad"
curl -fsSL https://github.com/synrc/mad/raw/master/mad > $DIR_ERL/mad
chmod +x $DIR_ERL/mad >/dev/null
cp $DIR_ERL/mad /usr/local/bin >/dev/null

echo "OK"
