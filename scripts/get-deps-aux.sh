#!/bin/bash

# forces script to trap on error
set -e

# insert additional dependencies here
declare -a deps=(
  "sass:gem install sass"
  "compass:gem install compass"
  "sass-css-importer:gem install --pre sass-css-importer"
  "bourbon:gem install bourbon -v 4.3.4"
  "neat:gem install neat"
  "webpack:npm install -g webpack"
  "uglifyjs:npm install -g uglify"
)

for dependency in "${deps[@]}"
do
  key=${dependency%%:*}
  value=${dependency#*:}
  echo "==> dependency: $key"
  eval "$value"
done

echo "OK"
