#!/bin/bash

# forces script to trap on error
set -e

mkdir -p apps/client/scss/vendor/

echo "==> dependency: bourbon"
cd apps/client/scss/vendor/
bourbon install

echo "==> dependency: neat"
neat install 

echo "==> dependency: N2O websocket"
cd ../../../../
mkdir -p apps/client/src/javascript/lib
mad static min
sudo mv apps/client/src/javascript/lib/ggs.min.js apps/client/src/javascript/lib/ws.min.js


echo -e "\nOK"
