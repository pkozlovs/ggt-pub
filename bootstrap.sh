#!/bin/bash

# forces script to trap on error
set -e

DIR_ERL="./bin/erlang"

function log {
    echo "---------------------------------------------------------------------"
    echo "[bootstrap] $1"
    echo "---------------------------------------------------------------------"
}

mkdir -p $DIR_ERL

set +e
log "Resetting apt-get..."
rm /var/lib/apt/lists/* -vf &>/dev/null
apt-get update -y &>/dev/null && apt-get upgrade -y
echo "OK"
set -e

chmod +x ./scripts/*

log "Installing system dependencies..."
./scripts/get-deps-native.sh

log "Updating system dependencies..."
./scripts/update-deps-native.sh

log "Installing additional dependencies..."
./scripts/get-deps-aux.sh

# temporarily turn off error trapping since kerl likes to error a lot for no good reason
set +e

log "Installing erlang..."
$DIR_ERL/kerl build 19.0 b19.0
$DIR_ERL/kerl install b19.0 $DIR_ERL/i19.0
cp $DIR_ERL/i19.0/bin/* /usr/local/bin

# reset error trapping
set -e

log "Done!"
