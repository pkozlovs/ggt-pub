#!/bin/bash

# forces script to trap on error
set -e

rsync -rav -e "ssh -p24" --exclude-from ".gitignore" \
./* \
"$1@cs-gitlab.cs.surrey.sfu.ca:/home/$1/project-pipe/"