#!/bin/bash

# forces script to trap on error
set -e

function log {
    echo "---------------------------------------------------------------------"
    echo "[Installation] $1"
    echo "---------------------------------------------------------------------"
    echo -e "\n\n\n\n"
}

log "Starting Installation process..."

log "Running ./bootstrap..."
./bootstrap.sh
log "Finished ./bootstrap"

log "Getting dependencies..."
mad deps
log "Fishing getting depdencies"

log "Compiling..."
mad comp
log "Finished compiling"

log "Done!"
