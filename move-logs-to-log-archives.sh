if [ ! -d "log_archives" ] 
then 
  mkdir log_archives 
fi 

now="$(date '+%d-%b-%Y-%T')"
mkdir log_archives/$now 
sudo mv log/* log_archives/$now