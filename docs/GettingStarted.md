# Getting Started

  1. [Installation](#installation)
  2. [Running](#running)
  3. [Project Layout](#project-layout)
  4. [Core](#core)
  5. [Source](#source)
  6. [Model](#model)
  7. [Server](#server)
  8. [Client](#client)
  9. [Trouble Shooting](#trouble-shooting)

## Building And Running

From the root of the project run:
```
sudo ./build.sh
```
This will take a while. After which ```sudo mad plan repl``` can be run to launch the server.

[The readme](README.md) has more info, and a full installation guide may be written in the future.

## Cleaning And Running 

### TL;DR

Cleaning and running the project 

```
sudo mad clean      # remove compiled outputs
sudo mad deps       # get all dependencies
sudo mad comp       # compile the project
sudo mad plan repl  # start server and enter REPL
                    # browse to https://localhost/
```

### In Depth

#### Dependencies

`mad deps` pulls in all project dependencies. 

It starts off by pulling Erlang dependencies listed in `rebar.config` in the `{deps, [...]}` tuple. See the `rebar` docs for how this is structured. 

After that, as per the `{post_hooks, [...]}` tuple in `rebar.config`, it calls `get-deps-local.sh`, followed by an installation of our NodeJS client.

If dependencies are changed or added, it may be necessary to delete the `deps` folder entirely, and rerun `mad deps`.

#### Compilation

`mad comp` will compile all dependencies, our own Erlang code and the DTL. `rebar.config`'s `{post_hooks, [...}` has additional directives here for compiling our SCSS and Javascript.

Warnings are expected from many of our dependencies but, excluding erlydtl written, our own apps should aim to have no warnings in production code.

#### Run and REPL

`mad plan repl` are almost always called in conjunction. These commands run the server, with all necessary setup and all dependencies and local apps started, and launch into the Erlang Shell (REPL). From the REPL you have full access to the entire running server. 

## Project Layout

The Project has the following root structure:

```
.
├── Mnesia.nonode@nohost 
├── apps
├── deps
├── scripts
└── ssh
```

  - `Mnesia.whatever` contains the database
  - `apps` contains all of our code, split into applications which will be explored separately later.
  
```
        apps/
        ├── source
        ├── client
        ├── core
        ├── helper
        ├── model
        └── server    
```

  - `deps` carries all Erlang dependencies brought it from `mad deps`.
  - `scripts` contains pieces of `bootstrap.sh`, and scripts to setup the environment for the server, e.g. raising open file limits.
  
There are additional installations done in the home directory under `~/.ggs`

```
.
└── proj
```

  - `proj` contains all projects cloned by our users
  
## Apps

### Core

This app is responsible for communication with Git. It clones repos, as well as providing access to them. The Git Core API is written to be as generic as possible, so it returns results in `map()` form, which is then filtered and transformed by its clients, e.g. the model.

### Source

Source communicates with other backend services, such as GitLab. It has a generic entry-point, the `source` module, which then splits requests off to various backends depending on the endpoint selected. Currently only GitLab is supported. Values returned are opaque records that can be read with a corresponding `source_*` wrapper.

### Model

Model is currently two things: the database and the analyzer. It will be split up soon. Analysis is started in `snapshot_generator` and database access is done through `db`. 

### Server

The backend for our own app, the server facilitates communication between the front-end (Client) and the model. It runs on N2O and makes heavy use of WebSockets.

### Client

The front-end portion of our project, Client contains Javascript, (S)CSS, HTML, and Erlang that compiles into the aforementioned languages. 

## Trouble Shooting

> This language is dumb and ugly!

Go [Learn You Some Erlang](http://learnyousomeerlang.com/content) and get over it. 
Helpful chapters include:

  - [lists](http://learnyousomeerlang.com/starting-out-for-real#lists)
  - [modules](http://learnyousomeerlang.com/modules#what-are-modules)
  - [pattern matching](http://learnyousomeerlang.com/syntax-in-functions#pattern-matching)
  - [cases](http://learnyousomeerlang.com/syntax-in-functions#in-case-of)
  - [recursion](http://learnyousomeerlang.com/recursion#hello-recursion)
  - [static typing](http://learnyousomeerlang.com/dialyzer#typing-about-types-of-types)
  
You can learn a lot of computer science through this dumb and ugly language.

---

> I don't understand the squiggles in the HTML!

Those squiggles are used to inject content into otherwise static HTML server-side. The squiggles call Erlang code in the appropriate module. Go [read about DTL](https://github.com/erlydtl/erlydtl/wiki).

---

> My code doesn't compile. Who do I complain to?

First off, do a fresh pull, delete the `deps` folder, delete the `Mnesia` folder, and run `mad clean`. After that, run `mad deps comp`. 

---

> I'm getting crashes in analysis after I pulled!

Delete your `Mnesia` folder, you may have out-dated record definitions. 
Then run `mad clean comp plan repl`.

---

> I'm getting crashes in Erlang's own `lists` implementation, this language sucks!

Go down the stacktrace until you see code in one of our apps; look up the type-spec of the functions to check that you're actually passing in a list. 

---

> I changed a dependency in the list, but it doesn't work!

Delete your `deps` folder, and run `mad deps`. 

---

> My records don't work or show up in the shell!

Record definitions are not loaded into the shell by default; they are just wrappers around tuples. You can load record definitions manually via `rr("apps/my-app/include/record.hrl")`. It be helpful to keep a text doc of all the record definitions you frequently use for pasting into the REPL.

---

>  I want to see what the database contains for a specific snapshot!

To do this you'll need to follow these instructions:
  1. Generate a snapshot
  2. Click on the snapshot and copy the snapshotId from the url. `localhost/snapshots/SnapshotId`
  3. Load the data_dump utility `c("apps/db/src/convenience/data_dump.erl")`
  4. Run data_dump:dump_data("SnapshotId", "pathToFile")
  5. Open the file you passed to the function to view the dump of the snapshot

The dump of the snapshot currently does not include every possible entry within the databaase, however, it does provide the most common things you'll want to look at. These include most merge information, most commit information, and information on project author and members.