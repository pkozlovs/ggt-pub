# Git Core

## Table of Contents
1. [Introduction](#introduction)
2. [The Process](#the-process)
3. [Shell Calls](#shell-calls)
4. [Parsing](#parsing)
5. [Maps](#maps)
6. [Repo Management](#repo-management)
    1. [Bragging Rights](#bragging-rights)
    2. [Storage](#storage)
    2. [Naming](#naming)
    3. [Cloning and Pulling](#cloning-and-pulling)
7. [MultiProcessing](#multiprocessing)
    1. [But Why?](#but-why)
    2. [The Implementation](#the-implementation)
    2. [Limitations](#limitations)
5. [Epilogue](#epilogue)

## Introduction
This is a shallow overview of our Git Core app. Its current implementation now contadicts many principles that we evolved to throughout the development of this tool, and so the plan is to have this rewritten in the foreseeable future.

The point of this framework is to interact with Git software installed on a Unix system from our tool. We then format the data and transform it to Erlang's map data structure. The current implementation heavily relies on Synrc's [External Shell Protocol library](https://github.com/synrc/sh).

## The Process
The process of data movement is very simple:
1. We make an external call to shell's git
2. We parse the output
3. We distribute the parsed output into appropriate data structures

## Shell Calls
First, we utilize sh's ability to execute shell commands at a specific destination. Sh is really just a wrapper around Erlang's ports. However, it provides a simpler API which helps to produce hastier solutions. For each type of information we require, we have a specific git call that will yield a textual return formatted so that it's easy for us to parse. For example, to get commit's information such as email, date and message we do this:
```erl
Raw = show(Sha_l, ["--quiet", "--pretty=%an%n%ae%n%ct%n%P%n%s"], Cwd),
```
Where `Cwd` is the directory for the local repo we are looking at. (we'll cover local repos and paths later) `show` is a wrapper around sh's code, which eventually leads to:
```erl
show(Sha, Args, Cwd) -> execute_git(["show", Sha|Args], Cwd).
```
and
```erl
execute(Command, Args, Cwd) ->
  {done, _Status, Out} = sh:run(Command, Args, binary, Cwd),
  if
    ?DEBUG -> error_logger:info_msg("  git exec: ~p ~p~n", [Command, Args]);
    true   -> skip
  end,
  Out.
```

## Parsing
After we receive the output from Git, we use Erlang's powerful binary functions to separate the string into tokens, which then can be treated as real pieces of data.
```erl
TokenList = binary:split(Raw, <<"\n">>, [global, trim]),
[AuthorName, AuthorEmail, CommitDate, ParentShas, Title] = TokenList,
.
.
.
```

## Maps
The final stage is to put the date into a data structure that can be read by the client modules. As we can see it's not rocket science just yet.
```erl
#{
    sha     => Sha,
    aname   => AuthorName,
    aemail  => AuthorEmail,
    date    => DateTime,
    pshas   => binary:split(ParentShas, <<" ">>, [global, trim]),
    title   => Title,
    message => Message,
    files   => dirty_files(Cwd, Sha_l)
  }.
```

## Repo Management
### Bragging Rights
None of this would be possible if we did not have any repositories stored locally. One of the questions that may come up is "why would one want this framework to begin with, when we can get all this information from the git service providers via `Source` app?". One of the bigger concerns to us as developers is the speed of our tool. Establishing a remote connection, fetching data, going through all possible errors, timeouts and other obstacles add up to create a massive overhead. When it comes to a large amount of data that needs to be constantly fetched and updated, we will eventually hit a curbstone with remote services. Not to mention that we may turn into DDOSers. As far as speed goes, this is not the only concept that we tackle, but more on this later.

### Storage
In order to store the repositories, our app creates a hidden directory at `$HOME`. Every time we pull a repo, it gets placed there.

### Naming
One of the core ideas we practice is that every repo should be uniquely identified. Since, we do not keep track of the path in our database, and repositories may repeat when it comes to names, we rely on identities. Identity can be a `binary` of any size or type. This allows us to get identities from virtually any type of data we have on our hands. This way the client does not have to worry about the implemntation details and just pass the binary to Git Core and everything is handled from there. So now we have a binary, how can we use it as a repository path/name? Quite simple: we convert binary into a hexadecimal string. (and vise-verse)
```erl
-spec bin_to_hex(binary()) -> binary().
bin_to_hex(Binary) ->
  <<<<Y>> || <<X:4>> <= Binary, Y <- integer_to_list(X, 16)>>.

hex_to_bin(Hex) ->
  try
    <<<<Z>> || <<X:8, Y:8>> <= Hex, Z <- [binary_to_integer(<<X, Y>>, 16)]>>
  of
    Result -> Result
  catch
    _:_ -> {error, {bad_hash, Hex}}
  end.
```

### Cloning and Pulling
Similarly to getting information about the repo, we clone it. (or pull to update if the repo is already there) We use the `HTTP` protocol, thus, we require a username and a password. As far as security goes, this is perfectly safe. The beauty of external shells is that they are like shadows: they appear and disappear without a trace. There is no shell history or logs or anything of that sort. Therefore, we can safely pass passwords as a command line argument.

## Multiprocessing
### But Why?
Conceptually, this app wouldn't be any better than a python script that an average comp sci student could write in their third or fourth years. However, as previously mentioned, we are _very_ concerned about speed. To our luck, we happen to be using Erlang and Erlang Virtual Machine, so processes are very cheap.

### The Implementation
Inspired by the idea of mapping a list (given a function f and a list of items, apply f to each item and replace in the said list) and MapReduce, we created our own variation. We perform the same task, but each application of a function is done in parallel to others. The list is then reconstructed with the transformed items in the original order.

The process of doing so is rather straight forward:
1. For each item, spawn a monitored* process that will return result to the parent process
2. Add the reference to the process to the head of reference list
3. Move on to the next item and repeat steps 1-3
4. Once the list is exhausted, catch responses in the order of the reference list and construct the new list
5. Reverse the list and return

\* We use Erlang's monitors to safely manage processes, keep track of their references and catch any crashes.

### Limitations
There is no utopia for programmers. While this multiprocessing approach is very powerful, we find ourselves limited by the OS's resources rather soon. There is a limit on how many shells, or any port-type of file can be open at once. While we can forcefully increase it, (and we do: `sudo ulimit -n`), it's not infinite. Once we exceeed it, strange things begin to happen and the entire system may go down. Our current solution is to batch the processes. Long story short: we found a sweet spot for how many opened ports can coexist at once, and limited multiprocessing to this number. Once we reach the limit, we prematurely reconstruct the portion of the list, and then move on to the next batch. Every time the batch is complete, we add it to the existing list of transformed items.

_Full implementation can be seen in `apps/helper/src/notmyjob.erl`_

## Epilogue
This monolithic app has successfully demonstrated the power of dividing tasks between operating system and remote APIs. Given its multiprocessing nature and careful implementation, we have achieved significant performance improvements over any other similar apps or scripts. One of the next steps to perfection is to fully employ OTP into this.