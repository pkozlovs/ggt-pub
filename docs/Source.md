# Source

## Table of Contents
1. [Introduction](#introduction)
2. [Structure](#structure)
3. [The Core](#the-core)
    1. [Pools](#pools)
    2. [Connections](#connections)
        1. [Generic Endpoint](#generic-endpoint)
        2. [Specialized Endpoint](#specialized-endpoint)
4. [Parsing & Errors](#parsing-&-errors)
    1. [Status Codes](#status-codes)
    2. [Body Parsing](#body-parsing)
5. [Wrapping](#wrapping)
    1. [Constructing](#constructing)
    2. [Accessors](#accessors)
6. [Putting It All Together](#putting-it-all-together)
7. [Implementing Your Own API](#implementing-your-own-api)

## Introduction
One of the core functionalities our application performs is accessing data from third-party git service providers, such as GitLab, GitHub, BitBucket, etc. (Supports GitLab only at the moment) 

Incidentally, these services provide REST API to gain access to their frameworks; and so it is as reliable as a house built out of matches.

And so, knowing the two simple rules, "we can't trust the input" and "we can't trust the output", how do we approach this feature?

_Note: this application utilizes [Hackney library](https://github.com/benoitc/hackney) for most of its core functionality._

## Structure
Let's first look at how our Source app is organized.

```
apps/
└── source/
    ├── include/
        └── source.hrl
    └── src/
        ├── endpoints/
            ├── endpoint.erl
            └── endpoint_gitlab.erl
        ├── wrappers/
            ├── source_commit.erl
            ├── source_diff.erl
            ├── source_group.erl
            ├── source_issue.erl
            ├── source_mr.erl
            ├── source_note.erl
            ├── source_project.erl
            └── source_user.erl
        ├── source.app.src
        ├── source.erl
        ├── source_app.erl
        └── source_utils.erl
```
We will be looking at all these files at the right time. It is also important to note that some of the configuration is placed in `sys.config` in the root of the project.

## The Core
To begin, we must make a request to a service provider via their REST API, and get some sort of response. Many issues arise here already: various timeouts, invalid requests, external server errors, forced pagination and so on.

### Pools

We start in `source_app.erl`. This is the heart of our application. It prepares our Hackney framework to establish connections with the correct settings, and ensures that all the required modules are loaded. Hackney operates on `pools`. Pool is a unification of all connections under one management system, allowing easy and maintainable control. We do _not_ care about the implementation of the said pool, but we do care about the settings we pass to create one. The first option we pass is `timeout`. This option determines for how long will an established connection live, i.e. "keep-alive".  The `max_connections` option sets the maximum number of connections that can reside within the pool. So, if a 1001st connection is about to be created, it will queue up and wait until there is room. Given the multiprocessing nature of our application and the inconsistency of GitLab, we set these values to be rather large. We also pass the name of the pool, `source_api_pool`, which we will use to create the connections. And so `hackney_pool:start_pool(source_api_pool, [{timeout, 150000}, {max_connections, 1000}])` starts the engine.

### Connections
While establishing a connection first, and then making a request would make the most sense. However, each server behaves differently, and we cannot possibly rely on that ideology. So, we will rely on our framework to figure it out for us by simply telling it to make one of the REST calls to the server.

#### Generic Endpoint
We will be inspecting our core bottom up to gain a better understanding of this process. First, let's look at `endpoint.erl`, line `100`. 
```erlang
request(Method, URL, Headers, Body) ->
  Options = [insecure, {pool, source_api_pool}],
  hackney:request(Method, URL, Headers, Body, Options).
```
Here, we specify the pool we created earlier and set the connection type to insecure to avoid ssl issues (it's a hack, but it doesn't expose any vulnerabilities in the given context). We then tell Hackney to make a request of type `Method` (either post, get, delete, or put) to the given URL with provided headers, body, and options. Moving up to the function `loop_request`, one may wonder: "why would we need that?" As it turns out, responses to request do not always return the entire data, and will often specify the "next" URL to the get the next chunk of resources. And so, on each request, we will be checking if such URL exists, and making a new request with that URL. We then unite all the responses into a single linked list.

_Note: we can (and probably will later), specify other options, such as 2 timeouts: one for establishing a connection, and the other for receiving one (unlikely to be helpful to us)._

Going up again, we have our wrapper functions that make it simple to invoke REST requests. They are the API of this module.

#### Specialized Endpoint
Next, we move to `endpoint_gitlab.erl`. This is a service provider specific abstraction layer that allows us to specify the URL, headers, body and formatting of everything else specific to the said provider before passing it to the generic `endpoint` module. Here, the requests are detailed and each function performs a specific task. For example, to get projects from GitLab, we would do this:
```erlang
-spec get_projects(source:token(), source:options()) -> endpoint:result().
get_projects(Token, []) ->
  URL = construct_url([<<"projects">>], []),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers);
get_projects(Token, [
  {group_id, GroupId}
]) ->
  URL = construct_url(
    [<<"groups">>, GroupId, <<"projects">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).
```
We construct a custom to the service provider URL, specify the personal access token in the header, and tell our `Endpoint` to handle the request with this data.

## Parsing & Errors
Our next stage is parsing the responses that services return to us. We don't trust the output, so there are only two types we expect: some form of data, or an error. Hackney utilizes Erlang's references, which can be be thought of as pointers to a certain extent, except the way memory managed is completely different. These references point to the data returned by the service _if_ the library does not error. After we determined whether the return is an error or an actual HTTP response, we can take a look at the status code and the actual body. Our point of interest is again `endpoint.erl`.

### Status Codes
Each HTTP response contains a response code. Hackney will conveniently place it in its return tuple: `{ok, StatusCode, RespHeaders, ClientRef}`. We do not care much about the headers other than the pagination mentioned earlier, so we disregard them at this point. This is one of the stages where we do defensive error checking. We will error on any status code other than `200` and `201`.

### Body Parsing
Next, we access the data from the reference. We make a naive assumption of the data being in JSON format (let's face it, it's not worth preparing for any other less common formats at this stage), and transform it to Erlang's map data structure.

This is what the parsing function looks like:
```erlang
-spec parse_refs(endpoint:result()) -> body() | error() | http_error().
parse_refs(Result) ->
  parse_refs(Result, []).

parse_refs({error, Reason}, _) ->
  {error, Reason};
parse_refs([], Acc) ->
  {ok, lists:flatten(Acc)};
parse_refs([{ok, {200, _, Ref}}|Rest], Acc) ->
  {ok, RawBody} = hackney:body(Ref),
  Body = jsone:decode(RawBody, [{object_format, map}]),
  parse_refs(Rest, [Body|Acc]);
parse_refs([{ok, {201, _, Ref}}|Rest], Acc) ->
  {ok, RawBody} = hackney:body(Ref),
  Body = jsone:decode(RawBody, [{object_format, map}]),
  parse_refs(Rest, [Body|Acc]);
parse_refs([{ok, {304, _, _}}|_], _) ->
  {error, not_modified};
parse_refs([{ok, {400, _, _}}|_], _) ->
  {error, bad_request};
parse_refs([{ok, {401, _, _}}|_], _) ->
  {error, unauthorized};
parse_refs([{ok, {403, _, _}}|_], _) ->
  {error, forbidden};
parse_refs([{ok, {404, _, _}}|_], _) ->
  {error, not_found};
parse_refs([{ok, {405, _, _}}|_], _) ->
  {error, method_not_allowed};
parse_refs([{ok, {409, _, _}}|_], _) ->
  {error, conflict};
parse_refs([{ok, {422, _, _}}|_], _) ->
  {error, unprocessable};
parse_refs([{ok, {500, _, _}}|_], _) ->
  {error, server_error};
parse_refs([{ok, {Code, _, _}}|_], _) ->
  {error, {unknown_status_code, Code}}.
```

At this point our parsing of raw data is complete and we move on to the higher level.

## Wrapping
Given the variety of data and its different formats, we do not want anyone using our API to suffer. (deep inside we don't actually care) And so, our next stage is wrapping the response data into a convenient "data structure" that provides clear access to its fields. Our file of interest is `source_project.erl`.

### Constructing
The first step to this is a transforming function, that takes the map returned by our parser and turns them to our opaque data structure. An opaque datastructure is one that has it's internal structure invisible to the outside world. (hence the need for accessors; it's by good design, reall) A constructor will look like this:
```erlang
dirty_construct_project(gitlab, #{
  <<"id">> := Id,
  <<"name">> := Name,
  <<"namespace">> := NamespaceMap,
  <<"name_with_namespace">> := SpecialName,
  <<"web_url">> := WebURL,
  <<"http_url_to_repo">> := CloneURL
}) ->
  Namespace = maps:get(<<"name">>, NamespaceMap),
  NamespaceId = maps:get(<<"id">>, NamespaceMap),
  OwnerId = case maps:get(<<"owner_id">>, NamespaceMap) of
    null -> null;
    UserId -> integer_to_binary(UserId)
  end,
  #project{
    id = integer_to_binary(Id),
    name = Name,
    namespace = Namespace,
    namespace_id = integer_to_binary(NamespaceId),
    owner_id = OwnerId,
    special_name = SpecialName,
    web_url = WebURL,
    clone_url = CloneURL
  }.
```

Here, we pattern match the previously created map and transfer its contents to our record. If we look in the file `source.hrl`, we can see the formal definitions of the records. These are important to the developer and should not be used by the client at any point. This is what a project record looks like:
```erlang
-record(project, {
  id :: binary(),
  name :: binary(),
  namespace :: binary(),
  namespace_id :: binary(),
  owner_id :: null | binary(),
  special_name :: binary(),
  web_url :: binary(),
  clone_url :: binary()
}).
-type project() :: #project{}.
```

### Accessors
The final stage of wrapping is to introduce field accessors that will allow clients to read specific data from our data structure. Each wrapper module will only accept a corresponding record and return data for its type. Defining a accessor is as simple as:
```erlang
-spec id(source:project()) -> binary().
id(#project{id = Id}) -> Id.
```

## Putting It All Together
Our last step is to create a public API that makes use of all the aforementioned features in the right order. We will first define a function that accepts the name of the service provider, the token used to gain access to the REST API, and options specific to the request. We then will make use of the transaction function which takes in the specifics of the provider, a high-level transforming function and the said options. The transactor will first make a request to a service provider, then parse the response, and apply a transformer function to it. That's right - we can perform other actions than just wrapping if we wish.

```erlang
-spec projects(source(), token(), options()) -> {ok, [project()]} | {error, term()}.
projects(Source, Token, [
  {group_id, _GroupId}
] = Options) ->
  transaction(Source, get_projects, fun(RawProjects) ->
    source_project:construct_projects(Source, RawProjects)
  end, Token, Options).
```

## Implementing Your Own API
The following depicts the order of implementation necessary to create custom API:
1. Define a method-specific request in `{sourcename}_endpoint.erl`
2. Define a record (if does not exist) specifcying your structure or modify the existing one in `source.hrl`
3. Implement a wrapper module (if does not exist), or modify it to add/remove accessors or single item constructor.
4. Create the uniting API function in `source.erl` that puts them all together.

_Note: instead of making use of a wrapper, one may want to define custom actions of some other nature. It can also be made it source-specific if you implement it similar to the wrapper architecture._