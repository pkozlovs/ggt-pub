# Database

## Introduction

Our database of choice is [Mnesia](http://erlang.org/doc/man/mnesia.html), sporting a hybrid data model with compiled queries, disk persistence, and flexible schemas.

Data in Mnesia is organized into tables. Each table stores one kind of record. 
Records have an ID and an arbitrary set of data. This data can include other records' IDs, which allows using one set of data to query for another, similarly to the way joins work in a typical SQL database. 

Database records are defined in their own modules, e.g. `db_commit`, and their schemas/tables are created at server-launch in `db_app`. The database is queried through the monolothic `db` module.

## Records

A database object is nothing more than an arbitrary collection of structured data with a name and an ID, implementing the `db_object` behaviour.

### `db_object`

All database objects must implement the `db_object` behaviour; not as a Mnesia requirement, but as a requirement of this project. From an object-oriented perspective, a behaviour is akin to a Protocol or Interface in other languages.

This behaviour has 6 functional requirements:

  - `id/1`, which is self-explanatory
  - `identity/1` which produces a stable `id` from some subset of the fields
  - `new/1` which acts as an initializer for the record
  - `record_info/0` which is required for generating database schemas
  - `is_self/1` which acts as a record-type guard. Because our records are opaque, this is a necessity. 
  - `assert_self/1` which throws if `is_self/1` is false, as a convenience to exit
  
In turn, the `db_object` provides a method for updating a conforming object's ID after mutation, provides a consistent hashing method for ID generation, and an accessor for getting any object's ID.

### Identity

Identity is the description of what an object intrinsically _is_. First name, last name, and SIN could potentially be uniquely identifying characteristics of a person; their address or hair colour can change, while the identity does not.

In our Mnesia tables, objects are uniquely identified by their ID: there can never be two objects in on table with the same `id` field; writing such an object, even with a different secondary field such as `hair_colour` for a person, will simply over-write the old one. 

It is for the above reason that `identity/1` is used to generate IDs: since only IDs are looked at by Mnesia, we generate IDs from all the fields we need to be unique. 

### Example

Let's take a look at a simple record that implements the `db_object` behaviour:

```erlang
%%% db_member.erl
-module(db_member).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0]).
%%% export accessors
-export([username/1, name/1, web_url/1, project_id/1]).
%%% export convenience
-export([new_fake/1]).

-type id() :: key().

-record(db_member, 
  {
    id :: id(),
    username :: binary(),
    name :: binary(),
    web_url :: binary(),
    project_id :: db_project:id()
  }).
-opaque self() :: #db_member{}.
```

  - We prefixed our object name with `db_` to make it clear that it's database object. 
  - We then include the `db_object` behaviour so we get compile-time warnings if we forget to implement a callback. 
  - We include a shared header used by all `db_object`s, which includes some shared types like `source()` and `key()`
  - We export two required types (not compile-time enforced),
    - `id/0` is the same for every object, but we define unique types for readability reasons
    - `self/0` is an opaque type representing the object, used for dialyzer and readability
  - We export all `db_object` callbacks, as required
  - We then export all accessors and other convenience functions (detailed further down)
  - The `db_member` record has an `id` as its first parameter, and arbitrary data afterwards
    - binary string fields for `username`, `name`, and `web_url`
    - a `project_id` of type `db_project:id()`, it's easy to understand what this is because of all the rigidness we've defined through `db_object`
  - `db_member` is exported as an `-opaque self()`, because clients should not depend on our internal structure.
  
----------------------

Right now this will fail to compile: we haven't actually implemented any of our callbacks, accessors, or convenience functions!

```erlang
id(#db_member{id=Id}) -> Id.

identity(#db_member{username=Username, project_id=ProjId}) ->
  Fields = {Username, ProjId},
  db_object:hash_fields(Fields).

new({Username, Name, Url, ProjId}) ->
  Self = #db_member{
    username = Username,
    name = Name,
    web_url = Url,
    project_id = ProjId
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_member).

is_self(#db_member{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).
```

`id/1` is trivial; all the accessors will look something like this. 

`identity/1` picks some subset of the fields (possibly all of them) and hashes them to generate an ID. 

`new/1` takes a tuple (for constency's sake) and creates a new record from its fields. It's a tad clunky, because breaking this API will require project-wide changes, but there's nothing we can do about that. 

The final line of `new/1` calls `db_object:update_id` as a short-hand for hashing its identifying fields and putting that value in the `id` field.

`record_info/0` simply calls the psuedo-function `record_info/2` on itself. This is used by Mnesia to generate schemas, but Mnesia cannot call the `/2` variant itself since our `db_member` is an internal record type. 

`is_self/1` and `assert_self/1` are trivial, and are implemented for API user convenience.

----------------------

```erlang
%%%
%%% Accessors
%%%
username(#db_member{username=Username}) -> Username.
name(#db_member{name=Name}) -> Name.
web_url(#db_member{web_url=Url}) -> Url.
project_id(#db_member{project_id=ProjId}) -> ProjId.
```

Accessors are trivial. 

Accessors should be clear and consistent across all db objects: the `project_id` should only ever refer to the database `db_project:id()`; the creation time of some object should always be `create_date :: calendar:datetime()`. Peruse the various `db_object` conformers to familiarize yourself with our naming conventions. 

------------------

Convenience functions can be essentially anything that deals with the defined object. In our `db_member` example we have a convenience `new_fake/1` function for creating members when GitLab encounters an error. 

## `db_app`

The database application is launched when the server starts. 

It is responsible for creating our tables and schemas in Mnesia in the `create_tables/0` function. 

An excerpt from the function:

```erlang
create_tables() -> 
  ?CREATE_TABLE(db_project, []),
  ?CREATE_TABLE(db_commit, [sha, author_id]),

  mnesia:create_table(db_project, 
                      [{disc_copies, [node()]}, 
                        {attributes, db_project:record_info()}
                      ]),
  mnesia:create_table(db_commit, 
                      [{disc_copies, [node()]}, 
                        {attributes, db_commit:record_info()},
                        {index, [sha, author_id]}
                      ]),
```

In order to keep the code clean and consistent, we use macros to create our tables.  

The first macro argument is the name of the table: we always make this the same as the record name.

The second macro argument is the list of fields to index. This _can_ speed up look-ups, but increases storage costs. 

Under the hood our macro expands into:

```erlang
create_tables() -> 
  mnesia:create_table(db_project, 
                      [{disc_copies, [node()]}, 
                        {attributes, db_project:record_info()}
                      ]),
  mnesia:create_table(db_commit, 
                      [{disc_copies, [node()]}, 
                        {attributes, db_commit:record_info()},
                        {index, [sha, author_id]}
                      ]),
```

We do this to avoid repetition. We almost always have the following arguments:

  - `{disk_copies, [node()]}`: states how we want to store our data, and on which nodes. If we ever decide to distribute our database, the second element of this tuple will change. 
  - `{attributes, db_project:record_info()}`: the schema definition, which `db_object` forces us to export. This tells Mnesia how much space to allocate in the table
  - `{index, [sha, author_id]}`: an optional parameter indicating which fields to index in this table. This will speed up look ups at the cost of memory usage.

Note: records defined in a header included by `db_app` can have their attributes retreived by `{attributed, record_info(fields, 'HEADER RECORD NAME')}`, but this practice should be avoided as `db_object` is the new standard.

Table creation _can_ fail, and in some cases this is appropriate: if the table is already created we have no need in creating again. For this reason we do not assert a successful return.

As verbose as it is to create a table, unfortunately it cannot be done dynamically; 
the `record_info/2` psuedo-function gets substituted at compile-time, 
so it's not possible to do a dynamic function invocation e.g. in a loop. 
It's for this reason that we have switched over to using macros. 

Finally, worth noting, is the non-standard create-table, which should be avoided. It is used for legacy tables that have not been ported to 
`db_object`, or ones whos authors were too lazy to read this doc. It looks like this:

```erlang
?NON_STANDARD_CREATE_TABLE(unique_id, record_info(fields, unique_id),
                            []),
```

Largely, the only difference is that we use the `record_info/2` psuedo-function. 
This function should not be available if our record is properly encapsulated. 

## Database Access

`db` is a monolothic module that provides database access APIs. 
Clients must never write their own queries outside of a select set of database modules. 

`db` provides constructors (to be superceeded by the `db_object:new/1` callback), 
getters, setters, and deletion functions, as well as several debug functions 
and a function for running a series of queries atomically. 

A typical `db` call will consist of arguments and a transaction which will 
atomically run queries and catch errors. 

### Naming

The `db` modules provides a fairly consistent API that we would like to maintain. Some guidelines:

  - Getters start with `get_`
  - A function that creates an object, but errors if it already exists is prefixed with `create_`
  - A function that simply writes and doesn't care if it already exists is prefixed with `update_`
  - Deleting functions are prefixed with `delete_`  

### Arguments

Database functions can be error-prone to call since all IDs are of the same type under the hood; 
calling `get_snapshot({snapshot_id, _})` with a `db_commit:id()` 
will not raise an error (the ID is valid) but will most likely return nothing. 

For this reason, we qualify the argument with an atom representing the expected data-type: 
making the call a little bit more tedious for clients in this way causes them 
to think a little harder about what input they're supplying. 

### Transactions

Transactions take a function to run, provide some simple error coalescing and catch all errors. 
Tables can be queried only from within a transaction for atomicity and consistency guarantees 
— to not do so is an error. 

Transactions promote Mnesia's return values to a `result` type, 
consisting of either `{ok, Value}` or `{error, Reason}`, 
where one special `Reason` value is `none`. 

`none` is a special error case for calls that should return at most one value. 
Let's take the functions `get_snapshots/1` and `get_snapshot/1` as examples: 
for the former it is perfectly valid to return an empty list if none matched the required criteria. 
The latter, however, does not return a list, but a single value; 
the lack of such a value gets promoted to `{error, none}` for easy pattern-matching. 

### Queries

#### Dirty Functions

Although queries can be written directly inside of a `db` transaction, 
we are moving towards "dirty" modules, to decrease the size of `db`. 
These "dirty" modules contain dirty functions, which are meant to be run inside of a transaction, 
thus making them "clean". Dirty functions are meant to be simple blocks 
that can be combined into more complex queries.

An example module is `dirty_commit`. 
This module provides simple functions such as `with_id/1` which takes a `db_commit:id()` 
and returns either the commit matching that ID, or `none`; 
as well as more complex queries such as `all_for_member/2`, 
which call 3 other dirty functions, as well as doing its own join afterwards. 

Note that dirty functions do not impose the same argument requirements as `db` functions: 
by the time a dirty function is called input is assumed to be of the correct semantic type. 

#### Queries

Queries are done through the `qlc` module. 

QLC stands for "Query List Comprehension", which is an elegant way of querying the database 
in a way already familiar to any seasoned Erlang developer: through list comprehension syntax. 
If you are unsure of how list comprehensions work, [RTFM](http://erlang.org/doc/programming_examples/list_comprehensions.html).

Once a query is formed it is transformed into QLC's internal format through `qlc:q/1`, 
and then executed with `qlc:e/1`. QLC queries do not return a `result` the way transactions do; 
they will either crash or return a list of values, making combining them much more straight forward. 
The crash will be caught and returned by the transaction, and no further work will be done.

One major difference between QLC and vanilla list comprehensions is the use of `Value <- mnesia:table(db_value)`. 
This generator will behave the same way as a list, but performs database magic under the hood. 

-------------------

Let's take a look at an example from `dirty_commit.erl`:

```erlang
with_id(CommitId) ->
  Query = qlc:q([Commit ||
    Commit <- mnesia:table(db_commit),
    db_commit:id(Commit) == CommitId
  ]),
  db:head(qlc:e(Query)).
```
      
At a high level: 

  1. Create our query
  2. Execute it
  3. Use the `head/1` convenience function to return `none` if the list is empty

What we're interested in is the query itself:

```erlang
[Commit ||
  Commit <- mnesia:table(db_commit),
  db_commit:id(Commit) == CommitId
]
```

In plain English: "Return every `Commit` such that `Commit` is in the `db_commit` table, 
and the `Commit`'s ID is the same as the passed in `CommitId`"

Generically, we can see the QLC as looking at every element of every generator, 
performing a boolean test on every combination of elements, 
and returning those that satisfy all conditions. 

--------------------------

A more involved example: 

```erlang
all_in_merge_request(MergeRequestId) ->
  qlc:e(qlc:q([Commit ||
    MRC <- mnesia:table(db_merge_request_commit),
    db_merge_request_commit:merge_request_id(MRC) == MergeRequestId,
    Commit <- mnesia:table(db_commit),
    db_commit:id(Commit) == db_merge_request_commit:commit_id(MRC)
  ])).
```
      
Plain english first: "Return every `Commit` such that there is a Merge Request Commit (`MRC`) 
that matches the passed in `MergeRequestId`, 
and the `Commit`'s ID is the same as the `MRC`'s `commit_id`". 

This effectively allows us to do a join: 

First we filter Merge Request Commits by their Merge Request ID, 
and then get all Commits that have the same ID as the Commit ID of any remaining Merge Request Commit. 

One thing to note here is that if two `MRC`s have the same `commit_id`, 
the corresponding `Commit` (if any) will be returned _twice_, 
as both combinations matched the condition. 
In our example this wouldn't happen because of the way we identify `MRC`s, 
but in other queries this could be an issue. 

------------------

As a final, complex example:

```erlang
-spec all_for_member(db_member:id(), db_snapshot:id()) -> [db_author:self()].
all_for_member(MemberId, SnapshotId) ->
   Snapshot = dirty_snapshot:with_id(SnapshotId),
   Project = dirty_project:for_snapshot(SnapshotId),
   ProjId = db_project:id(Project),
   Authors = dirty_author:all_for_member(MemberId, ProjId),
   qlc:e(qlc:q([Commit ||
     Commit <- mnesia:table(db_commit),
     Author <- Authors,
     db_commit:author_id(Commit) == db_author:id(Author),
     db_commit:project_id(Commit) == ProjId,
     db_snapshot:contains_datetime(db_commit:create_date(Commit), Snapshot)
    ])). 
```

We are using other dirty functions inside of this dirty function, such as

  - `dirty_snapshot:with_id/1`
  - `dirty_project:for_snapshot/1`
  - `dirty_author:all_for_member/2`

However, we do not do error handling to see if we _actually_ got a `db_snapshot:self()` or `none`; 
if we attempt to access `Snapshot` as a `db_snapshot:self()`, but it is `none`, we will simply crash, 
the transaction will handle it, and we'll continue on our merry way. 

The query is also more complex: 

  - `Commit` comes from all Commits in the table
  - `Author` comes from the `Authors` we computed earlier
  - We only keep `Commit` if it's authored by an `Author`,
  - _and_ is in the `Project`
  - _and_ is contained in the `Snapshot` date range
  
Which, as the name accurately sums up, is all commits for a member in a snapshot. 

## Writes

When creating or updating a value in the database, the generic `db:dirty_write/1` should be used. 
This function takes a list or single element. 

Mnesia will happily over-write anything you want; 
if you want a function that _only_ creates new objects you must do the check yourself: 
call the appropriate getter, and then write only if `{error, none}` is returned; 
otherwise return `{error, {exists, Object}}`.

## Putting It All Together

Let's say we want to add a new object to the database: a favourite project. 

Favourite projects link Users and Projects. 

We write a module, `db_favourite_project` with the record containing an `id`, 
`user_id`, and `project_id` and implement all `db_object` behaviour callbacks.

The `identity/1` for our favourite project consists of both fields, so we hash those. 

Next, we create the table in `db_app`, electing to index `user_id`, in order to quickly retreive all projects for a user. 

In `dirty_project` we write a new function `favourites_for_user/1` which takes a `db_user:id()`. 
The query will get all `db_favourite_project`s that match the `user_id` 
and then return `db_project`s with the `project_id` of each `db_favourite_project`. 

Additionally, we create `update_` and `delete_` functions: 
we do not care about checking if a favourite already exists, as overwriting it has no effect.

We write a simple wrapper around these in `db.erl`, putting our dirty functions into transactions… and we're done!