# Admin
This document gives an IT administrator all the information they need to install, run, and maintain the GGT server on a fresh Linux install.

_Note: this app uses `$HOME` directory to store the local repos, so make sure you are running it as a user (although you are still likely to use `sudo`)_

## Getting Started
In order to setup the server for the first time, we must first get the repo from the release branch,
```sh
$ git clone https://csil-git1.cs.surrey.sfu.ca/415-16-3-GitLab/GitLabAnalyzer.git
```
and then go through the installation section of the [README](../README.md#installation) documentation.

Due to the overwhelming power of our tool and the unfortunate limitations of the operating systems, it is necessary to force an increased number of allowed opened files. (The tool uses ports for shell calls to the local git executable, which is done in a very parallel nature requiring a very high limit on the number of ports/files). In most cases using `run.sh` will handle setting the correct file descriptor limit. If however you do wish to override the hard and soft limit on the file descriptors across the entire system the following commands will help.

Simply add the following two lines at the end of `/etc/security/limits.conf` and relog:
```sh
* hard nofile 102400
* soft nofile 102400
```

To check that limits were updated run
```sh
$ ulimit -n
```

## Dependencies
When setting up for the first time or when the dependencies in `rebar.config` change, it is necessary to manually update them. To do so, run the command:
```sh
$ sudo mad deps
```

Sometimes the versions of the dependencies may also change, so it's best to delete the `deps` folder entirely before running the aforementioned command. Doing so will require a recompilation.

## Database
Sometimes there will be major changes to the schema of our database. With our current implementation, updating the DB requires extra work. First, stop the server, then delete the `Mnesia.nonode@nohost` directory, then restart. _This will wipe the DB._

## Compilation
To compile our application, run:
```sh
$ sudo mad com
```

While running, our server is capable of automatically picking up the modified Erlang files and compiling them on its own. However, large changes or changes to front-end files will require one to run this command.

We provide a clean-compile and run script `nuke-and-run.sh`. (It will also wipe the directories)

Most of the updates can be done without stopping the server, however deletion of physical directories (i.e. `~/.ggs` or `Mnesia*`) will require a stop.

_Note: sometimes the javascript/bash script files appear to be in `CRLF` format instead of `LF`, which breaks the compilation/other tasks. If that's the case, running `$ dos2unix filename` on these files will solve the issue._

## Running
To start the server, run:
```sh
$ sudo mad plan repl
```

It can then be stopped with a Ctrl-C. Once the server finished initialization, it is possible to use the shell in the Erlang's VM. This permits debugging on the fly and may prove helpful. Obviously, the language the shell supports is Erlang.

## Logging
All of the output is logged and stored to the `log` directory. There are 4 files:
1. `console.log` for info messages on various tasks
2. `crash.log` for reports on processes crashing
3. `debug.log` for info messages on specific parts of code, similar to `console.log` but contains messages that are more niche
4. `error.log` for errors caused by the code

## VM
Currently the system is hosted on the SFU VM `cs-ggt.cs.sfu.ca` and is running as a systemd service called `ggt.service`.