#!/bin/bash

# forces script to trap on error
set -e

echo "Overriding port limit to 102400."
ulimit -n 102400
mad comp 
mad plan
mad rep