-ifndef(FRONTEND_STRUCTS).
-define(FRONTEND_STRUCTS, true).

-record(cumulative_commit_score,
  {
    commit :: db_commit:self(),
    scores :: db_diff_scores:self(),
    raw_scores :: db_diff_scores:self(),
    sum :: integer()
  }).

-record(cumulative_merge_request_score,
  {
    merge_request :: db_merge_request:self(),
    scores :: db_diff_scores:self(),
    raw_scores :: db_diff_scores:self(),
    sum :: integer()
  }).

-endif.
