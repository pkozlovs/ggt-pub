-ifndef(DB_RECORDS).
-define(DB_RECORDS, true).

%% Misc Subtypes
-type source() :: gitlab.
%-type merge_request_state() :: opened | closed | merged | binary(). % TODO: properly wrap this.
-type sp_id() :: {source(), binary()}. %% source-project ID combo
-type note_type() :: merge_request.
-type key() :: binary() | 'undefined'. %% semantic difference only.

-type snapshot_commit_id()            :: key().
-type api_token_id()                  :: key().
-record(snapshot_commit, % TODO: figure out if we need this
  {
    id :: snapshot_commit_id(),
    snapshot_id :: db_snapshot:id(),
    commit_id :: db_commit:id()
  }).
-type snapshot_commit() :: #snapshot_commit{}.
  
-record(api_token, 
  {
    id :: api_token_id(),
    user_id :: db_user:id(),
    source :: source(),
    username :: binary(),
    value :: binary()
  }).
-type api_token() :: #api_token{}.

-record(unique_id, {type, id}).

-endif.
