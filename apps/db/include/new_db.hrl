-ifndef(NEW_DB_RECORDS).
-define(NEW_DB_RECORDS, true).

-type source() :: gitlab.
-type merge_request_state() :: opened | closed | merged | binary(). % TODO: properly wrap this.
-type note_type() :: merge_request.
-type key() :: binary() | 'undefined'. %% semantic difference only.

-endif.
