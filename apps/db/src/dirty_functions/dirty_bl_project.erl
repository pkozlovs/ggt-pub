-module(dirty_bl_project).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1, with_sp_id/1]).

-spec with_id(db_bl_project:id()) 
  -> db_bl_project:self() | none.
with_id(ProjId) ->
   Query = qlc:q([ Project ||
    Project <- mnesia:table(db_bl_project),
    db_bl_project:id(Project) == ProjId
  ]),
  db:head(qlc:e(Query)).

with_sp_id(SpId) ->
   Query = qlc:q([Project || 
    Project <- mnesia:table(db_bl_project),
    db_bl_project:sp_id(Project) == SpId
  ]),
  db:head(qlc:e(Query)).
