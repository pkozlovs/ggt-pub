-module(dirty_user).
-export([with_id/1, with_username/1]).
-include_lib("stdlib/include/qlc.hrl").

with_id(UserId) ->
  db:head(qlc:e(qlc:q([User ||
    User <- mnesia:table(db_user),
    db_user:id(User) == UserId
  ]))).

with_username(Username) ->
  db:head(qlc:e(qlc:q([User ||
    User <- mnesia:table(db_user),
    db_user:username(User) == Username
  ]))).

