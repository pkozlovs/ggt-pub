-module(dirty_commit_diff_scores).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([all_for_commit/1, all_for_commits/1, 
         with_file_diff_id/1, cumulative_score/1]).

all_for_commit(CommitId) ->
  qlc:e(qlc:q([Score ||
    Score <- mnesia:table(db_commit_diff_scores),
    db_commit_diff_scores:commit_id(Score) == CommitId
  ])).

all_for_commits(CommitIds) ->
   qlc:e(qlc:q([Score ||
    Score <- mnesia:table(db_commit_diff_scores),
    CommitId <- CommitIds,
    db_commit_diff_scores:commit_id(Score) == CommitId
  ])).

with_file_diff_id(FileDiffId) ->
  Query = qlc:q([Score ||
    Score <- mnesia:table(db_commit_diff_scores),
    db_commit_diff_scores:commit_diff_id(Score) == FileDiffId
  ]),
  db:head(qlc:e(Query)).

cumulative_score(Scores) -> 
  CommitId = db_commit_diff_scores:commit_id(Scores),
  Commit = dirty_commit:with_id(CommitId),
  ProjId = db_commit:project_id(Commit),
  Ext = db_commit_diff_scores:file_extension(Scores),
  Multiplier = dirty_diff_score_multipliers:for_extension(Ext, ProjId),
  commits:combine_scores([Scores], [Multiplier], Commit).
