-module(dirty_merge_request_diff).
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1, all_for_merge_request_id/1]).

with_id(DiffId) ->
  db:head(qlc:e(qlc:q([Diff ||
    Diff <- mnesia:table(db_merge_request_diff),
    db_merge_request_diff:id(Diff) == DiffId
  ]))).

all_for_merge_request_id(MrId) ->
  qlc:e(qlc:q([Diff ||
    Diff <- mnesia:table(db_merge_request_diff),
    db_merge_request_diff:merge_request_id(Diff) == MrId
  ])).
