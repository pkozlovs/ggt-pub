-module(dirty_merge_request_diff_scores).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([all_for_merge_request/1, with_merge_request_diff_id/1, 
        cumulative_score/1]).

all_for_merge_request(MergeRequestId) ->
  qlc:e(qlc:q([Score ||
    Score <- mnesia:table(db_merge_request_scores),
    db_merge_request_scores:merge_request_id(Score) == MergeRequestId
  ])).

with_merge_request_diff_id(MergeRequestDiffId) ->
  Query = qlc:q([Score ||
    Score <- mnesia:table(db_merge_request_scores),
    db_merge_request_scores:merge_request_diff_id(Score) == MergeRequestDiffId
  ]),
  db:head(qlc:e(Query)).

cumulative_score(Scores) -> 
  MergeRequestId = db_merge_request_scores:merge_request_id(Scores),
  MergeRequest = dirty_merge_request:with_id(MergeRequestId),
  ProjId = db_merge_request:project_id(MergeRequest),
  Ext = db_merge_request_scores:file_extension(Scores),
  Multiplier = dirty_diff_score_multipliers:for_extension(Ext, ProjId),
  merge_requests:combine_scores([Scores], [Multiplier], MergeRequest).
