-module(dirty_author).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([all_for_member/2, all_in_project/1, with_ids/1]).

-spec all_for_member(db_member:id(), db_project:id()) -> [db_author:self()].
all_for_member(MemberId, ProjId) ->
  qlc:e(qlc:q([ Author ||
    UnifiedMember <- mnesia:table(db_unified_member),
    db_unified_member:member_id(UnifiedMember) == MemberId,
    db_unified_member:project_id(UnifiedMember) == ProjId,
    Author <- mnesia:table(db_author),
    db_author:id(Author) == db_unified_member:author_id(UnifiedMember)
  ])).

-spec all_in_project(db_project:id()) -> [db_author:self()].
all_in_project(ProjId) ->
  qlc:e(qlc:q([Author ||
    ProjAuthor <- mnesia:table(db_project_author),
    db_project_author:project_id(ProjAuthor) == ProjId,
    Author <- mnesia:table(db_author),
    db_author:id(Author) == db_project_author:author_id(ProjAuthor)
  ])).

-spec with_ids([db_author:id()]) -> [db_author:self()].
with_ids(AuthorIds) ->
  qlc:e(qlc:q([ Author ||
    Author <- mnesia:table(db_author),
    AuthorId <- AuthorIds,
    db_author:id(Author) == AuthorId
  ])).
