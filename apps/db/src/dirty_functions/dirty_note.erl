-module(dirty_note).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([all_by_member/2, all_in_snapshot/1, all_in_merge_request/1]).

all_by_member(MemberId, SnapshotId) ->
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  Project = dirty_project:for_snapshot(SnapshotId),
  ProjId = db_project:id(Project),
  qlc:e(qlc:q([ Note ||
    ProjMR <- mnesia:table(db_project_merge_request),
    db_project_merge_request:project_id(ProjMR) == ProjId,
    Note <- mnesia:table(db_note),
    db_note:merge_request_id(Note) == db_project_merge_request:merge_request_id(ProjMR),
    db_note:creator_id(Note) == MemberId,
    db_snapshot:contains_datetime(db_note:create_date(Note), Snapshot)
  ])). 

all_in_snapshot(SnapshotId) ->
   MergeRequests = dirty_merge_request:all_in_snapshot(SnapshotId),
    qlc:e(qlc:q([ Note ||
      MergeRequest <- MergeRequests,
      Note <- mnesia:table(db_note),
      db_note:merge_request_id(Note) == db_merge_request:id(MergeRequest)
    ])).

all_in_merge_request(MergeRequestId) ->
  qlc:e(qlc:q([ Note ||
    Note <- mnesia:table(db_note),
    db_note:type(Note) == merge_request,
    db_note:merge_request_id(Note) == MergeRequestId
  ])).
