-module(dirty_diff_score_locations).
-include_lib("stdlib/include/qlc.hrl").
-export([for_diff_id/1]).

for_diff_id(DiffId) ->
  db:head(qlc:e(qlc:q([Locs ||
    Locs <- mnesia:table(db_diff_score_locations),
    db_diff_score_locations:diff_id(Locs) == DiffId
  ]))).
