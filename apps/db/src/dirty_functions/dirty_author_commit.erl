-module(dirty_author_commit).
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1, all_for_author/1, all_for_commit/1, all_for_authors/1]).

with_id(AuthorCommitId) ->
  qlc:e(qlc:q([AC || 
    AC <- mnesia:table(db_author_commit),
    db_author_commit:id(AC) == AuthorCommitId
  ])).

all_for_author(AuthorId) ->
  qlc:e(qlc:q([AC || 
    AC <- mnesia:table(db_author_commit),
    db_author_commit:author_id(AC) == AuthorId
  ])).

all_for_commit(CommitId) ->
  qlc:e(qlc:q([AC || 
    AC <- mnesia:table(db_author_commit),
    db_author_commit:commit_id(AC) == CommitId
  ])).

all_for_authors(Authors) ->
  qlc:e(qlc:q([AC ||
    AC <- mnesia:table(db_author_commit),
    Author <- Authors,
    db_author_commit:author_id(AC) == db_author:id(Author)
  ]), unique_all).
