-module(dirty_project).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1, for_snapshot/1, with_sp_id/1]).

-spec with_id(db_project:id()) 
  -> db_project:self() | none.
with_id(ProjId) ->
   Query = qlc:q([ Project ||
    Project <- mnesia:table(db_project),
    db_project:id(Project) == ProjId
  ]),
  db:head(qlc:e(Query)).

with_sp_id(SpId) ->
   Query = qlc:q([Project || 
    Project <- mnesia:table(db_project),
    db_project:sp_id(Project) == SpId
  ]),
  db:head(qlc:e(Query)).

-spec for_snapshot(db_snapshot:id()) 
  -> db_project:self() | none.
for_snapshot(SnapshotId) ->
  case dirty_snapshot:with_id(SnapshotId) of
    none -> none;
    Snapshot -> with_id(db_snapshot:project_id(Snapshot))
  end.

