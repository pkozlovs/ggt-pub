-module(dirty_issue).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([all_for_member/2, all_when_author/2, all_when_assignee/2]).
-export([all_in_snapshot/1]).

-spec all_in_snapshot(db_snapshot:id()) -> [db_issue:self()].
all_in_snapshot(SnapshotId) ->
  Project = dirty_project:for_snapshot(SnapshotId),
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  ProjId = db_project:id(Project),
  qlc:e(qlc:q([ Issue ||
    Issue <- mnesia:table(db_issue),
    db_issue:project_id(Issue) == ProjId,
    db_snapshot:contains_datetime(db_issue:create_date(Issue), Snapshot)
  ])).

-spec all_for_member(db_member:id(), db_snapshot:id()) -> [db_issue:self()].
all_for_member(MemberId, SnapshotId) ->
  [Issue ||
    Issue <- all_in_snapshot(SnapshotId),
    db_issue:creator_id(Issue) == MemberId orelse
    db_issue:assignee_id(Issue) == MemberId
  ].

-spec all_when_author(db_member:id(), db_snapshot:id()) -> [db_issue:self()].
all_when_author(MemberId, SnapshotId) ->
  [Issue ||
    Issue <- all_for_member(MemberId, SnapshotId),
    db_issue:creator_id(Issue) == MemberId
  ].

-spec all_when_assignee(db_member:id(), db_snapshot:id()) -> [db_issue:self()].
all_when_assignee(MemberId, SnapshotId) ->
  [Issue ||
    Issue <- all_for_member(MemberId, SnapshotId),
    db_issue:assignee_id(Issue) == MemberId
  ].
