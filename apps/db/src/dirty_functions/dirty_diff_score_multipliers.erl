-module(dirty_diff_score_multipliers).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([for_extensions/2, for_extension/2]).

for_extensions(Extensions, ProjectId) ->
  qlc:e(qlc:q([Multiplier ||
    Multiplier <- mnesia:table(db_score_multipliers),
    db_score_multipliers:project_id(Multiplier) == ProjectId,
    db_score_multipliers:file_extension(Multiplier) == Extensions
  ])).

for_extension(Extension, ProjectId) ->
  Query = qlc:q([Multiplier ||
    Multiplier <- mnesia:table(db_score_multipliers),
    db_score_multipliers:project_id(Multiplier) == ProjectId,
    db_score_multipliers:file_extension(Multiplier) == Extension
  ]),
  db:head(qlc:e(Query)).
