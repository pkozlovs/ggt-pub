-module(dirty_snapshot).
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1]).
-export([all_in_project/1, all_with_state/1]).
-export([all_by_user/1, latest_by_user/1]).

-spec with_id(db_snapshot:id()) -> db_snapshot:self() | none.
with_id(SnapshotId) ->
  Query = qlc:q([Snapshot ||
    Snapshot <- mnesia:table(db_snapshot),
    SnapshotId == db_snapshot:id(Snapshot)
  ]),
  db:head(qlc:e(Query)).

-spec all_with_state(in_progress | failed | completed) -> [db_snapshot:self()].
all_with_state(State) ->
  qlc:e(qlc:q([ Snapshot ||
    Snapshot <- mnesia:table(db_snapshot),
		db_snapshot:simple_state(Snapshot) == State
  ])).

-spec all_in_project(db_project:id()) -> [db_snapshot:self()].
all_in_project(ProjId) ->
   Query = qlc:q([Snapshot ||
    Snapshot <- mnesia:table(db_snapshot),
    ProjId == db_snapshot:project_id(Snapshot)
  ]),
  qlc:e(Query).

all_by_user(UserId) ->
  qlc:e(qlc:q([Snapshot ||
    Snapshot <- mnesia:table(db_snapshot),
    db_snapshot:creator_id(Snapshot) == UserId
  ])).

latest_by_user(UserId) ->
  db:head(db_snapshot:sort(all_by_user(UserId))).
