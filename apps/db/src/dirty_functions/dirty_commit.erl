-module(dirty_commit).
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1, with_sha/1, with_ids/1]).
-export([all_for_member/2, all_in_merge_request/1, all_in_merge_requests/1]).
-export([all_in_snapshot/1, all_strictly_in_snapshot/1]).
-export([all_cumulative_scores/2,cumulative_score/2]).

-spec all_for_member(db_member:id(), db_snapshot:id()) -> [db_author:self()].
all_for_member(MemberId, SnapshotId) ->
   Snapshot = dirty_snapshot:with_id(SnapshotId),
   ProjId = db_snapshot:project_id(Snapshot),
   Authors = dirty_author:all_for_member(MemberId, ProjId),
   AuthorCommits = dirty_author_commit:all_for_authors(Authors),
   CommitsInSnapshot = all_in_snapshot(SnapshotId),
   qlc:e(qlc:q([Commit ||
     Commit <- CommitsInSnapshot,
     AuthorCommit <- AuthorCommits,
     db_commit:id(Commit) == db_author_commit:commit_id(AuthorCommit)
  ]), unique_all). 

all_in_merge_request(MergeRequestId) ->
  qlc:e(qlc:q([Commit ||
    MRC <- mnesia:table(db_merge_request_commit),
    db_merge_request_commit:merge_request_id(MRC) == MergeRequestId,
    Commit <- mnesia:table(db_commit),
    db_commit:id(Commit) == db_merge_request_commit:commit_id(MRC)
  ])).

all_strictly_in_snapshot(SnapshotId) ->
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  ProjId = db_snapshot:project_id(Snapshot),
  qlc:e(qlc:q([Commit ||
    Commit <- mnesia:table(db_commit),
    db_commit:project_id(Commit) == ProjId,
    db_snapshot:contains_datetime(db_commit:create_date(Commit), Snapshot)    
  ])).

all_in_snapshot(SnapshotId) ->
  MRs = dirty_merge_request:all_in_snapshot(SnapshotId),
  MRIds = [db_merge_request:id(MR) || MR <- MRs],
  CommitsInMRs = all_in_merge_requests(MRIds),
  CommitsWithoutMRs = all_without_merge_request(SnapshotId),
  lists:usort(CommitsInMRs ++ CommitsWithoutMRs).

all_in_merge_requests(MergeRequestIds) ->
  qlc:e(qlc:q([Commit ||
    MRC <- mnesia:table(db_merge_request_commit),
    MergeRequestId <- MergeRequestIds,
    db_merge_request_commit:merge_request_id(MRC) == MergeRequestId,
    Commit <- mnesia:table(db_commit),
    db_commit:id(Commit) == db_merge_request_commit:commit_id(MRC)
  ], unique)).

all_without_merge_request(SnapId) ->
  MRCs = qlc:e(qlc:q([ MRCs || 
                       MRCs <- mnesia:table(db_merge_request_commit) ])),
  Snapshot = dirty_snapshot:with_id(SnapId),
  ProjId = db_snapshot:project_id(Snapshot),
  MRCommitIds = lists:map(fun(MRC) ->
    db_merge_request_commit:commit_id(MRC)
  end, MRCs),
  MRCommitIDsSet = sets:from_list(MRCommitIds),
  Commits = qlc:e(qlc:q([ Commit || 
    Commit <- mnesia:table(db_commit),
    db_commit:project_id(Commit) == ProjId,
    commits:is_in_snapshot(Commit, Snapshot)
  ])),
  MRLessCommits = lists:filter(fun(Commit) ->
    CommitId = db_commit:id(Commit),
    not sets:is_element(CommitId, MRCommitIDsSet)
  end, Commits),
  MRLessCommits.

-spec with_id(db_commit:id()) -> db_commit:self() | none.
with_id(CommitId) ->
  Query = qlc:q([Commit ||
    Commit <- mnesia:table(db_commit),
    db_commit:id(Commit) == CommitId
  ]),
  db:head(qlc:e(Query)).

with_ids(CommitIds) ->
  qlc:e(qlc:q([Commit ||
    Commit <- mnesia:table(db_commit),
    Id <- CommitIds,
    db_commit:id(Commit) == Id
  ])).

-spec with_sha(binary()) -> db_commit:self() | none. 
with_sha(Sha) ->
  Query = qlc:q([Commit ||
    Commit <- mnesia:table(db_commit),
    db_commit:sha(Commit) == Sha
  ]),
  db:head(qlc:e(Query)).

all_cumulative_scores(CommitIds, ProjId) ->
  Commits = with_ids(CommitIds),
  [cumulative_score(Commit, ProjId) || Commit <- Commits].

cumulative_score(Commit, ProjId) ->
  CommitId = db_commit:id(Commit),
  ScoresList = dirty_commit_diff_scores:all_for_commit(CommitId),
  Exts = sets:to_list(sets:from_list(
    [db_commit_diff_scores:file_extension(Score) || Score <- ScoresList])),
  Multipliers = dirty_diff_score_multipliers:for_extensions(Exts, ProjId),
  commits:combine_scores(ScoresList, Multipliers, Commit).
