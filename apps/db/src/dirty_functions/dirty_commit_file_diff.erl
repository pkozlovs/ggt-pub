-module(dirty_commit_file_diff).
-include_lib("stdlib/include/qlc.hrl").
-export([all_for_commit/1]).
-export([stats_for_commit/1]).
-export([with_id/1]).

with_id(DiffId) ->
  db:head(qlc:e(qlc:q([Diff ||
    Diff <- mnesia:table(db_commit_diff),
    db_commit_diff:id(Diff) == DiffId
  ]))).

all_for_commit(CommitId) ->
  Query = qlc:q([FileDiff ||
    FileDiff <- mnesia:table(db_commit_diff),
    db_commit_diff:commit_id(FileDiff) == CommitId
  ]),
  qlc:e(Query).

stats_for_commit(CommitId) ->
  CommitFileDiffs = qlc:e(qlc:q([ FileDiff ||
      FileDiff <- mnesia:table(db_commit_diff),
      db_commit_diff:commit_id(FileDiff) == CommitId
    ])),
    {Ins, Dels, Net} = lists:foldl(fun(Diff, {Ins, Dels, Net}) ->
      {
        Ins + db_commit_diff:insertions(Diff),
        Dels + db_commit_diff:deletions(Diff),
        Net + db_commit_diff:net(Diff)
      }
    end, {0, 0, 0}, CommitFileDiffs),
    db:construct_commit_stats(CommitId, Ins, Dels, Net).

