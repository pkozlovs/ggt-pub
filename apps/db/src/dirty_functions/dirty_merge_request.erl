-module(dirty_merge_request).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([with_id/1]).
-export([all_in_project/1, all_in_snapshot/1]).
-export([all_contributed_by_member/2, all_by_member/2, cumulative_score/2]).

with_id(MergeRequestId) ->
  Query = qlc:q([MR ||
    MR <- mnesia:table(db_merge_request),
    MergeRequestId == db_merge_request:id(MR)
  ]),
  db:head(qlc:e(Query)).

all_in_project(ProjectId) ->
  Query = qlc:q([ MR ||
    ProjMR <- mnesia:table(db_project_merge_request),
    db_project_merge_request:project_id(ProjMR) == ProjectId,
    MR <- mnesia:table(db_merge_request),
    db_merge_request:id(MR) == db_project_merge_request:merge_request_id(ProjMR)
  ]),
  qlc:e(Query).
 
all_in_snapshot(SnapshotId) ->
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  ProjectId = db_snapshot:project_id(Snapshot),
  qlc:e(qlc:q([MergeRequest ||
    MergeRequest <- mnesia:table(db_merge_request),
    db_merge_request:project_id(MergeRequest) == ProjectId,
    db_snapshot:contains_datetime(
      db_merge_request:merge_date(MergeRequest), Snapshot)
  ])).

all_by_member(MemberId, SnapshotId) ->
  [MergeRequest ||
    MergeRequest <- all_in_snapshot(SnapshotId),
    db_merge_request:creator_id(MergeRequest) == MemberId
  ].

all_contributed_by_member(MemberId, SnapshotId) ->
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  ProjId = db_snapshot:project_id(Snapshot),
  Authors = dirty_author:all_for_member(MemberId, ProjId),
  AuthorCommits = dirty_author_commit:all_for_authors(Authors),
  MergeRequests = all_in_snapshot(SnapshotId),
  lists:filter(fun(MergeRequest) ->
    Commits = dirty_commit:all_in_merge_request(
                db_merge_request:id(MergeRequest)
              ),
    case [Commit || 
            Commit <- Commits, 
            AuthorCommit <- AuthorCommits,
            db_commit:id(Commit) == db_author_commit:commit_id(AuthorCommit)
         ] of
      [] -> false;
      _  -> true
    end
  end, MergeRequests).

cumulative_score(MergeRequest, ProjId) -> 
  MergeRequestId = db_merge_request:id(MergeRequest),
  ScoresList = 
    dirty_merge_request_diff_scores:all_for_merge_request(MergeRequestId),
  Exts = sets:to_list(sets:from_list(
          [db_merge_request_scores:file_extension(Score) || 
            Score <- ScoresList])),
  Multipliers = dirty_diff_score_multipliers:for_extensions(Exts, ProjId),
  merge_requests:combine_scores(ScoresList, Multipliers, MergeRequest).
  
