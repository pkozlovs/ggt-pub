-module(dirty_member).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([with_ids/1, all_in_project/1, for_author/2, with_username/2]).

-spec with_ids([db_member:id()]) -> [db_member:self()].
with_ids(MemberIds) ->
  qlc:e(qlc:q([Member ||
    Member <- mnesia:table(db_member),
    MemberId <- MemberIds,
    db_member:id(Member) == MemberId
  ])).

-spec all_in_project(db_project:id()) -> [db_member:self()].
all_in_project(ProjId) ->
  qlc:e(qlc:q([ Member ||
    Member <- mnesia:table(db_member),
    db_member:project_id(Member) == ProjId
  ])).

-spec for_author(db_author:id(), db_project:id()) -> db_member:self() | none.
for_author(AuthorId, ProjId) ->
  db:head(qlc:e(qlc:q([ Member ||
      UnifiedMember <- mnesia:table(db_unified_member),
      db_unified_member:author_id(UnifiedMember) == AuthorId, 
      db_unified_member:project_id(UnifiedMember) == ProjId,
      Member <- mnesia:table(db_member),
      db_member:id(Member) == db_unified_member:member_id(UnifiedMember)
    ]))).

-spec with_username(binary(), db_project:id()) -> db_member:self() | none.
with_username(Username, ProjId) ->
   Query = qlc:q([ Member ||
      Member <- mnesia:table(db_member),
      db_member:project_id(Member) == ProjId,
      db_member:username(Member) == Username
    ]),
    db:head(qlc:e(Query)).

