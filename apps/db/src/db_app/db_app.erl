-module(db_app).
-behaviour(application).
%% Application callbacks
-export([start/2, stop/1, start/0, install/0]).
-include_lib("stdlib/include/qlc.hrl").
-include_lib("apps/db/include/db.hrl").
%% ===================================================================
%% Application callbacks
%% ===================================================================
start() ->
  application:ensure_all_started(database), % start all dependent applications
  application:start(database),
  install().

start(_StartType, _StartArgs) ->
  start(),
  load_modules(),
  db_sup:start_link().

stop(_State) ->
  ok.

install() ->
  mnesia:stop(),
  Nodes = [node()],
  mnesia:create_schema(Nodes),
  ok = mnesia:start(),
  case (catch create_tables()) of
    ok -> error_logger:info_msg("Database initialized successfully.~n");
    Other -> error_logger:error_report(["Database fatal error", Other])  end,
  ok.
                      
%% ===================================================================
%% Private methods
%% ===================================================================

load_modules() ->
  [code:ensure_loaded(M) || M <- [
    snapshot_generator,
    git_core_analysis,
    gitlab_analysis,
    source_analysis,
    meta_analysis,
    diff_analysis,
    result,
    gitlab_convert,
    git_core_convert,
    commits,
    merge_requests,
    snapshots,
    notes
  ]].

-define(CREATE_TABLE(Name, Indices),
  case mnesia:create_table(Name,
    [{disc_copies, [node()]},
      {attributes, Name:record_info()},
      {index, Indices}
    ]) of
    {aborted, {already_exists, Name}} -> ok;
    {atomic, ok} -> ok
  end).

-define(NON_STANDARD_CREATE_TABLE(Name, RecordInfo, Indices),
  case mnesia:create_table(Name,
    [{disc_copies, [node()]},
      {attributes, RecordInfo},
      {index, Indices}
    ]) of
    {aborted, {already_exists, Name}} -> ok;
    {atomic, ok} -> ok
  end).
    

create_tables() -> 
  ?CREATE_TABLE(db_project, []),
  ?CREATE_TABLE(db_merge_request, []),
  ?CREATE_TABLE(db_member, []),
  ?CREATE_TABLE(db_commit, [sha, author_id]),
  ?CREATE_TABLE(db_merge_request_commit, [commit_id, merge_request_id]),
  ?CREATE_TABLE(db_snapshot, [project_id]),
  ?CREATE_TABLE(db_author, [email, name]),
  ?CREATE_TABLE(db_commit_diff, [commit_id]),
  ?CREATE_TABLE(db_project_author, [project_id, author_id]),
  ?CREATE_TABLE(db_project_merge_request, [project_id, merge_request_id]),
  ?CREATE_TABLE(db_unified_member, [project_id, member_id, author_id]),
  ?CREATE_TABLE(db_author_commit, [commit_id, author_id]),
  ?CREATE_TABLE(db_user, [username]),
  ?CREATE_TABLE(db_note, [creator_id, merge_request_id, type]),
  ?CREATE_TABLE(db_merge_request_diff, [merge_request_id]),
  ?CREATE_TABLE(db_commit_diff_scores, [commit_id, commit_diff_id]),
  ?CREATE_TABLE(db_merge_request_scores, [merge_request_id,
                                          merge_request_diff_id]),
  ?CREATE_TABLE(db_score_multipliers, [project_id, file_extension]),
  ?CREATE_TABLE(db_issue, [project_id, creator_id, assignee_id, 
                           create_date]),
  ?CREATE_TABLE(db_diff_score_locations, [diff_id]),
  ?CREATE_TABLE(db_bl_project, []),
  ?NON_STANDARD_CREATE_TABLE(unique_id, record_info(fields, unique_id),
                            []),
  ?NON_STANDARD_CREATE_TABLE(api_token, record_info(fields, api_token),
                             [user_id, source]),
  ?NON_STANDARD_CREATE_TABLE(snapshot_commit, 
                             record_info(fields, snapshot_commit), 
                             [snapshot_id, commit_id]),
  ok.
