-module(db).
-include_lib("apps/db/include/db.hrl").
%% necessary libs for queries.
-include_lib("stdlib/include/qlc.hrl").
%% constructors
-export([
  construct_project/4, construct_snapshot/7, construct_commit/7, 
  construct_commit_author/2, construct_commit_file_diff/6,
  construct_project_author/2, construct_snapshot_commit/2,
  construct_merge_request/13, construct_project_merge_request/2,
  construct_member/4, construct_merge_request_commit/2,
  construct_unified_member/3, construct_user/1, 
  construct_api_token/3, construct_merge_request_diff/3,
  construct_merge_request_stats/3, construct_merge_request_diff_scores/3,
  construct_merge_request_diff_scores/9, construct_diff_scores_multipliers/2,
  construct_note/6, construct_issue/9, construct_commit_diff_scores/3,
  construct_commit_diff_scores/9, construct_commit_stats/4, 
  get_merge_request_score/2, get_commit_diff_scores/1
]).
%% Black list constructors
-export([
  construct_bl_project/1
]).

%% fake constructors
-export([
  construct_fake_commit/2, construct_fake_commit_author/0,
  construct_fake_member/1
]).
%% projects
-export([
  get_project/2, get_project/1, create_project/1
]).
%% Black listed projects
-export([
  get_bl_project/2, create_bl_project/1, delete_bl_project/1
]).

%% snapshots
-export([
  get_snapshot/1, get_snapshots/1, create_snapshot/1, update_snapshot_state/2,
  delete_snapshots/1, delete_snapshot/1, rename_snapshot/2, 
  get_last_snapshot/1
]).
%% merge requests
-export([
  get_merge_requests/2, update_merge_requests/2,
  get_merge_request/1,
  update_merge_request_commits/1, update_notes/1,
  get_notes/1, get_notes/2, update_merge_request_diffs/1,
  get_merge_request_diffs/1, get_merge_request_stats/1,
  update_merge_request_diff_scores/1, update_issues/1, get_issues/1,
  get_issues/3, get_merge_requests_contributed/2, 
  get_merge_requests_in_project/1, get_merge_requests_in_snapshot/1
]).
%% commits
-export([
  get_sorted_commits/1, get_commits/1, get_commit_file_diffs/1,
  update_commit/2, update_commits/2, update_commit_file_diffs/1,
  get_commits_without_mr/1, get_commit_stats/1, update_commit_diff_scores/1,
  get_commits/2, get_commit_score/2, get_commit/1, get_augmented_diffs/1
]).
%% members/authors
-export([
  get_member/2, get_members/1, get_author/1, get_authors/1, update_commit_author/2,
  update_commit_authors/2, update_members/1, associate_member_author/3,
  get_authors_for_member/2, dissociate_member_author/2, 
  dissociate_member_authors/1, update_author_commits/1
]).
%% diff scoring settings
-export([
  get_diff_score_multipliers/2, update_diff_score_multipliers/1,
  get_merge_request_diff_scores/1, get_commit_diff_raw_scores/1,
  get_total_commit_score/2, get_total_commit_score_counts/2,
  update_diff_score_locations/1
]).
%% users/tokens
-export([
  update_user/1, get_user/1, update_api_token/3, get_api_token/2
]).
%% helpers
-export([head/1, atomically_do/1, gen_unique_id/1]).
%% DEBUG ONLY (?)
-export([show_whole_table/1]).

%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%

% Constructors

-spec construct_project(
  SpId :: sp_id(), 
  Name :: binary(), 
  HttpUrl :: binary(), 
  SshUrl :: binary()
) -> db_project:self().
construct_project(SpId, Name, WebUrl, HttpUrl) ->
  {Sp, Id} = SpId,
  db_project:new({Sp, Id, Name, WebUrl, HttpUrl}).

-spec construct_bl_project(
  SpId :: sp_id()
) -> db_project:self().
construct_bl_project(SpId) ->
  {Sp, Id} = SpId,
  db_bl_project:new({Sp, Id}).
  
-spec construct_snapshot(
  ProjectId :: db_project:id(), 
  Start :: calendar:datetime(), 
  End :: calendar:datetime(), 
  CreationDate :: calendar:datetime() | now,
  CreatorId :: db_user:id(), 
  Name :: binary(),
  State :: db_snapshot:state()
) -> db_snapshot:self().
construct_snapshot(ProjectId, Start, End, CreationDate, CreatorId, Name,
                   State) ->
  db_snapshot:new({ProjectId, Start, End, CreationDate, CreatorId, Name, State}).

-spec construct_commit(
  ProjectId :: db_project:id(), 
  Title :: binary(), 
  Message :: binary(), 
  Sha :: binary(), 
  ParentShas :: [binary()], 
  AuthorId :: key(), 
  Date :: calendar:datetime()
) -> db_commit:self().
construct_commit(ProjectId, Title, Message, Sha, ParentShas, AuthorId, Date) ->
  db_commit:new({ProjectId, Title, Message, Sha, ParentShas, AuthorId, Date}).
-spec construct_fake_commit(
  ProjectId :: db_project:id(), 
  Sha :: binary()
) -> db_commit:self().
construct_fake_commit(ProjectId, Sha) ->
  db_commit:new_fake(ProjectId, Sha).
  
-spec construct_fake_commit_author() -> db_author:self().
construct_fake_commit_author() ->
  db_author:new_fake().
  
-spec construct_commit_author(
  Name :: binary(),
  Email :: binary()
) -> db_author:self().  
construct_commit_author(Name, Email) ->
  db_author:new({Name, Email}).

-spec construct_commit_file_diff(
  CommitId :: db_commit:id(),
  FileName :: binary(),
  Ins :: non_neg_integer(),
  Del :: non_neg_integer(),
  Net :: integer(),
  Diff :: binary()
) -> db_commit_diff:self().
construct_commit_file_diff(CommitId, FileName, Ins, Del, Net, Diff) ->
  db_commit_diff:new({CommitId, FileName, Ins, Del, Net, Diff}).

construct_commit_stats(CommitId, Ins, Dels, Net) ->
  db_commit_stats:new({CommitId, Ins, Dels, Net}).

construct_commit_diff_scores(CommitId, FileDiffId, {ignored, Reason}) ->
  db_commit_diff_scores:new({CommitId, FileDiffId, {ignored, Reason}}).

construct_commit_diff_scores(CommitId, DiffId, Ext, Whitespace, 
                             Syntax, Moves, Refactors, Ins, Dels) ->
  db_commit_diff_scores:new({CommitId, DiffId, Ext, Whitespace, 
                             Syntax, Moves, Refactors, Ins, Dels}).

construct_diff_scores_multipliers(ProjId, Ext) ->
  db_score_multipliers:new({ProjId, Ext}).

-spec construct_project_author(
  ProjectId :: db_project:id(),
  AuthorId :: db_author:id()
)  -> db_project_author:self().
construct_project_author(ProjectId, AuthorId) ->
  db_project_author:new({ProjectId, AuthorId}).

-spec construct_snapshot_commit(
  SnapshotId :: db_snapshot:id(),
  CommitId :: db_commit:id()
) -> snapshot_commit().
construct_snapshot_commit(SnapshotId, CommitId) ->
  SnapshotCommit = #snapshot_commit{
    snapshot_id = SnapshotId, commit_id = CommitId
  },
  SnapshotCommit#snapshot_commit{id = identity(SnapshotCommit)}.

-spec construct_merge_request(
  IdAtSource :: binary(),
  IidAtSource :: binary(),
  ProjectId :: db_project:id(),
  Title :: binary(),
  Message :: binary(),
  AuthorId :: db_member:id(),
  AssigneeId :: list_utils:one_or_none(db_member:id()),
  State :: db_merge_request:state(),        % TODO: define this
  TargetBranch :: binary(), % TODO: define this
  SourceBranch :: binary(), % TODO: define this
  Sha :: binary(),
  MergeSha :: binary(),      % TODO: define this 
  Url :: binary()
) -> db_merge_request:self().
construct_merge_request(IdAtSource, IidAtSource, ProjectId, Title, Message,
                        AuthorId, AssigneeId, State, TargetBranch, 
                        SourceBranch, Sha, MergeSha, Url) ->
  db_merge_request:new({IdAtSource, IidAtSource, ProjectId, Title, Message,
                        AuthorId, AssigneeId, State, TargetBranch,
                        SourceBranch, Sha, MergeSha, defined, Url}).
 
construct_merge_request_diff(MergeRequestId, Filename, Diff) ->
  db_merge_request_diff:new({MergeRequestId, Filename, Diff}).

construct_merge_request_stats(MergeRequestId, NetLineCount, CommitCount) ->
  db_merge_request_stats:new({MergeRequestId, NetLineCount, CommitCount}).

construct_merge_request_diff_scores(MRId, MRDiffId, {ignored, Reason}) ->
  db_merge_request_scores:new({MRId, MRDiffId, {ignored, Reason}}).

construct_merge_request_diff_scores(MRId, MRDiffId, Ext, Whitespace, Syntax,
                             Moves, Refactors, Insertions, Deletions) ->
  db_merge_request_scores:new({MRId, MRDiffId, Ext, Whitespace, Syntax,
                               Moves, Refactors, Insertions, Deletions}).


construct_note(MemberId, MergeRequestId, Date, Body, Type, SourceId) ->
  db_note:new({MemberId, MergeRequestId, Date, Body, Type, SourceId}).

construct_issue(ProjId, IdAtSource, Title, Message, State, Date, AuthorId,
                AssigneeId, Url) ->
  db_issue:new({ProjId, IdAtSource, Title, Message, State, Date, AuthorId,
                AssigneeId, Url}).

-spec construct_project_merge_request(
  ProjectId :: db_project:id(),
  MergeRequestId :: db_merge_request:id()
) -> db_project_merge_request:self().
construct_project_merge_request(ProjectId, MergeRequestId) ->
  db_project_merge_request:new({ProjectId, MergeRequestId}).

-spec construct_member(
  Username :: binary(),
  Name :: binary(),
  WebUrl :: binary(),
  ProjId :: db_project:id()
) -> db_member:self(). 
construct_member(Username, Name, WebUrl, ProjId) ->
  db_member:new({Username, Name, WebUrl, ProjId}).

-spec construct_fake_member(ProjId :: db_project:id()) -> db_member:self().
construct_fake_member(ProjId) ->
  db_member:new_fake(ProjId).

construct_unified_member(MemberId, AuthorId, ProjId) ->
  db_unified_member:new({ProjId, MemberId, AuthorId}).

-spec construct_merge_request_commit(
  CommitId :: db_commit:id(),
  MergeRequestId :: db_merge_request:id()
) -> db_merge_request:self().
construct_merge_request_commit(CommitId, MergeRequestId) ->
  db_merge_request_commit:new({CommitId, MergeRequestId}).

construct_user(Username) ->
  db_user:new({Username}).

construct_api_token(UserId, Source, Value) ->
  Token = #api_token{
    user_id = UserId,
    source = Source,
    value = Value
  },
  Token#api_token{id = identity(Token)}.

%%% Custom CRUD
-spec get_project(source(), db_project:id()) 
  -> {ok, db_project:self()} | {error, none}.
get_project(Source, ProjId) ->
  transaction(fun() ->
    dirty_project:with_sp_id({Source, ProjId})
  end).

-spec get_project({project_id, db_project:id()} | {snapshot_id, db_snapshot:id()}) 
  -> result:either(db_project:self(), none).
get_project({project_id, ProjId}) ->
  transaction(fun() -> dirty_project:with_id(ProjId) end);
get_project({snapshot_id, SnapshotId}) ->
  transaction(fun() -> 
    dirty_project:for_snapshot(SnapshotId)
  end).

-spec get_bl_project({source, source()}, {id, db_bl_project:id()}) 
  -> {ok, db_bl_project:self()} | {error, none}.
get_bl_project({source, Source}, {id, ProjId}) ->
  transaction(fun() ->
    dirty_bl_project:with_sp_id({Source, ProjId})
  end).

-spec get_merge_requests(
  {member_id, db_member:id()},
  {snapshot_id, db_snapshot:id()}
) -> result:either([db_merge_request:self()], any()).
get_merge_requests({member_id, MemberId}, {snapshot_id, SnapId}) ->
  transaction(fun() -> dirty_merge_request:all_by_member(MemberId, SnapId) end).

-spec get_merge_request({merge_request_id, binary()}) ->
  result:either(db_merge_request:self(), any()).
get_merge_request({merge_request_id, MergeRequestId}) ->
  transaction(fun() -> dirty_merge_request:with_id(MergeRequestId) end).

get_merge_requests_contributed({member_id, MemberId}, 
                               {snapshot_id, SnapshotId}) ->
  transaction(fun() ->
    dirty_merge_request:all_contributed_by_member(MemberId, SnapshotId)
  end).

get_merge_requests_in_project({project_id, ProjId}) ->
  transaction(fun() -> dirty_merge_request:all_in_project(ProjId) end).

get_merge_requests_in_snapshot({snapshot_id, SnapId}) ->
  transaction(fun() -> dirty_merge_request:all_in_snapshot(SnapId) end).

get_merge_request_diffs({merge_request_id, MergeRequestId}) ->
  Result = transaction(fun() ->
    qlc:e(qlc:q([ Diff ||
      Diff <- mnesia:table(db_merge_request_diff),
      db_merge_request_diff:merge_request_id(Diff) == MergeRequestId
    ]))
  end),
  result:map(Result, fun(List) ->
    lists:sort(fun(Left, Right) ->
      LeftFilename = db_merge_request_diff:filename(Left),
      RightFilename = db_merge_request_diff:filename(Right),
      LeftFilename =< RightFilename
    end, List)
  end).
  
-spec get_merge_request_stats(
  {merge_request_id, db_merge_request:id()}
) -> result:either(db_merge_request_stats:self(), any()).
get_merge_request_stats({merge_request_id, MergeRequestId}) ->
  transaction(fun() ->
    Commits = qlc:e(qlc:q([Commit ||
      MRC <- mnesia:table(db_merge_request_commit),
      db_merge_request_commit:merge_request_id(MRC) == MergeRequestId,
      Commit <- mnesia:table(db_commit),
      db_commit:id(Commit) == db_merge_request_commit:commit_id(MRC)
    ])),
    CommitCount = length(Commits),
    CommitIdSet = sets:from_list(
      lists:map(fun(Commit) -> db_commit:id(Commit) end, Commits)
    ),
    Diffs = qlc:e(qlc:q([Diff ||
      Diff <- mnesia:table(db_commit_diff),
      sets:is_element(db_commit_diff:commit_id(Diff), CommitIdSet)
    ])),
    NetLineCount = lists:foldl(fun(Diff, Acc) -> 
      Acc + db_commit_diff:net(Diff)
    end, 0, Diffs),
    db:construct_merge_request_stats(MergeRequestId, NetLineCount, CommitCount)
  end).

-spec get_sorted_commits(
  {merge_request_id, db_merge_request:id()}
) -> result:either([db_commit:self()], any()). 
get_sorted_commits({merge_request_id, MRId}) ->
  MaybeCommits = get_commits({merge_request_id, MRId}),
  case MaybeCommits of
    {error, Reason} -> 
      {error, Reason};
    {ok, Commits} ->
      Sorter = fun(Left, Right) ->
        LeftDate = db_commit:create_date(Left),
        RightDate = db_commit:create_date(Right),
        LeftSec = calendar:datetime_to_gregorian_seconds(LeftDate),
        RightSec = calendar:datetime_to_gregorian_seconds(RightDate),
        LeftSec =< RightSec
      end,
      {ok, lists:sort(Sorter, Commits)}
  end.

-spec get_commit({commit_id, db_commit:id()}) -> result:either(db_commit:self(), any()).
get_commit({commit_id, CommitId}) ->
  transaction(fun() -> 
    dirty_commit:with_id(CommitId)
  end).

-spec get_commits(
  {merge_request_id, db_merge_request:id()}
) -> result:either([db_commit:self()], any()).
get_commits({merge_request_id, MRId}) ->
  transaction(fun() -> dirty_commit:all_in_merge_request(MRId) end).

get_commits({snapshot_id, SnapshotId}, {member_id, MemberId}) ->
  transaction(fun() -> dirty_commit:all_for_member(MemberId, SnapshotId) end).

get_total_commit_score({snapshot_id, SnapshotId}, {member_id, MemberId}) ->
  transaction(fun() -> 
    Commits = dirty_commit:all_for_member(MemberId, SnapshotId),
    CommitIds = [db_commit:id(C) || C <- Commits],
    Snapshot = dirty_snapshot:with_id(SnapshotId),
    ProjId = db_snapshot:project_id(Snapshot),
    Scores = dirty_commit_diff_scores:all_for_commits(CommitIds),
    Exts = [db_commit_diff_scores:file_extension(Score) || Score <- Scores],
    Multipliers = dirty_diff_score_multipliers:for_extensions(Exts, ProjId),
    db_commit_diff_scores:cumulative_score(Scores, Multipliers)
  end).

get_total_commit_score_counts({snapshot_id, SnapshotId}, 
                              {member_id, MemberId}) ->
  transaction(fun() ->
    Commits = dirty_commit:all_for_member(MemberId, SnapshotId),
    CommitIds = [db_commit:id(C) || C <- Commits],
    Scores = dirty_commit_diff_scores:all_for_commits(CommitIds),
    db_commit_diff_scores:cumulative_counts(Scores)
  end).

-spec get_commits_without_mr(SnapshotId :: db_snapshot:id()) -> result:either([db_commit:self()], any()).
get_commits_without_mr(SnapshotId) ->
  transaction(fun() ->
    MRCs = qlc:e(qlc:q([ MRCs || MRCs <- mnesia:table(db_merge_request_commit) ])),
    Snapshot = dirty_snapshot:with_id(SnapshotId),
    MRCommitIds = lists:map(fun(MRC) ->
      db_merge_request_commit:commit_id(MRC)
    end, MRCs),
    MRCommitIDsSet = sets:from_list(MRCommitIds),
    Commits = qlc:e(qlc:q([ Commit || 
      Commit <- mnesia:table(db_commit),
      db_commit:project_id(Commit) == db_snapshot:project_id(Snapshot),
      commits:is_in_snapshot(Commit, Snapshot)
    ])),
    MRLessCommits = lists:filter(fun(Commit) ->
      CommitId = db_commit:id(Commit),
      not sets:is_element(CommitId, MRCommitIDsSet)
    end, Commits),
    MRLessCommits
  end). 

-spec get_commit_file_diffs(
  {commit_id, db_commit:id()}
) -> result:either([db_commit_diff:self()], any()).
get_commit_file_diffs({commit_id, CommitId}) ->
  transaction(fun() -> dirty_commit_file_diff:all_for_commit(CommitId) end).


-spec get_commit_stats(
  {commit_id, db_commit:id()}
) -> result:either(db_commit_stats:self(), any()).
get_commit_stats({commit_id, CommitId}) ->
  transaction(fun() ->
    dirty_commit_file_diff:stats_for_commit(CommitId)
  end).

get_augmented_diffs({merge_request_id, MrId}) ->
  transaction(fun() -> 
    Diffs = dirty_merge_request_diff:all_for_merge_request_id(MrId),
    lists:map(fun(Diff) ->
      try
        DiffId = db_merge_request_diff:id(Diff),
        Locations = dirty_diff_score_locations:for_diff_id(DiffId),
        db_diff_score_locations:assert_self(Locations),
        AugmentedDiff = augmented_diff:augment(Diff, Locations),
        db_merge_request_diff:set_diff(AugmentedDiff, Diff)
      catch _:_ -> Diff 
      end
    end, Diffs)
  end);
get_augmented_diffs({commit_id, CommitId}) ->
  transaction(fun() ->
    Diffs = dirty_commit_file_diff:all_for_commit(CommitId),
    lists:map(fun(Diff) ->
      try 
        DiffId = db_commit_diff:id(Diff),
        Locations = dirty_diff_score_locations:for_diff_id(DiffId),
        db_diff_score_locations:assert_self(Locations),
        AugmentedDiff = augmented_diff:augment(Diff, Locations),
        db_commit_diff:set_diff(AugmentedDiff, Diff)
      catch _:_ -> Diff
      end
    end, Diffs)
  end).

-spec get_commit_diff_scores(
  {commit_file_diff_id, db_commit_diff:id()}
) -> result:either(db_commit_stats:self(), any).
get_commit_diff_scores({commit_file_diff_id, FileDiffId}) ->
  transaction(fun() ->
    CommitDiffScores = dirty_commit_diff_scores:with_file_diff_id(FileDiffId),
    dirty_commit_diff_scores:cumulative_score(CommitDiffScores)
  end);
get_commit_diff_scores({commit_diff_id, DiffId}) ->
  get_commit_diff_scores({commit_file_diff_id, DiffId}).

get_commit_diff_raw_scores({commit_diff_id, DiffId}) ->
  transaction(fun() -> dirty_commit_diff_scores:with_file_diff_id(DiffId) end).

get_commit_score({commit_id, CommitId}, {project_id, ProjId}) ->
  transaction(fun() ->
    Commit = dirty_commit:with_id(CommitId),
    dirty_commit:cumulative_score(Commit, ProjId)
  end).

get_merge_request_score({merge_request_id, MrId}, {project_id, ProjId}) ->
  transaction(fun() -> 
    MergeRequest = dirty_merge_request:with_id(MrId),
    dirty_merge_request:cumulative_score(MergeRequest, ProjId)
  end).

-spec get_merge_request_diff_scores(
  {merge_request_diff_id, db_merge_request_diff:id()}
) -> result:either(db_merge_request_scores:self(), any()).
get_merge_request_diff_scores({merge_request_diff_id, MrDiffId}) ->
  transaction(fun() -> 
  MergeRequestDiffScores = 
          dirty_merge_request_diff_scores:with_merge_request_diff_id(MrDiffId),
  dirty_merge_request_diff_scores:cumulative_score(MergeRequestDiffScores)
  end).
  
-spec get_snapshots(
  {project_id, db_project:id()}
) -> result:either([db_snapshot:self()], any()).
get_snapshots({project_id, ProjId}) ->
  transaction(fun() -> 
    UnsortedSnapshots = dirty_snapshot:all_in_project(ProjId),
    sort_snapshots(UnsortedSnapshots)  
  end).

get_last_snapshot({username, Username}) ->
  transaction(fun() -> 
    User = dirty_user:with_username(Username),
    dirty_snapshot:latest_by_user(db_user:id(User))
  end).

-spec sort_snapshots([db_snapshot:self()]) -> [db_snapshot:self()].
sort_snapshots(UnsortedSnapshots) ->
  Sorter = fun(Left, Right) ->
    LeftDate = db_snapshot:create_date(Left),
    RightDate = db_snapshot:create_date(Right),
    LeftSec = calendar:datetime_to_gregorian_seconds(LeftDate),
    RightSec = calendar:datetime_to_gregorian_seconds(RightDate),
    LeftSec >= RightSec
  end,
  lists:sort(Sorter, UnsortedSnapshots).
  
-spec get_snapshot({snapshot_id, db_snapshot:id()}) 
  -> result:either(db_snapshot:self(), any()).
get_snapshot({snapshot_id, SnapshotId}) ->
  transaction(fun() -> dirty_snapshot:with_id(SnapshotId) end).  

-spec get_members({project_id, db_project:id()}) 
  -> result:either(db_project:self(), any()).
get_members({project_id, ProjectId}) ->
  transaction(fun() -> dirty_member:all_in_project(ProjectId) end).

-spec get_member(
  {project_id, db_project:id()},
  {username, binary()} | {author_id, db_author:id()}
) -> result:either(db_member:self(), any()).
get_member({project_id, ProjId}, {username, Username}) ->
  transaction(fun() -> dirty_member:with_username(Username, ProjId) end);
get_member({project_id, ProjId}, {author_id, AuthorId}) ->
  transaction(fun() -> dirty_member:for_author(AuthorId, ProjId) end).

-spec get_authors({project_id, db_project:id()})
    -> result:either([db_author:self()], any()).
get_authors({project_id, ProjId}) ->
  transaction(fun() -> dirty_author:all_in_project(ProjId) end).

get_author({author_id, AuthorId}) ->
  transaction(fun() -> head(dirty_author:with_ids([AuthorId])) end).

-spec get_notes({merge_request_id, db_merge_request:id()}) 
  -> result:either([db_note:self()], any()).
get_notes({merge_request_id, MergeRequestId}) ->
  transaction(fun() -> dirty_note:all_in_merge_request(MergeRequestId) end);  
get_notes({snapshot_id, SnapshotId}) ->
  transaction(fun() -> dirty_note:all_in_snapshot(SnapshotId) end).

-spec get_notes(
  {snapshot_id, db_snapshot:id()},
  {member_id, db_member:id()}
) -> result:either([db_note:self()], any()).
get_notes({snapshot_id, SnapshotId}, {member_id, MemberId}) ->
  transaction(fun() -> dirty_note:all_by_member(MemberId, SnapshotId) end).

get_issues({snapshot_id, SnapshotId}) ->
  transaction(fun() -> dirty_issue:all_in_snapshot(SnapshotId) end).

get_issues({snapshot_id, SnapshotId}, {member_id, MemberId}, author) ->
  transaction(fun() ->  dirty_issue:all_when_author(MemberId, SnapshotId) end);
get_issues({snapshot_id, SnapshotId}, {member_id, MemberId}, assignee) ->
  transaction(fun() -> dirty_issue:all_when_assignee(MemberId, SnapshotId) end).

-spec get_authors_for_member(
  ProjectId :: {project_id, db_project:id()},
  MemberId :: {member_id, db_member:id()}  
) -> result:either([db_author:self()], any()).
get_authors_for_member({project_id, ProjId}, {member_id, MemberId}) ->
  transaction(fun() -> dirty_author:all_for_member(MemberId, ProjId) end).

-spec associate_member_author(
  {project_id, ProjId :: db_project:id()},  
  {member_id, MemberId :: db_member:id()},
  {author_id, AuthorId :: db_author:id()}
) -> result:either(db_unified_member:self(), any()).
associate_member_author({project_id, ProjId}, {member_id, MemberId}, 
                        {author_id, AuthorId}) ->
  UnifiedMember = construct_unified_member(MemberId, AuthorId, ProjId),
  transaction(fun() -> dirty_write(UnifiedMember) end).

%% will return defaults if none have been registered. 
get_diff_score_multipliers({project_id, ProjId}, {file_extension, Ext}) ->
  transaction(fun() ->
    case dirty_get_diff_score_multipliers({project_id, ProjId},
                                          {file_extension, Ext}) of
      none ->
        Scores = construct_diff_scores_multipliers(ProjId, Ext),
        dirty_write(Scores),
        Scores;
      Scores -> Scores
    end
  end).

update_diff_score_multipliers(Multipliers) ->
  transaction(fun() -> dirty_write(Multipliers) end).

update_diff_score_locations(Locations) ->
  transaction(fun() -> dirty_write(Locations) end).

dirty_get_diff_score_multipliers({project_id, ProjId}, {file_extension, Ext}) ->
  dirty_diff_score_multipliers:for_extension(Ext, ProjId).

% ------------- creators --------------

-spec create_project(db_project:self()) -> result:either(db_project:self(), any()).
create_project(Project) ->
  transaction(fun() ->
    case dirty_project:with_id(db_project:id(Project)) of
      none ->
        dirty_write(Project);
      Project ->
        {error, {exists, Project}}
    end
  end).

-spec create_bl_project(db_bl_project:self()) ->
  result:either(db_bl_project:self(), any()).
create_bl_project(Project) ->
  transaction(fun() ->
    case dirty_bl_project:with_id(db_bl_project:id_at_source(Project)) of
      none ->
        dirty_write(Project);
      Project ->
        {error, {exists, Project}}
    end
  end).
    
-spec create_snapshot(db_snapshot:self()) 
  -> {ok, db_snapshot:self()} | {error, any()}.
create_snapshot(Snapshot) ->
  Fun = fun() ->
    Id = db_snapshot:id(Snapshot),
    case dirty_snapshot:with_id(Id) of
      none ->
        dirty_write(Snapshot),
        Snapshot;
      Existing ->
        {error, {exists, Existing}}
    end
  end,
  transaction(Fun).

% ------------- updaters ----------------  

update_commit_author(Author, ProjectID) ->
  transaction(fun() -> 
    dirty_update_author(Author, ProjectID)
  end).
  
update_commit_authors(Authors, ProjectID) ->
  transaction(fun() ->  
    lists:map(fun(Author) -> dirty_update_author(Author, ProjectID) end, 
      Authors)
  end).

update_author_commits([]) ->
  {ok, none};
update_author_commits(AuthorCommits) ->
  transaction(fun() -> dirty_write(AuthorCommits) end).
  
update_commit(Commit, SnapshotID) ->
  transaction(fun() -> 
    dirty_update_commit(Commit, SnapshotID)
  end).
  
update_commits(Commits, SnapshotID) ->
  transaction(fun() -> 
    lists:foreach(fun(Commit) -> 
      dirty_update_commit(Commit, SnapshotID) 
    end, Commits),
    Commits
  end).
  
update_commit_file_diffs(FileDiffs) ->
  transaction(fun() -> dirty_write(FileDiffs) end).

update_commit_diff_scores(DiffStats) ->
  transaction(fun() -> dirty_write(DiffStats) end).
  
update_snapshot_state(SnapshotId, State) ->
  transaction(fun() ->
    case dirty_snapshot:with_id(SnapshotId) of
      none -> erlang:error({none, {snapshot, SnapshotId}});
      Snapshot -> 
        NewSnapshot = db_snapshot:set_state(State, Snapshot),
        dirty_write(NewSnapshot)
    end
  end).
  
update_merge_requests(MergeRequests, ProjectId) ->
  transaction(fun() ->
    ProjMRs = lists:map(fun(MR) ->
      MRId = db_merge_request:id(MR),
      construct_project_merge_request(ProjectId, MRId)
    end, MergeRequests),
    dirty_write(ProjMRs),
    dirty_write(MergeRequests)
  end).
  
update_merge_request_commits(MergeRequestCommits) ->
  transaction(fun() -> 
    lists:foreach(fun(MRCommit) ->
      % makes sure that the commit exists before linking it.
      CommitId = db_merge_request_commit:commit_id(MRCommit),
      case dirty_commit:with_id(CommitId) of
        none -> erlang:error({no_commit_matching, MRCommit});
        _Some -> dirty_write(MRCommit)
      end
    end, MergeRequestCommits)
  end).

update_merge_request_diffs(MergeRequestDiffs) ->
  transaction(fun() -> dirty_write(MergeRequestDiffs) end).

update_merge_request_diff_scores(MRDSs) ->
  transaction(fun() -> dirty_write(MRDSs) end).
  
update_members(Members) ->
  transaction(fun() -> dirty_write(Members) end).
 
atomically_do(Fun) ->
  transaction(Fun).

update_issues(Issues) -> 
  transaction(fun() -> dirty_write(Issues) end). 

-spec update_user(
  Username :: binary()
) -> result:either(db_user:self(), any()).
update_user(Username) ->
  User = construct_user(Username),
  transaction(fun() -> dirty_write(User) end).
  
update_api_token(Username, Source, Value) ->
  transaction(fun() ->
    case dirty_get_user({username, Username}) of
      none -> none;
      User ->
        Token = construct_api_token(db_user:id(User), Source, Value),
        dirty_write(Token)
    end
  end).

update_notes(Notes) ->
  transaction(fun() -> dirty_write(Notes) end).

get_api_token(Username, Source) ->
  transaction(fun() ->
    User = dirty_get_user({username, Username}),
    head(qlc:e(qlc:q([Token ||
      Token <- mnesia:table(api_token),
      Token#api_token.source == Source,
      Token#api_token.user_id == db_user:id(User) 
    ])))
  end).

-spec get_user({user_id, binary()}) -> result:either(db_user:self(), any()).
get_user({user_id, UserId}) ->
  transaction(fun() ->
    Query = qlc:q([ User ||
      User <- mnesia:table(db_user),
      db_user:id(User) == UserId
    ]),
    head(qlc:e(Query))
  end).

-spec delete_bl_project({sp_id, db_bl_project:sp_id()}
) -> result:either(1, any()).
delete_bl_project({sp_id, SpId}) ->
  transaction(fun() ->
    Project = dirty_bl_project:with_sp_id(SpId),
    mnesia:delete_object(Project)
  end).

-spec delete_snapshots(
  {state, State :: failed | in_progress | completed}
) -> result:either(Count :: non_neg_integer(), any());
(
  {snapshot_ids, [db_snapshot:id()]}
) -> result:either(Count :: non_neg_integer(), any()).
delete_snapshots({state, State}) ->
  transaction(fun() ->
    Snapshots = dirty_snapshot:all_with_state(State),
    lists:foldl(fun(Snapshot, X) ->
      mnesia:delete_object(Snapshot),
      X + 1
    end, 0, Snapshots)
  end);

delete_snapshots({snapshot_ids, SnapshotIds}) ->
  transaction(fun() ->
    lists:foldl(fun(SnapshotId, X) ->
      dirty_delete_snapshot({snapshot_id, SnapshotId}),
      X + 1
    end, 0, SnapshotIds)
  end).

-spec delete_snapshot({snapshot_id, db_snapshot:id()}
) -> result:either(1, any()).
delete_snapshot({snapshot_id, SnapshotId}) ->
  SingleSnapshotList = [SnapshotId],
  delete_snapshots({snapshot_ids, SingleSnapshotList}).

-spec rename_snapshot(
  {snapshot_id, db_snapshot:id()},
  NewTitle :: binary()
) -> result:either(db_snapshot:self(), any()).
rename_snapshot({snapshot_id, SnapshotId}, NewTitle) -> 
  transaction(fun() -> 
    case dirty_snapshot:with_id(SnapshotId) of
      none -> erlang:error({none, {snapshot, SnapshotId}});
      Snapshot -> 
        NewSnapshot = db_snapshot:set_title(NewTitle, Snapshot),
        dirty_write(NewSnapshot)
    end
  end).

%% returns number of associations deleted. 
-spec dissociate_member_authors(
  {project_id, db_project:id()}
) -> result:either(Count :: non_neg_integer(), any()).
dissociate_member_authors({project_id, ProjId}) ->
  transaction(fun() ->
    UnifiedMembers = qlc:e(qlc:q([ UnifiedMember ||
      UnifiedMember <- mnesia:table(db_unified_member),
      db_unified_member:project_id(UnifiedMember) == ProjId 
    ])),
    dirty_delete(UnifiedMembers)
  end).

dissociate_member_author({project_id, ProjId}, {author_id, AuthorId}) ->
  transaction(fun() ->
    UnifiedMembers = qlc:e(qlc:q([ UnifiedMember ||
      UnifiedMember <- mnesia:table(db_unified_member),
      db_unified_member:author_id(UnifiedMember) == AuthorId,
      db_unified_member:project_id(UnifiedMember) == ProjId
    ])),
    dirty_delete(UnifiedMembers)
  end);
dissociate_member_author({project_id, ProjId}, {member_id, MemberId}) ->
  transaction(fun() ->
    UnifiedMembers = qlc:e(qlc:q([ UnifiedMember ||
      UnifiedMember <- mnesia:table(db_unified_member),
      db_unified_member:project_id(UnifiedMember) == ProjId,
      db_unified_member:member_id(UnifiedMember) == MemberId
    ])),
    dirty_delete(UnifiedMembers)
  end).

%%%
%%%
%%%
%%%
%%%
%%%
%%%
%%%
%%%
%%%  
%%% PRIVATE METHODS
%%%
%%%
%%%
%%%
%%%
%%%
%%%
%%%
%%%

%% --------------------------------------------------------------------
%%  Dedicated creation function for Commits. Must be used instead of
%%  the default write as this will create an association between Snapshots
%%  and Commits.
%% --------------------------------------------------------------------
dirty_update_commit(Commit, SnapshotID) ->
  CommitId = db_commit:id(Commit),
  SnapshotCommit = construct_snapshot_commit(SnapshotID, CommitId),
  dirty_write(Commit),
  dirty_write(SnapshotCommit).
  
dirty_update_author(Author, ProjectID) ->
  ProjectAuthor = construct_project_author(ProjectID, db_author:id(Author)),
  dirty_write(Author),
  dirty_write(ProjectAuthor).

-spec dirty_write(
  Value :: list() | tuple()
) -> result:either(any(), any()).
dirty_write(List) when is_list(List) ->
  lists:foreach(fun(Record) -> 
    case mnesia:write(Record) of
      ok -> Record;
      {error, Reason} -> {error, Reason}
    end
  end, List);
dirty_write(Record) when is_tuple(Record) ->
  case mnesia:write(Record) of
    ok -> Record;
    {error, Reason} -> {error, Reason}
  end.

-spec dirty_delete(list()) -> result:either(non_neg_integer(), any());
            (tuple()) -> result:either(ok, any()).
dirty_delete(List) when is_list(List) ->
  lists:foldl(fun(Item, X) ->
    mnesia:delete_object(Item),
    X + 1
  end, 0, List);
dirty_delete(Record) when is_tuple(Record) ->
  mnesia:delete_object(Record).  

-spec dirty_get_user(
  {username, Username :: binary()}
) -> db_user:self() | none.
dirty_get_user({username, Username}) ->
  head(qlc:e(qlc:q([User ||
    User <- mnesia:table(db_user),
    db_user:username(User) == Username
  ]))).

-spec dirty_delete_snapshot(
  {snapshot_id, db_snapshot:id()}
) -> result:either(ok, any()).
dirty_delete_snapshot({snapshot_id, SnapshotId}) ->
  Snapshot = dirty_snapshot:with_id(SnapshotId),
  mnesia:delete_object(Snapshot).

  

  
%%% This function determines what you, the user, receives.
%%% Any successful transaction will return the tuple {ok, Result}.
%%%
%%% An aborted transaction will have the the tuple {error, Reason}. 
%%% Reason usually won't matter much, but this API may choose 
%%% to return a predefined Reason
%%% that the server can then action upon, such as email_exists 
%%% if you attempt to create a new user with an email that 
%%% is already in the database. 
%%%
%%% Make sure to read all API docs to handle these errors.  
-spec transaction(function()) -> result:either(any(), any()).
transaction(F) -> 
  case mnesia:transaction(F) of
    %% catch API errors and make them first class
    {_, {error, Reason}} -> {error, Reason};
    {atomic, none} -> {error, none};
    {atomic, Result} -> {ok, Result};
    {aborted, Reason} -> {error, Reason}
  end.

%% safely extract the head of a list
-spec head([Type]) -> Type | none.
head(List) -> 
  list_utils:head(List).
  
%% generates a unique ID for a given table
-spec gen_unique_id(atom()) -> non_neg_integer().
gen_unique_id(Table) ->
  mnesia:dirty_update_counter(unique_id, Table, 1).

%% Extracts the fields out of a record ignoring record name and ID
-spec record_to_fields(tuple()) -> tuple().
record_to_fields(Record) ->
  WithoutTable = erlang:delete_element(1, Record),
  WithoutID = erlang:delete_element(1, WithoutTable),
  WithoutID.

%% Returns the Name of a table record.
-spec record_name(tuple()) -> atom().
record_name(Record) ->
  element(1, Record).
  
%% Returns the identity of a record. 
%% Identity is defined as:
%% A deterministically computed fingerprint from the properties of a record
%% that uniquely identify it, ignoring mutable properties that do not change
%% the the identity of the record itself. 
%% 
%% Identity computations ignore the record name and ID, and mutable properties 
%% like the message on a commit; a commit's identity is fundamentally what
%% project it belongs to and its SHA as those are immutable truths about what
%% it is. 
-spec identity(tuple()) -> key() | none().
identity(Record) ->
  Fields = record_to_fields(Record),
  ReqProperties = case record_name(Record) of
    snapshot_commit ->
      Fields;
    project_author ->
      Fields;
    api_token ->
      UserId = Record#api_token.user_id,
      Source = Record#api_token.source,
      {UserId, Source};
    UndefinedRecord -> 
      erlang:error({undefined_identity, UndefinedRecord, Record})
  end,
  crypto:hash(sha, term_to_binary(ReqProperties)).
  
%%%
%%%
%%% Debug Methods must NEVER to be used in production code.
%%%
%%%

%%%
%%% enter the atom for a table name, and it'll return entire contents.
%%% reference dnd_db.hrl for a list of table names.
%%%
-spec show_whole_table(atom()) -> result:either({non_neg_integer(), 
                                                list()}, any()).
show_whole_table(Table) ->
    Q = qlc:q([Char || Char <- mnesia:table(Table)]),
    Fun = fun() -> qlc:e(Q) end,
    case transaction(Fun) of
      {ok, List} -> {length(List), List};
      {error, Reason} -> {error, Reason}
    end.
