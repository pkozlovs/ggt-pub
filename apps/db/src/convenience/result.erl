-module(result).
-export([map/2, flatMap/2, mapError/2, recover/2, zip/2, zip3/3, zip_list/1]).
-export([flatten_list/1, values/1, map_list/2, flatMap_list/2]).
-export([value/2, value/1]).
-export_type([either/2]).

-type either(Value, Error) :: {ok, Value} | {error, Error}.

-spec map(
  Result :: either(T, E), 
  Fun :: fun((T) -> U) 
) -> either(U, E).
map(Result, Fun) ->
  case Result of
    {ok, Value} -> {ok, Fun(Value)};
    {error, _} -> Result
  end.
  
-spec flatMap(
  Result :: either(T, E1), 
  fun((T) -> either(U, E2))
) -> either(T, E1) | either(U, E2).
flatMap(Result, Fun) -> 
  case map(Result, Fun) of
    {ok, {ok, Value}} -> 
      {ok, Value};
    {ok, {error, NewError}} ->
      {error, NewError};
    {error, OldError} -> 
      {error, OldError}
  end.
  
-spec mapError(
  Result :: either(T, E),
  Fun :: fun((E) -> U)
) -> either(T | U, none()).
mapError(Result, Fun) ->
  case Result of
    {ok, _} -> 
      Result;
    {error, Error} -> 
      {error, Fun(Error)}
  end.
  
-spec recover(
  Result :: either(T, E),
  Default :: U
) -> either(T | U, E).
recover(Result, Default) ->
  case Result of
    {ok, _} -> 
      Result;
    {error, _} ->
      {ok, Default}
  end. 
  
-spec zip(
  Left :: either(V1, _),
  Right :: either(V2, _)
) -> either({V1, V2}, any()).
zip({ok, Value1}, {ok, Value2}) ->
  {ok, {Value1, Value2}};
zip({error, Error1}, {error, Error2}) ->
  {error, {{left_error, Error1}, {right_error, Error2}}};
zip({error, Error}, _) ->
  {error, Error};
zip(_, {error, Error}) ->
  {error, Error}.
  
-spec zip_list(
  List :: [either(T, E)]
) -> either([T], {{errors, [E]}, {values, [T]}}).
zip_list(List) when is_list(List) ->
  do_zip_list(List, [], []).

do_zip_list([], [], Values) ->
  {ok, lists:reverse(Values)};
do_zip_list([], Errors, Values) ->
  {error, 
    {
      {errors, lists:reverse(Errors)}, 
      {values, lists:reverse(Values)}
    }
  };
do_zip_list([Head | Tail], Errors, Values) ->
  case Head of
    {ok, Value} ->
      do_zip_list(Tail, Errors, [Value | Values]);
    {error, Reason} ->
      do_zip_list(Tail, [Reason | Errors], Values)
  end.

-spec zip3(
  Result1 :: either(V1, E1),
  Result2 :: either(V2, E2),
  Result3 :: either(V3, E3)
) -> either(
       {V1, V2, V3}, 
       {either(V1, E1), either(V2, E2), either(V3, E3)}
      ). 
zip3({ok, Val1}, {ok, Val2}, {ok, Val3}) ->
  {ok, {Val1, Val2, Val3}};
zip3(Result1, Result2, Result3) ->
  {error, {Result1, Result2, Result3}}.
  
-spec flatten_list(
  ResultList :: [either(T, E)]
) -> either([T], {first_error_reason, E}).
flatten_list([{ok, Value} | Tail]) ->
  do_flatten_list(Tail, {ok, Value});
flatten_list([{error, Reason} | _Tail]) ->
  {error, {first_error_reason, Reason}}.

-spec do_flatten_list(
  ResultList :: [either(T, E)],
  Acc :: either(T, {first_error_reason, E})
) -> either([T], {first_error_reason, E}).
do_flatten_list([], {ok, Values}) ->
  {ok, lists:reverse(Values)};
do_flatten_list([{ok, Value} | Tail], {ok, Values}) ->
  do_flatten_list(Tail, {ok, [Value | Values]});
do_flatten_list([{error, Reason} | _], _Values) ->
  {error, {first_error_reason, Reason}}.
  
-spec values(
  Results :: [either(T, _)]
) -> [T].
values(Results) when is_list(Results) ->
  lists:filtermap(fun(Result) ->
    case Result of
      {ok, Value} -> {true, Value};
      {error, _Reason} -> false;
      _ -> false % TODO: remove this when Pavel fixes his commit garbage.
    end
  end, Results).

value(Result, Default) ->
  case Result of 
    {ok, Value} -> Value;
    {error, _} -> Default
  end.

value({ok, Value}) -> Value.
  
-spec map_list(
  ListResult :: [either(T, E)], 
  Fun :: fun((T) -> U)
) -> [either(U, E)].
map_list(ListResult, Fun) -> 
  lists:map(fun(Result) ->
    result:map(Result, Fun)
  end, ListResult).

flatMap_list(ResultList, Fun) ->
  lists:map(fun(Result) ->
    result:flatMap(Result, Fun)
  end, ResultList).

