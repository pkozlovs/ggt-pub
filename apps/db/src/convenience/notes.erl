-module(notes).
-include_lib("apps/db/include/db.hrl").
-export([split_by_day/1, counts_by_day/1, compare/3]).

-spec split_by_day(
  Notes :: [db_note:self()]
) -> [{calendar:date(), [db_note:self()]}].
split_by_day(Notes) ->
  NoteMap = lists:foldl(fun(Note, Map) ->
    {Date, _} = db_note:create_date(Note),
    Ns = maps:get(Date, Map, []),
    maps:put(Date, [Note|Ns], Map)
  end, #{}, Notes),
  lists:sort(fun({LDate, _}, {RDate, _}) ->
    LDays = calendar:date_to_gregorian_days(LDate),
    RDays = calendar:date_to_gregorian_days(RDate),
    compare(LDays, RDays, ascending)
  end, maps:to_list(NoteMap)).

-spec counts_by_day(
  Notes :: [db_note:self()]
) -> [{calendar:date(), non_neg_integer()}].
counts_by_day(Notes) ->
  NoteDates = split_by_day(Notes),
  lists:map(fun({Date, Ns}) -> {Date, length(Ns)} end, NoteDates).

compare(LSec, RSec, ascending) ->
  LSec =< RSec;
compare(LSec, RSec, descending) ->
  LSec >= RSec.
