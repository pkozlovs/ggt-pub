-module(merge_requests).
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/db/include/frontend_structs.hrl").
-export([sorted/1, sorted/2, creation_unix_time/1, split_by_day/1,
        counts_by_day/1, combine_scores/3]).

sorted(MergeRequests) ->
  sorted(MergeRequests, descending).

sorted(MergeRequests, Order) ->
  Sorter = fun(Left, Right) ->
    LSec = creation_unix_time(Left),
    RSec = creation_unix_time(Right),
    compare(LSec, RSec, Order)
  end,
  lists:sort(Sorter, MergeRequests).

-spec split_by_day(
  MergeRequests :: [db_merge_request:self()]
) -> [{calendar:date(), [db_merge_request:self()]}].
split_by_day(MergeRequests) ->
  MRMap = lists:foldl(fun(MergeRequest, Map) ->
    {Date, _} = db_merge_request:merge_date(MergeRequest),
    MRs = maps:get(Date, Map, []),
    maps:put(Date, [MergeRequest|MRs], Map)
  end, #{}, MergeRequests),
  lists:sort(fun({LDate, _}, {RDate, _}) ->
    LDays = calendar:date_to_gregorian_days(LDate),
    RDays = calendar:date_to_gregorian_days(RDate),
    compare(LDays, RDays, ascending)
  end, maps:to_list(MRMap)).

-spec counts_by_day(
  MergeRequests :: [db_merge_request:self()]
) -> [{calendar:date(), non_neg_integer()}].
counts_by_day(MergeRequests) ->
  MRDates = split_by_day(MergeRequests),
  lists:map(fun({Date, MRs}) -> {Date, length(MRs)} end, MRDates).

compare(LSec, RSec, ascending) ->
  LSec =< RSec;
compare(LSec, RSec, descending) ->
  LSec >= RSec.

creation_unix_time(MergeRequest) ->
  Date = db_merge_request:merge_date(MergeRequest),
  calendar:datetime_to_gregorian_seconds(Date).

combine_scores(ScoresList, Multipliers, MergeRequest) ->
  MultMap = maps:from_list(lists:map(fun(Mult) ->
    {db_score_multipliers:file_extension(Mult), Mult}
  end, Multipliers)),
  ScoreMults = lists:map(fun(Scores) ->
    Ext = db_merge_request_scores:file_extension(Scores),
    {Scores, maps:get(Ext, MultMap, db_score_multipliers:new())}
  end, ScoresList),
  MultipliedScores = lists:map(fun({MergeRequestScores, Mults}) ->
    diff_scores:multiply_merge_request_scores(MergeRequestScores, Mults)
  end, ScoreMults),
  RawScores = lists:map(fun(RawScore) ->
    db_merge_request_scores:scores(RawScore)
  end, ScoresList),
  CumScore = diff_scores:sum_scores(MultipliedScores),
  CumRawScore = diff_scores:sum_scores(RawScores),
  TotalScore = diff_scores:total_score(CumScore),
  #cumulative_merge_request_score{
    merge_request = MergeRequest,
    raw_scores = CumRawScore,
    scores = CumScore,
    sum = TotalScore
  }.
