-module(diff_scores).
-include_lib("apps/db/include/db.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([multiply_commit_scores/2, multiply_scores/2, 
        multiply_merge_request_scores/2]).
-export([sum_scores/1, total_score/1]).
-export([add/2, multiply/2]).

-spec multiply_commit_scores(
  CommitScores :: db_commit_diff_scores:self(),
  Multipliers :: db_score_multipliers:self()
) -> db_diff_scores:self().
multiply_commit_scores(CommitScores, Multipliers) ->
  Scores = db_commit_diff_scores:scores(CommitScores),
  multiply_scores(Scores, Multipliers).

-spec multiply_merge_request_scores(
  MergeRequestScores :: db_merge_request_scores:self(),
  Multipliers :: db_score_multipliers:self()
) -> db_diff_scores:self().
multiply_merge_request_scores(MergeRequestScores, Multipliers) ->
  Scores = db_merge_request_scores:scores(MergeRequestScores),
  multiply_scores(Scores, Multipliers).

-spec multiply_scores(
  Scores :: db_diff_scores:self(),
  ScoreMultipliers :: db_score_multipliers:self()
) -> db_diff_scores:self().
multiply_scores(Scores, ScoreMultipliers) ->
  Mults = db_score_multipliers:multipliers(ScoreMultipliers),
  multiply(Scores, Mults).

-spec sum_scores([db_diff_scores:self()]) -> db_diff_scores:self().
sum_scores(ScoresList) ->
  ZeroScores = db_diff_scores:new(),
  lists:foldl(fun(Scores, Acc) ->
    add(Scores, Acc)
  end, ZeroScores, ScoresList). 

-spec total_score(db_diff_scores:self()) -> integer().
total_score(Scores) ->
  List = erlang:tuple_to_list(erlang:delete_element(1, Scores)),
  lists:sum(List).

-spec multiply(db_diff_scores:self(), db_diff_scores:self()) 
  -> db_diff_scores:self().
multiply(L, R) ->
  db_diff_scores:multiply(L, R). % TODO: remove me completely

-spec add(db_diff_scores:self(), db_diff_scores:self()) 
  -> db_diff_scores:self().
add(L, R) ->
  db_diff_scores:add(L, R). % TODO: remove me too, but clean code base
