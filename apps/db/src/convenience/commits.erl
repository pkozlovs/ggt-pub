-module(commits).
-export([sort/2, sort/1, compare/3]).
-export([creation_unix_time/1]).
-export([is_in_snapshot/2]).
-export([split_by_day/1, counts_by_day/1]). 
-export([combine_scores/3]).
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/db/include/frontend_structs.hrl").

-spec sort(
  Commits :: [db_commit:self()]
) -> [db_commit:self()].
sort(Commits) ->
  sort(Commits, descending).

-spec sort(
  Commits :: [db_commit:self()],
  Order :: ascending | descending
) -> [db_commit:self()].
sort(Commits, Order) ->
  Sorter = fun(Left, Right) ->
    LSec = creation_unix_time(Left),
    RSec = creation_unix_time(Right),
    case Order of
      ascending ->
        LSec =< RSec;
      descending ->
        LSec >= RSec
    end
  end,
  lists:sort(Sorter, Commits).

-spec creation_unix_time(
  Commit :: db_commit:self()
) -> non_neg_integer().
creation_unix_time(Commit) ->
  calendar:datetime_to_gregorian_seconds(db_commit:create_date(Commit)).

-spec is_in_snapshot(
  Commit :: db_commit:self(),
  Snapshot :: db_snapshot:self()
) -> boolean().
is_in_snapshot(Commit, Snapshot) ->
  db_snapshot:contains_datetime(db_commit:create_date(Commit), Snapshot).

-spec split_by_day(
  Commits :: [db_commit:self()]
) -> [{calendar:date(), [db_commit:self()]}].
split_by_day(Commits) ->
  CommitMap = lists:foldl(fun(Commit, Map) ->
    {Date, _} = db_commit:create_date(Commit),
    Coms = maps:get(Date, Map, []),
    maps:put(Date, [Commit|Coms], Map)
  end, #{}, Commits),
  lists:sort(fun({LDate, _}, {RDate, _}) ->
    LDays = calendar:date_to_gregorian_days(LDate),
    RDays = calendar:date_to_gregorian_days(RDate),
    compare(LDays, RDays, ascending)
  end, maps:to_list(CommitMap)).

-spec counts_by_day(
  Commits :: [db_commit:self()]
) -> [{calendar:date(), non_neg_integer()}].
counts_by_day(Commits) ->
  CommitDates = split_by_day(Commits),
  lists:map(fun({Date, Coms}) -> {Date, length(Coms)} end, CommitDates).

-spec compare(non_neg_integer(), non_neg_integer(), ascending | descending)
  -> boolean().
compare(LSec, RSec, ascending) ->
  LSec =< RSec;
compare(LSec, RSec, descending) ->
  LSec >= RSec.

combine_scores(ScoresList, Multipliers, Commit) ->
   MultMap = maps:from_list(lists:map(fun(Mult) ->
    {db_score_multipliers:file_extension(Mult), Mult}
  end, Multipliers)),
  ScoreMults = lists:map(fun(Scores) ->
    Ext = db_commit_diff_scores:file_extension(Scores),
    {Scores, maps:get(Ext, MultMap, db_score_multipliers:new())}
  end, ScoresList),
  MultipliedScores = lists:map(fun({CommitScores, Mults}) ->
    diff_scores:multiply_commit_scores(CommitScores, Mults)
  end, ScoreMults),
  RawScores = lists:map(fun(RawScore) -> 
    db_commit_diff_scores:scores(RawScore)
  end, ScoresList),
  CumScore = diff_scores:sum_scores(MultipliedScores),
  CumRawScore = diff_scores:sum_scores(RawScores),
  TotalScore = diff_scores:total_score(CumScore),
  #cumulative_commit_score{
    commit = Commit,
    raw_scores = CumRawScore,
    scores = CumScore,
    sum = TotalScore
  }.


