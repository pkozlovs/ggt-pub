-module(pair_programming_parser).
-export([parse/1]).

-type error() :: {?MODULE,
                    message_empty
                  | no_marker
                  | author_formatting
                 }.

-type return() :: result:either([binary()], error()).

-spec parse(binary()) -> return().
parse(<<>>) -> {error, message_empty};
parse(Msg) ->
  Lines = binary:split(Msg, <<"\n">>, [global, trim_all]),
  parse_lines(Lines).

-spec parse_lines([binary()]) -> return().
parse_lines([]) -> {error, no_marker};
parse_lines(Lines) ->
  parse_lines(Lines, []).

-spec parse_lines([binary()], [binary()]) -> return().
parse_lines([], Authors) -> {ok, lists:usort(lists:flatten(Authors))};
parse_lines([<<"[PP:", Users/binary>> | Tail], Authors) ->
  parse_lines_helper(Users, Tail, Authors);
parse_lines([<<"PP:", Users/binary>> | Tail], Authors) ->
  parse_lines_helper(Users, Tail, Authors);
parse_lines([_|T], Authors) -> parse_lines(T, Authors).
  
parse_lines_helper(Users, Tail, Authors) ->
  case parse_authors(Users) of
    {ok, NewAuthors} -> parse_lines(Tail, [NewAuthors, Authors]);
    Error={error,_} -> Error
  end.

parse_authors(Bin) ->
  NoBracket = binary:replace(Bin, <<"]">>, <<"">>, [global]), % this is a hack, but screw it
  NoSpace = binary:replace(NoBracket, <<" ">>, <<"">>, [global]),
  case binary:split(NoSpace, <<",">>, [global, trim_all]) of
    [] -> {error, author_formatting};
    [Bin] -> % get the same string in list if no split
      {error, author_formatting};
    Authors -> 
      % we copy for garbage collection reasons; split returns references. 
      CopiedAuthors = lists:map(fun binary:copy/1, Authors),
      {ok, CopiedAuthors}
  end.

  

