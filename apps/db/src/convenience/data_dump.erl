-module(data_dump).
-export([dump_data/2]).

dump_data(SnapshotString, Filename) ->
    SnapshotId = hash:hex_to_bin(list_to_binary(SnapshotString)),
    {ok, Snapshot} = local:snapshot(SnapshotId),

    io:format("Dumping data to file ~p~n", [Filename]),
    write_to_file(Filename, "Snapshot: ~p ~n", [db_snapshot:title(Snapshot)]),
    ProjectId = db_snapshot:project_id(Snapshot),
    Members = local:members(ProjectId),
    MemberNames = lists:map(fun db_member:name/1, Members),
    AuthorsNames = lists:map(fun db_author:name/1, local:authors(ProjectId)),
    write_to_file(Filename, "Project authors: ~p, ~nProject members: ~p ~n", [AuthorsNames, MemberNames]),

    dump_mr_for_members(Snapshot, Filename, Members).

dump_mr_for_members(_, _, []) -> done;
dump_mr_for_members(Snapshot, Filename, [H|T]) ->
    write_to_file(Filename, "MregeRequests for member ~p~n", [db_member:name(H)]),
    MergeRequests = local:merge_requests(Snapshot, H),
    ProjectId = db_snapshot:project_id(Snapshot),
    print_merge_requests_data(MergeRequests, Filename, ProjectId),
    dump_mr_for_members(Snapshot, Filename, T).

print_merge_requests_data(MergeRequests, Filename, ProjectId) ->
    lists:map(fun (MergeRequest) ->
        Id = db_merge_request:id(MergeRequest),
        Title = db_merge_request:title(MergeRequest),
        Message = db_merge_request:message(MergeRequest),

        Creator = db_merge_request:creator_id(MergeRequest),
        Assignee = db_merge_request:assignee_id(MergeRequest),
        State = db_merge_request:state(MergeRequest),
        TargetBranch = db_merge_request:target_branch(MergeRequest),
        SourceBranch = db_merge_request:source_branch(MergeRequest),

        Sha = db_merge_request:sha(MergeRequest),
        MergeSha = db_merge_request:merge_sha(MergeRequest),
        MergeDate = db_merge_request:merge_date(MergeRequest),
        MergeURL = db_merge_request:web_url(MergeRequest),

        {ok, Stats} = db:get_merge_request_stats({merge_request_id, Id}),
        CommitCount = db_merge_request_stats:commit_count(Stats),
        Net = db_merge_request_stats:net(Stats),
        Score = local:merge_request_score(Id, ProjectId),

        write_to_file(Filename, "MergeRequest id - title (~p - ~p)
                  \tMessage ~p
                  \tCreator ~p
                  \tAssignee ~p
                  \tState ~p
                  \tTargetBranch ~p
                  \tSourceBranch ~p
                  \tSha ~p
                  \tMergeSha ~p
                  \tMergeDate ~p
                  \tMergeURL ~p
                  \tCommit Count ~p
                  \tNet ~p
                  \tScore ~p~n", [Id, Title, Message, Creator, Assignee, State, 
                                    TargetBranch, SourceBranch, Sha, MergeSha, MergeDate, MergeURL, CommitCount, Net, Score]),
        print_commits_data(MergeRequest, Filename, ProjectId)
     end, MergeRequests).

print_commits_data(MergeRequest, Filename, ProjectId) ->
    MergeId = db_merge_request:id(MergeRequest),
    Commits = local:commits(MergeId),
    lists:map(fun (Commit) ->
        Id = db_commit:id(Commit),
        Title = db_commit:title(Commit),
        Message = db_commit:title(Commit),

        Sha = db_commit:title(Commit),
        ParentShas = db_commit:parent_shas(Commit),
        Author = db_commit:author_id(Commit),
        CreationDate = db_commit:create_date(Commit),

        {ok, Stats} = db:get_commit_stats({commit_id, Id}),
        {Inserts, Del, Net} = db_commit_stats:counts(Stats),
        Score = local:score(Id, ProjectId),
        {ok, Diffs} = db:get_commit_file_diffs({commit_id, Id}),

        write_to_file(Filename, "\tCommit id - title (~p - ~p)
                \t\tMessage ~p
                \t\tSha ~p,
                \t\tParentShas ~p
                \t\tAuthor ~p
                \t\tCreationDate ~p
                \t\tInserts ~p
                \t\tDeletions ~p
                \t\tNet ~p
                \t\tScore ~p
                \t\tDiff ~p~n", [Id, Title, Message, Sha, ParentShas, Author, CreationDate, Inserts, Del, Net, Score, Diffs])
    end, Commits).

write_to_file(Filename, Format, Data) ->
    case file:write_file(Filename, io_lib:fwrite(Format, Data), [append]) of
        {error, Reason} -> io:format("Failed to write to file ~p for reason ~p", Filename, Reason);
        _ -> ok
    end.