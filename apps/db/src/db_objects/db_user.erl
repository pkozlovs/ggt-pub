-module(db_user).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([username/1]).

-type id() :: key().

-record(db_user,
  {
    id :: id(),
    username :: binary()
  }).
-type self() :: #db_user{}.

id(#db_user{id=Id}) -> Id.

identity(#db_user{username=Username}) ->
  db_object:hash_fields(Username).

new({Username}) -> 
  Self = #db_user{username=Username},
  db_object:update_id(Self).

record_info() -> record_info(fields, db_user).

is_self(#db_user{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
username(#db_user{username=Username}) -> Username.
