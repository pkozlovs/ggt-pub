-module(db_object).
-include_lib("apps/db/include/db.hrl").
-export([hash_fields/1, id/1, update_id/1, assert_self/2]).

-define(ID, 2).
-define(NAME, 1).

-callback id(Self :: tuple()) -> key().

-callback identity(Self :: tuple()) -> key().

-callback new(Args :: tuple()) -> tuple().

-callback record_info() -> tuple().

-callback is_self(Self :: tuple()) -> boolean().

-callback assert_self(Self :: tuple()) -> tuple() | no_return().

hash_fields(Fields) -> 
  crypto:hash(sha, term_to_binary(Fields)).

id(Object) ->
  erlang:element(?ID, Object).

update_id(Object) ->
  Name = erlang:element(?NAME, Object),
  Id = Name:identity(Object),
  IdRemoved = erlang:delete_element(?ID, Object),
  erlang:insert_element(?ID, IdRemoved, Id). 

assert_self(Module, Object) ->
  case Module:is_self(Object) of
    true -> Object;
    false -> throw({?MODULE, unexpected_record, Object})
  end.
