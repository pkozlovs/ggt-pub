-module(db_merge_request_scores).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([merge_request_id/1, merge_request_diff_id/1, file_extension/1,
        ignored/1, ignore_reason/1, scores/1]).
%%% export convenience

-type id() :: key().

-record(db_merge_request_scores,
  {      
    id :: id(),
    merge_request_id :: db_merge_request:id(),
    merge_request_diff_id :: db_merge_request_diff:id(),
    file_extension = <<>> :: binary(),
    ignored = false :: boolean(),
    ignore_reason = undefined :: undefined 
      | too_many_insertions 
      | too_many_deletions,
    scores = db_diff_scores:new() :: db_diff_scores:self()
  }).
-type self() :: #db_merge_request_scores{}.

id(#db_merge_request_scores{id=Id}) -> Id.

identity(Scores=#db_merge_request_scores{}) ->
  MrId = merge_request_id(Scores),
  MrDiffId = merge_request_diff_id(Scores),
  Fields = {MrId, MrDiffId},
  db_object:hash_fields(Fields).

new({MrId, MrDiffId, {ignored, Reason}}) ->
  Self = #db_merge_request_scores{
      merge_request_id = MrId,
      merge_request_diff_id = MrDiffId,
      ignored = true,
      ignore_reason = Reason
  },
  db_object:update_id(Self);
new({MrId, MrDiffId, Ext, White, Syntax, Moves, Refs, Ins, Dels}) ->
  Scores = db_diff_scores:new({White, Syntax, Moves, Refs,
    Ins, Dels}),
  Self = #db_merge_request_scores{
    merge_request_id = MrId,
    merge_request_diff_id = MrDiffId,
    file_extension = Ext,
    scores = Scores
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_merge_request_scores).

is_self(#db_merge_request_scores{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
merge_request_id(#db_merge_request_scores{merge_request_id=MrId}) -> MrId.
merge_request_diff_id(#db_merge_request_scores{merge_request_diff_id=Id}) 
  -> Id.
file_extension(#db_merge_request_scores{file_extension=Ext}) -> Ext.
ignored(#db_merge_request_scores{ignored=Bool}) -> Bool.
ignore_reason(#db_merge_request_scores{ignore_reason=Reason}) -> Reason.
scores(#db_merge_request_scores{scores=Scores}) -> Scores.

