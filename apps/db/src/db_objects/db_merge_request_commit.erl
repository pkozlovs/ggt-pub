-module(db_merge_request_commit).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([commit_id/1, merge_request_id/1]).
-export([pretty/1]).

-type id() :: key().

-record(db_merge_request_commit,
  {
    id :: id(),
    commit_id :: db_commit:id(),
    merge_request_id :: db_merge_request:id()
  }).
-opaque self() :: #db_merge_request_commit{}.

id(#db_merge_request_commit{id=Id}) -> Id.

identity(#db_merge_request_commit{commit_id=CId, merge_request_id=MrId}) ->
  Fields = {CId, MrId},
  db_object:hash_fields(Fields).

new({CommitId, MrId}) ->
  Self = #db_merge_request_commit{
    commit_id = CommitId,
    merge_request_id = MrId
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_merge_request_commit).

is_self(#db_merge_request_commit{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
commit_id(#db_merge_request_commit{commit_id=CId}) -> CId.
merge_request_id(#db_merge_request_commit{merge_request_id=MrId}) -> MrId.

pretty(Self) ->
  #{id => id(Self), commit_id => commit_id(Self), 
    merge_request_id => merge_request_id(Self) }.
