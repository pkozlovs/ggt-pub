-module(db_merge_request_stats).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([merge_request_id/1, net/1, commit_count/1]).
%%% export convenience

-type id() :: key().

-record(db_merge_request_stats,
  {
   id :: id(),
   merge_request_id :: db_merge_request:id(),
   net = 0 :: integer(),
   commit_count = 0 :: integer()
  }).
-type self() :: #db_merge_request_stats{}.

id(#db_merge_request_stats{id=Id}) -> Id.

identity(#db_merge_request_stats{merge_request_id=MrId}) -> 
  db_object:hash_fields(MrId).

new({MrId, Net, CommitCount}) ->
  Self = #db_merge_request_stats{
    merge_request_id = MrId, net = Net, commit_count = CommitCount},
  db_object:update_id(Self).

record_info() -> record_info(fields, db_merge_request_stats).

is_self(#db_merge_request_stats{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
merge_request_id(#db_merge_request_stats{merge_request_id=MrId}) -> MrId.
net(#db_merge_request_stats{net=Net}) -> Net.
commit_count(#db_merge_request_stats{commit_count=Count}) -> Count.

