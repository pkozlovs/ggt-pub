-module(db_project).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% getters
-export([source/1, id_at_source/1, name/1, web_url/1, clone_url/1, sp_id/1]).

-type id() :: key().
-record(db_project, 
  {
    id = undefined :: id(),
    source :: source(),
    id_at_source :: binary(),
    name :: binary(),
    web_url :: binary(),
    clone_url :: binary()
  }).
-opaque self() :: #db_project{}.

%%% 
%%% Required Callbacks
%%%
id(Self) -> Self#db_project.id.

identity(Self) ->
  Fields = {Self#db_project.source, Self#db_project.id_at_source},
  db_object:hash_fields(Fields).

new({Source, SourceId, Name, WebUrl, CloneUrl}) ->
  Self = #db_project{
    source = Source,
    id_at_source = SourceId,
    name = Name,
    web_url = WebUrl,
    clone_url = CloneUrl
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_project).

is_self(#db_project{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
source(Self) -> Self#db_project.source.
id_at_source(Self) -> Self#db_project.id_at_source.
name(Self) -> Self#db_project.name.
web_url(Self) -> Self#db_project.web_url.
clone_url(Self) -> Self#db_project.clone_url.
sp_id(Self) -> {source(Self), id_at_source(Self)}.

