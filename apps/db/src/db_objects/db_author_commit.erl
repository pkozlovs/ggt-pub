-module(db_author_commit).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([author_id/1, commit_id/1]).

-type id() :: key().

-record(db_author_commit, {
  id :: id(),
  author_id :: db_author:id(),
  commit_id :: db_commit:id()
}).

-opaque self() :: #db_author_commit{}.

id(#db_author_commit{id=Id}) -> Id.

identity(Self=#db_author_commit{}) ->
  AuthorId = author_id(Self),
  CommitId = commit_id(Self),
  Fields = {AuthorId, CommitId},
  db_object:hash_fields(Fields).

new({AuthorId, CommitId}) ->
  Self = #db_author_commit{author_id = AuthorId, commit_id = CommitId},
  db_object:update_id(Self).

record_info() -> record_info(fields, db_author_commit).

is_self(#db_author_commit{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).


%%%
%%% Accessors
%%%
author_id(#db_author_commit{author_id=Id}) -> Id.
commit_id(#db_author_commit{commit_id=Id}) -> Id. 
