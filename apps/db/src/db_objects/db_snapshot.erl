-module(db_snapshot).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0, state/0, simple_state/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export getters
-export([project_id/1, start_date/1, end_date/1, creator_id/1, create_date/1, 
         title/1, state/1]).
%%% export convenience
-export([date_range/1, date_range_sec/1, contains_datetime/2, 
         creation_unix_time/1]).
-export([simple_state/1]).
%%% sorting
-export([sort/1, sort/2]).
%%% export setters
-export([set_title/2, set_state/2]).

-type id() :: key().
-type state() :: in_progress | {failed, any()} | completed.
-type simple_state() :: in_progress | failed | completed.
-record(db_snapshot,
  {
    id :: id(),
    project_id :: db_project:id(),
    start_date :: calendar:datetime(),
    end_date :: calendar:datetime(),
    creator_id :: key(), 
    create_date :: calendar:datetime(), 
    title :: binary(),
    state :: state()
  }).
-opaque self() :: #db_snapshot{}.

id(Self) -> Self#db_snapshot.id.

identity(Self) ->
  Start = Self#db_snapshot.start_date,
  End = Self#db_snapshot.end_date,
  ProjectId = Self#db_snapshot.project_id,
  Creator = Self#db_snapshot.creator_id,
  Fields = {Start, End, ProjectId, Creator},
  db_object:hash_fields(Fields).

new({ProjId, Start, End, CreateDate, CreatorId, Name, State}) ->
  Date = case CreateDate of
    now -> erlang:localtime();
    CreationDate -> CreationDate
  end,
  Self = #db_snapshot{
    project_id = ProjId,
    start_date = Start,
    end_date = End,
    create_date = Date,
    creator_id = CreatorId,
    title = Name,
    state = State
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_snapshot).

is_self(#db_snapshot{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(Self) -> Self#db_snapshot.project_id.
start_date(Self) -> Self#db_snapshot.start_date.
end_date(Self) -> Self#db_snapshot.end_date.
create_date(Self) -> Self#db_snapshot.create_date.
creator_id(Self) -> Self#db_snapshot.creator_id.
title(Self) -> Self#db_snapshot.title.
state(Self) -> Self#db_snapshot.state.

%%%
%%% Convenience functions
%%%
-spec date_range(
  Snapshot :: self()
) -> {calendar:datetime(), calendar:datetime()}.
date_range(Self) ->
  Start = start_date(Self),
  End = end_date(Self),
  {Start, End}.

-spec date_range_sec(
  Snapshot :: self()
) -> {non_neg_integer(), non_neg_integer()}.
date_range_sec(Snapshot) ->
  {Start, End} = date_range(Snapshot),
  StartSec = calendar:datetime_to_gregorian_seconds(Start),
  EndSec = calendar:datetime_to_gregorian_seconds(End),
  {StartSec, EndSec}.

-spec contains_datetime(
  DateTime :: calendar:datetime(), 
  Snapshot :: self()
) -> boolean().
contains_datetime(DateTime, Snapshot) ->
  Sec = calendar:datetime_to_gregorian_seconds(DateTime),
  {StartSec, EndSec} = date_range_sec(Snapshot),
  StartSec =< Sec andalso Sec =< EndSec.

-spec simple_state(
  Snapshot :: self()
) -> in_progress | completed | failed.
simple_state(Snapshot) ->
  case state(Snapshot) of
    in_progress -> in_progress;
    completed -> completed;
    {failed, _} -> failed
  end.

sort(Snapshots) -> sort(Snapshots, descending).

sort(Snapshots, descending) -> 
  lists:sort(fun sorter_descending/2, Snapshots);
sort(Snapshots, ascending) ->
  lists:reverse(sort(Snapshots, descending)).
    
sorter_descending(Left, Right) ->
  LSec = creation_unix_time(Left),
  RSec = creation_unix_time(Right),
  LSec >= RSec.

creation_unix_time(Self) ->
  calendar:datetime_to_gregorian_seconds(create_date(Self)).

%%%
%%% Setters
%%%
set_title(Title, Self) ->
  New = Self#db_snapshot{title=Title},
  db_object:update_id(New).

set_state(State, Self) ->
  New = Self#db_snapshot{state = State},
  db_object:update_id(New).
