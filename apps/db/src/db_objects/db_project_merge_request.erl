-module(db_project_merge_request).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, merge_request_id/1]).

-type id() :: key().

-record(db_project_merge_request, 
  {
    id :: id(),
    project_id :: db_project:id(),
    merge_request_id :: db_merge_request:id()
  }).
-opaque self() :: #db_project_merge_request{}.

id(#db_project_merge_request{id=Id}) -> Id.

identity(#db_project_merge_request{project_id=ProjId, merge_request_id=MrId}) ->
  Fields = {ProjId, MrId},
  db_object:hash_fields(Fields).

new({ProjId, MrId}) ->
  Self = #db_project_merge_request{project_id=ProjId, merge_request_id=MrId},
  db_object:update_id(Self).

record_info() -> record_info(fields, db_project_merge_request).

is_self(#db_project_merge_request{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(#db_project_merge_request{project_id=ProjId}) -> ProjId.
merge_request_id(#db_project_merge_request{merge_request_id=MrId}) -> MrId.

