-module(db_member).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([username/1, name/1, web_url/1, project_id/1]).
%%% export convenience
-export([new_fake/1]).

-type id() :: key().

-record(db_member, 
  {
    id :: id(),
    username :: binary(),
    name :: binary(),
    web_url :: binary(),
    project_id :: db_project:id()
  }).
-type self() :: #db_member{}.

id(#db_member{id=Id}) -> Id.

identity(#db_member{username=Username, project_id=ProjId}) ->
  Fields = {Username, ProjId},
  db_object:hash_fields(Fields).

new({Username, Name, Url, ProjId}) ->
  Self = #db_member{
    username = Username,
    name = Name,
    web_url = Url,
    project_id = ProjId
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_member).

is_self(#db_member{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
username(#db_member{username=Username}) -> Username.
name(#db_member{name=Name}) -> Name.
web_url(#db_member{web_url=Url}) -> Url.
project_id(#db_member{project_id=ProjId}) -> ProjId.

%%%
%%% Convenience
%%%
new_fake(ProjId) -> 
  Self = #db_member{
    username = <<"Error User">>,
    name = <<"Not found on GitLab">>,
    web_url = <<"www.http.cat/404">>,
    project_id = ProjId
  },
  db_object:update_id(Self).


