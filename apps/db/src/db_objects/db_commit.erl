-module(db_commit).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, title/1, message/1, sha/1, parent_shas/1,
         author_id/1, create_date/1]).
%%% export setters
-export([set_message/2]).
%%% export convenience
-export([new_fake/2]).
-type id() :: key().
-record(db_commit,
  {
    id :: id(), % fake commits use an integer id
    project_id :: db_project:id(),
    title :: binary(),
    message :: binary(),
    sha :: binary(),
    parent_shas :: [binary()], %% TODO: should this be a list of DB IDs, or a list of SHAs?
    author_id :: db_author:id(), 
    create_date :: calendar:datetime()
  }).
-opaque self() :: #db_commit{}. 

id(#db_commit{id=Id}) -> Id.

identity(#db_commit{sha=Sha, project_id=ProjId}) -> 
  Fields = {Sha, ProjId},
  db_object:hash_fields(Fields).

new({ProjId, Title, Msg, Sha, ParentShas, AuthorId, Date}) ->
  Self = #db_commit{
    project_id = ProjId,
    title = Title,
    message = Msg,
    sha = Sha,
    parent_shas = ParentShas,
    author_id = AuthorId,
    create_date = Date
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_commit).

is_self(#db_commit{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(#db_commit{project_id=ProjId}) -> ProjId.
title(#db_commit{title=Title}) -> Title.
message(#db_commit{message=Msg}) -> Msg.
sha(#db_commit{sha=Sha}) -> Sha.
parent_shas(#db_commit{parent_shas=PShas}) -> PShas.
author_id(#db_commit{author_id=AuthorId}) -> AuthorId.
create_date(#db_commit{create_date=Date}) -> Date.

%%% Convenience
new_fake(ProjId, Sha) ->
  #db_commit{
    id = db:gen_unique_id(commit),
    project_id = ProjId,
    title = <<"Error Loading Commit">>,
    message = <<"Commit not found on master.">>,
    sha = Sha,
    parent_shas = [],
    create_date = {{1970, 1, 1}, {0, 0, 0}}
  }.

set_message(Msg, Self) ->
  Self#db_commit{message=Msg}.
