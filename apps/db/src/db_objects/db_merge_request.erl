-module(db_merge_request).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export types
-export_type([self/0, id/0, state/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export getters
-export([project_id/1, id_at_source/1, iid_at_source/1, title/1, message/1,
 creator_id/1, assignee_id/1, state/1, target_branch/1, source_branch/1,
 sha/1, merge_sha/1, merge_date/1, web_url/1
]).
%%% export setters
-export([set_merge_date/2]).
%%% type definition
-type id() :: key().
-type state() :: opened | closed | merged | binary().
%%% struct definition
-record(db_merge_request, 
  {
    id :: id(),
    project_id :: db_project:id(),
    id_at_source :: binary(),
    iid_at_source :: binary(),
    title :: binary(),
    message :: binary(),
    creator_id :: key(), % TODO: change to db_member:id()
    assignee_id :: list_utils:one_or_none(key()), % TODO: member:id()
    state :: state(),
    target_branch :: binary(),
    source_branch :: binary(),
    sha :: binary(),
    merge_sha :: binary(),
    merge_date :: calendar:datetime() | undefined,
    web_url :: binary()
  }).
-opaque self() :: #db_merge_request{}.

%%%
%%% db_object callbacks
%%%
id(Self) -> Self#db_merge_request.id.

identity(Self) ->
  ProjId = project_id(Self),
  SourceId = id_at_source(Self),
  Fields = {ProjId, SourceId},
  db_object:hash_fields(Fields).

new({IdAtSrc, IidAtSrc, ProjId, Title, Msg, CreatorId, AssId, State, 
     TargetB, SourceB, Sha, MergeSha, MergeDate, Url}) ->
  Self = #db_merge_request{
    project_id = ProjId,
    id_at_source = IdAtSrc,
    iid_at_source = IidAtSrc,
    title = Title,
    message = Msg,
    creator_id = CreatorId,
    assignee_id = AssId,
    state = State,
    target_branch = TargetB,
    source_branch = SourceB,
    sha = Sha,
    merge_sha = MergeSha,
    merge_date = MergeDate,
    web_url = Url
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_merge_request).

is_self(#db_merge_request{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%% Accessors
project_id(Self) -> Self#db_merge_request.project_id.
id_at_source(Self) -> Self#db_merge_request.id_at_source.
iid_at_source(Self) -> Self#db_merge_request.iid_at_source.
title(Self) -> Self#db_merge_request.title.
message(Self) -> Self#db_merge_request.message.
creator_id(Self) -> Self#db_merge_request.creator_id.
assignee_id(Self) -> Self#db_merge_request.assignee_id.
state(Self) -> Self#db_merge_request.state.
target_branch(Self) -> Self#db_merge_request.target_branch.
source_branch(Self) -> Self#db_merge_request.source_branch.
sha(Self) -> Self#db_merge_request.sha.
merge_sha(Self) -> Self#db_merge_request.merge_sha.
merge_date(Self) -> Self#db_merge_request.merge_date.
web_url(Self) -> Self#db_merge_request.web_url.

%%% Setters
set_merge_date(Date, Self) ->
  New = Self#db_merge_request{merge_date=Date},
  db_object:update_id(New).
