-module(db_bl_project).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0, id_at_source/0, sp_id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% getters
-export([source/1, id_at_source/1, sp_id/1]).

-type id() :: key().
-type id_at_source() :: binary().
-type sp_id() :: {source(), binary()}.
-record(db_bl_project, 
  {
    id = undefined :: id(),
    source :: source(),
    id_at_source :: binary()
  }).
-opaque self() :: #db_bl_project{}.

%%% 
%%% Required Callbacks
%%%
id(Self) -> Self#db_bl_project.id.

identity(Self) ->
  Fields = {Self#db_bl_project.source, Self#db_bl_project.id_at_source},
  db_object:hash_fields(Fields).

new({Source, SourceId}) ->
  Self = #db_bl_project{
    source = Source,
    id_at_source = SourceId
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_bl_project).

is_self(#db_bl_project{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
source(Self) -> Self#db_bl_project.source.
id_at_source(Self) -> Self#db_bl_project.id_at_source.
sp_id(Self) -> {source(Self), id_at_source(Self)}.

