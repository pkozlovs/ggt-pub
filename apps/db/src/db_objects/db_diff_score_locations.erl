-module(db_diff_score_locations).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([diff_id/1, whitespace/1, syntax/1, moves/1, refactors/1, 
         insertions/1, deletions/1]).
%%% export convenience
-export([new_fake/1]).
-export([whitespace_count/1, syntax_count/1, moves_count/1, refactors_count/1,
         insertions_count/1, deletions_count/1]).

-type id() :: key().

-type location() :: non_neg_integer().
-type location_pair() :: {location(), location()}.

-type whitespace_location() :: location() | location_pair().
-type syntax_location() :: location() | location_pair().
-type move_location() :: location_pair().
-type refactor_location() :: location_pair().
-type insertion_location() :: location().
-type deletion_location() :: location().

-record(db_diff_score_locations, 
  {
    id :: id(),
    diff_id :: db_commit_diff:id() | db_merge_request_diff:id(),
    whitespace = []:: [whitespace_location()],
    syntax = []:: [syntax_location()],
    moves = [] :: [move_location()],
    refactors = []:: [refactor_location()],
    insertions = []:: [insertion_location()],
    deletions = [] :: [deletion_location()]
  }).

-opaque self() :: #db_diff_score_locations{}.

%%%
%%% behaviour calls
%%%
id(#db_diff_score_locations{id=Id}) -> Id.

identity(#db_diff_score_locations{diff_id=DiffId}) -> 
  db_object:hash_fields(DiffId).

new({DiffId, Whites, Syntax, Moves, Refactors, Insertions, Deletions}) ->
  Self = #db_diff_score_locations{
    diff_id = DiffId,
    whitespace = Whites,
    syntax = Syntax,
    moves = Moves,
    refactors = Refactors, 
    insertions = Insertions,
    deletions = Deletions
  },
  db_object:update_id(Self);
new({DiffId, {Whites, Syntax, Moves, Refactors, Insertions, Deletions}}) ->
  new({DiffId, Whites, Syntax, Moves, Refactors, Insertions, Deletions}).

record_info() -> record_info(fields, db_diff_score_locations).

% add these two functions to db_object; they're helpful. 
is_self(#db_diff_score_locations{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
diff_id(#db_diff_score_locations{diff_id=DiffId}) -> DiffId.
whitespace(#db_diff_score_locations{whitespace=Locs}) -> Locs.
syntax(#db_diff_score_locations{syntax=Locs}) -> Locs.
moves(#db_diff_score_locations{moves=Locs}) -> Locs.
refactors(#db_diff_score_locations{refactors=Locs}) -> Locs.
insertions(#db_diff_score_locations{insertions=Locs}) -> Locs.
deletions(#db_diff_score_locations{deletions=Locs}) -> Locs.

%%%
%%% Convenience
%%%
new_fake(DiffId) -> #db_diff_score_locations{diff_id = DiffId}.

whitespace_count(Self) -> length(whitespace(Self)).
syntax_count(Self) -> length(syntax(Self)).
moves_count(Self) -> length(moves(Self)).
refactors_count(Self) -> length(refactors(Self)).
insertions_count(Self) -> length(insertions(Self)).
deletions_count(Self) -> length(deletions(Self)).
