-module(db_commit_diff).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([commit_id/1, filename/1, file_extension/1, insertions/1, deletions/1,
         net/1, diff/1]).
-export([set_diff/2]).

-type id() :: key().
-record(db_commit_diff,
  {
    id :: id(),
    commit_id :: db_commit:id(),
    filename :: binary(),
    file_extension :: binary(),
    insertions :: non_neg_integer(),
    deletions :: non_neg_integer(),
    net :: integer(),
    diff = <<>> :: binary()
  }).
-opaque self() :: #db_commit_diff{}.

id(#db_commit_diff{id=Id}) -> Id.

identity(#db_commit_diff{commit_id=CommitId, filename=Name}) ->
  Fields = {CommitId, Name},
  db_object:hash_fields(Fields).

new({CommitId, Filename, Ins, Dels, Net, Diff}) ->
  Self = #db_commit_diff{
    commit_id = CommitId,
    filename = Filename,
    file_extension = filename:extension(Filename),
    insertions = Ins,
    deletions = Dels,
    net = Net,
    diff = Diff
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_commit_diff).

is_self(#db_commit_diff{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
commit_id(#db_commit_diff{commit_id=CommitId}) -> CommitId.
filename(#db_commit_diff{filename=Filename}) -> Filename.
file_extension(#db_commit_diff{file_extension=Ext}) -> Ext.
insertions(#db_commit_diff{insertions=Ins}) -> Ins.
deletions(#db_commit_diff{deletions=Dels}) -> Dels.
net(#db_commit_diff{net=Net}) -> Net.
diff(#db_commit_diff{diff=Diff}) -> Diff.


%%%
%%% Setters
%%%
set_diff(Diff, Self) -> Self#db_commit_diff{diff = Diff}.
