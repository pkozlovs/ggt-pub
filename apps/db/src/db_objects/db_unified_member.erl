-module(db_unified_member).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, member_id/1, author_id/1]).

-type id() :: key().

-record(db_unified_member,
  {
    id :: id(),
    project_id :: db_project:id(),
    member_id :: db_member:id(),
    author_id :: db_author:id()
  }).
-type self() :: #db_unified_member{}.

id(#db_unified_member{id=Id}) -> Id.

identity(User=#db_unified_member{}) ->
  ProjId = project_id(User),
  MemberId = member_id(User),
  AuthorId = author_id(User),
  Fields = {ProjId, MemberId, AuthorId},
  db_object:hash_fields(Fields).

new({ProjId, MemberId, AuthorId}) ->
  Self = #db_unified_member{
    project_id = ProjId, member_id = MemberId, author_id = AuthorId
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_unified_member).

is_self(#db_unified_member{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(#db_unified_member{project_id=ProjId}) -> ProjId.
member_id(#db_unified_member{member_id=MemberId}) -> MemberId.
author_id(#db_unified_member{author_id=AuthorId}) -> AuthorId.

