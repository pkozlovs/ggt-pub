-module(db_commit_stats).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([commit_id/1, insertions/1, deletions/1, net/1]).
%%% export convenience
-export([counts/1]).
-type id() :: key().
-record(db_commit_stats,
  {
    id :: id(),
    commit_id :: db_commit:id(),
    insertions :: non_neg_integer(),
    deletions :: non_neg_integer(),
    net :: non_neg_integer()
  }).
-type self() :: #db_commit_stats{}.

id(#db_commit_stats{id=Id}) -> Id.

identity(#db_commit_stats{commit_id=CommitId}) ->
  db_object:hash_fields(CommitId).

new({CommitId, Ins, Dels, Net}) ->
  Self = #db_commit_stats{
    commit_id = CommitId,
    insertions = Ins,
    deletions = Dels,
    net = Net
  },
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_commit_stats).

is_self(#db_commit_stats{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
commit_id(#db_commit_stats{commit_id=CommitId}) -> CommitId.
insertions(#db_commit_stats{insertions=Ins}) -> Ins.
deletions(#db_commit_stats{deletions=Dels}) -> Dels.
net(#db_commit_stats{net=Net}) -> Net.

%%%
%%% Convenience
%%%
counts(Self) ->
  Ins = insertions(Self),
  Dels = deletions(Self),
  Net = net(Self),
  {Ins, Dels, Net}.
