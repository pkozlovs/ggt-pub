-module(db_issue).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, id_at_source/1, title/1, message/1, state/1,
         create_date/1, creator_id/1, assignee_id/1, web_url/1]).

-type id() :: key().

-record(db_issue,
  {
    id :: id(),
    project_id :: db_project:id(),
    id_at_source :: binary(),
    title :: binary(),
    message :: binary(),
    state :: binary(),
    create_date :: calendar:datetime(),
    creator_id :: db_member:id(),
    assignee_id :: db_member:id() | none,
    web_url :: binary()
  }).
-type self() :: #db_issue{}.

id(#db_issue{id=Id}) -> Id.

identity(Self=#db_issue{}) ->
  ProjId = project_id(Self),
  Date = create_date(Self),
  Author = creator_id(Self),
  Fields = {ProjId, Date, Author},
  db_object:hash_fields(Fields).

new({ProjId, SourceId, Title, Msg, State, Date, CreatorId, AssId, Url}) ->
  Self = #db_issue{
    project_id = ProjId,
    id_at_source = SourceId,
    title = Title,
    message = Msg,
    state = State,
    create_date = Date,
    creator_id = CreatorId,
    assignee_id = AssId,
    web_url = Url
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_issue).

is_self(#db_issue{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(#db_issue{project_id=Id}) -> Id.
id_at_source(#db_issue{id_at_source=Id}) -> Id.
title(#db_issue{title=Title}) -> Title.
message(#db_issue{message=Msg}) -> Msg.
state(#db_issue{state=State}) -> State.
create_date(#db_issue{create_date=Date}) -> Date.
creator_id(#db_issue{creator_id=Id}) -> Id.
assignee_id(#db_issue{assignee_id=Id}) -> Id.
web_url(#db_issue{web_url=Url}) -> Url.
