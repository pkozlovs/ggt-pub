-module(db_diff_scores).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0]).
%%% export init
-export([new/1, new/0, new_ones/0, new_bad/0, default_multipliers/0]).
%%% export accessors
-export([whitespace/1, syntax/1, moves/1, refactors/1, insertions/1,
         deletions/1]).
%%% export convenience
-export([all/1, multiply/2, add/2, is_equal/2, flatten/1]).

-record(db_diff_scores, 
  {
    whitespace  = 0 :: non_neg_integer() | float(),
    syntax      = 0 :: non_neg_integer() | float(),
    moves       = 0 :: non_neg_integer() | float(),
    refactors   = 0 :: non_neg_integer() | float(),
    insertions  = 0 :: non_neg_integer() | float(),
    deletions   = 0 :: non_neg_integer() | float()
  }).
-type self() :: #db_diff_scores{}.

new({White, Syntax, Moves, Refs, Ins, Dels}) ->
  #db_diff_scores{
    whitespace = White,
    syntax = Syntax,
    moves = Moves,
    refactors = Refs,
    insertions = Ins,
    deletions = Dels
  }.

new() -> #db_diff_scores{}.

new_bad() -> new({nan, nan, nan, nan, nan, nan}).

new_ones() -> new({1, 1, 1, 1, 1, 1}).

default_multipliers() -> new({0.0, 0.0, 0.25, 0.75, 1.0, 0.1}).

%%%
%%% Accessors
%%%
whitespace(#db_diff_scores{whitespace=W}) -> W.
syntax(#db_diff_scores{syntax=S}) -> S.
moves(#db_diff_scores{moves=M}) -> M.
refactors(#db_diff_scores{refactors=R}) -> R.
insertions(#db_diff_scores{insertions=I}) -> I.
deletions(#db_diff_scores{deletions=D}) -> D.

%%%
%%% Convenience
%%%
all(Self) ->
  White = whitespace(Self),
  Syntax = syntax(Self),
  Moves = moves(Self),
  Refs = refactors(Self),
  Ins = insertions(Self),
  Dels = deletions(Self),
  {White, Syntax, Moves, Refs, Ins, Dels}.

-spec multiply(self(), self()) -> self().
multiply(L, R) ->
  #db_diff_scores{
    whitespace = L#db_diff_scores.whitespace * R#db_diff_scores.whitespace,
    syntax = L#db_diff_scores.syntax * R#db_diff_scores.syntax,
    moves = L#db_diff_scores.moves * R#db_diff_scores.moves,
    refactors = L#db_diff_scores.refactors * R#db_diff_scores.refactors,
    insertions = L#db_diff_scores.insertions * R#db_diff_scores.insertions,
    deletions = L#db_diff_scores.deletions * R#db_diff_scores.deletions
  }.

-spec add(self(), self()) -> self().
add(L, R) ->
  #db_diff_scores{
    whitespace = L#db_diff_scores.whitespace + R#db_diff_scores.whitespace,
    syntax = L#db_diff_scores.syntax + R#db_diff_scores.syntax,
    moves = L#db_diff_scores.moves + R#db_diff_scores.moves,
    refactors = L#db_diff_scores.refactors + R#db_diff_scores.refactors,
    insertions = L#db_diff_scores.insertions + R#db_diff_scores.insertions,
    deletions = L#db_diff_scores.deletions + R#db_diff_scores.deletions
  }.

flatten(Self) ->
  whitespace(Self) 
  + syntax(Self)
  + moves(Self)
  + refactors(Self)
  + insertions(Self)
  + deletions(Self).

is_equal(L, R) ->
  L == R.
