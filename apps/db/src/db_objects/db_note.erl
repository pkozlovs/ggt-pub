-module(db_note).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([id_at_source/1, creator_id/1, merge_request_id/1, create_date/1,
         body/1, type/1]).

-type id() :: key().

-record(db_note,
  {
    id :: id(),
    id_at_source :: binary(),
    creator_id :: db_member:id(),
    merge_request_id :: db_merge_request:id(),
    create_date :: calendar:datetime(),
    body :: binary(),
    type :: note_type()
  }).
-opaque self() :: #db_note{}.

id(#db_note{id=Id}) -> Id.

identity(Note=#db_note{}) ->
  MrId = merge_request_id(Note),
  MemberId = creator_id(Note),
  Date = create_date(Note),
  Fields = {MrId, MemberId, Date},
  db_object:hash_fields(Fields).

new({MemberId, MrId, Date, Body, Type, SourceId}) ->
  Self = #db_note{
    id_at_source = SourceId,
    creator_id = MemberId,
    merge_request_id = MrId,
    create_date = Date,
    body = Body,
    type = Type
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_note).

is_self(#db_note{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
id_at_source(#db_note{id_at_source=Id}) -> Id.
creator_id(#db_note{creator_id=CreatorId}) -> CreatorId.
merge_request_id(#db_note{merge_request_id=MrId}) -> MrId.
create_date(#db_note{create_date=Date}) -> Date.
body(#db_note{body=Body}) -> Body.
type(#db_note{type=Type}) -> Type.

