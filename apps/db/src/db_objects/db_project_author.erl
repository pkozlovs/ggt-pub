-module(db_project_author).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, author_id/1]).
%%% export convenience

-type id() :: key().

-record(db_project_author, 
  {
    id :: id(),
    project_id :: db_project:id(),
    author_id :: db_author:id()
  }).
-type self() :: #db_project_author{}.

id(#db_project_author{id=Id}) -> Id.

identity(#db_project_author{project_id=ProjId, author_id=AuthorId}) ->
  Fields = {ProjId, AuthorId},
  db_object:hash_fields(Fields).

new({ProjId, AuthorId}) ->
  Self = #db_project_author{project_id = ProjId, author_id = AuthorId},
  db_object:update_id(Self).

record_info() -> record_info(fields, db_project_author).

is_self(#db_project_author{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
project_id(#db_project_author{project_id=ProjId}) -> ProjId.
author_id(#db_project_author{author_id=AuthorId}) -> AuthorId.
