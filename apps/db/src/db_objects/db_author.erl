-module(db_author).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([name/1, email/1]).
%%% export convenience
-export([new_fake/0]).
%%%
%%% db_object callbacks
%%%
-type id() :: key().
-record(db_author,
  {
    id :: id(),
    name :: binary(),
    email :: binary()
  }).
-opaque self() :: #db_author{}.

id(#db_author{id=Id}) -> Id.

identity(#db_author{email=Email}) -> 
  db_object:hash_fields(Email).

new({Name, Email}) ->
  Self = #db_author{name = Name, email = Email},
  db_object:update_id(Self).

record_info() ->
  record_info(fields, db_author).

is_self(#db_author{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
name(#db_author{name=Name}) -> Name.
email(#db_author{email=Email}) -> Email.

%%%
%%% Convenience
%%%
new_fake() ->
  #db_author{
    id = <<0>>,
    name = <<"Unknown User">>,
    email = <<"error@commit">>
  }.  
