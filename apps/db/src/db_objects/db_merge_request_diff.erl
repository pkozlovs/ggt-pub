-module(db_merge_request_diff).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([merge_request_id/1, filename/1, file_extension/1, diff/1]).
%%% export convenience

%%% export setters
-export([set_diff/2]).

-type id() :: key().

-record(db_merge_request_diff,
  {
    id :: id(),
    merge_request_id :: db_merge_request:id(),
    filename = <<>> :: binary(),
    file_extension = <<>> :: binary(),
    diff = <<>> :: binary()
  }).
-type self() :: #db_merge_request_diff{}.

id(#db_merge_request_diff{id=Id}) -> Id.

identity(#db_merge_request_diff{merge_request_id=MrId, filename=Filename}) ->
  Fields = {MrId, Filename},
  db_object:hash_fields(Fields).

new({MrId, Filename, Diff}) ->
  Self = #db_merge_request_diff{
    merge_request_id = MrId,
    filename = Filename,
    file_extension = filename:extension(Filename),
    diff = Diff
  },
  db_object:update_id(Self).

record_info() -> record_info(fields, db_merge_request_diff).

is_self(#db_merge_request_diff{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
merge_request_id(#db_merge_request_diff{merge_request_id=Id}) -> Id.
filename(#db_merge_request_diff{filename=Filename}) -> Filename.
file_extension(#db_merge_request_diff{file_extension=Ext}) -> Ext.
diff(#db_merge_request_diff{diff=Diff}) -> Diff.

%%%
%%% Setters
%%%
set_diff(Diff, Self) -> Self#db_merge_request_diff{diff = Diff}.
