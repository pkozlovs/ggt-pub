-module(db_commit_diff_scores).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([commit_id/1, commit_diff_id/1, file_extension/1, ignored/1, 
         ignore_reason/1, scores/1, cumulative_score/2, cumulative_counts/1]).
%%% export convenience
-type id() :: key().

-record(db_commit_diff_scores,
  {
    id :: id(),
    commit_id :: db_commit:id(),
    commit_diff_id :: db_commit_diff:id(),
    file_extension = <<>> :: binary(),
    ignored = false :: boolean(),
    ignore_reason = undefined :: undefined 
      | file_extension
      | too_many_insertions 
      | too_many_deletions,
    scores = db_diff_scores:new() :: db_diff_scores:self()
  }).
-opaque self() :: #db_commit_diff_scores{}.

id(#db_commit_diff_scores{id=Id}) -> Id.

identity(#db_commit_diff_scores{commit_id=CommitId, commit_diff_id=DiffId}) ->
  Fields = {CommitId, DiffId},
  db_object:hash_fields(Fields).

new({CommitId, FileDiffId, {ignored, Reason}}) ->
  Self = #db_commit_diff_scores{
    commit_id = CommitId,
    commit_diff_id = FileDiffId,
    ignored = true,
    ignore_reason = Reason
  },
  db_object:update_id(Self);
new({CommitId, DiffId, Ext, White, Syntax, Moves, Refs, Ins, Dels}) ->
  Scores = db_diff_scores:new({White, Syntax, Moves, Refs, Ins, Dels}),
  Self = #db_commit_diff_scores{
    commit_id = CommitId,
    commit_diff_id = DiffId,
    file_extension = Ext,
    scores = Scores
  },
  db_object:update_id(Self).
  
record_info() -> record_info(fields, db_commit_diff_scores).

is_self(#db_commit_diff_scores{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%
commit_id(#db_commit_diff_scores{commit_id=Id}) -> Id.
commit_diff_id(#db_commit_diff_scores{commit_diff_id=Id}) -> Id.
file_extension(#db_commit_diff_scores{file_extension=Ext}) -> Ext.
ignored(#db_commit_diff_scores{ignored=Bool}) -> Bool.
ignore_reason(#db_commit_diff_scores{ignore_reason=Reason}) -> Reason.
scores(#db_commit_diff_scores{scores=Scores}) -> Scores.

%%%
%%% Convenience
%%%
-spec cumulative_score([self()], [db_score_multipliers:self()]) 
  -> integer().
cumulative_score(SelfList, Multipliers) ->
  MultMap = db_score_multipliers:file_extension_map(Multipliers),
  % pair up scores and their multipliers
  ScoreMultiplierPairs = lists:map(fun(Score) ->
    Ext = db_commit_diff_scores:file_extension(Score),
    Numbers = db_commit_diff_scores:scores(Score),
    {Numbers, maps:get(Ext, MultMap, db_diff_scores:default_multipliers())}
  end, SelfList),
  % fold over them, multipling pairs and summing
  lists:foldl(fun({Score, Mult}, Acc) ->
    Multiplied = db_diff_scores:multiply(Score, Mult),
    db_diff_scores:add(Acc, Multiplied)
  end, db_diff_scores:new(), ScoreMultiplierPairs).

cumulative_counts(SelfList) ->
  Scores = [scores(Self) || Self <- SelfList],
  lists:foldl(fun db_diff_scores:add/2, db_diff_scores:new(), Scores).
