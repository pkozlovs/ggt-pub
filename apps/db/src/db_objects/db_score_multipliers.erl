-module(db_score_multipliers).
-behaviour(db_object).
-include_lib("apps/db/include/new_db.hrl").
%%% export type
-export_type([self/0, id/0]).
%%% export db_object callbacks
-export([id/1, identity/1, new/1, record_info/0, is_self/1, assert_self/1]).
%%% export accessors
-export([project_id/1, file_extension/1, multipliers/1]).
%%% export convenience
-export([new/0, file_extension_map/1]).

-type id() :: key(). 

-record(db_score_multipliers,
  {
    id :: id(),
    project_id :: db_project:id(),
    file_extension = <<>> :: binary(),
    multipliers = db_diff_scores:default_multipliers() 
      :: db_diff_scores:self()
  }).
-type self() :: #db_score_multipliers{}.

id(#db_score_multipliers{id=Id}) -> Id.

identity(#db_score_multipliers{id=Id, file_extension=Ext}) ->
  Fields = {Id, Ext},
  db_object:hash_fields(Fields).

new({ProjId, Ext}) ->
  Self = #db_score_multipliers{
    project_id = ProjId,
    file_extension = Ext
  },
  db_object:update_id(Self).

new() -> #db_score_multipliers{}.

record_info() -> record_info(fields, db_score_multipliers).

is_self(#db_score_multipliers{}) -> true;
is_self(_) -> false.

assert_self(Self) -> db_object:assert_self(?MODULE, Self).

%%%
%%% Accessors
%%%

project_id(#db_score_multipliers{project_id=Id}) -> Id.
file_extension(#db_score_multipliers{file_extension=Ext}) -> Ext.
multipliers(#db_score_multipliers{multipliers=Scores}) -> Scores.

%%%
%%% Convenience
%%%
file_extension_map(SelfList) ->
  maps:from_list(lists:map(fun file_extension_pair/1, SelfList)).

file_extension_pair(Self) -> {file_extension(Self), Self}.
