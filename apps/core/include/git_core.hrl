-ifndef(GIT_CORE_HRL).
-define(GIT_CORE_HRL, true).

-ifndef(DIR_ROOT).
-define(DIR_ROOT, filename:join([
  env:home_dir(),
  env:config(git_core_app, dir_root, ".ggs")
])).
-endif.

-ifndef(DIR_PROJ).
-define(DIR_PROJ, filename:join([
  ?DIR_ROOT,
  env:config(git_core_app, dir_proj, "proj")
])).
-endif.

-ifndef(DIR_SSH_SRC).
-define(DIR_SSH_SRC, env:config(git_core_app, dir_ssh_src, "./ssh")).
-endif.

-ifndef(DIR_SSH_TGT).
-define(DIR_SSH_TGT, filename:join([
  ?DIR_ROOT,
  env:config(git_core_app, dir_ssh_tgt, "ssh")
])).
-endif.

-ifndef(FILE_SSH_PRV).
-define(FILE_SSH_PRV, env:config(git_core_app, file_ssh_prv, "id_rsa")).
-endif.

-ifndef(FILE_SSH_PUB).
-define(FILE_SSH_PUB, env:config(git_core_app, file_ssh_pub, "id_rsa.pub")).
-endif.

-endif.