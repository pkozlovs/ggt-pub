%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Git access API for GGS.
%%% @end
%%%-------------------------------------------------------------------
-module(ssh_agent).
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([init/0, start/0, add_key/2]).
-export([save_handle/2, get_handle/1]).
%%--------------------------------------------------------------------
%% Macro definitions
%%--------------------------------------------------------------------
-define(ETS_TAB, ssh_agent_handles).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Initialize the ets table to store socket references
%% @spec init() -> ok
%% @end
%%--------------------------------------------------------------------
-spec init() -> ok.
init() ->
  ?ETS_TAB = ets:new(?ETS_TAB,
    [named_table, public, {write_concurrency, true}]
  ), ok.

%%--------------------------------------------------------------------
%% @doc Start the ssh agent
%% @spec start() -> {ok, Handle}
%% @end
%%--------------------------------------------------------------------
-spec start() -> {ok, string()}.
start() ->
  Out = os:cmd("eval 'ssh-agent -s'"),
  {ok, auth_sock(Out)}.

%%--------------------------------------------------------------------
%% @doc Add ssh key to the current agent session
%% @spec add_key(Handle, Key) -> ok | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec add_key(string(), string()) -> ok | {error, any()}.
add_key(Handle, Key) ->
  Response = os:cmd([Handle, " ssh-add ", Key]),
  verify_add_key(Response).

%%--------------------------------------------------------------------
%% @doc Save the handle into ets table
%% @spec save_handle(Key, Handle) -> true
%% @end
%%--------------------------------------------------------------------
-spec save_handle(atom(), string()) -> true.
save_handle(Key, Handle) ->
  ets:insert(?ETS_TAB, {Key, Handle}).

%%--------------------------------------------------------------------
%% @doc Get a list of matching by key handles 
%% @spec get_handle(Key) -> [{key, Handle}]
%% @end
%%--------------------------------------------------------------------
-spec get_handle(atom()) -> [tuple()].
get_handle(Key) ->
  ets:lookup(?ETS_TAB, Key).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Get handle from ssh-agent return string
%%--------------------------------------------------------------------
auth_sock(Out) ->
  [AuthLockEnv|_] = string:tokens(Out, ";"),
  AuthLockEnv.

%%--------------------------------------------------------------------
%% Output verification
%%--------------------------------------------------------------------
%% Can't optimize this, this translates to exactly the same thing you'd
%%  wanna do:
%%    [$a, $b, $c, ... | _]
%%    [$a|[$b|[$c|[$d|_]]]]
%% Same!
verify_add_key("Identity added:" ++ _) ->
  ok;
verify_add_key(Other) ->
  {error, Other}.
