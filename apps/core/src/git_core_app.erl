%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc App for git-core
%%% @end
%%%-------------------------------------------------------------------
-module(git_core_app).
%%--------------------------------------------------------------------
%% Behaviour inheritance
%%--------------------------------------------------------------------
-behaviour(supervisor).
-behaviour(application).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/core/include/git_core.hrl").
%%--------------------------------------------------------------------
%% Internal exports
%%--------------------------------------------------------------------
-export([init/1, start/2, stop/1]).

%%====================================================================
%% Internal functions
%%====================================================================
start(_, _) -> supervisor:start_link({local, ?MODULE}, ?MODULE, []).
stop(_) -> ok.
init([]) -> load_modules(), deploy(), sup().

%%====================================================================
%% Private functions
%%====================================================================
sup() -> {ok, {{one_for_one, 1, 1}, []}}.
load_modules() ->
  [code:ensure_loaded(M) || M <- [
    git_core
  ]].
deploy() ->
  {ok, DirReport} = init_directories(),
  error_logger:info_msg(
      "application: ~p~n    dir state: ~p~n~n",
    [?MODULE, DirReport]
  ).


init_directories() ->
  RootExists = filelib:is_dir(?DIR_ROOT),
  Result = process_root(RootExists),
  protect_ssh_files(),
  Result.

process_root(true) ->
  ProjDirStatus = case filelib:is_dir(?DIR_PROJ) of
    true -> ok;
    false -> file:make_dir(?DIR_PROJ), new
  end,
  SshDirStatus = case filelib:is_dir(?DIR_SSH_TGT) of
    true -> ok;
    false -> file:make_dir(?DIR_SSH_TGT), new
  end,
  {ok, {exists, {proj_dir, ProjDirStatus}, {ssh_dir, SshDirStatus}}};
process_root(false) ->
  create_directories(),
  {ok, new}.

create_directories() ->
  file:make_dir(?DIR_ROOT),
  file:make_dir(?DIR_PROJ),
  file:make_dir(?DIR_SSH_TGT).

protect_ssh_files() ->
  env:chmod(env:join([?DIR_SSH_TGT, ?FILE_SSH_PRV]), 8#00400),
  env:chmod(env:join([?DIR_SSH_TGT, ?FILE_SSH_PUB]), 8#00400).
