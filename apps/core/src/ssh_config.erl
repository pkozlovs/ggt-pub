%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Git access API for GGS.
%%% @end
%%%-------------------------------------------------------------------
-module(ssh_config).
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([generate_hosts_file/0]).
%%--------------------------------------------------------------------
%% Includes
%%--------------------------------------------------------------------
-include_lib("git_core.hrl").

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Creates and writes a custome ~/.ssh/config file, WARNING: overwrites
%% @spec generate_hosts_file() -> {ok, {FileName, Contents}} | 
%%                                         {error, {FileName, Content, Reason}}
%% @end
%%--------------------------------------------------------------------
-spec generate_hosts_file() -> {ok, tuple()} | {error, any()}.
generate_hosts_file() ->
  SshKey = env:join([?DIR_SSH_TGT, ?FILE_SSH_PRV]),
  Contents = host_entry("cs-gitlab-db.cs.surrey.sfu.ca", SshKey),
  MaybeWritten = file:write_file(filename:join(
    [env:home_dir(), ".ssh/config"]
  ), Contents),
  verify_generated(MaybeWritten, "./ssh/config", Contents).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Host template
%%--------------------------------------------------------------------
host_entry(HostName, SshKey) ->
  [
    "Host ", HostName, "\n",
    "\tHostName ", HostName, "\n",
    "\tStrictHostKeyChecking no\n",
    "\tUserKnownHostsFile /dev/null\n",
    "\tPreferredAuthentications publickey\n",
    "\tIdentityFile ", SshKey, "\n"
  ].

%%--------------------------------------------------------------------
%% Error checking
%%--------------------------------------------------------------------
verify_generated(ok, FileName, Contents) ->
  {ok, {FileName, Contents}};
verify_generated({error, Reason}, FileName, Content) ->
  {error, {FileName, Content, Reason}}.
