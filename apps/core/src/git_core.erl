%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Git access API for GGS.
%%% @end
%%%-------------------------------------------------------------------
-module(git_core).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/core/include/git_core.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([clone_project/4]).
-export([delete_project/1]).
-export([commits/3, commit/2, commit_files/2]).
-export([authors/1, authors/3]).
-export([first_merge_commit/2]).
-compile(export_all).

-define(DIR_KEY, {git_core, exec_dir}).
-define(DEBUG, false).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Create directory ProjectId if it doesn't exist and clone, otherwise 
%% pull.
%% @spec clone_project(ProjectId, URL, Username, Token) ->
%%  {ok, {Type, Out}} | {error, {Type, Out}}.
%% @end
%%--------------------------------------------------------------------
-spec clone_project(binary(), binary(), binary(), binary()) ->
  {ok, {clone|pull, binary()}} | {error, {clone|pull, binary()}}.
clone_project(ProjectId, URL, Username, Token) ->
  ProjectId_hex = hash:bin_to_hex(ProjectId),

  {Protocol, StrippedURL} = strip_url(URL),
  NewURL = io_lib:format(
    "~s://~s:~s@~s",
    [Protocol, Username, Token, StrippedURL]
  ),
  case project_exists(ProjectId_hex) of
    false ->
      MaybeCloned = execute_git_verbose(
        ["clone", NewURL, ProjectId_hex],
        ?DIR_PROJ
      ),
      verify_shell(clone, MaybeCloned);
    true ->
      ProjectDir = env:join([?DIR_PROJ, ProjectId_hex]),
      MaybePulled = execute_git_verbose(
        ["pull", NewURL],
        ProjectDir
      ),
      verify_shell(pull, MaybePulled)
  end.

strip_url(<<"http://", URL/binary>>) ->
  {http, URL};
strip_url(<<"https://", URL/binary>>) ->
  {https, URL};
strip_url(URL) ->
  {http, URL}.

verify_shell(Type, {done, 0, Out}) ->
  {ok, {Type, Out}};
verify_shell(Type, {done, _Status, Out}) ->
  {error, {Type, Out}}.

%%--------------------------------------------------------------------
%% @doc Delete project's directory if it exists
%% @spec delete_project(ProjectId) -> {ok, Output} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec delete_project(binary()) -> {ok, binary()} | {error, atom()}.
delete_project(ProjectId) ->
  ProjectId_x = bin_to_hex(ProjectId),
  case project_exists(ProjectId_x) of
    true ->
      {ok, execute("rm", ["-rf", env:join([?DIR_PROJ, ProjectId_x])], ".")};
    false ->
      {error, project_does_not_exist}
  end.

%%--------------------------------------------------------------------
%% @doc Get project's commits
%% @spec commits(ProjectId, TimeFrom, TimeTo) -> {ok, List} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec commits(binary(), calendar:datetime(), calendar:datetime()) ->
        {ok, list()} | {error, any()}.
commits(ProjectId, TimeFrom, TimeTo) ->
  TimeFrom_l = erlang:binary_to_list(time:datetime_to_iso8601(TimeFrom)),
  TimeTo_l = erlang:binary_to_list(time:datetime_to_iso8601(TimeTo)),
  Fun = fun(Cwd) -> dirty_commits(Cwd, TimeFrom_l, TimeTo_l) end,
  do_for_project(Fun, ProjectId).

%%--------------------------------------------------------------------
%% @doc Get project's commit
%% @spec commits(ProjectId, CommitSha) -> {ok, Commit} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec commit(binary(), binary()) -> {ok, map()} | {error, any()}.
commit(ProjectId, Sha) ->
  Fun = fun(Cwd) -> dirty_commit(Cwd, Sha) end,
  do_for_project(Fun, ProjectId).

-spec first_merge_commit(db_project:id(), Sha :: binary()) 
        -> {ok, map()} | {error, any()}.
first_merge_commit(ProjectId, Sha) ->
  Fun = fun(Cwd) -> dirty_first_merge_commit(Cwd, Sha) end,
  do_for_project(Fun, ProjectId).

%%--------------------------------------------------------------------
%% @doc Get project commit's files with change nums and diff
%% @spec commit_files(ProjectId, Sha) -> {ok, List} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec commit_files(binary(), binary()) -> {ok, list()} | {error, any()}.
commit_files(ProjectId, Sha) ->
  Sha_l = erlang:binary_to_list(Sha),
  Fun = fun(Cwd) -> dirty_files(Cwd, Sha_l) end,
  do_for_project(Fun, ProjectId).

%%--------------------------------------------------------------------
%% @doc Get project's authors
%% @spec authors(ProjectId) -> {ok, List} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec authors(binary()) ->
        {ok, list()} | {error, any()}.
authors(ProjectId) ->
  Fun = fun(Cwd) -> 
    TimeFrom = execute_git(["rev-list","--max-parents=0","HEAD"], Cwd),
    TimeTo_l = "HEAD",
    dirty_authors(Cwd, binary_to_list(TimeFrom), TimeTo_l)
  end,
  do_for_project(Fun, ProjectId).

%%--------------------------------------------------------------------
%% @doc Get project's authors for timeframe
%% @spec authors(ProjectId, TimeFrom, TimeTo) -> {ok, List} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec authors(binary(), calendar:datetime(), calendar:datetime()) ->
        {ok, list()} | {error, any()}.
authors(ProjectId, TimeFrom, TimeTo) ->
  TimeFrom_l = erlang:binary_to_list(time:datetime_to_iso8601(TimeFrom)),
  TimeTo_l = erlang:binary_to_list(time:datetime_to_iso8601(TimeTo)),
  Fun = fun(Cwd) -> dirty_authors(Cwd, TimeFrom_l, TimeTo_l) end,
  do_for_project(Fun, ProjectId).

%%====================================================================
%% Private functions
%%====================================================================
%% Converters
bin_to_hex(Binary) ->
  [Y || <<X:4>> <= Binary, Y <- integer_to_list(X, 16)].

flat(List) -> lists:flatten(List).

%%--------------------------------------------------------------------
%% Shell command transformers
%%--------------------------------------------------------------------
execute(Command, Args, Cwd) ->
  {done, _Status, Out} = sh:run(Command, Args, binary, Cwd),
  if
    ?DEBUG -> error_logger:info_msg("  git exec: ~p ~p~n", [Command, Args]);
    true   -> skip
  end,
  Out.

execute_git(Args, Cwd) -> 
  execute("git", Args, Cwd).
execute_git(Predef, Args, Cwd) ->
  sh:oneliner(flat([Predef, " git ", Args]), Cwd).
execute_git_verbose(Args, Cwd) ->
  sh:run("git", Args, binary, Cwd).

%%--------------------------------------------------------------------
%% Wrapper around git commands
%%--------------------------------------------------------------------
log(Args, Cwd) -> execute_git(["log"|Args], Cwd).
show(Sha, Args, Cwd) -> execute_git(["show", Sha|Args], Cwd).

set_dir(Dir) -> erlang:put(?DIR_KEY, Dir).
get_dir() -> erlang:get(?DIR_KEY).
clear_dir() -> erlang:put(?DIR_KEY, undefined).

%%--------------------------------------------------------------------
%% Clone helpers
%%--------------------------------------------------------------------
verify_cloned(0, Out) ->
  {ok, Out};
verify_cloned(Status, Out) ->
  {error, {Status, Out}}.
  

%%--------------------------------------------------------------------
%% Checks if project exists
%%--------------------------------------------------------------------
project_exists(ProjectId) -> filelib:is_dir(env:join([?DIR_PROJ, ProjectId])).

%%--------------------------------------------------------------------
%% Executes git wrappers for specific directory
%%--------------------------------------------------------------------
do_for_project(Fun, ProjectId_b) ->
  ProjectId = bin_to_hex(ProjectId_b),
  ProjectDir = env:join([?DIR_PROJ, ProjectId]),
  MaybeExists =  project_exists(ProjectId),
  case do_for_dir(MaybeExists, Fun, ProjectDir) of
    {error, Reason} -> {error, Reason};
    Result          -> {ok, Result}
  end.

do_for_dir(true, Fun, ProjectDir) -> Fun(ProjectDir);
do_for_dir(false, _, _)           -> {error, project_does_not_exist}.

%%--------------------------------------------------------------------
%% Commit helpers
%%--------------------------------------------------------------------
dirty_commits(Cwd, TimeFrom, TimeTo) ->
  Shas = commit_shas(Cwd, TimeFrom, TimeTo),
  notmyjob:dmap(fun(Sha) -> dirty_commit(Cwd, Sha) end, Shas).
  
commit_shas(Cwd, TimeFrom, TimeTo) ->
  RawShas = log([
    "--format=%H",
    "--no-merges",
    flat(["--since=", TimeFrom]), 
    flat(["--until=", TimeTo])
  ], Cwd),
  binary:split(RawShas, <<"\n">>, [global, trim]).

dirty_commit(Cwd, Sha) ->
  Sha_l = binary_to_list(Sha),
  Raw = show(Sha_l, ["--quiet", "--pretty=%an%n%ae%n%ct%n%P%n%s"], Cwd),
  transform_commit(Cwd, Raw, {Sha, Sha_l}).

transform_commit(_, <<"fatal: ", Error/binary>>, _) ->
  {error, {bad_sha, Error}};
transform_commit(Cwd, Raw, {Sha, Sha_l}) ->
  TokenList = binary:split(Raw, <<"\n">>, [global, trim]),
  [AuthorName, AuthorEmail, CommitDate, ParentShas, Title] = TokenList,
  DateTime = time:timestamp_to_datetime(CommitDate),
  Message = show(Sha_l, ["--quiet", "--pretty=%b"], Cwd),
  #{
    sha     => Sha,
    aname   => AuthorName,
    aemail  => AuthorEmail,
    date    => DateTime,
    pshas   => binary:split(ParentShas, <<" ">>, [global, trim]),
    title   => Title,
    message => Message,
    files   => dirty_files(Cwd, Sha_l)
  }.

dirty_first_merge_commit(Cwd, Sha) ->
  Sha_l = binary_to_list(Sha),
  Cmd = ["log",  "--format=format:%H", "--ancestry-path", "--merges",
              flat([Sha_l, "..HEAD"])],
  Raw = execute_git(Cmd, Cwd),
  Result = transform_first_merge_commit(Cwd, Raw),
  case Result of
    {error, Reason} ->
      error_logger:error_msg("Could not find first merge commit for Sha ~p in Project: ~p. Reason: ~p", [Sha_l, Cwd, Reason]);
    _ -> ok
  end,
  Result.
  
transform_first_merge_commit(_, <<"fatal: ", Error/binary>>) ->
  {error, Error};
transform_first_merge_commit(Cwd, MergedShas) ->
  ReversedShaList = binary:split(MergedShas, <<"\n">>, [global]),
  [OldestSha|_] = lists:reverse(ReversedShaList),
  dirty_commit(Cwd, OldestSha).
%%--------------------------------------------------------------------
%% File helpers
%%--------------------------------------------------------------------
dirty_files(Cwd, Sha) ->
  MaybeFilenames = filenames(Cwd, Sha),
  transform_files(Cwd, Sha, MaybeFilenames).

filenames(Cwd, Sha) ->
  RawFilenames = show(Sha, ["--pretty=format:", "--name-only"], Cwd),
  binary:split(RawFilenames, <<"\n">>, [global, trim]).

transform_files(_, _, [<<"fatal: ", Error/binary>>|_]) ->
  {error, {bad_sha, Error}};
transform_files(Cwd, Sha, Filenames) ->
  notmyjob:dmap(fun(Filename) ->
    {NumIns, NumDel, NumNet} = file_metrics(Cwd, Sha, Filename),
    Diff = file_diff(Cwd, Sha, Filename),
    #{
      name        => Filename,
      insertions  => NumIns,
      deletions   => NumDel,
      net         => NumNet,
      diff        => Diff
    }
  end, Filenames).

file_metrics(Cwd, Sha, Filename) ->
  Filename_l = binary:bin_to_list(Filename),
  Raw = show(Sha, ["--shortstat", "--oneline", "--", Filename_l], Cwd),
  [_, RawMetric] = binary:split(Raw, <<"\n">>, [trim]),
  NumIns = binary_to_integer(match_num(RawMetric, <<"([0-9]+ insertions)">>)),
  NumDel = binary_to_integer(match_num(RawMetric, <<"([0-9]+ deletions)">>)),
  NumNet = NumIns - NumDel,
  {NumIns, NumDel, NumNet}.

match_num(Value, Expr) ->
  case re:run(Value, Expr, [{capture, first, binary}]) of
    {match, [Match]} ->
      [Num, _] = binary:split(Match, <<" ">>, [trim]), Num;
    nomatch ->
      <<"0">>
  end.

file_diff(Cwd, Sha, Filename) ->
  Filenamel = binary:bin_to_list(Filename),
  show(Sha, ["--format=", "--", Filenamel], Cwd).

%%--------------------------------------------------------------------
%% Author helpers
%%--------------------------------------------------------------------
dirty_authors(Cwd, TimeFrom, TimeTo) ->
  Raw = log([
    "--format=%ae|%an",
    flat(["--since=", TimeFrom]),
    flat(["--until=", TimeTo])
  ], Cwd),
  RawAuthors = binary:split(Raw, <<"\n">>, [global, trim_all]),
  UnsortedAuthors = lists:map(fun(RawAuthor) ->
    parse_raw_author(RawAuthor)
  end, RawAuthors),
  SortedAuthors = lists:ukeysort(1, UnsortedAuthors),
  lists:map(fun(Author) -> transform_author(Author) end, SortedAuthors).

parse_raw_author(RawAuthor) ->
  list_to_tuple(binary:split(RawAuthor, <<"|">>)).

transform_author({Email, Username}) -> #{username => Username, email => Email}.
