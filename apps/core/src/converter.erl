-module(converter).
-export([binary_to_hex/1]).

binary_to_hex(Bin) ->
  << <<Y>> || <<X:4>> <= Bin, Y <- integer_to_list(X, 16)>>. 
