-ifndef(API_HRL).
-define(API_HRL, true).

-record(endpoint, {name, url, token}).

-type endpoint() :: #endpoint{}.

-define(EP_GITLAB_SFU(Token), #endpoint{
  name = gitlab,
  url = <<"https://csil-git1.cs.surrey.sfu.ca/api/v3/projects">>,
  token = Token
}).

-define(SOURCES, [
  #{
    name          => "CSIL GitLab",
    url           => "https://cs-gitlab-db.cs.surrey.sfu.ca",
    endpoint_ref  => <<"gitlab">>
  }
]).

-endif.