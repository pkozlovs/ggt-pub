-ifndef(GITLAB_HRL).
-define(GITLAB_HRL, true).

-define(BASE_URL, "https://csil-git1.cs.surrey.sfu.ca/api/v3/projects").
-define(HEADERS, [{<<"PRIVATE-TOKEN">>, <<"Z7G55aytdnee-iLzyKtv">>}]).
%%-define(HEADERS, [{<<"PRIVATE-TOKEN">>, wf:session(gitlab_privkey)}]).

-define(STATE_ALL, <<"all">>).
-define(STATE_MERGED, <<"merged">>).
-define(STATE_OPENED, <<"opened">>).
-define(STATE_CLOSED, <<"closed">>).

-define(QUERY_ADD_USER, [
  {<<"user_id">>, <<"1310">>},
  {<<"access_level">>, <<"30">>}
]).

-record(response, {code, data}).

-endif.
