%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Universal API wrapper.
%%% @end
%%%-------------------------------------------------------------------
-module(api).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("api.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([read/3]).
-compile(export_all).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Query the API.
%% @spec read(Table, Endpoint, Options) -> {ok, Result} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec read(atom(), endpoint(), [tuple()]) ->
        {ok, list() | map()} | {error, any()}.
read(projects, Endpoint, []) ->
  Module = Endpoint#endpoint.name,
  Module:projects(Endpoint);
read(project, Endpoint, [
  {project_id, ProjectId}
]) ->
  Module = Endpoint#endpoint.name,
  Module:project(Endpoint, ProjectId);
read(merge_requests, Endpoint, [
  {project_id,  ProjectId},
  {state,       State}
]) ->
  Module = Endpoint#endpoint.name,
  Module:merge_requests(Endpoint, ProjectId, State);
read(merge_request_commits, Endpoint, [
  {project_id,        ProjectId},
  {merge_request_id,  MergeRequestId}
]) ->
  Module = Endpoint#endpoint.name,
  Module:merge_request_commits(Endpoint, ProjectId, MergeRequestId);
read(merge_request_notes, Endpoint, [
  {project_id,        ProjectId},
  {merge_request_id,  MergeRequestId}
]) ->
  Module = Endpoint#endpoint.name,
  Module:merge_request_notes(Endpoint, ProjectId, MergeRequestId);
read(merge_request_changes, Endpoint, [
  {project_id,        ProjectId},
  {merge_request_id,  MergeRequestId}
]) ->
  Module = Endpoint#endpoint.name,
  Module:merge_request_changes(Endpoint,  ProjectId, MergeRequestId);
read(issues, Endpoint, [
  {project_id,  ProjectId},
  {state,       State}
]) ->
  Module = Endpoint#endpoint.name,
  Module:issues(Endpoint, ProjectId, State);
read(members, Endpoint, [
  {project_id, ProjectId}
]) ->
  Module = Endpoint#endpoint.name,
  Module:members(Endpoint, ProjectId);
read(commit, Endpoint, [
  {project_id, ProjectId},
  {commit_sha, CommitSha}
]) ->
  Module = Endpoint#endpoint.name,
  Module:commit(Endpoint, ProjectId, CommitSha).

%%--------------------------------------------------------------------
%% @doc Insert via API
%% @spec insert(Table, Endpoint, Options) -> {ok, Result} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec insert(atom(), endpoint(), [tuple()]) ->
        {ok, list() | map()} | {error, any()}.
insert(bot, Endpoint, [{project_id, ProjectId}]) ->
  Module = Endpoint#endpoint.name,
  Module:insert_bot(Endpoint, ProjectId).


%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Provides Endpoint record based on source type
%%--------------------------------------------------------------------
get_endpoint(<<"gitlab">>, Token) -> ?EP_GITLAB_SFU(Token).

%%--------------------------------------------------------------------
%% Temporary test functions
%%--------------------------------------------------------------------
test_projects() ->
  api:read(projects, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>), []).
test_mr_commits() ->
  api:read(merge_request_commits, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4359">>}, {merge_request_id, <<"4905">>}]).
test_members() ->
  api:read(members, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4359">>}]).
test_mr() ->
  api:read(merge_requests, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4359">>}, {state, <<"all">>}]).
test_mr_notes() ->
  api:read(merge_request_notes, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4359">>}, {merge_request_id, <<"4905">>}]).
      
test_mr_changes() ->
  api:read(merge_request_changes, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4359">>}, {merge_request_id, <<"4905">>}]).

test_commit() ->
  api:read(commit, ?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>),
           [{project_id, <<"4765">>}, {commit_sha,
          <<"ce0368c30645d99fe2d4dd2bed372f06e75ed329">>}]).

test_insert() ->
  gitlab:insert_bot(?EP_GITLAB_SFU(<<"Z7G55aytdnee-iLzyKtv">>), <<"4765">>).
