%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Wrapper around Gitlab's REST API.
%%% @end
%%%-------------------------------------------------------------------
-module(gitlab).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("gitlab.hrl").
-include_lib("api.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([
  projects/1, project/2
]).
-export([
  commit/3
]).
-export([
  merge_requests/3, merge_request_commits/3, merge_request_notes/3,
  merge_request_changes/3
]).
-export([
  issues/3
]).
-export([
  members/2
]).
-export([
  insert_bot/2
]).


%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Get a list of projects.
%% @spec projects(Endpoint) -> {ok, Projects} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec projects(endpoint()) -> {ok, list()} | {error, any()}.
projects(Endpoint) ->
  Response = cget(Endpoint, [], []),
  transform_raw(fun(RawProjects) ->
    transform_raw_projects(RawProjects)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific project's details.
%% @spec project(Endpoint, ProjectId) -> {ok, Project} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec project(endpoint(), binary()) -> {ok, map()} | {error, any()}.
project(Endpoint, ProjectId) ->
  Path = [ProjectId],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawProject) ->
    transform_raw_project(RawProject)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific project's merge requests.
%% @spec merge_requests(Endpoint, ProjectId, State) ->
%%         {ok, MergeRequests} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec merge_requests(endpoint(), binary(), binary()) ->
        {ok, list()} | {error, any()}.
merge_requests(Endpoint, ProjectId, State) ->
  Path = [ProjectId, <<"merge_requests">>],
  Queries = [{<<"state">>, State}],
  Response = cget(Endpoint, Path, Queries),
  transform_raw(fun(RawMergeRequests) ->
    transform_raw_merge_requests(RawMergeRequests)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific merge request's commits.
%% @spec merge_request_commits(Endpoint, ProjectId, MergeRequestId) ->
%%         {ok, Commits} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec merge_request_commits(endpoint(), binary(), binary()) ->
        {ok, list()} | {error, any()}.
merge_request_commits(Endpoint, ProjectId, MergeRequestId) ->
  Path = [ProjectId, <<"merge_requests">>, MergeRequestId, <<"commits">>],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawCommits) ->
    transform_raw_mr_commits(RawCommits)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific merge request's comments.
%% @spec merge_request_notes(Endpoint, ProjectId, MergeRequestId) ->
%%         {ok, Notes} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec merge_request_notes(endpoint(), binary(), binary()) ->
        {ok, list()} | {error, any()}.
merge_request_notes(Endpoint, ProjectId, MergeRequestId) ->
  Path = [ProjectId, <<"merge_requests">>, MergeRequestId, <<"notes">>],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawNotes) ->
    transform_raw_mr_notes(RawNotes)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific merge request's changes.
%% @spec merge_request_changes(Endpoint, ProjectId, MergeRequestId) ->
%%         {ok, Changes} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec merge_request_changes(endpoint(), binary(), binary()) ->
        {ok, list()} | {error, any()}.
merge_request_changes(Endpoint, ProjectId, MergeRequestId) ->
  Path = [ProjectId, <<"merge_requests">>, MergeRequestId, <<"changes">>],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawChanges) ->
    transform_raw_mr_changes(RawChanges)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific project's issues.
%% @spec issues(Endpoint, ProjectId, State) -> {ok, Issues} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec issues(endpoint(), binary(), binary()) -> {ok, list()} | {error, any()}.
issues(Endpoint, ProjectId, State) ->
  Path = [ProjectId, <<"issues">>],
  Queries = [{<<"state">>, State}],
  Response = cget(Endpoint, Path, Queries),
  transform_raw(fun(RawIssues) ->
    transform_raw_issues(RawIssues)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific project's members.
%% @spec members(Endpoint, ProjectId) -> {ok, Members} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec members(endpoint(), binary()) -> {ok, list()} | {error, any()}.
members(Endpoint, ProjectId) ->
  Path = [ProjectId, <<"members">>],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawMember) ->
    transform_raw_members(RawMember)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Get a specific commit
%% @spec commit(Endpoint, ProjectId, CommitSha) -> {ok, Commit} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec commit(endpoint(), binary(), binary()) -> {ok, map()} | {error, any()}.
commit(Endpoint, ProjectId, CommitSha) ->
  Path = [ProjectId, <<"repository">>, <<"commits">>, CommitSha],
  Response = cget(Endpoint, Path, []),
  transform_raw(fun(RawCommit) ->
    transform_raw_commit(RawCommit)
  end, Response).

%%--------------------------------------------------------------------
%% @doc Add bot analyzer account to the Project
%% @spec insert_bot(Endpoint, ProjectId) -> {ok, BotMember} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec insert_bot(endpoint(), binary()) -> {ok, map()} | {error, any()}.
insert_bot(Endpoint, ProjectId) ->
  Path = [ProjectId, <<"members">>],
  Queries = ?QUERY_ADD_USER,
  Response = cpost(Endpoint, Path, Queries),
  transform_raw(fun(RawMember) ->
    transform_raw_member(RawMember)
  end, Response).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Generic transformation helper
%%--------------------------------------------------------------------
transform_raw(Fun, RequestResponse) ->
  case RequestResponse of
    {ok, Response}  -> {ok, Fun(Response)};
    {error, Reason} -> {error, Reason}
  end.

%%--------------------------------------------------------------------
%% Request Method helpers
%%--------------------------------------------------------------------
cget(Endpoint, Path, Queries) -> request(get, Endpoint, Path, Queries).
cpost(Endpoint, Path, Queries) -> request(post, Endpoint, Path, Queries).

%%--------------------------------------------------------------------
%% Requesting helpers
%%--------------------------------------------------------------------
request(Method, Endpoint, Path, Queries) ->
  RawUrl = hackney_url:make_url(Endpoint#endpoint.url, Path, Queries),
  Headers = [header(private_token, Endpoint#endpoint.token)],
  ExpectedCode = 200,
  request(continue, {Method, Headers, ExpectedCode, [], RawUrl}).

%% - Goes through all pages 
request(continue, {_, _, 200, ResultList, false}) ->
  {ok, ResultList};
request(continue, {_, _, 201, ResultList, false}) ->
  {ok, ResultList};
request(continue, {Method, Headers, 200, ResultList, NextUrl}) ->
  Options = [insecure, {connect_timeout, infinity}],
  case hackney:request(Method, NextUrl, Headers, <<>>, Options) of
    {ok, StatusCode, RespHeaders, ClientRef} ->
      {ok, Body} = hackney:body(ClientRef),
      Result = jsone:decode(Body, [{object_format, map}]),
      NewResult = case is_list(Result) of
        true -> ResultList ++ Result;
        false -> Result
      end,
      request(continue,
        {Method, Headers, StatusCode, NewResult, link_next_url(RespHeaders)}
      );
    {error, Reason} ->
      {error, {server_error, Reason}}
  end;
request(continue, {_, _, StatusCode, Result, _}) ->
  {error, {bad_status_code, StatusCode, Result}}.

%%--------------------------------------------------------------------
%% Header helpers
%%--------------------------------------------------------------------
header(private_token, Token) -> {<<"PRIVATE-TOKEN">>, Token}.

link_next_url(RespHeaders) ->
  Link = hackney_headers:parse(<<"Link">>, RespHeaders),
  case Link of
    undefined ->
      false;
    _ ->
      DirtyRefs = binary:split(Link, <<", ">>, [global]),
      Refs = lists:map(fun(Ref) ->
        list_to_tuple(binary:split(Ref, <<"; ">>))
      end, DirtyRefs),
    case lists:keyfind(<<"rel=\"next\"">>, 2, Refs) of
      {DirtyNextUrl, _Direction} -> 
        binary:replace(DirtyNextUrl, [<<"<">>, <<">">>], <<"">>, [global]);
      false ->
        false
    end
  end.

%%--------------------------------------------------------------------
%% Project helpers
%%--------------------------------------------------------------------
transform_raw_projects(RawProjects) ->
  notmyjob:dmap(fun(RawProject) -> 
    #{
      <<"id">>                  := ProjectId,
      <<"name_with_namespace">> := Name
    } = RawProject,
    #{
      id    => integer_to_binary(ProjectId),
      name  => Name
    }
  end, RawProjects).

transform_raw_project(RawProject) ->
  #{
    <<"name">>            := Name,
    <<"namespace">>       := NamespaceMap,
    <<"web_url">>         := WebUrl,
    <<"ssh_url_to_repo">> := SshUrl,
    <<"http_url_to_repo">> := HTTPURL
  } = RawProject,
  Namespace = maps:get(<<"name">>, NamespaceMap),
  #{
    name      => Name,
    namespace => Namespace,
    web_url   => WebUrl,
    ssh_url   => SshUrl,
    http_url => HTTPURL
  }.

%%--------------------------------------------------------------------
%% Merge Request helpers
%%--------------------------------------------------------------------
transform_raw_merge_requests(RawMergeRequests) ->
  notmyjob:dmap(fun(RawMergeRequest) ->
    #{
      <<"id">>               := Id,
      <<"iid">>              := Iid,
      <<"title">>            := Title,
      <<"description">>      := Message,
      <<"author">>           := Author,
      <<"assignee">>         := Assignee,
      <<"state">>            := State,
      <<"target_branch">>    := TargetBranch,
      <<"source_branch">>    := SourceBranch,
      <<"sha">>              := Sha,
      <<"merge_commit_sha">> := MergeSha,
      <<"web_url">>          := Url
    } = RawMergeRequest,
    #{<<"username">> := AuthorUsername} = Author,
    AssigneeUsername = case Assignee of
      null -> null;
      #{<<"username">> := Username} -> Username
    end,
    ParsedMessage = case Message of
      null -> <<>>;
      Other -> Other
    end,
    #{
      id      => integer_to_binary(Id),
      iid     => integer_to_binary(Iid),
      title   => Title,
      message => ParsedMessage,
      auname  => AuthorUsername,
      asname  => AssigneeUsername,
      state   => State,
      tbranch => TargetBranch,
      sbranch => SourceBranch,
      sha     => Sha,
      merge_sha => MergeSha,
      url       => Url
    }
  end, RawMergeRequests).

transform_raw_mr_commits(RawCommits) ->
  notmyjob:dmap(fun(RawCommit) ->
    #{<<"id">> := Id} = RawCommit,
    #{sha => Id}
  end, RawCommits).

transform_raw_mr_notes(RawNotes) ->
  notmyjob:dmap(fun(RawNote) ->
    #{
      <<"id">>          := Id,
      <<"body">>        := Body,
      <<"author">>      := Author,
      <<"created_at">>  := Date,
      <<"system">>      := System
    } = RawNote,
    DateTime = time:iso8601_to_datetime(Date),
    #{<<"username">> := Authorname} = Author,
    #{
      id      => integer_to_binary(Id),
      auname  => Authorname,
      date    => DateTime,
      body    => Body,
      system  => System
    }
  end, RawNotes).

transform_raw_mr_changes(RawMergeRequest) ->
  #{<<"changes">> := RawChanges} = RawMergeRequest,
  notmyjob:dmap(fun(RawChange) ->
    #{
      <<"new_path">>  := Name,
      <<"diff">>      := Diff
    } = RawChange,
    #{
      name  => Name,
      diff  => Diff
    }
  end, RawChanges).

%%--------------------------------------------------------------------
%% Issue helpers
%%--------------------------------------------------------------------
transform_raw_issues(RawIssues) ->
  notmyjob:dmap(fun(RawIssue) ->
    #{
      <<"id">>          := Id,
      <<"title">>       := Title,
      <<"description">> := Message,
      <<"state">>       := State,
      <<"created_at">>  := DateCreate,
      <<"author">>      := Author,
      <<"assignee">>    := Assignee,
      <<"web_url">>     := Url
    } = RawIssue,
    #{<<"username">> := AuthorUsername} = Author,
    AssigneeUsername = case Assignee of
      null -> null;
      #{<<"username">> := Username} -> Username
    end,
    DateTime = time:iso8601_to_datetime(DateCreate),
    #{
      id      => integer_to_binary(Id),
      title   => Title,
      message => Message,
      state   => State,
      date    => DateTime,
      auname  => AuthorUsername,
      asname  => AssigneeUsername,
      url     => Url
    }
  end, RawIssues).

%%--------------------------------------------------------------------
%% Member helpers
%%--------------------------------------------------------------------
transform_raw_members(RawMembers) ->
  notmyjob:dmap(fun(RawMember) ->
    #{
      <<"username">> := Username,
      <<"name">>     := Name,
      <<"web_url">>  := Url
    } = RawMember,
    #{
      username => Username,
      name     => Name,
      url      => Url
    }
  end, RawMembers).

transform_raw_member(RawMember) ->
  #{
    <<"username">> := Username,
    <<"name">>     := Name
   } = RawMember,
  #{
    username => Username,
    name     => Name
  }.

%%--------------------------------------------------------------------
%% Commit Helper
%%--------------------------------------------------------------------
transform_raw_commit(RawCommit) ->
  #{
    <<"id">> := Sha,
    <<"author_name">>   := AuthorName,
    <<"author_email">>  := AuthorEmail,
    <<"created_at">>    := RawDate,
    <<"parent_ids">>    := ParentShas,
    <<"title">>         := Title,
    <<"message">>       := Message
  } = RawCommit,
  DateTime = time:iso8601_to_datetime(RawDate),
  #{
    sha     => Sha,
    aname   => AuthorName,
    aemail  => AuthorEmail,
    date    => DateTime,
    pshas   => ParentShas,
    title   => Title,
    message => Message
  }.
