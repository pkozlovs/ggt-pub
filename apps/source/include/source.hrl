-ifndef(SOURCE_HRL).
-define(SOURCE_HRL, true).

-record(project, {
  id :: binary(),
  name :: binary(),
  namespace :: binary(),
  namespace_id :: binary(),
  owner_id :: null | binary(),
  special_name :: binary(),
  web_url :: binary(),
  clone_url :: binary()
}).
-type project() :: #project{}.

-record(merge_request, {
  id :: binary(),
  local_id :: binary(),
  title :: binary(),
  description :: binary(),
  author :: binary(),
  assignee :: undefined | binary(),
  state :: open | closed | merged,
  target_branch :: binary(),
  source_branch :: binary(),
  merge_date :: calendar:datetime(),
  web_url :: binary()
}).
-type merge_request() :: #merge_request{}.

-record(commit, {
  sha :: binary(),
  short_sha :: binary(),
  title :: binary(),
  message :: binary(),
  author_name :: binary(),
  author_email :: binary(),
  date :: calendar:datetime()
}).
-type commit() :: #commit{}.

-record(diff, {
  filename :: binary(),
  data :: binary()
}).
-type diff() :: #diff{}.

-record(issue, {
  id :: binary(),
  local_id :: binary(),
  title :: binary(),
  description :: binary(),
  state :: open | closed,
  date :: calendar:datetime(),
  author :: binary(),
  assignee :: undefined | binary(),
  web_url :: binary()
}).
-type issue() :: #issue{}.

-record(note, {
  id :: binary(),
  author :: binary(),
  date :: calendar:datetime(),
  body :: binary(),
  system :: true | false
}).
-type note() :: #note{}.

-record(user, {
  id :: binary(),
  name :: binary(),
  username :: binary(),
  web_url :: binary()
}).
-type user() :: #user{}.

-record(group, {
  id :: binary(),
  name :: binary(),
  path :: binary()
}).
-type group() :: #group{}.

-endif.
