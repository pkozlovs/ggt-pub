-module(source).

-include_lib("apps/source/include/source.hrl").

-export([projects/2]).
-export([projects/3]).
-export([project/3]).
-export([merge_requests/3]).
-export([commits/3]).
-export([commit/3]).
-export([diffs/3]).
-export([issues/3]).
-export([notes/3]).
-export([users/2]).
-export([users/3]).
-export([groups/2]).

-export([create_project/3]).
-export([create_group/3]).
-export([create_merge_request/3]).
-export([accept_merge_request/3]).

-export([add_user/3]).

-export([delete_project/3]).
-export([delete_group/3]).
-export([remove_user/3]).

%%====================================================================
%% External types
%%====================================================================
-type source() :: gitlab.
-export_type([source/0]).

-type token() :: binary().
-export_type([token/0]).

-type id() :: binary().
-export_type([id/0]).

-type state() :: all | opened | closed.
-export_type([state/0]).

-type mr_state() :: state() | merged.
-export_type([mr_state/0]).

-type commit_sha() :: binary().
-export_type([commit_sha/0]).

-type option() ::
  {project_id, id()}
  | {mr_id, id()}
  | {state, state()}
  | {mr_state, mr_state()}
  | {commit_sha, commit_sha()}
  | {source_branch, id()}
  | {target_branch, id()}
  | {title, binary()}
  | {pageNum, binary()}.
-export_type([option/0]).

-type options() :: [option()].
-export_type([options/0]).

%%--------------------------------------------------------------------
%% Opaques
%%--------------------------------------------------------------------
-export_type([project/0]).
-export_type([merge_request/0]).
-export_type([commit/0]).
-export_type([diff/0]).
-export_type([issue/0]).
-export_type([note/0]).
-export_type([user/0]).
-export_type([group/0]).

%%====================================================================
%% External functions
%%====================================================================
-spec projects(source(), token()) -> {ok, [project()]} | {error, term()}.
projects(Source, Token) ->
  transaction(Source, get_projects, fun(RawProjects) ->
    source_project:construct_projects(Source, RawProjects)
  end, Token, []).

-spec projects(source(), token(), options()) -> {ok, [project()]} | {error, term()}.
projects(Source, Token, Options) ->
  transaction_with_headers(Source, get_projects, fun(RawProjects) ->
    source_project:construct_projects(Source, RawProjects)
  end, Token, Options).

-spec project(source(), token(), options()) ->
  {ok, project()} | {error, term()}.
project(Source, Token, [
  {project_id, _ProjectId}
] = Options) ->
  transaction(Source, get_project, fun(RawProject) ->
    source_project:construct_project(Source, RawProject)
  end, Token, Options).

-spec merge_requests(source(), token(), options()) ->
  {ok, [merge_request()]} | {error, term()}.
merge_requests(Source, Token, [
  {project_id, ProjectId},
  {state, _State}
] = Options) ->
  transaction(Source, get_mrs, fun(RawMergeRequests) ->
    source_mr:construct_mrs(Source, Token, RawMergeRequests, ProjectId)
  end, Token, Options).

-spec commits(source(), token(), options()) ->
  {ok, commit()} | {error, term()}.
commits(Source, Token, [
  {project_id, _ProjectId}
] = Options) ->
  transaction(Source, get_commits, fun(RawCommits) ->
    source_commit:construct_commits(Source, RawCommits)
  end, Token, Options);
commits(Source, Token, [
  {project_id, _ProjectId},
  {merge_request_id, _MergeRequestId}
] = Options) ->
  transaction(Source, get_mr_commits, fun(RawCommits) ->
    source_commit:construct_commits(Source, RawCommits)
  end, Token, Options).

-spec commit(source(), token(), options()) ->
  {ok, commit()} | {error, term()}.
commit(Source, Token, [
  {project_id, _ProjectId},
  {commit_sha, _CommitSha}
] = Options) ->
  transaction(Source, get_commit, fun(RawCommit) ->
    source_commit:construct_commit(Source, RawCommit)
  end, Token, Options).

-spec diffs(source(), token(), options()) ->
  {ok, [diff()]} | {error, term()}.
diffs(Source, Token, [
  {project_id, _ProjectId},
  {merge_request_id, _MergeRequestId}
] = Options) ->
  transaction(Source, get_mr_diffs, fun(RawDiffs) ->
    source_diff:construct_diffs(Source, RawDiffs)
  end, Token, Options).

-spec issues(source(), token(), options()) ->
  {ok, [issue()]} | {error, term()}.
issues(Source, Token, [
  {project_id, _ProjectId},
  {state, _State}
] = Options) ->
  transaction(Source, get_issues, fun(RawIssues) ->
    source_issue:construct_issues(Source, RawIssues)
  end, Token, Options).

-spec notes(source(), token(), options()) ->
  {ok, [note()]} | {error, term()}.
notes(Source, Token, [
  {project_id, _ProjectId},
  {merge_request_id, _MergeRequestId}
] = Options) ->
  transaction(Source, get_mr_notes, fun(RawNotes) ->
    source_note:construct_notes(Source, RawNotes)
  end, Token, Options).

-spec users(source(), token()) ->
  {ok, [user()]} | {error, term()}.
users(Source, Token) ->
  transaction(Source, get_users, fun(RawUsers) ->
    source_user:construct_users(Source, RawUsers)
  end, Token, []).

-spec users(source(), token(), options()) ->
  {ok, [user()]} | {error, term()}.
users(Source, Token, [
  {project_id, _ProjectId}
] = Options) ->
  transaction(Source, get_users, fun(RawUsers) ->
    source_user:construct_users(Source, RawUsers)
  end, Token, Options).

-spec groups(source(), token()) ->
  {ok, [group()]} | {error, term()}.
groups(Source, Token) ->
  transaction(Source, get_groups, fun(RawGroups) ->
    source_group:construct_groups(Source, RawGroups)
  end, Token, []).

-spec create_project(source(), token(), options()) ->
  {ok, project()} | {error, term()}.
create_project(Source, Token, [
  {project_name, _Name},
  {namespace_id, _NameSpaceId}
] = Options) ->
  transaction(Source, post_project, fun(RawProject) ->
    source_project:construct_project(Source, RawProject)
  end, Token, Options).

-spec create_group(source(), token(), options()) ->
  {ok, group()} | {error, term()}.
create_group(Source, Token, [
  {group_name, _Name},
  {group_path, _Path}
] = Options) ->
  transaction(Source, post_group, fun(RawGroup) ->
    source_group:construct_group(Source, RawGroup)
  end, Token, Options).

-spec create_merge_request(source(), token(), options()) ->
  {ok, merge_request()} | {error, term()}.
create_merge_request(Source, Token, [
  {project_id, ProjId},
  {source_branch, _SourceB},
  {target_branch, _TargetB},
  {title, _Title}
] = Options) ->
  transaction(Source, post_merge_request, fun(RawMr) ->
    result:map(source_mr:construct_mrs(Source, Token, RawMr, ProjId), fun hd/1)
  end, Token, Options).

accept_merge_request(Source, Token, [
  {project_id, ProjId},
  {mr_id, _MrId}
] = Options) ->
  transaction(Source, accept_merge_request, fun(RawMr) ->
    result:map(source_mr:construct_mrs(Source, Token, RawMr, ProjId), fun hd/1)
  end, Token, Options).

-spec add_user(source(), token(), options()) ->
  {ok, user()} | {error, term()}.
add_user(Source, Token, [
  {project_id, _ProjectId},
  {user_id, _UserId}
] = Options) ->
  transaction(Source, post_add_user, fun(RawGroup) ->
    source_user:construct_user(Source, RawGroup)
  end, Token, Options);
add_user(Source, Token, [
  {group_id, _ProjectId},
  {user_id, _UserId}
] = Options) ->
  transaction(Source, post_add_user, fun(RawGroup) ->
    source_user:construct_user(Source, RawGroup)
  end, Token, Options).

-spec delete_project(source(), token(), options()) ->
  {ok, done} | {error, term()}.
delete_project(Source, Token, [
  {project_id, _ProjectId}
] = Options) ->
  transaction(Source, delete_project, fun(RawProject) ->
    source_project:construct_project(Source, RawProject)
  end, Token, Options).

-spec delete_group(source(), token(), options()) ->
  {ok, group()} | {error, term()}.
delete_group(Source, Token, [
  {group_id, _GroupId}
] = Options) ->
  transaction(Source, delete_group, fun(RawGroup) ->
    source_group:construct_group(Source, RawGroup)
  end, Token, Options).

-spec remove_user(source(), token(), options()) ->
  {ok, user()} | {error, term()}.
remove_user(Source, Token, [
  {project_id, _ProjectId},
  {user_id, _UserId}
] = Options) ->
  transaction(Source, delete_user, fun(RawUser) ->
    source_user:construct_user(Source, RawUser)
  end, Token, Options).

transaction(Source, Request, Constructor, Token, Options) ->
  Endpoint = source_endpoint(Source),
  MaybeRefResult = Endpoint:Request(Token, Options),
  case endpoint:parse_refs(MaybeRefResult) of
    {ok, [true]} ->
      {ok, done};
    Other ->
      Constructor(Other)
  end.

transaction_with_headers(Source, Request, Constructor, Token, Options) ->
  Endpoint = source_endpoint(Source),
  MaybeRefResult = Endpoint:Request(Token, Options),
  Header = lists:nth(1, MaybeRefResult),
  case endpoint:parse_refs(MaybeRefResult) of
    {ok, [true]} ->
      {ok, done, Header};
    Other ->
      {Constructor(Other), Header}
  end.

source_endpoint(gitlab) ->
  endpoint_gitlab;
source_endpoint(_) ->
  endpoint_unknown.
