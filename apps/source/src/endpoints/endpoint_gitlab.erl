-module(endpoint_gitlab).

-export([get_projects/2]).
-export([get_project/2]).
-export([get_mrs/2]).
-export([get_commits/2]).
-export([get_mr_commits/2]).
-export([get_commit/2]).
-export([get_mr_diffs/2]).
-export([get_issues/2]).
-export([get_mr_notes/2]).
-export([get_users/2]).
-export([get_groups/2]).

-export([post_project/2]).
-export([post_group/2]).

-export([post_add_user/2]).
-export([post_merge_request/2]).
-export([accept_merge_request/2]).

-export([delete_project/2]).
-export([delete_group/2]).
-export([delete_user/2]).

%%====================================================================
%% External types
%%====================================================================


%%====================================================================
%% External functions
%%====================================================================
-spec get_projects(source:token(), source:options()) -> endpoint:result().
get_projects(Token, []) ->
  URL = construct_url([<<"projects">>], []),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers);
get_projects(Token, [
  {page_num, PageNum}
]) ->
  io:format("Getting projects on page ~p!~n", [PageNum]),
  URL = construct_url([<<"projects">>], [{<<"page">>, PageNum}]),
  Headers = [token_header(Token)],
  endpoint:get_single_request(URL, Headers);
get_projects(Token, [
  {group_id, GroupId}
]) ->
  URL = construct_url(
    [<<"groups">>, GroupId, <<"projects">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_project(source:token(), source:options()) -> endpoint:result().
get_project(Token, [
  {project_id, ProjectId}
]) ->
  URL = construct_url([<<"projects">>, ProjectId], []),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_mrs(source:token(), source:options()) -> endpoint:result().
get_mrs(Token, [
  {project_id, ProjectId},
  {state, State}
]) ->
  State_bin = atom_to_binary(State, latin1),
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"merge_requests">>],
    [{<<"state">>, State_bin}]
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_commits(source:token(), source:options()) -> endpoint:result().
get_commits(Token, [
  {project_id, ProjectId}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"repository">>, <<"commits">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_mr_commits(source:token(), source:options()) -> endpoint:result().
get_mr_commits(Token, [
  {project_id, ProjectId},
  {merge_request_id, MergeRequestId}
]) ->
  URL = construct_url(
    [
      <<"projects">>, ProjectId,
      <<"merge_requests">>, MergeRequestId, <<"commits">>
    ],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_commit(source:token(), source:options()) -> endpoint:result().
get_commit(Token, [
  {project_id, ProjectId},
  {commit_sha, CommitSha}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"repository">>, <<"commits">>, CommitSha],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_mr_diffs(source:token(), source:options()) -> endpoint:result().
get_mr_diffs(Token, [
  {project_id, ProjectId},
  {merge_request_id, MergeRequestId}
]) ->
  URL = construct_url(
    [
      <<"projects">>, ProjectId,
      <<"merge_requests">>, MergeRequestId, <<"changes">>
    ],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

get_issues(Token, [
  {project_id, ProjectId},
  {state, State}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"issues">>],
    [{<<"state">>, State}]
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_mr_notes(source:token(), source:options()) -> endpoint:result().
get_mr_notes(Token, [
  {project_id, ProjectId},
  {merge_request_id, MergeRequestId}
]) ->
  URL = construct_url(
    [
      <<"projects">>, ProjectId,
      <<"merge_requests">>, MergeRequestId, <<"notes">>
    ],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_users(source:token(), source:options()) -> endpoint:result().
get_users(Token, []) ->
  URL = construct_url(
    [<<"users">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers);
get_users(Token, [
  {project_id, ProjectId}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"members">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec get_groups(source:token(), source:options()) -> endpoint:result().
get_groups(Token, []) ->
  URL = construct_url(
    [<<"groups">>],
    [{<<"all_available">>, <<"true">>}]
  ),
  Headers = [token_header(Token)],
  endpoint:get(URL, Headers).

-spec post_group(source:token(), source:options()) -> endpoint:result().
post_group(Token, [
  {group_name, Name},
  {group_path, Path}
]) ->
  URL = construct_url(
    [<<"groups">>],
    [
      {<<"name">>, Name},
      {<<"path">>, Path}  
    ]
  ),
  Headers = [token_header(Token)],
  endpoint:post(URL, Headers).

-spec post_project(source:token(), source:options()) -> endpoint:result().
post_project(Token, [
  {project_name, Name},
  {namespace_id, NameSpaceId}
]) ->
  URL = construct_url(
    [<<"projects">>],
    [
      {<<"name">>, Name},
      {<<"namespace_id">>, NameSpaceId},
      {<<"visibility_level">>, 0}
    ]
  ),
  Headers = [token_header(Token)],
  endpoint:post(URL, Headers).

post_merge_request(Token, [
  {project_id, ProjId},
  {source_branch, SourceB},
  {target_branch, TargetB},
  {title, Title}
]) ->
  Url = construct_url(
    [<<"projects">>, ProjId, <<"merge_requests">>],
    [
      {source_branch, SourceB},
      {target_branch, TargetB},
      {title, Title}
    ]
  ),
  Headers = [token_header(Token)],
  endpoint:post(Url, Headers).

accept_merge_request(Token, [
  {project_id, ProjId},
  {mr_id, MrId}
]) ->
  Url = construct_url(
    [<<"projects">>, ProjId, <<"merge_requests">>, MrId, <<"merge">>],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:put(Url, Headers).


-spec post_add_user(source:token(), source:options()) -> endpoint:result().
post_add_user(Token, [
  {project_id, ProjectId},
  {user_id, UserId}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"members">>],
    [
      {<<"user_id">>, UserId},
      {<<"access_level">>, 40}
    ]
  ),
  Headers = [token_header(Token)],
  endpoint:post(URL, Headers);
post_add_user(Token, [
  {group_id, ProjectId},
  {user_id, UserId}
]) ->
  URL = construct_url(
    [<<"groups">>, ProjectId, <<"members">>],
    [
      {<<"user_id">>, UserId},
      {<<"access_level">>, 30}
    ]
  ),
  Headers = [token_header(Token)],
  endpoint:post(URL, Headers).

-spec delete_project(source:token(), source:options()) -> endpoint:result().
delete_project(Token, [
  {project_id, ProjectId}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:delete(URL, Headers).

-spec delete_group(source:token(), source:options()) -> endpoint:result().
delete_group(Token, [
  {group_id, GroupId}
]) ->
  URL = construct_url(
    [<<"groups">>, GroupId],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:delete(URL, Headers).

-spec delete_user(source:token(), source:options()) -> endpoint:result().
delete_user(Token, [
  {project_id, ProjectId},
  {user_id, UserId}
]) ->
  URL = construct_url(
    [<<"projects">>, ProjectId, <<"members">>, UserId],
    []
  ),
  Headers = [token_header(Token)],
  endpoint:delete(URL, Headers).

construct_url(Path, Queries) ->
  BaseURL = source_utils:base_url(gitlab, 
    <<"https://csil-git1.cs.surrey.sfu.ca/api/v3">>),
  PerPage = source_utils:per_page_with_default(50),
  PagedQueries = [{<<"per_page">>, PerPage}|Queries],
  hackney_url:make_url(BaseURL, Path, PagedQueries).

token_header(Token) ->
  {<<"PRIVATE-TOKEN">>, Token}.
