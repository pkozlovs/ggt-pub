-module(endpoint).

-export([get/2]).
-export([get_single_request/2]).
-export([post/2]).
-export([put/2]).
-export([delete/2]).
-export([parse_refs/1]).

%%====================================================================
%% External types
%%====================================================================
-type method() :: get | post | delete | put.
-export_type([method/0]).

-type header() :: {binary(), binary()}.
-export_type([header/0]).

-type headers() :: [header()].
-export_type([headers/0]).

-type error() :: {error, term()}.
-export_type([error/0]).

-type response() ::
  {ok, {integer(), headers(), reference()}}
  | error().
-export_type([response/0]).

-type result() :: response() | [response()].
-export_type([result/0]).

-type body() :: {ok, [map()]}.
-export_type([body/0]).

-type http_error() ::
  {error, not_modified}
  | {error, bad_request}
  | {error, unauthorized}
  | {error, forbidden}
  | {error, not_found}
  | {error, method_not_allowed}
  | {error, conflict}
  | {error, unprocessable}
  | {error, server_error}.
-export_type([http_error/0]).


%%====================================================================
%% External functions
%%====================================================================
-spec get(binary(), headers()) -> result().
get(URL, Headers) ->
  loop_request(get, URL, Headers, <<>>, []).

-spec get_single_request(binary(), headers()) -> result().
get_single_request(URL, Headers) ->
  MaybeResponse = request(get, URL, Headers, <<>>),
  case MaybeResponse of
    {error, Reason} -> [{error, Reason}];
    {ok, Code, RespHeaders, Ref} -> [{ok, {Code, RespHeaders, Ref}}]
  end.

-spec post(binary(), headers()) -> result().
post(URL, Headers) ->
  loop_request(post, URL, Headers, <<>>, []).

-spec put(binary(), headers()) -> result().
put(URL, Headers) -> 
  loop_request(put, URL, Headers, <<>>, []).

-spec delete(binary(), headers()) -> result().
delete(URL, Headers) ->
  io:format("URL: ~p~n", [URL]),
  loop_request(delete, URL, Headers, <<>>, []).

loop_request(_, <<>>, _, _, Acc) ->
  Acc;
loop_request(Method, URL, Headers, Body, Acc) ->
  MaybeResponse = request(Method, URL, Headers, Body),
  go_to_next(MaybeResponse, Method, Headers, Body, Acc).

go_to_next({ok, Code, RespHeaders, Ref}, Method, Headers, Body, Acc) ->
  NextURL = next_url(RespHeaders),
  Response = {ok, {Code, RespHeaders, Ref}},
  loop_request(Method, NextURL, Headers, Body, [Response|Acc]);
go_to_next({error, Reason}, _, _, _, _) ->
  {error, Reason}.

next_url(RespHeaders) ->
  MaybeNextLink = hackney_headers:parse(<<"Link">>, RespHeaders),
  parse_next_link(MaybeNextLink).

parse_next_link(undefined) ->
  <<>>;
parse_next_link(NextLink) ->
  RawRefs = binary:split(NextLink, <<", ">>, [global, trim_all]),
  Refs = lists:map(fun(Ref) ->
    list_to_tuple(binary:split(Ref, <<"; ">>))
  end, RawRefs),
  MaybeRawURL = lists:keyfind(<<"rel=\"next\"">>, 2, Refs),
  format_next_url(MaybeRawURL).

format_next_url(false) ->
  <<>>;
format_next_url({RawURL, _Direction}) ->
  binary:replace(RawURL, [<<"<">>, <<">">>], <<"">>, [global]).

request(Method, URL, Headers, Body) ->
  Options = [insecure, {pool, source_api_pool}],
  hackney:request(Method, URL, Headers, Body, Options).


-spec parse_refs(endpoint:result()) -> body() | error() | http_error().
parse_refs(Result) ->
  parse_refs(Result, []).

parse_refs([{error, Reason}], _) ->
  {error, Reason};
parse_refs({error, Reason}, _) ->
  {error, Reason};
parse_refs([], Acc) ->
  {ok, lists:flatten(Acc)};
parse_refs([{ok, {200, _, Ref}}|Rest], Acc) ->
  {ok, RawBody} = hackney:body(Ref),
  Body = jsone:decode(RawBody, [{object_format, map}]),
  parse_refs(Rest, [Body|Acc]);
parse_refs([{ok, {201, _, Ref}}|Rest], Acc) ->
  {ok, RawBody} = hackney:body(Ref),
  Body = jsone:decode(RawBody, [{object_format, map}]),
  parse_refs(Rest, [Body|Acc]);
parse_refs([{ok, {304, _, _}}|_], _) ->
  {error, not_modified};
parse_refs([{ok, {400, _, _}}|_], _) ->
  {error, bad_request};
parse_refs([{ok, {401, _, _}}|_], _) ->
  {error, unauthorized};
parse_refs([{ok, {403, _, _}}|_], _) ->
  {error, forbidden};
parse_refs([{ok, {404, _, _}}|_], _) ->
  {error, not_found};
parse_refs([{ok, {405, _, _}}|_], _) ->
  {error, method_not_allowed};
parse_refs([{ok, {409, _, _}}|_], _) ->
  {error, conflict};
parse_refs([{ok, {422, _, _}}|_], _) ->
  {error, unprocessable};
parse_refs([{ok, {500, _, _}}|_], _) ->
  {error, server_error};
parse_refs([{ok, {Code, _, _}}|_], _) ->
  {error, {unknown_status_code, Code}}.
