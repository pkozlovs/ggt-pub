-module(source_utils).

-export([config/1]).
-export([config/2]).
-export([base_url/2]).
-export([per_page_with_default/1]).
-export([null_to_bin/1]).

-spec config(term()) -> term().
config(Key) ->
  config(Key, undefined).

-spec config(term(), term()) -> term().
config(Key, Default) ->
  application:get_env(source, Key, Default).

-spec base_url(atom(), binary()) -> binary().
base_url(Source, Default) ->
  BaseURLs = config(base_url, []),
  proplists:get_value(Source, BaseURLs, Default).

-spec per_page_with_default(integer()) -> binary().
per_page_with_default(Default) ->
  PerPage_int = config(objects_per_page, Default),
  integer_to_binary(PerPage_int).

-spec null_to_bin(null|binary()) -> binary().
null_to_bin(null) ->
  <<>>;
null_to_bin(Bin) ->
  Bin.