-module(source_app).
%%--------------------------------------------------------------------
%% Behaviour inheritance
%%--------------------------------------------------------------------
-behaviour(supervisor).
-behaviour(application).


%%--------------------------------------------------------------------
%% Internal exports
%%--------------------------------------------------------------------
-export([init/1, start/2, stop/1]).

%%====================================================================
%% Internal functions
%%====================================================================
start(_, _) -> supervisor:start_link({local, ?MODULE}, ?MODULE, []).
stop(_) -> ok.
init([]) -> load_modules(), deploy(), sup().

sup() -> {ok, {{one_for_one, 1, 1}, []}}.
load_modules() ->
  [code:ensure_loaded(M) || M <- [
    hackney,
    source,
    endpoint,
    endpoint_gitlab,
    source_project,
    source_utils
  ]].
deploy() ->
  hackney_pool:start_pool(
    source_api_pool,
    [
      {timeout, 150000},
      {max_connections, 1000}
    ]
  ),
  error_logger:info_msg(
      "    application: source~n"
      "    started normally~n~n",
    []
  ).
