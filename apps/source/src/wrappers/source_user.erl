-module(source_user).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([name/1]).
-export([username/1]).
-export([web_url/1]).

-export([construct_users/2]).
-export([construct_user/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:user()) -> binary().
id(#user{id = Id}) -> Id.

-spec name(source:user()) -> binary().
name(#user{name = Name}) -> Name.

-spec username(source:user()) -> binary().
username(#user{username = Username}) -> Username.

-spec web_url(source:user()) -> binary().
web_url(#user{web_url = WebURL}) -> WebURL.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_users(source:source(), endpoint:body()) ->
  {ok, [source:user()]} | {error, term()}.
construct_users(_, {error, Reason}) ->
  {error, Reason};
construct_users(Source, {ok, Users}) ->
  {ok, lists:map(fun(User) ->
    dirty_construct_user(Source, User)  
  end, Users)}.

-spec construct_user(source:source(), endpoint:body()) ->
  {ok, source:user()} | {error, term()}.
construct_user(_, {error, Reason}) ->
  {error, Reason};
construct_user(Source, {ok, [User]}) ->
  {ok, dirty_construct_user(Source, User)}.

dirty_construct_user(gitlab, #{
  <<"id">> := Id,
  <<"username">> := Username,
  <<"name">> := Name,
  <<"web_url">> := WebURL
}) ->
  #user{
    id = integer_to_binary(Id),
    name = Name,
    username = Username,
    web_url = WebURL
  }.
