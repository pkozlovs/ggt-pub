-module(source_mr).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([local_id/1]).
-export([title/1]).
-export([description/1]).
-export([author/1]).
-export([assignee/1]).
-export([state/1]).
-export([target_branch/1]).
-export([source_branch/1]).
-export([merge_date/1]).
-export([web_url/1]).

-export([construct_mrs/4]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:merge_request()) -> binary().
id(#merge_request{id = Id}) -> Id.

-spec local_id(source:merge_request()) -> binary().
local_id(#merge_request{local_id = LocalId}) -> LocalId.

-spec title(source:merge_request()) -> binary().
title(#merge_request{title = Title}) -> Title.

-spec description(source:merge_request()) -> binary().
description(#merge_request{description = Description}) -> Description.

-spec author(source:merge_request()) -> binary().
author(#merge_request{author = Author}) -> Author.

-spec assignee(source:merge_request()) -> binary().
assignee(#merge_request{assignee = Assignee}) -> Assignee.

-spec state(source:merge_request()) -> binary().
state(#merge_request{state = State}) -> State.

-spec target_branch(source:merge_request()) -> binary().
target_branch(#merge_request{target_branch = TBranch}) -> TBranch.

-spec source_branch(source:merge_request()) -> binary().
source_branch(#merge_request{source_branch = SBranch}) -> SBranch.

-spec merge_date(source:merge_request()) -> calendar:datetime().
merge_date(#merge_request{merge_date = MergeDate}) -> MergeDate.

-spec web_url(source:merge_request()) -> binary().
web_url(#merge_request{web_url = WebURL}) -> WebURL.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_mrs(
  source:source(), source:token(), endpoint:body(), binary()
) ->
  {ok, [source:merge_request()]} | {error, term()}.
construct_mrs(_, _, {error, Reason}, _) ->
  {error, Reason};
construct_mrs(Source, Token, {ok, MergeRequestList}, ProjectId) ->
  {ok, lists:map(fun(MergeRequest) ->
    dirty_construct_mr(Source, Token, MergeRequest, ProjectId)  
  end, MergeRequestList)}.

dirty_construct_mr(gitlab, Token, #{
  <<"id">> := Id,
  <<"iid">> := LocalId,
  <<"title">> := Title,
  <<"description">> := Description,
  <<"author">> := #{<<"username">> := Author},
  <<"assignee">> := AssigneeMap,
  <<"state">> := State,
  <<"target_branch">> := TargetBranch,
  <<"source_branch">> := SourceBranch,
  <<"updated_at">> := RawDate,
  <<"merge_commit_sha">> := MergeCommitSha,
  <<"web_url">> := WebURL
}, ProjectId) ->
  Datetime = time:iso8601_to_datetime(RawDate),
  Date = get_merge_date(MergeCommitSha, gitlab, Token, ProjectId, Datetime),
  #merge_request{
    id = integer_to_binary(Id),
    local_id = integer_to_binary(LocalId),
    title = Title,
    description = source_utils:null_to_bin(Description),
    author = Author,
    assignee = parse_assignee(AssigneeMap),
    state = binary_to_atom(State, utf8),
    target_branch = TargetBranch,
    source_branch = SourceBranch,
    merge_date = Date,
    web_url = WebURL
  }.

parse_assignee(#{<<"username">> := Username}) ->
  Username;
parse_assignee(null) ->
  undefined.

get_merge_date(null, _, _, _, UpdateDatetime) ->
  UpdateDatetime;
get_merge_date(CommitSha, Source, Token, ProjectId, UpdateDatetime) ->
  MaybeCommit = source:commit(Source, Token, [
    {project_id, ProjectId},
    {commit_sha, CommitSha}
  ]),
  case MaybeCommit of
    {error, _} ->
      UpdateDatetime;
    {ok, Commit} ->
      source_commit:date(Commit)
  end.
  
