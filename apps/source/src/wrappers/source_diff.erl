-module(source_diff).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([filename/1]).
-export([data/1]).

-export([construct_diffs/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec filename(source:diff()) -> binary().
filename(#diff{filename = Filename}) -> Filename.

-spec data(source:diff()) -> binary().
data(#diff{data = Data}) -> Data.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_diffs(source:source(), endpoint:body()) ->
  {ok, [source:diff()]} | {error, term()}.
construct_diffs(_, {error, Reason}) ->
  {error, Reason};
construct_diffs(Source, {ok, DiffList}) ->
  {ok, lists:map(fun(Diff) ->
    dirty_construct_diff(Source, Diff)  
  end, DiffList)}.

dirty_construct_diff(gitlab, #{
  <<"changes">> := RawDiffs
}) ->
  notmyjob:dmap(fun(#{
    <<"new_path">> := Filename,
    <<"diff">> := Data
  }) ->
    #diff{
      filename = Filename,
      data = Data
    }
  end, RawDiffs).
