-module(source_note).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([author/1]).
-export([date/1]).
-export([body/1]).
-export([system/1]).

-export([construct_notes/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:note()) -> binary().
id(#note{id = Id}) -> Id.

-spec author(source:note()) -> binary().
author(#note{author = Author}) -> Author.

-spec date(source:note()) -> calendar:datetime().
date(#note{date = Date}) -> Date.

-spec body(source:note()) -> binary().
body(#note{body = Body}) -> Body.

-spec system(source:note()) -> binary().
system(#note{system = System}) -> System.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_notes(source:source(), endpoint:body()) ->
  {ok, [source:note()]} | {error, term()}.
construct_notes(_, {error, Reason}) ->
  {error, Reason};
construct_notes(Source, {ok, NoteList}) ->
  {ok, lists:map(fun(Note) ->
    dirty_construct_note(Source, Note)  
  end, NoteList)}.

dirty_construct_note(gitlab, #{
  <<"id">> := Id,
  <<"body">> := Body,
  <<"author">> := #{<<"username">> := Author},
  <<"created_at">> := Date,
  <<"system">> := System
}) ->
  DateTime = time:iso8601_to_datetime(Date),
  #note{
    id = Id,
    body = Body,
    author = Author,
    date = DateTime,
    system = System
  }.
