-module(source_project).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([name/1]).
-export([namespace/1]).
-export([namespace_id/1]).
-export([owner_id/1]).
-export([special_name/1]).
-export([web_url/1]).
-export([clone_url/1]).

-export([construct_projects/2]).
-export([construct_project/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:project()) -> binary().
id(#project{id = Id}) -> Id.

-spec name(source:project()) -> binary().
name(#project{name = Name}) -> Name.

-spec namespace(source:project()) -> binary().
namespace(#project{namespace = Namespace}) -> Namespace.

-spec namespace_id(source:project()) -> binary().
namespace_id(#project{namespace_id = NamespaceId}) -> NamespaceId.

-spec owner_id(source:project()) -> binary().
owner_id(#project{owner_id = OwnerId}) -> OwnerId.

-spec special_name(source:project()) -> binary().
special_name(#project{special_name = SpecialName}) -> SpecialName.

-spec web_url(source:project()) -> binary().
web_url(#project{web_url = WebURL}) -> WebURL.

-spec clone_url(source:project()) -> binary().
clone_url(#project{clone_url = CloneURL}) -> CloneURL.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_projects(source:source(), endpoint:body()) ->
  {ok, [source:project()]} | {error, term()}.
construct_projects(_, {error, Reason}) ->
  {error, Reason};
construct_projects(Source, {ok, Projects}) ->
  {ok, lists:map(fun(Project) ->
    dirty_construct_project(Source, Project)  
  end, Projects)}.

-spec construct_project(source:source(), endpoint:body()) ->
  {ok, source:project()} | {error, term()}.
construct_project(_, {error, Reason}) ->
  {error, Reason};
construct_project(Source, {ok, [Project]}) ->
  {ok, dirty_construct_project(Source, Project)}.

dirty_construct_project(gitlab, #{
  <<"id">> := Id,
  <<"name">> := Name,
  <<"namespace">> := NamespaceMap,
  <<"name_with_namespace">> := SpecialName,
  <<"web_url">> := WebURL,
  <<"http_url_to_repo">> := CloneURL
}) ->
  Namespace = maps:get(<<"name">>, NamespaceMap),
  NamespaceId = maps:get(<<"id">>, NamespaceMap),
%% owner_id depricated in GitLab and removed.
%%  OwnerId = case maps:get(<<"owner_id">>, NamespaceMap) of
%%    null -> null;
%%    UserId -> integer_to_binary(UserId)
%%  end,
  OwnerId = null,
  #project{
    id = integer_to_binary(Id),
    name = Name,
    namespace = Namespace,
    namespace_id = integer_to_binary(NamespaceId),
    owner_id = OwnerId,
    special_name = SpecialName,
    web_url = WebURL,
    clone_url = CloneURL
  }.
