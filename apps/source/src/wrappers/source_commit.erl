-module(source_commit).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([sha/1]).
-export([short_sha/1]).
-export([title/1]).
-export([message/1]).
-export([author_name/1]).
-export([author_email/1]).
-export([date/1]).

-export([construct_commits/2]).
-export([construct_commit/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec sha(source:commit()) -> binary().
sha(#commit{sha = Sha}) -> Sha.

-spec short_sha(source:commit()) -> binary().
short_sha(#commit{short_sha = ShortSha}) -> ShortSha.

-spec title(source:commit()) -> binary().
title(#commit{title = Title}) -> Title.

-spec message(source:commit()) -> binary().
message(#commit{message = Message}) -> Message.

-spec author_name(source:commit()) -> binary().
author_name(#commit{author_name = AuthorName}) -> AuthorName.

-spec author_email(source:commit()) -> binary().
author_email(#commit{author_email = AuthorEmail}) -> AuthorEmail.

-spec date(source:commit()) -> binary().
date(#commit{date = Date}) -> Date.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_commits(source:source(), endpoint:body()) ->
  {ok, [source:commit()]} | {error, term()}.
construct_commits(_, {error, Reason}) ->
  {error, Reason};
construct_commits(Source, {ok, CommitList}) ->
  {ok, lists:map(fun(Commit) ->
    dirty_construct_commit(Source, Commit)  
  end, CommitList)}.

-spec construct_commit(source:source(), endpoint:body()) ->
  {ok, source:commit()} | {error, term()}.
construct_commit(_, {error, Reason}) ->
  {error, Reason};
construct_commit(Source, {ok, [Commit]}) ->
  {ok, dirty_construct_commit(Source, Commit)}.

dirty_construct_commit(gitlab, #{
  <<"id">> := Sha,
  <<"short_id">> := ShortSha,
  <<"title">> := Title,
  <<"message">> := Message,
  <<"author_name">> := AuthorName,
  <<"author_email">> := AuthorEmail,
  <<"created_at">> := RawDate
}) ->
  Datetime = time:iso8601_to_datetime(RawDate),
  #commit{
    sha = Sha,
    short_sha = ShortSha,
    title = Title,
    message = Message,
    author_name = AuthorName,
    author_email = AuthorEmail,
    date = Datetime
  }.
