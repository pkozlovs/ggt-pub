-module(source_group).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([name/1]).
-export([path/1]).

-export([construct_groups/2]).
-export([construct_group/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:group()) -> binary().
id(#group{id = Id}) -> Id.

-spec name(source:group()) -> binary().
name(#group{name = Name}) -> Name.

-spec path(source:group()) -> binary().
path(#group{path = Path}) -> Path.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_groups(source:source(), endpoint:body()) ->
  {ok, [source:group()]} | {error, term()}.
construct_groups(_, {error, Reason}) ->
  {error, Reason};
construct_groups(Source, {ok, Users}) ->
  {ok, lists:map(fun(Note) ->
    dirty_construct_group(Source, Note)  
  end, Users)}.

-spec construct_group(source:source(), endpoint:body()) ->
  {ok, source:project()} | {error, term()}.
construct_group(_, {error, Reason}) ->
  {error, Reason};
construct_group(Source, {ok, [Project]}) ->
  {ok, dirty_construct_group(Source, Project)}.

dirty_construct_group(gitlab, #{
  <<"id">> := Id,
  <<"name">> := Name,
  <<"path">> := Path
}) ->
  #group{
    id = integer_to_binary(Id),
    name = Name,
    path = Path
  }.
