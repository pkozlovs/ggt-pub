-module(source_issue).

%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/source/include/source.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([id/1]).
-export([local_id/1]).
-export([title/1]).
-export([description/1]).
-export([author/1]).
-export([assignee/1]).
-export([state/1]).
-export([date/1]).
-export([web_url/1]).

-export([construct_issues/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% Getters
%%--------------------------------------------------------------------
-spec id(source:issue()) -> binary().
id(#issue{id = Id}) -> Id.

-spec local_id(source:issue()) -> binary().
local_id(#issue{local_id = LocalId}) -> LocalId.

-spec title(source:issue()) -> binary().
title(#issue{title = Title}) -> Title.

-spec description(source:issue()) -> binary().
description(#issue{description = Description}) -> Description.

-spec author(source:issue()) -> binary().
author(#merge_request{author = Author}) -> Author.

-spec assignee(source:issue()) -> undefined | binary().
assignee(#issue{assignee = Assignee}) -> Assignee.

-spec state(source:issue()) -> open | closed.
state(#issue{state = State}) -> State.

-spec date(source:issue()) -> calendar:datetime().
date(#issue{date = Date}) -> Date.

-spec web_url(source:issue()) -> binary().
web_url(#issue{web_url = WebURL}) -> WebURL.

%%--------------------------------------------------------------------
%% Constructor
%%--------------------------------------------------------------------
-spec construct_issues(
  source:source(), endpoint:body()
) ->
  {ok, [source:issue()]} | {error, term()}.
construct_issues(_, {error, Reason}) ->
  {error, Reason};
construct_issues(Source, {ok, IssueList}) ->
  {ok, lists:map(fun(Issue) ->
    dirty_construct_issue(Source, Issue)  
  end, IssueList)}.

dirty_construct_issue(gitlab, #{
  <<"id">> := Id,
  <<"iid">> := LocalId,
  <<"title">> := Title,
  <<"description">> := Description,
  <<"state">> := State,
  <<"updated_at">> := RawDate,
  <<"author">> := #{<<"username">> := Author},
  <<"assignee">> := MaybeAssignee,
  <<"web_url">> := WebUrl
}) ->
  Assignee = parse_assignee(MaybeAssignee),
  Datetime = time:iso8601_to_datetime(RawDate),
  #issue{
    id = Id,
    local_id = LocalId,
    title = Title,
    description = Description,
    state = binary_to_atom(State, utf8),
    date = Datetime,
    author = Author,
    assignee = Assignee,
    web_url = WebUrl
  }.

parse_assignee(#{<<"username">> := Username}) ->
  Username;
parse_assignee(null) ->
  undefined.
  
