require 'sass-css-importer'
add_import_path Sass::CssImporter::Importer.new("./node_modules")

preferred_syntax = :scss
css_dir = './static/css-new'
sass_dir = './scss'
cache_path = "./tmp/.sass-cache"
relative_assets = true
line_comments = true
sourcemap = (environment == :production) ? false : true
output_style = (environment == :production) ? :compressed : :expanded