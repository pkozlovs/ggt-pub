var core = require("./modules/core");

import CommitsTable from "./tables/commits_table";

core.init();

let columnHeadingToHelp = {
  WS: "Number of lines that were changed by just whitespace, such as tabs, spaces and linefeeds.",
  Sntx: "Number of lines that were changed by just syntax, such as replacing '.' with '->'.",
  Mv: "Number of lines that were moved (relocated inside the file).",
  Edit: "Number of lines that were changed by small line edits (changing 4 or fewer characters on the line).",
  Ins: "Number of lines that were inserted (new line added).",
  Del: "Number of lines that were deleted (previous line has beenremoved).",
  Score: "User's score earned on this commit."
};

let tableHeader = [
  {
    data: "title_msg",
    title: "Title & Message",
    className: "ttl"
  },
  {
    data: "author",
    title: "Author",
    className: "auth"
  },
  {
    data: "date",
    title: "Date",
    className: "right date"
  },
  {
    data: "add",
    title: "(+)",
    className: "right add"
  },
  {
    data: "del",
    title: "(-)",
    className: "right del"
  },
  {
    data: "net",
    title: "&Delta;",
    className: "right net"
  },
  {
    data: "whitespace",
    title: "WS",
    className: "right whitespace"
  },
  {
    data: "syntax",
    title: "Sntx",
    className: "right syntax"
  },
  {
    data: "moves",
    title: "Mv",
    className: "right moves"
  },
  {
    data: "refactors",
    title: "Edit",
    className: "right refactors"
  },
  {
    data: "insertions",
    title: "Ins",
    className: "right insertions"
  },
  {
    data: "deletions",
    title: "Del",
    className: "right deletions"
  },
  {
    data: "sum",
    title: "Score",
    className: "right sum"
  }
];

let table = new CommitsTable(columnHeadingToHelp);
table.createRoutes();
table.subscribeToUpdateTable("update_commits", "#table_commits", tableHeader, false);
table.subscribeToHighlightFiles();
Backbone.history.start();


$(function () {
  //Hide menu:
  $("body").addClass("collapsed");

  $("#nav_dropdown_members").dropit({ submenuEl: "div" });

  $("#panel_lists").resizable({
    handleSelector: "#divider",
    resizeHeight: false
  });
});