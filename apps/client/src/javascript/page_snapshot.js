var core = require("./modules/core");
import Websocket from "./modules/websocket";

function getPairs() {
  let pairs = [];
  $("select").each(function() {
    let authorId = $(this).data("ggs-author");
    let memberId = $(this).val();
    pairs.push({"author": authorId, "member": memberId});
  });
  return pairs;
}

$(function() {
  $("#nav_dropdown_snapshots").dropit({submenuEl: "div"});
});

window.websocket.on(window.websocket.READY, function() {
  $(function() {
    $("#btn_apply").on("click", function() {
      let pairs = getPairs();
      Websocket.sendRequest("merge", {"pairs": pairs});
    });
  });
});

core.init();
