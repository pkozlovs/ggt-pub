let core = require("./modules/core");

core.init();

$(function() {
  //Hide menu:
  $("body").addClass("collapsed");

  $("thead th").each(function () {
    let $th = $(this);
    $th.attr("title", $th.text());
  });

  $("tbody td").each(function () {
    let $td = $(this);
    $td.attr("title", $td.text());
  });
});
