let core = require("./modules/core");

let Highcharts = require("highcharts/highstock");

import Chart from "./modules/chart";
import Websocket from "./modules/websocket";


function sort2DArray(array, index) {
  array.sort(function(a, b) {
    return a[index] - b[index];
  });
}

function setExtremes(object, event, ticks) {
  if (event.max) {
    var frequency = Math.floor((event.max - event.min) / 7);
    object.options.tickInterval = frequency;
  } else {
    object.options.tickInterval = ticks;
  }
}


$(function() {
  $("#nav_dropdown_members").dropit({submenuEl: "div"});
  
  //Hide menu:
  $("body").addClass("collapsed");
});

core.init();

Websocket.on("update_graphs", function(data) {
  let categories = data.date_range;
  let commits = data.commits;
  let mergeRequests = data.merge_requests;
  let notes = data.notes;

  let sortedCommits = [];
  for (let commit of commits) {
    sortedCommits.push([categories.indexOf(commit[0]), commit[1]]);
  }
  sort2DArray(sortedCommits, 0);

  let sortedMergeRequests = [];
  for (let mergeRequest of mergeRequests) {
    sortedMergeRequests.push([categories.indexOf(mergeRequest[0]), mergeRequest[1]]);
  }
  sort2DArray(sortedMergeRequests, 0);
  for (let i = 0; i < sortedMergeRequests.length; i++) {
    sortedMergeRequests[i][1] *= -1;
  }

  let sortedNotes = [];
  for (let note of notes) {
    sortedNotes.push([categories.indexOf(note[0]), note[1]]);
  }
  sort2DArray(sortedNotes, 0);

  let ticks = Math.round(categories.length / 9);
  Highcharts.chart("graph_commit_mr", {
    chart: {type: "column", zoomType: "x"},
    title: {text: null},
    credits: false,
    tooltip: {
      formatter: function() {
        return `${this.point.category}<br>${this.series.name}: ${Highcharts.numberFormat(Math.abs(this.point.y), 0)}`;
      }
    },
    xAxis: [{
      reversed: false,
      crosshair: true,
      min: 0,
      max: categories.length - 1,
      categories: categories,
      showFirstLabel: true,
      showLastLabel: true,
      tickInterval: ticks,
      minRange: 14,
      events: {setExtremes: function(event) {
        setExtremes(this, event, ticks);
      }},
    }, { // mirror axis on right side
      opposite: true,
      crosshair: true,
      reversed: false,
      categories: categories,
      linkedTo: 1,
    }],
    yAxis: {
      title: {
        text: "Merge Requests & Commits"
      },
      labels: {
        formatter: function () {
          return Math.abs(this.value);
        }
      },
      tickInterval: 2
    },
    plotOptions: {
      series: {
        stacking: "normal"
      }
    },
    series: [
      {
        name: "Merge Requests",
        data: sortedMergeRequests,
        pointPadding: 0.0,
        groupPadding: 0.0,
        showInNavigator: true,
        navigatorOptions: {
          type: "column",
          threshold: null,
        }
      },
      {
        name: "Commits",
        color: "rgba(0,0,0,0.5)",
        data: sortedCommits,
        pointPadding: 0.0,
        groupPadding: 0.0,
        showInNavigator: true,
        navigatorOptions: {
          color: "rgba(0,0,0,0.5)",
          type: "column",
          threshold: null,
        }
      }
    ],
    navigator: {
      enabled: true,
      xAxis: {
        categories: categories,
        labels: {enabled: false}
      }
    }
  });

  Chart.generateColumnGraph(
    "graph_note",
    categories,
    [
      {
        title: {text: "Notes"},
        tickInterval: 1
      }
    ],
    [
      {
        name: "Notes",
        data: sortedNotes,
        color: "rgba(56,142,60,0.7)",
        pointPadding: 0.0,
        groupPadding: 0.0,
        showInNavigator: true,
        navigatorOptions: {
          type: "column",
          threshold: null,
        }
      }
    ]
  );
});