var core = require("./modules/core");

import Websocket from "./modules/websocket";
let List = require("list.js");

core.init();

let projectList = {};

function updateCheckboxes() {
  let checkCount = 0;
  $("input:checkbox:not(#select_all)").each(function() {
    if ($(this).prop("checked") == true) {
      checkCount += 1;
    }
  });
  if (checkCount > 0) {
    $("#btn_leave").prop("disabled", false);
    $("#btn_delete").prop("disabled", false);
    $("#btn_hide").prop("disabled", false);
    $("#btn_show").prop("disabled", false);
  } else {
    $("#btn_leave").prop("disabled", true);    
    $("#btn_delete").prop("disabled", true);
    $("#btn_hide").prop("disabled", true);
    $("#btn_show").prop("disabled", true);
  }
}
function updateListeners() {
  $("input:checkbox").on("click", function() {
    updateCheckboxes();
  });
}

function getSelectedProjects() {
  let projects = [];
  $("input[type='checkbox'].real").each(function() {
    if ($(this).prop("checked") == true) {
      let id = $(this).attr("id");
      let name = $(this).data("name");
      projects.push({id: id, name: name});
    }
  });
  return projects;
}

$(function(){
  $("#select_all").on("click", function() {
    if ($(this).prop("checked") == true ) {
      console.log("Checked.");
      $("input[type='checkbox']").prop("checked", true);
    } else if ($(this).prop("checked") == false ) {
      $("input[type='checkbox']").prop("checked", false);
    }
  });
});

Websocket.on(Websocket.READY, function() {
  $(function() {
    $("#btn_leave").on("click", function() {
      let projects = getSelectedProjects();
      $("#selected_list_leave").html("");
      for (let project of projects) {
        $("#selected_list_leave").append(`<div>${project.name}</div>`);
      }
      $("#count_leave").html(`${projects.length}`);
      window.modal.showPersistent("#modal_confirm_leave");
    });
    $("#btn_delete").on("click", function() {
      let projects = getSelectedProjects();
      $("#selected_list_delete").html("");
      for (let project of projects) {
        $("#selected_list_delete").append(`<div>${project.name}</div>`);
      }
      $("#count_delete").html(`${projects.length}`);
      window.modal.showPersistent("#modal_confirm_delete");
    });
    $("#btn_hide").on("click", function() {
      let projects = getSelectedProjects();
      $("#selected_list_hide").html("");
      for (let project of projects) {
        $("#selected_list_hide").append(`<div>${project.name}</div>`);
      }
      $("#count_hide").html(`${projects.length}`);
      window.modal.showPersistent("#modal_confirm_hide");
    });

    $("#btn_leave_confirm").on("click", function() {
      let projectIds = [];
      $("input[type='checkbox'].real").each(function() {
        if ($(this).prop("checked") == true) {
          let id = $(this).attr("id");
          projectIds.push({id: id});
        }
      });
      $.modal.close();
      Websocket.sendRequest("leave_projects", {project_ids: projectIds});
    });
    $("#btn_delete_confirm").on("click", function() {
      let projects = getSelectedProjects();
      $.modal.close();
      window.modal.showPersistent("#modal_wait");
      Websocket.sendRequest("delete_projects", {projects: projects});
    });
    $("#btn_hide_confirm").on("click", function() {
      let projects = getSelectedProjects();
      $.modal.close();
      window.modal.showPersistent("#modal_wait_hid");
      Websocket.sendRequest("hide_projects", {projects: projects});
    });
    $("#btn_show").on("click", function() {
      let projects = [];
      $("input[type='checkbox'].real").each(function() {
        if ($(this).prop("checked") == true) {
          let id = $(this).attr("id");
          let name = $(this).data("name");
          projects.push({id: id, name: name});
        }
      });
      window.modal.showPersistent("#modal_wait_hid");
      Websocket.sendRequest("show_projects", {projects: projects});
    });
  });
});

Websocket.on("projects_ready", function() {
  let options = {
    valueNames: ["project-name"]
  };
  projectList = new List("list_projects_wrapper", options);
  $(".input-search").keyup(function() {
    projectList.search($(this).val());
  });
  updateListeners();
});

Websocket.on("projects_update", function() {
  projectList.reIndex();
  updateCheckboxes();
  updateListeners();
});