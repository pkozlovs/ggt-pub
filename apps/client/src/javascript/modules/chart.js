let Highcharts = require("highcharts/highstock");

const TICK_RATIO = 9;

function setExtremes(object, event, ticks) {
  if (event.max) {
    var frequency = Math.floor((event.max - event.min) / 7);
    object.options.tickInterval = frequency;
  } else {
    object.options.tickInterval = ticks;
  }
}

class Chart {
  static generateColumnGraph(selectorId, categories, yAxis, series) {
    let ticks = Math.round(categories.length / TICK_RATIO);
    return Highcharts.chart(selectorId, {
      chart: {type: "column", zoomType: "x"},
      title: {text: null},
      credits: false,
      xAxis: {
        crosshair: true,
        min: 0,
        max: categories.length - 1,
        categories: categories,
        showFirstLabel: true,
        showLastLabel: true,
        tickInterval: ticks,
        minRange: 14,
        events: {setExtremes: function(event) {
          setExtremes(this, event, ticks);
        }},
      },
      yAxis: yAxis,
      series: series,
      legend: {
        shadow: false
      },
      tooltip: {
        shared: true,
        shadow: false
      },
      plotOptions: {
        column: {
          grouping: false,
          shadow: false,
          borderWidth: 0
        }
      },
      navigator: {
        enabled: true,
        xAxis: {
          categories: categories,
          labels: {enabled: false}
        }
      }
    });
  }
}

export default Chart;