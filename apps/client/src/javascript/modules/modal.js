require("jquery-modal");

window.modal = {};

window.modal.show = function(selector) {
  let $modal = $(selector).modal({
    showClose: false,
    closeExisting: false
  });
  $(selector + " .close").on("click", function() {
    $.modal.close();
  });
  /*eslint-disable no-unused-vars*/
  $modal.on($.modal.AFTER_CLOSE, function(event, _) {
    /*eslint-enable no-unused-vars*/
    $modal.remove();
  });

  return $modal;
};

window.modal.showPersistent = function(selector) {
  let $modal = $(selector).modal({
    showClose: false,
    closeExisting: false
  });
  $(selector + " .close").on("click", function() {
    $.modal.close();
  });
  /*eslint-disable no-unused-vars*/
  $modal.on($.modal.AFTER_CLOSE, function(event, _) {
    /*eslint-enable no-unused-vars*/
    $modal.prependTo("#modal_cache");
  });
  return $modal;
};
