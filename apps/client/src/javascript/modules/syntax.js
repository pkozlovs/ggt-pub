"user strict";

import HighlightJS from "highlight.js";

function transformUnknownLanguage(name) {
  switch (name) {
  case "hrl":
    return "erl";
  case "config":
    return "erlang";
  case "iml":
    return "xml";
  default:
    return "json";
  }
}

class Syntax {
  constructor() {
    this.stack = null;
  }

  highlight(code, language) {
    let highlightedCode = HighlightJS.highlight(
      language,
      code,
      false,
      this.stack
    );
    this.stack = highlightedCode.top;
    return highlightedCode;
  }

  reset() {
    this.stack = null;
  }

  static getLanguage(name) {
    let lang = HighlightJS.getLanguage(name);
    return lang == undefined ? transformUnknownLanguage(name) : name;
  }
}

export default Syntax;