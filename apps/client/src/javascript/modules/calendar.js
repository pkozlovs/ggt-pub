"user strict";

require("expose-loader?flatpickr!flatpickr");

const DEFAULT_CONFIG = {
  wrap: true,
  enableTime: true,
  dateFormat: "U",
  altInput: true,
  altFormat: "F j, Y h:i K"
};

const START_CONFIG = {
  defaultHour: 0,
  defaultMinute: 0
};

const END_CONFIG = {
  defaultHour: 23,
  defaultMinute: 59
};

exports.create = function(selector) {
  return $(selector).flatpickr(DEFAULT_CONFIG);
};

exports.createStartDatePickr = function(selector) {
  let COMBINED_CONFIG = Object.assign(DEFAULT_CONFIG, START_CONFIG);
  return $(selector).flatpickr(COMBINED_CONFIG);
};

exports.createEndDatePickr = function(selector) {
  let COMBINED_CONFIG = Object.assign(DEFAULT_CONFIG, END_CONFIG);
  return $(selector).flatpickr(COMBINED_CONFIG);
};