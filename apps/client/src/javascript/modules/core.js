require("jquery");
require("underscore");
require("backbone");
require("jb-dropit");

var websocket = require("./websocket");

require("./modal");

function setupDOM() {
  $(function() {
    // Togglable Sidebar
    $("#btn_sidebar_collapse").on("click", function() {
      $("body").addClass("collapsed");
    });
    $("#btn_sidebar_expand").on("click", function() {
      $("body").removeClass("collapsed");
    });

    // Interactive User Avatar
    $("#avatar_nav").dropit();
  });
}

exports.init = function() {
  websocket.connect();
  setupDOM();
};