import _ from "underscore";
import Backbone from "backbone";

require("script-loader!./../lib/ws.min");

const READY = "ready";
const TERMINATED = "terminated";

window.websocket = {
  READY: "ready",
  TERMINATED: "terminated"
};

_.extend(window.websocket, Backbone.Events);

class Websocket {
  static get READY() { return READY; }
  static get TERMINATED() { return TERMINATED; }

  static connect() {
    N2O_start();
  }

  static sendRequest(callback, data) {
    window[`request_${callback}`](data);
  }

  static on(eventName, callback) {
    window.websocket.on(eventName, callback);
  }
}

export default Websocket;


window.websocket.sendRequest = function(callback, data) {
  window["request_" + callback](data);
};

exports.connect = function() {
  N2O_start();
};
