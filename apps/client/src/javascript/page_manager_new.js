var core = require("./modules/core");

import Websocket from "./modules/websocket";

core.init();

$(function(){
  $("input").blur(function(evt) {
    evt.target.checkValidity();
  }).bind("invalid", function(event) {
    console.log(event);
  });
});

Websocket.on(Websocket.READY, function() {
  $(function() {
    console.log("DOM WS READY");
    $("#btn_create").on("click", function() {
      let groupPrefix = $("#input_groupPre").val();
      let groupSuffix = $("#input_groupSuf").val();
      let groupNames = $("#input_groupNames").val();
      let projectNames = $("#input_projectNames").val();
      let userEmails = $("#ta_userEmails").val();

      let body = {
        group_prefix: groupPrefix,
        group_suffix: groupSuffix,
        group_names: groupNames,
        project_names: projectNames,
        grouped_emails: userEmails
      };

      window.modal.showPersistent("#modal_wait");
      Websocket.sendRequest("preview", body);
    });
  });
});