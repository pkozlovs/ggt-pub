import BaseTable from "./base_table";
import Websocket from "../modules/websocket";

class CommitsTable extends BaseTable {
  createRoutes() {
    let Router = Backbone.Router.extend({
      routes: {
        "mr/:id/cm/:iid": "getCommit",
      },
      getCommit: this.getCommit.bind(this)
    });
    new Router();
  }

  getCommit(id, iid) {
    console.log("getCommit");
    $(`#table_mr tbody tr#${id}`).addClass("selected");
    $("#table_commits tr.selected").removeClass("selected");
    $(`#table_commits tbody tr#${iid}`).first().addClass("selected");

    if (iid in this.commitsCache) {
      $(".panel-files .wrapper").scrollTop(0);
      $("#files_list").html(this.commitsCache[iid]);
    } else {
      Websocket.sendRequest("select_commit", { "id": iid });
    }
  }

  subscribeToUpdateTable(event, selector, headers, emptyTable) {
    var self = this;
    Websocket.on(event, function (data) {
      console.log("In " + event + "....");
      $(function () {
        _.defer(function () {
          if (emptyTable) {
            self.updateTable(selector, headers, []);
          } else {
            self.updateTable(selector, headers, data);
          }
        });
      });
    });
  }

  updateTable(selector, headers, data) {
    var self = this;
    console.log("updating table.....");
    this.initializeTable(
      selector,
      data,
      headers,
      this.infoCallback,
      function () {
        $(selector + " tr.selected").removeClass("selected");
        $(this).addClass("selected");

        let id = $(this).attr("id");
        location.href = `#/mr/${self.selectedMR}/cm/${id}`;
      },
      this.sortColumn
    );

    if (!Backbone.History.started) {
      Backbone.history.start();
    }
  }

  subscribeToReplaceCommits() {
    Websocket.on("replace_commits", (data) => {
      let table = $("#table_commits").DataTable();
      table.clear();
      table.rows.add(data);
      table.draw();
      this.addTableDataTooltip();
    });
  }
}

export default CommitsTable;