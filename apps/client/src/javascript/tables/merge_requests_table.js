import Websocket from "../modules/websocket";
import BaseTable from "./base_table";

class MergeRequestsTable extends BaseTable {
  constructor(columnHeadingToHelp) {
    super(columnHeadingToHelp);
    this.mergeRequestCache = [];
  }

  createRoutes() {
    let Router = Backbone.Router.extend({
      routes: {
        "mr/:id": "getMergeRequest",
        "*_": "getDefault"
      },
      getMergeRequest: (id) => {
        $("#table_mr tr.selected").removeClass("selected");
        $(`#table_mr tbody tr#${id}`).first().addClass("selected");

        this.selectedMR = id;

        if (id in this.mergeRequestCache) {
          $(".panel-files .wrapper").scrollTop(0);
          $("#files_list").html(this.mergeRequestCache[id]);
        } else {
          Websocket.sendRequest("select_mr", { "id": id });
        }
      },
      getDefault: () => {
        $("#table_mr tr.selected").removeClass("selected");
        $("#table_mr tbody tr").first().addClass("selected");
        let id = $("#table_mr tbody tr").first().attr("id");
        this.selectedMR = id;
        Websocket.sendRequest("select_mr", { "id": id });
      }
    });
    new Router();
  }

  updateTable(selector, headers, data) {
    this.initializeTable(
      selector,
      data,
      headers,
      this.infoCallback,
      function () {
        $("#table_commits tr.selected").removeClass("selected");
        $(this).addClass("selected");

        let id = $(this).attr("id");
        location.href = `#/mr/${id}`;
      },
      this.sortColumn
    );


    if (!Backbone.History.started) {
      Backbone.history.start();
    }
  }
}

export default MergeRequestsTable;