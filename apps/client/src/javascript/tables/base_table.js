require("jquery-resizable-dom");
require("datatables.net");

import Websocket from "../modules/websocket";
import Syntax from "../modules/syntax";

class BaseTable {
  constructor(columnHeadingToHelp) {
    this.commitsCache = [];
    this.headingToHelp = columnHeadingToHelp;
    this.sortColumn = 2;
    this.selectedMR = null;

    /*eslint-disable no-unused-vars*/
    this.infoCallback = function (settings, start, end, max, total, pre) {
      /*eslint-enable no-unused-vars*/
      return `Showing ${start} to ${end} of ${total} commits`;
    };
  }

  initializeTable(selector, data, columns, infoCallback, clickCallback, sortColumn) {
    var self = this;
    let table = $(selector).DataTable({
      searching: false,
      scrollX: "100%",
      autoWidth: false,
      paging: false,
      order: [[sortColumn, "asc"]],
      data: data,
      columns: columns,
      infoCallback: infoCallback,
      initComplete: function () {
        $("thead th").each(function () {
          let $th = $(this);
          $th.attr("title", self.displayText($th.text()));
        });

        self.addTableDataTooltip();
      }
    });
    table.on("click", "tbody tr", clickCallback);
  }

  displayText(type) {
    let help = this.headingToHelp[type];
    if (typeof help !== "undefined") {
      return help;
    } else {
      return type;
    }
  }

  addTableDataTooltip() {
    $("tbody td").each(function () {
      let $td = $(this);
      $td.attr("title", $td.text());
    });
  }

  subscribeToUpdateTable(event, selector, headers) {
    var self = this;
    Websocket.on(event, function (data) {
      console.log("In " + event + "....");
      $(function () {
        _.defer(function () {
          self.updateTable(selector, headers, data);
        });
      });
    });
  }

  /*eslint-disable no-unused-vars*/
  updateTable(selector, headers, data) {
    throw new Error("Update table not implemented in sub class");
  }

  subscribeToHighlightFiles() {
    Websocket.on("highlight_files", function () {
      console.log("highlight_files.");

      $(".panel-files .wrapper").scrollTop(0);
      $("#files_list li").each(function () {
        let ref = $(this);
        setTimeout(function () {
          let lines = [];
          ref.find("td.line-data:not(.hunk) code").each(function () {
            lines.push({ ref: $(this), code: $(this).text() });
          });
          setTimeout(function () {
            let syntax = new Syntax();
            let lang = Syntax.getLanguage(ref.attr("class"));
            for (let line of lines) {
              setTimeout(function () {
                let newLine = syntax.highlight(line.code, lang);
                line.ref.html(newLine.value);
              }, 1);
            }
          }, 1);
        }, 1);
      });
    });
  }
}

export default BaseTable;