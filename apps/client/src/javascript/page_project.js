"user strict";

var core = require("./modules/core");
var Calendar = require("./modules/calendar");
$(function() {
  $("#nav_dropdown_projects").dropit({submenuEl: "div"});
});
//private functions
var snapshotList = [];
function toggle_delete_button(snapshots){
  if(snapshots.length == 0) {
    $("#btn_delete_snapshot").css({"visibility":"hidden"});
  } else{
    $("#btn_delete_snapshot").css({"visibility":"visible"});
  }
}

function remove_snapshot_from_list(snapshotID, snapshotList){
  let index = snapshotList.indexOf(snapshotID);
  if (index > -1) {
    snapshotList.splice(index, 1);
  }
  toggle_delete_button(snapshotList);
}
function add_to_delete_list(snapshotList, snapshotID){
  if(snapshotID == "select_all"){
    return;
  }
  let index = snapshotList.indexOf(snapshotID);
  if(index == -1) {
    snapshotList.push(snapshotID);
  }
  toggle_delete_button(snapshotList);
}

function add_all_snapshots_to_list(snapshotList) {
  $("input:checkbox").each(function() {
    let snapshotID = $(this).attr("id");
    add_to_delete_list(snapshotList, snapshotID);
  });
  toggle_delete_button(snapshotList);
}

function remove_all_snapshot_from_list(snapshotList) {
  $("input:checkbox").each(function() {
    $(this).prop("checked", false);
    let snapshotId = $(this).attr("id");
    remove_snapshot_from_list(snapshotId, snapshotList);
  });
  toggle_delete_button(snapshotList);
}

function get_last_snapshot_dates(calendarFrom, calendarTo, lastStartDate,
                                 lastEndDate) {
  let numStartDate = parseInt(lastStartDate);
  let numEndDate = parseInt(lastEndDate);
  if( !Number.isInteger(numStartDate) || !Number.isInteger(numEndDate)) {
    return;
  }else{
    numStartDate *= 1000;
    numEndDate *= 1000;
  }
  calendarFrom.setDate(numStartDate, false);
  calendarTo.setDate(numEndDate, false);
}

//end

window.websocket.on(window.websocket.READY, function() {
  $(function() {
    let calendarFrom = Calendar.createStartDatePickr("#datetime_from");
    let calendarTo = Calendar.createEndDatePickr("#datetime_to");
    $("#btn_create_snapshot").click(function() {
      let timestampFrom = $(calendarFrom.input).val();
      let timestampTo = $(calendarTo.input).val();
      let snapshotName = $("#input_name").val();
      let requestBody = {
        "name": snapshotName,
        "timestamp_from": timestampFrom,
        "timestamp_to": timestampTo
      };
      $.modal.close();
      window.websocket.sendRequest("createSnapshot", requestBody);
    });

    $("#btn_open_modal").click(function() {
      let lastStartDate = $("#last_start_date").attr("value");
      let lastEndDate = $("#last_end_date").attr("value");
      get_last_snapshot_dates(calendarFrom, calendarTo, 
                              lastStartDate, lastEndDate);  
      window.modal.showPersistent("#modal_create_snapshot");
    });

    $("#btn_delete_snapshot").click(function() {
      let requestBody = {
        "snapshot_id": snapshotList
      };
      if(snapshotList.length > 0){
        window.websocket.sendRequest("deleteSnapshots", requestBody);
        snapshotList = [];
        toggle_delete_button(snapshotList);
      }
    });

    $("#select_all").click(function(){  //"select all" change
      if($(this).prop("checked") == true){
        $("input:checkbox").prop("checked", 
          $(this).prop("checked", add_all_snapshots_to_list(snapshotList)));
      }else{
        remove_all_snapshot_from_list(snapshotList);
      }
    });
  });
});

window.websocket.on("list_update", function() {
  $(function() { 
    let calendarFromEdit; 
    let calendarToEdit; 
    $(".button-edit").click(function() {
      let snapshotID = $(this).attr("id");
      window.modal.showPersistent("#modal_edit_snapshot"+snapshotID);
      calendarFromEdit = Calendar.create("#datetime_from_edit"+snapshotID);
      calendarToEdit = Calendar.create("#datetime_to_edit"+snapshotID);
    });

    $("button[id^='btn_recreate_snapshot']").click(function() {
      let snapshotID = $(this).attr("id").replace(/btn_recreate_snapshot/, "");
      let timestampFromEdit = $(calendarFromEdit.input).val();
      let timestampToEdit = $(calendarToEdit.input).val();
      let snapshotNameEdit = $("#input_name_edit"+snapshotID).val();
      let requestBody = {
        "name": snapshotNameEdit,
        "timestamp_from": timestampFromEdit,
        "timestamp_to": timestampToEdit,
        "snapshot_id": snapshotID
      };
      $.modal.close();
      window.websocket.sendRequest("editSnapshot", requestBody);
    });

    $("input:checkbox").click(function(){
      let snapshotID = $(this).attr("id");
      if($(this).prop("checked") == true){
        add_to_delete_list(snapshotList, snapshotID);
      }else{ 
        $("#select_all").prop("checked", false);
        remove_snapshot_from_list(snapshotID, snapshotList);
      }
    });
  });
});


core.init();
