var core = require("./modules/core");

import MergeRequestsTable from "./tables/merge_requests_table";
import CommitsTable from "./tables/commits_table";

core.init();

let mergeRequestsTableColumnHeadingToHelp = {
  WS: "Number of lines that were changed by just whitespace, such as tabs, spaces and linefeeds.",
  Sntx: "Number of lines that were changed by just syntax, such as replacing '.' with '->'.",
  Mv: "Number of lines that were moved (relocated inside the file).",
  Edit: "Number of lines that were changed by small line edits (changing 4 or fewer characters on the line).",
  Ins: "Number of lines that were inserted (new line added).",
  Del: "Number of lines that were deleted (previous line has been removed).",
  Score: "Total score for all changes in this merge request / commit.",
  CScore: "Total score for all changes for all commits in this merge request made by the current student."
};

let mergeRequestsTableHeader = [
  {
    data: "title_msg",
    title: "Title & Message",
    className: "ttl"
  },
  {
    data: "source_branch",
    title: "Src Branch",
    className: "right sb"
  },
  {
    data: "date",
    title: "Date",
    className: "right date"
  },
  {
    data: "file_num",
    title: "Files &Delta;",
    sType: "numeric",
    className: "right loc"
  },
  {
    data: "loc",
    title: "LoC &Delta;",
    sType: "numeric",
    className: "right fn"
  },
  {
    data: "whitespace",
    title: "WS",
    sType: "numeric",
    className: "right whitespace"
  },
  {
    data: "syntax",
    title: "Sntx",
    sType: "numeric",
    className: "right syntax"
  },
  {
    data: "moves",
    title: "Mv",
    sType: "numeric",
    className: "right moves"
  },
  {
    data: "refactors",
    title: "Edit",
    sType: "numeric",
    className: "right refactors"
  },
  {
    data: "insertions",
    title: "Ins",
    sType: "numeric",
    className: "right insertions"
  },
  {
    data: "deletions",
    title: "Del",
    sType: "numeric",
    className: "right deletions"
  },
  {
    data: "sum",
    title: "Score",
    sType: "num",
    className: "right sum"
  },
  {
    data: "commitsum",
    title: "CScore",
    sType: "num",
    className: "right commitsum"
  },
  {
    data: "url",
    title: "URL",
    className: "mid url"
  }
];

let mergeRequestsTable = new MergeRequestsTable(mergeRequestsTableColumnHeadingToHelp);
mergeRequestsTable.createRoutes();
mergeRequestsTable.subscribeToUpdateTable("update_mr", "#table_mr", mergeRequestsTableHeader);
mergeRequestsTable.subscribeToHighlightFiles();


let commitsColumnHeadingToHelp = {
  WS: "Number of lines that were changed by just whitespace, such as tabs, spaces and linefeeds.",
  Sntx: "Number of lines that were changed by just syntax, such as replacing '.' with '->'.",
  Mv: "Number of lines that were moved (relocated inside the file).",
  Edit: "Number of lines that were changed by small line edits (changing 4 or fewer characters on the line).",
  Ins: "Number of lines that were inserted (new line added).",
  Del: "Number of lines that were deleted (previous line has beenremoved).",
  Score: "User's score earned on this commit."
};

let commitsTableHeader = [
  {
    data: "title_msg",
    title: "Title & Message",
    className: "ttl"
  },
  {
    data: "author",
    title: "Author",
    className: "auth"
  },
  {
    data: "date",
    title: "Date",
    className: "right date"
  },
  {
    data: "add",
    title: "(+)",
    className: "right add"
  },
  {
    data: "del",
    title: "(-)",
    className: "right del"
  },
  {
    data: "net",
    title: "&Delta;",
    className: "right net"
  },
  {
    data: "whitespace",
    title: "WS",
    className: "right whitespace"
  },
  {
    data: "syntax",
    title: "Sntx",
    className: "right syntax"
  },
  {
    data: "moves",
    title: "Mv",
    className: "right moves"
  },
  {
    data: "refactors",
    title: "Edit",
    className: "right refactors"
  },
  {
    data: "insertions",
    title: "Ins",
    className: "right insertions"
  },
  {
    data: "deletions",
    title: "Del",
    className: "right deletions"
  },
  {
    data: "sum",
    title: "Score",
    className: "right sum"
  }
];

let commitsTable = new CommitsTable(commitsColumnHeadingToHelp);
commitsTable.createRoutes();
commitsTable.subscribeToUpdateTable("update_mr", "#table_commits", commitsTableHeader, true);
commitsTable.subscribeToHighlightFiles();
commitsTable.subscribeToReplaceCommits();


$(function () {
  //Hide menu:
  $("body").addClass("collapsed");

  $("#nav_dropdown_members").dropit({ submenuEl: "div" });

  $("#panel_lists").resizable({
    handleSelector: "#divider",
    resizeHeight: false
  });
});
