var path = require("path");
var webpack = require("webpack");

module.exports = {
  entry: {
    projects: "./src/javascript/page_projects.js",
    project: "./src/javascript/page_project.js",
    snapshot: "./src/javascript/page_snapshot.js",
    member_mr: "./src/javascript/page_member_mr.js",
    member_graphs: "./src/javascript/page_member_graphs.js",
    member_notes: "./src/javascript/page_member_notes.js",
    member_scores: "./src/javascript/page_member_scores.js",
    profile_access_tokens: "./src/javascript/page_profile_access_tokens.js",
    manager_projects: "./src/javascript/page_manager_projects.js",
    manager_new: "./src/javascript/page_manager_new.js"
  },
  output: {
    path: path.join(__dirname, "static/javascript/dist"),
    filename: "[name].bundle.js"
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: "eslint-loader",
        exclude: [
          /node_modules/,
          path.resolve(__dirname, "src/javascript/lib/ws.min.js"),
        ]
      }
    ],
    loaders: [
      {
        test: require.resolve("jquery"),
        loader: "expose-loader?$!expose-loader?jQuery"
      },
      {
        test: require.resolve("underscore"),
        loader: "expose-loader?_"
      },
      {
        test: require.resolve("backbone"),
        loader: "expose-loader?Backbone"
      },
      {
        test: /\.js$/,
        exclude: [
          /node_modules/,
          path.resolve(__dirname, "src/javascript/lib/ws.min.js"),
        ],
        loader: "babel-loader",
        query: {
          presets: ["es2015"]
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: false,
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ]
};
