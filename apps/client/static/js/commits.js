"use strict";

var currentUser = "";


$(document).ready(function() {
    var inSnapshot = false;
    currentUser = "current user";
    
    /***********************
     * INIT *
     ***********************/
    var gitGraph = new GitGraph({template: getGraphConfig()});

    /***********************
     * BRANCHS AND COMMITS *
     ***********************/
    // Create branch named "master"
    var master = gitGraph.branch("master", inSnapshot);

    // Commits OUTSIDE snapshot range
    addCommit(master, "commit before snapshot 0", "current user", inSnapshot);
    addCommit(master, "commit before snapshot 1", "dave", inSnapshot);

    // Tag to start snapshot range
    addMajorTag(master, "Start Snapshot", "Start", inSnapshot);
    inSnapshot = true;

    // Commits IN snapshot range
    addCommit(master, "commit in snapshot 1", "dave", inSnapshot);
    addCommit(master, "commit in snapshot 2", "jan", inSnapshot);

    // Create a new "uiBranch" branch from "master"
    var uiBranch = master.branch("UIChanges", inSnapshot);
	addBranchTag(uiBranch, "UIChanges");
		
    // Commits IN uiBranch
    addCommit(uiBranch, "uiBranch commit 1", "dave", inSnapshot);

    // Create a new "uiBugFix" branch from "uiBranch"
    var uiBugFix = uiBranch.branch("UIBugFix", inSnapshot);
	addBranchTag(uiBugFix, "UIBugFix");
	
    // Commit IN "uiBugFix"
    addCommit(uiBugFix, "uiBugFix commit  that is never merged", "steve", inSnapshot);

    // Commit IN "master"
    addCommit(master, "commit in master", "dave", inSnapshot);
    addCommit(master, "My commit 1", "current user", inSnapshot);

    // Commits IN uiBranch
    addCommit(uiBranch, "uiBranch commit 3", "dave", inSnapshot);
    addCommit(uiBranch, "uiBranch commit 4", "current user", inSnapshot);

    // Merge uiBranch to master
    addMerge(uiBranch, master, "", "current user", inSnapshot);

    // Create a "testBranch" branch from "master"
    var testBranch = gitGraph.branch("Test", inSnapshot);
	addBranchTag(testBranch, "Test");
	
    addCommit(testBranch, "test commit 1", "current user", inSnapshot);
    addCommit(testBranch, "test commit 2", "current user", inSnapshot);


    // Merge master to testBranch
    addMerge(master, testBranch, "Update new feature", "steve", inSnapshot);

    // Then, continue committing on the "testBranch" branch
    addCommit(testBranch, "testBranch commit 3", "dave", inSnapshot);
    addCommit(testBranch, "testBranch commit 4", "steve", inSnapshot);

    //Tag to end snapshot range
    addMajorTag(master, "End Snapshot", "End", inSnapshot);
    inSnapshot = false;

    // Merge testBranch branch to master
    master.checkout();
    addMerge(testBranch, master, "", "steve", inSnapshot);

    // Commit to master	
    addCommit(master, "commit in master", "steve", inSnapshot);

    //Rescale the graph because it is too big
    rescale();

    /***********************
     *       EVENTS        *
     ***********************/
    gitGraph.canvas.addEventListener("commit:mouseover", function(event) {
        //May do sth in the future
    });
});

function addMajorTag(branch, msg, tag) {
    branch.commit({
        message: msg,
        tag: tag,
        tagColor: "#ffffff",
        dotColor: "red",
        dotStrokeWidth: 15,
        messageHashDisplay: false,
        messageAuthorDisplay: false,
    });
}

function addBranchTag(branch, tag) {
    branch.commit({
        message: "New branch",
        tag: tag,
        tagColor: "#ffffff",
        dotColor: branch.color,
        dotStrokeWidth: 5,
        messageHashDisplay: false,
        messageAuthorDisplay: false,
    });
}

function addMerge(fromBranch, toBranch, msg, user, inSnapshot) {
    var dot;
    var colour;

    if (!inSnapshot) {
        dot = 2;
        colour = "black";
    } else if (user == currentUser) {
        dot = 10;
        colour = "white";
    } else {
        dot = 5;
        colour = "grey";
    }

    fromBranch.merge(toBranch, {
        dotColor: colour,
        dotSize: dot,
        dotStrokeWidth: 10,
        messageHashDisplay: false,
        messageAuthorDisplay: true,
        message: msg,
        author: user,
        onClick: function(commit) {
            //todo: so sth
        }
    });
}

function addCommit(branch, msg, user, inSnapshot) {
    var dot;
    var colour;

    if (!inSnapshot) {
        dot = 2;
        colour = "black";
    } else if (user == currentUser) {
        dot = 10;
        colour = "white";
    } else {
        dot = 5;
        colour = "grey";
    }

    branch.commit({
        dotColor: colour,
        dotSize: dot,
        dotStrokeWidth: 10,
        messageHashDisplay: false,
        messageAuthorDisplay: true,
        messageColor: "#000000",
        message: msg,
        author: user,
        onClick: function(commit) {
            //todo: so sth
        }
    });
}

function getGraphConfig() {
    
    var myTemplateConfig = {
        colors: ["#808080", "#993399", "#e6bd19", "#669933", "#00ffff", "#8000ff", "#ff00ff", "#ff8000", "#ff00bf", "#00bfff"], // branches colors, 1 per column
        branch: {
            lineWidth: 8,
            spacingX: 50
        },
        commit: {
            spacingY: -40,
            dot: {
                size: 12
            },
            message: {
                displayAuthor: true,
                displayBranch: false,
                displayHash: false,
                font: "normal 15pt Arial"
            },
            tooltipHTMLFormatter: function(commit) {
                return "<b>" + commit.sha1 + "</b>" + ": " + commit.message;
            }
        }
    };
    
    return new GitGraph.Template(myTemplateConfig);
}

function rescale() {
    var g = document.getElementById('gitGraph');
    var w = +g.style.width.slice(0, -2) / 1.5;
    var h = +g.style.height.slice(0, -2) / 1.5;
    g.style.width = w + 'px';
    g.style.height = h + 'px';
};