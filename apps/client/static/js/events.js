"use strict";

var wsModule = {};
$(wsModule).on("init", function () {
  $(wsModule).on("reply", function (event, message) {
    alert("Relayed from server: '" + message + "'!");
  });

  $("#btn_relay").click(function () {
    var message = $("#username").val();
    console.log("click: " + message);
    event_relayMessage({
      "data": Number(message)
    });
  });
});

$j(window).on("beforeunload", function () {
  wsModule.close();
});
