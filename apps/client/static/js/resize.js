"use strict";

function initHorizontalSplitterResize() {
  $(".panel-left").resizable({
    handleSelector: ".splitter",
    resizeHeight: false
  });
}

function initVerticalSplitterResize() {
   $(".panel-top").resizable({
   handleSelector: ".splitter-horizontal",
   resizeWidth: false
 });
}

