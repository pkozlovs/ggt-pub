"use strict";

$(document).ready(function () {
  $('#list_projects').SumoSelect({
    placeholder: 'Projects',
    search: true,
    searchText: 'Search for project',
    noMatch: 'No projects found'
  });
  
  $("#list_projects").prop("selectedIndex", -1);
  
  document.getElementById("list_projects").onchange = function() {
    if (this.selectedIndex!==0) {
      window.location.href = this.value;
    }        
  };
});