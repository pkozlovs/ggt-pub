"use strict";

function Modal_openDefault(Selector) {
  $(Selector).modal({
    showClose: false,
    closeExiting: false
  });
}

function Modal_close() {
  $.modal.close();
}