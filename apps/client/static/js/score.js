$(document).ready(function () {  
  var tableScore = $("#scoreExample").DataTable({
    searching: false,
    "paging": false,
    "order": [],
    "columns": [
      { "width": "15%" },
      { "width": "29%" },
      { "width": "8%" },
      { "width": "8%" },
      { "width": "8%" },
      { "width": "8%" },
      { "width": "8%" },
      { "width": "8%" },
      { "width": "8%" }
    ]
  });
  
});