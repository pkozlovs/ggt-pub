function setUpTable(exampleName) {
  // DataTable
  var table = $(exampleName).DataTable({
    "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
    "iDisplayLength": 50
  });
  
    setUpTableWorker(exampleName, table);
};

function setUpTableWithTotal(exampleName, totalIdx) {
  // DataTable
  var table = $(exampleName).DataTable({
    "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
    "iDisplayLength": 50,
    "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        for (var i = 0; i < totalIdx.length; i++) {
            // Total over all pages
            total = api
                .column( totalIdx[i]  )
                .data()
                .reduce( function (a, b) {
                    return +a + +b;
                }, 0 );

            // Update footer
            $( api.column( totalIdx[i]  ).footer() ).html(total);
        }
    }
  });
    
    setUpTableWorker(exampleName, table);
};

function setUpTableWorker(exampleName, table) {
  // Set up filter box
  $(exampleName + ' thead th').each(function () {
    var title = $(exampleName + ' thead th').eq($(this).index()).text();
    $(this).html(title + '<br> <input type="text" placeholder="Filter" size="5"/>');
  });
    
  // Apply the search
  table.columns().eq(0).each(function (colIdx) {
    $('input', table.column(colIdx).header()).on('keyup change', function () {
      table
        .column(colIdx)
        .search(this.value)
        .draw();
    });
  });
};
