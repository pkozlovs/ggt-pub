"use strict";

// $(document).ready(function () {
//   $(".dd_list").dropit({
//     submenuEl: 'div'
//   });
// });

function activityChart(mrData, commitsData) {
  var data = mrData;
  if (mrData.lenghth == 0 && commitsData.lenghth == 0) {
    data = [];
  } else {
    var leftOverCommits = [];
    for (var i = 0; i < commitsData.length; i++) {
      var isLeftOver = true;
      for (var j = 0; j < data.length; j++) {
        if (data[j].date == commitsData[i].date) {
          data[j].value2 = commitsData[i].value;
          isLeftOver = false;
          break;
        } 
      }

      if (isLeftOver) {
        leftOverCommits.push(commitsData[i]);
      }
    }

    for (var i = 0; i < leftOverCommits.length; i++) {
      data.push({
        "date": leftOverCommits[i].date,
        "value2": leftOverCommits[i].value
      });
    }

    data.sort(function(a, b) {
      var x = a["date"]; var y = b["date"];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
  
  activityChartConfig(mrData);
}

function activityChartConfig(data) {
  var chart = AmCharts.makeChart("activityChart", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "mouseWheelZoomEnabled":true,
    "dataDateFormat": "YYYY-MM-DD",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true,
        "stackType": "regular"
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "lineColor": "#5299e5",
        "type": "column",
        "fillAlphas": 0.5,
        "lineAlpha": 1,
        "valueField": "value",
        "balloonText": "<span style='font-size:12px;'><b>[[date]]</b><br>Merge Requests: <b>[[value]]</b></span>",
        "title": "MR"
    },
    {
        "lineColor": "#f48f42",
        "type": "column",
        "fillAlphas": 0.5,
        "lineAlpha": 1,
        "valueField": "value2",
        "balloonText": "<span style='font-size:12px;'><b>[[date]]</b><br>Commits: <b>[[value]]</b></span>",
        "title": "Commits"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 30,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": false
    },
    "dataProvider": data
  });

  chart.addListener("dataUpdated", zoomChart);

  var zoomChart = function() {
    chart.zoomToIndexes(data.length - 40, data.length - 1);
  };
  amChartsCheckEmptyData(chart);
}

function notesChart(data) {
  var chart = AmCharts.makeChart("notesChart", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "mouseWheelZoomEnabled":true,
    "dataDateFormat": "YYYY-MM-DD",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true,
        "stackType": "regular"
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "lineColor": "#5299e5",
        "type": "column",
        "fillAlphas": 0.5,
        "lineAlpha": 1,
        "valueField": "value",
        "balloonText": "<span style='font-size:12px;'><b>[[date]]</b><br>Notes: <b>[[value]]</b></span>",
        "title": "Notes"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 30,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": false
    },
    "dataProvider": data
  });

  chart.addListener("dataUpdated", zoomChart);

  var zoomChart = function() {
    chart.zoomToIndexes(data.length - 40, data.length - 1);
  };
  
  amChartsCheckEmptyData(chart);
}

function amChartsCheckEmptyData (chart) {
  if (chart.dataProvider == undefined || chart.dataProvider.length == 0) {
    // Add label to let users know the chart is empty
    chart.valueAxes[0].minimum = 0;
    chart.valueAxes[0].maximum = 100;
    var dataPoint = {
      dummyValue: 0
    };
    dataPoint[chart.categoryField] = '';
    chart.dataProvider = [dataPoint];
    chart.addLabel(0, '50%', 'No data in this snapshot', 'center');

    chart.chartDiv.style.opacity = 0.5;
    chart.validateNow();
  }
};