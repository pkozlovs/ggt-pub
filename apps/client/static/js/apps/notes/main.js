$(document).ready(function() {
  // $(".dd_list").dropit({
  //   submenuEl: 'div'
  // });

  console.log($(".panel-row").outerHeight());

  var tableNotes = $("#table_notes").DataTable({
    searching: false,
    // "scrollY": 1,
    scrollX: "100%",
    "scrollCollapse": true,
    "bLengthChange": false,
    "paging": false,
    "order": [],
    "columns": [
      { "width": "20%" },
      { "width": "10%" },
      { "width": "5%" },
      { "width": "5%" },
      { "width": "4%" },
    ],
    "infoCallback": function( settings, start, end, max, total, pre ) {
      var api = this.api();
      totalWordCount = api.column(4).data().reduce(function(a, b) {
        return +a + +b;
      }, 0);
      console.log(totalWordCount)

      return "Showing " + start + " to " + end + " of " + total + " notes" +
        "; Total word count: " + totalWordCount;
    }
  });
  
  // $("#table_notes_wrapper div.dataTables_scrollBody").css({"max-height":1});
  // $("#table_notes_wrapper div.dataTables_scrollBody").height(1);
  
 

  // $("#table_issues_wrapper div.dataTables_scrollBody").css({"max-height":1});
  // $("#table_issues_wrapper div.dataTables_scrollBody").height(1);

  // resizeRowTables();
  $(window).resize(function() {
    // resizeRowTables();
  });

});

function resizeRowTables() {
  console.log("RESIZED");
  console.log($(".panel-row").outerHeight());
  var panelHeight = $(".panel-row").first().outerHeight();
  var wrapperHeight = 68;//$("div.dataTables_wrapper").height();
  var scrollY = panelHeight - wrapperHeight;
  $("#table_notes_wrapper div.dataTables_scrollBody").css({"max-height":scrollY});
  $("#table_notes_wrapper div.dataTables_scrollBody").height(scrollY);

  var panelHeight_l = $(".panel-row").last().outerHeight();
  var scrollY_l = panelHeight_l - wrapperHeight;
  $("#table_issues_wrapper div.dataTables_scrollBody").css({"max-height":scrollY_l});
  $("#table_issues_wrapper div.dataTables_scrollBody").height(scrollY_l);

  $(".panel-row").hide().show(0);
}