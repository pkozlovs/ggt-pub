var topstack;

$(document).ready(function() {
  $(".dd_list").dropit({
    submenuEl: 'div'
  });

  $("#panel_lists").resizable({
    handleSelector: "#divider",
    resizeHeight: false
  });
  // $(".wrapper-left").trigger("resize");

  console.log($(".panel-row").outerHeight());
  console.log($(".panel-row").outerWidth());

  var tableMR = $("#table_mr").DataTable({
    searching: false,
    // "scrollY": 1,
    scrollX: "100%",
    "scrollCollapse": true,
    "bLengthChange": false,
    "paging": false,
    "order": []
,    "columns": [
      { "width": "20%" },
      { "width": "20%" },
      { "width": "14%" },
      { "width": "8%" },
      { "width": "7%" },
      { "width": "7%" },
    ],
    "infoCallback": function( settings, start, end, max, total, pre ) {
      return "Showing " + start + " to " + end + " of " + total + " merge requests";
    }
  });
  $("#table_mr tbody tr").first().addClass("selected");

  var tableCommits = $("#table_commits").DataTable({
    searching: false,
    // "scrollY": 1,
    scrollX: "100%",
    "scrollCollapse": true,
    "bLengthChange": false,
    "paging": false,
    "columns": [
      { "width": "18%" },
      { "width": "15%" },
      { "width": "14%" },
      { "width": "20%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" }
    ],
    "infoCallback": function( settings, start, end, max, total, pre ) {
      return "Showing " + start + " to " + end + " of " + total + " commits";
    }
  });

  // $(window).resize(function() {
  //   var panelHeight = $(".panel-row").first().outerHeight();
  //   var wrapperHeight = 68;//$("div.dataTables_wrapper").height();
  //   var scrollY = panelHeight - wrapperHeight;
  //   $("#table_mr_wrapper div.dataTables_scrollBody").css({"max-height":scrollY});
  //   $("#table_mr_wrapper div.dataTables_scrollBody").height(scrollY);

  //   var panelHeight_l = $(".panel-row").last().outerHeight();
  //   var scrollY_l = panelHeight_l - wrapperHeight;
  //   $("#table_commits_wrapper div.dataTables_scrollBody").css({"max-height":scrollY_l});
  //   $("#table_commits_wrapper div.dataTables_scrollBody").height(scrollY_l);

  //   $(".panel-row").hide().show(0);
  // });
  $(window).resize();

  $("code").each(function(i, block) {
    hljs.highlightBlock(block);
  });

  
  $(wsWatcher).on("init", function() {
    console.log("Server: init");
    
    $('#table_mr tbody').on( 'click', 'tr', function () {
      tableMR.$('#table_mr tr.selected').removeClass('selected');
      $(this).addClass('selected');

      MergeRequestId = $(this).data("ggs-id");
      console.log(MergeRequestId);
      request_getMergeRequestCommits({"id": MergeRequestId});
    });

    $('#table_commits tbody').on( 'click', 'tr', function () {
      tableCommits.$('#table_commits tr.selected').removeClass('selected');
      $(this).addClass('selected');

      CommitId = $(this).data("ggs-id");
      console.log(CommitId);
      request_getCommitFileDiffs({"id": CommitId});
    });
  });
  
  $(wsWatcher).on("purge_tables", function() {
    tableCommits.destroy();
  });
  $(wsWatcher).on("reinit_tables", function() {
    tableCommits = initCommitTable();
    $(window).resize();
    $('#table_commits .dataTables_scrollBody').scrollTop(0);
  });

  $(wsWatcher).on("highlight_diffs", function() {
    $('.panel-changes').scrollTop(0);
    $("td.line-data:not(.hunk) code").each(function(i, block) {
      var hsline = self.hljs.highlight("java", $(block).text(), false, topstack);
      console.log(topstack);
      topstack = hsline.top;
      $(this).html(hsline.value);
    });
  });
});

function initCommitTable() {
  return $("#table_commits").DataTable({
    searching: false,
    // "scrollY": 1,
    scrollX: "100%",
    "scrollCollapse": true,
    "bLengthChange": false,
    "paging": false,
    "columns": [
      { "width": "18%" },
      { "width": "15%" },
      { "width": "14%" },
      { "width": "20%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" }
    ]
  });
}