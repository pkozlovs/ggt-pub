$(document).ready(function() {

  var tableScores = $("#table_scores").DataTable({
    searching: false,
    scrollX: "100%",
    "scrollCollapse": true,
    "bLengthChange": false,
    "paging": false,
    "order": [],
    "columns": [
      { "width": "35%" },
      { "width": "16%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" },
      { "width": "7%" },
    ],
    "infoCallback": function( settings, start, end, max, total, pre ) {
      var api = this.api();
      totalScore = api.column(8).data().reduce(function(a, b) {
        return +a + +b;
      }, 0);

      return "Showing " + start + " to " + end + " of " + total + " score" +
        "; Total score: " + totalScore.toFixed(2);
    }
  });
  
  $(window).resize(function() {
    // resizeRowTables();
  });

});