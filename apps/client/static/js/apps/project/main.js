$(document).ready(function() {
  $(".dd_list").dropit({
    submenuEl: 'div'
  });

  datetimeConfig = {
    wrap: true,
    enableTime: true,
    dateFormat: "U",
//    dateFormat: "Y-m-dTH:i:S-08:00",
    altInput: true,
    altFormat: "F j, Y h:i K",
    
  };
  
  Calendar_dateFrom = flatpickr("#datetime_from", datetimeConfig);
  Calendar_dateTo = flatpickr("#datetime_to", datetimeConfig);
  
  $(wsWatcher).on("init", function() {
    $("#btn_open_create_snapshot").click(function() {
      Modal_openDefault("#modal_create_snapshot");
    });
    
    $("#btn_create_snapshot").click(function() {
      datetimeFrom = $(Calendar_dateFrom.input).val();
      datetimeTo = $(Calendar_dateTo.input).val();
      snapshotName = $("#input_name").val();
      requestBody = {
        "name": snapshotName,
        "timestamp_from": datetimeFrom,
        "timestamp_to": datetimeTo
      }
      request_createSnapshot(requestBody);
      Modal_close();
    });
    
  });
});