$(document).ready(function() {
  $(".dd_list").dropit({
    submenuEl: 'div'
  });

  $("select").SumoSelect();
  
  $(wsWatcher).on("init", function() {
    console.log("Server: ok");

    $("#btn_apply").click(function() {
      request_merge({"pairs": getPairs()});
    });
  });
});

function getPairs() {
  var pairs = [];
  $("select").each(function(i) {
    pairs.push({
      "author": $(this).data("ggs-author"),
      "member": $(this).val()
    });
  });
  console.log(pairs);
  return pairs;
}
