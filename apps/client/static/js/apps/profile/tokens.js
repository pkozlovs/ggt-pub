$(document).ready(function() {
  $(wsWatcher).on("init", function() {
    $("#btn_update").click(function() {
      request_updateTokens({"tokens": getTokens()});
    });
  });
});

function getTokens() {
  var tokens = [];
  $("input").each(function() {
    var source = $(this).data("source");
    var value = $(this).val();      
    tokens.push({"source": source, "value": value});
  });
  return tokens;
}