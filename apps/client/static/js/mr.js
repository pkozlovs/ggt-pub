"use strict";

$(document).ready(function () {
  initHorizontalSplitterResize();
  setUpTableWithTotal('#mrExample', [3,4]);
  setUpTableWithTotal('#mrCommitsExample', [3,4,5]);
});