-module(test_diff_analysis_project_parser).
-export([parse/1]).
-include_lib("apps/snapshot_gen/include/test_diff_analysis_project.hrl").

parse(File) ->
  % open the file
  % convert contents to erlang
  % parse
  % return structure.
  extract_map(File).

-spec extract_map(path()) -> config().
extract_map(File) ->
  {ok, Terms} = file:consult(File),
  #{
    title := Title,
    username := Username,
    token := Token,
    source := Source,
    file_location := FilePrefix,
    initial_commit := InitialFiles,
    actions := Actions
  } = maps:from_list(Terms),
  {ok, AlphaNumeric} = re:compile("[^a-zA-Z0-9]"),
  ProjFilename = string:to_lower(re:replace(Title, AlphaNumeric, "_", 
                                       [global, {return, list}])),
  ProjDir = ?PROJ_DIR(ProjFilename),
  Config = #config{
    title = Title, 
    username = Username,
    token = Token,
    source = Source,
    file_location = FilePrefix,
    project_dir = ProjDir,
    root_dir = env:join([?HOME, ?ROOT_DIR, ?TEST_DIR]),
    initial_commit = InitialFiles,
    actions = Actions
  },
  {ok, Config}.


