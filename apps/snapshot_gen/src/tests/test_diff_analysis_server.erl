-module(test_diff_analysis_server).
-include_lib("apps/api/include/api.hrl").
-export([start_link/1, init/1]).

-record(test_target, {
  project_id :: binary(),
  endpoint :: #endpoint{},
  username :: binary(),
  title :: binary()
}). % TODO: stop using this endpoint bullshish

-record(state, {
  test_target :: #test_target{},
  snapshot = undefined :: db_snapshot:self() | undefined,
  error = undefined :: any(),
  merge_requests = [] :: db_merge_request:self(),
  test_refs = undefined :: [{reference(), db_merge_request:self()}] 
                           | undefined,
  results = [] :: [{good, db_merge_request:self()} 
                  | {bad, db_merge_request:self(), Reason :: any()}]
}).

start_link(Args) ->
  {ok, erlang:spawn(?MODULE, init, [Args])}.

init(#{project_id:=ProjId, endpoint:=Endpoint, username:=Username, 
       title:=Title}) ->
  error_logger:info_msg("~p: Start New Snapshot~n", [?MODULE]),
  self() ! start_snapshot,
  Target = #test_target{
    project_id = ProjId, endpoint = Endpoint, username = Username,
    title = Title
  },
  State = #state{test_target=Target},
  process_flag(trap_exit, true),
  start_receive(State); 
init(#{snapshot:=Snapshot}) ->
  error_logger:info_msg("~p: Test Existing Snapshot~n", [?MODULE]),
  State = #state{snapshot=Snapshot},
  self() ! {snapshot_server, {complete, Snapshot}},
  process_flag(trap_exit, true),
  start_receive(State).

start_receive(S=#state{test_refs=[]}) ->
  terminate(normal, S);
start_receive(S=#state{test_refs=[{Ref, Mr}|Refs], error=undefined}) ->
  receive 
    {ok, {Ref, Good, Bad}}  -> 
      S1 = S#state{test_refs=Refs},
      S2 = handle_down(Mr, Good, Bad, S1),
      start_receive(S2);
    {'DOWN', _, process, _, normal} ->
      start_receive(S);
    {'DOWN', _Ref, process, _Pid, Reason} ->
      S1 = S#state{error=Reason},
      terminate(normal, S1)
  after 10000 ->
    S1 = S#state{error={error, {?MODULE, timeout}}},
    start_receive(S1)
  end;
start_receive(S=#state{error=undefined}) ->
  receive
    {snapshot_server, {complete, Snapshot}} ->
      S1 = test_snapshot(Snapshot, S),
      start_receive(S1);
    start_snapshot ->
      S1 = start_snapshot(S),
      start_receive(S1)
  end;
start_receive(S=#state{error=_Error}) ->
 terminate(normal, S). 

handle_down(Mr, Good, Bad, S=#state{results=Results}) ->
  Result = case Bad of
    [] -> {good, Mr, Good};
    Bad -> {bad, Mr, Good, Bad}
  end,
  NewResults = [Result | Results],
  S#state{results = NewResults}.

terminate(normal, #state{error=undefined, results=Results}) ->
  print_results(Results),
  ok;
terminate(normal, #state{error=Error}) ->
  error_logger:error_report([{?MODULE, terminate}, {reason, Error}]),
  ok; 
terminate(Reason, State) ->
  error_logger:error_report([{reason, Reason}, {state, State}]),
  ok.

%%%
%%% Private API
%%%
start_snapshot(S=#state{test_target=Target}) ->
  ProjId = Target#test_target.project_id,
  Endpoint = Target#test_target.endpoint,
  Username = Target#test_target.username,
  Title = Target#test_target.title,
  Start = {{2016, 1, 1}, {0, 0, 0}},
  End = {{2118, 1, 1}, {0,0,0}},
  delete_old_snapshots(gitlab, ProjId), % TODO: remove hard-coded gitlab
  {ok, Server} = snapshot_generator:gen_snapshot(ProjId, Endpoint, Start, End, 
                                                 Username, Title),
  snapshot_server:add_observer(Server, process, self()),
  S.

delete_old_snapshots(Source, ProjId) ->
  MaybeProject = db:get_project(Source, ProjId),
  MaybeSnapshots = result:flatMap(MaybeProject, fun(Project) ->
    Id = db_project:id(Project),
    db:get_snapshots({project_id, Id})
  end),
  result:flatMap(MaybeSnapshots, fun(Snapshots) ->
    SnapIds = lists:map(fun db_snapshot:id/1, Snapshots),
    db:delete_snapshots({snapshot_ids, SnapIds})
  end).

test_snapshot(Snapshot, S=#state{}) ->
  S1 = S#state{snapshot=Snapshot},
  S2 = get_merge_requests(S1),
  spawn_tests(S2).
  
get_merge_requests(S=#state{error=undefined, snapshot=Snapshot}) ->
  ProjId = db_snapshot:project_id(Snapshot),
  case db:get_merge_requests_in_project({project_id, ProjId}) of
    {error, _} -> S#state{error={error, {not_found, merge_requests}}};
    {ok, MergeRequests} -> 
      Sorted = merge_requests:sorted(MergeRequests, ascending),
      S#state{merge_requests=Sorted}
  end;
get_merge_requests(S=#state{error={error,_}}) -> S.

spawn_tests(S=#state{error=undefined, merge_requests=Mrs}) ->
  Self = self(),
  RefMrs = lists:map(fun(Mr) ->
    Args = #{merge_request => Mr, manager => Self},
    {ok, Pid} = test_diff_analysis_mr_server:start_link(Args),
    erlang:link(Pid),
    Ref = erlang:make_ref(),
    gen_server:call(Pid, {start_tests, Ref}),
    {Ref, Mr}
  end, Mrs),
  S#state{test_refs=RefMrs};
spawn_tests(S=#state{error={error,_}}) -> S.

print_results(Results) when is_list(Results) ->
  Start = ["\n\nTest Completed:\n---------------\n"], 
  Rest = lists:map(fun(Result) -> print_results(Result) end, Results),
  io:fwrite([Start, lists:reverse(Rest)]);
print_results({good, Mr, SuccessfulCommits}) ->
  Title = db_merge_request:title(Mr),
  Rest = lists:map(fun(Commit) ->
    CommitTitle = db_commit:title(Commit),
    ["\t✓ Commit ", "\"", CommitTitle, "\"", " succeeded\n"]
  end, SuccessfulCommits),
  ["✓ Test Suite ", "\"", Title, "\"", " passed all tests successfully.\n",
   Rest, "\n"];
print_results({bad, Mr, Good, Bad}) ->
  Title = db_merge_request:title(Mr),
  Successes = lists:map(fun(Commit) ->
    CommitTitle = db_commit:title(Commit),
    ["\t✓ Commit ", "\"", CommitTitle, "\"", " succeeded\n"]
  end, Good),
  Failures = lists:map(fun({Commit, Reason}) ->
    CommitTitle = db_commit:title(Commit),
    PrettyReason = pretty_commit_fail(Reason),
    ["\t✗ Commit ", "\"", CommitTitle, "\"", " failed: ", 
     PrettyReason, "\n"]
  end, Bad),
  ["✗ Test Suite ", "\"", Title, "\"", " failed:", "\n", 
    Successes, Failures, "\n"].
  
pretty_commit_fail({error, {diffs_failed, Reasons}}) ->
  lists:map(fun
    ({_File, {Exp, Actual}}) ->
      io_lib:format("expected~s; got~s", 
        [pretty_print_scores(Exp), pretty_print_scores(Actual)]);
    (Some) ->
      pretty_commit_fail(Some)
  end, Reasons);
pretty_commit_fail(Unknown) ->
  io_lib:format("Unknown Error: ~p", [Unknown]).

pretty_print_scores(Scores) ->
  White = case db_diff_scores:whitespace(Scores) of
    0 -> "";
    W -> io_lib:format(" ~p Whitespace", [W])
  end,
  Syntax = case db_diff_scores:syntax(Scores) of
    0 -> "";
    S -> io_lib:format(" ~p Syntax", [S])
  end,
  Moves = case db_diff_scores:moves(Scores) of
    0 -> "";
    M -> io_lib:format(" ~p Move", [M])
  end,
  Ref = case db_diff_scores:refactors(Scores) of
    0 -> "";
    R -> io_lib:format(" ~p Refactor", [R])
  end,
  Ins = case db_diff_scores:insertions(Scores) of
    0 -> "";
    X -> io_lib:format(" ~p Insertion", [X])
  end,
  Dels = case db_diff_scores:deletions(Scores) of
    0 -> "";
    D -> io_lib:format(" ~p Deletion", [D])
  end,
  lists:flatten(io_lib:format("~s~s~s~s~s~s", [White, Syntax, Moves, Ref, Ins, Dels])).
