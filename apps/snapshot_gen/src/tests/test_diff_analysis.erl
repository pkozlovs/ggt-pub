-module(test_diff_analysis).
-export([default_test/0, test_project/4, create_test_project/1,
        test_snapshot/1]).
-include_lib("apps/snapshot_gen/include/test_diff_analysis_project.hrl").

test_project(Endpoint, ProjId, Title, Username) ->
  Args = #{endpoint => Endpoint,
    project_id => ProjId,
    title => Title,
    username => Username
  },
  error_logger:info_msg("Starting testing with args: ~p", [Args]),
  test_diff_analysis_server:start_link(Args).

default_test() ->
  ConfigPath = "apps/snapshot_gen/priv/test_diff_analysis.config",
  try 
    {ok, Config, Project} = create_test_project(ConfigPath),
    ProjId = source_project:id(Project),
    Title = list_to_binary(Config#config.title),
    User = list_to_binary(Config#config.username),
    Endpoint = {endpoint, Config#config.source,
                <<"https://csil-git1.cs.surrey.sfu.ca/api/v3/projects">>,
                list_to_binary(Config#config.token)},
    test_project(Endpoint, ProjId, Title, User)
  catch
    Any:Error ->
      {Any, Error}
  end.

test_snapshot(Snapshot) ->
  test_diff_analysis_server:start_link(#{snapshot => Snapshot}).
    
create_test_project(ConfigPath) ->
  test_diff_analysis_project_manager:create_test_project(ConfigPath).
   
