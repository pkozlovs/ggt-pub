-module(test_diff_analysis_commit_worker).
-export([init/1]).

-record(state, {
  commit :: [db_commit:self()],
  commit_diffs = [] :: [db_commit_diff:self()],
  expectations = #{} :: map(),
  results = #{} :: map()
}).

init(Commit) ->
  CommitId = db_commit:id(Commit),
  {ok, Diffs} = db:get_commit_file_diffs({commit_id, CommitId}),
  S = #state{commit=Commit, commit_diffs=Diffs},
  Result = start(S),
  exit(Result).

start(S=#state{}) ->
  Result = try 
    S1 = parse_commit(S),
    S2 = get_results(S1),
    validate(S2)
  catch
    Error={error, _} -> Error;
    Unhandled -> {error, {?MODULE, unhandled_error, Unhandled}}
  end,
  Result.

parse_commit(S=#state{commit=Commit}) ->
  Sha = db_commit:sha(Commit),
  Msg = db_commit:message(Commit),
  Expectations = case (catch parse_message(Msg)) of
    Map when is_map(Map) -> Map;
    Error -> throw({error, {failed_parse, Sha, Error}})
  end,
  S#state{expectations=Expectations}.

parse_message(Msg) when is_binary(Msg) ->
  String = binary_to_list(Msg),
  parse_message(String);
parse_message(Text) when is_list(Text) ->
  {_, Tokens, _} = erl_scan:string(Text),
  {ok, Expectations} = erl_parse:parse_term(Tokens),
  maps:from_list(lists:map(fun({Filename, ScoreTuple}) ->
    Score = db_diff_scores:new(ScoreTuple),
    {erlang:list_to_binary(Filename), Score}
  end, Expectations)).

get_results(S=#state{commit_diffs=Diffs}) ->
  Results = lists:foldl(fun(Diff, Results) ->
    DiffId = db_commit_diff:id(Diff),
    {ok, DiffScore} = db:get_commit_diff_raw_scores({commit_diff_id, DiffId}),
    Filename = db_commit_diff:filename(Diff),
    RawScore = db_commit_diff_scores:scores(DiffScore),
    maps:put(Filename, RawScore, Results)
  end, #{}, Diffs),
  S#state{results=Results}.

validate(#state{expectations=Exp, results=Res}) ->
  ExpNames = maps:keys(Exp),
  {Goods, Bads} = lists:foldl(fun(Filename, {Goods, Bads}) ->
    ResScore = maps:get(Filename, Res, error),
    ExpScore = maps:get(Filename, Exp),
    case ResScore == ExpScore of
      true -> {[Filename|Goods], Bads};
      false -> {Goods, [{Filename, {ExpScore, ResScore}}|Bads]}
    end
  end, {[], []}, ExpNames),
  {ok, {Goods, Bads}}.
