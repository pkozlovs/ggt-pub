-module(test_diff_analysis_mr_server).
-behaviour(gen_server).
%% behaviour exports
-export([start_link/1, init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).

-record(state, {
    manager :: pid(),
    manager_ref = undefined :: reference() | undefined,
    merge_request :: db_merge_request:self(),
    commits = [] :: [db_commit:self()],
    test_refs = [] :: [reference()],
    good_commits = [] :: [db_commit:self()],
    bad_commits = [] :: [{db_commit:self(), Reason :: any()}]
  }).

start_link(Args) ->
  gen_server:start_link(?MODULE, Args, []).

init(#{merge_request := MergeRequest, manager := Manager}) ->
  State = #state{merge_request=MergeRequest, manager=Manager},
  {ok, State}.

handle_call({start_tests, Ref}, _Sender, S=#state{}) ->
  S1 = S#state{manager_ref=Ref},
  S2 = start_tests(S1),
  {reply, ok, S2};
handle_call(_Call, _Sender, State) ->
  {reply, ok, State}.

handle_cast(_Call, State) ->
  {noreply, State}.

handle_info({'DOWN', Ref, process, _, Reason}, S=#state{test_refs=Refs}) ->
  case maps:is_key(Ref, Refs) of
    true -> 
      S1 = handle_down(Ref, Reason, S),
      NewRefs = S1#state.test_refs,
      case maps:size(NewRefs) of
        0 -> {stop, normal, S1};
        _ -> {noreply, S1}
      end;
    false -> {noreply, S}
  end;
handle_info(Msg, S) ->
  error_logger:info_msg("~p: Unhandled message: ~p~n", [?MODULE, Msg]),
  {noreply, S}.

terminate(normal, #state{bad_commits=Bad, good_commits=Good, manager=Pid,
                        manager_ref=Ref}) ->
  Pid ! {ok, {Ref, Good, Bad}},
  ok;
terminate(Reason, _State) ->
  error_logger:error_report([{reason, Reason}]),
  ok.

code_change(_Vsn, State, _Extra) ->
  {ok, State}.

%%%
%%% Private API
%%%

start_tests(S=#state{}) ->
  S1 = get_commits(S),
  S2 = spawn_tests(S1),
  S2.

get_commits(S=#state{merge_request=Mr}) ->
  MrId = db_merge_request:id(Mr),
  {ok, Commits} = db:get_sorted_commits({merge_request_id, MrId}),
  S#state{commits = Commits}.

spawn_tests(S=#state{commits=Commits}) ->
  RefCommits = lists:map(fun(Commit) ->
    {_, Ref} = erlang:spawn_monitor(test_diff_analysis_commit_worker, init, 
                                    [Commit]),
    {Ref, Commit} 
  end, Commits),
  RefCommitMap = maps:from_list(RefCommits),
  S#state{test_refs=RefCommitMap}.

handle_down(Ref, Error={error, _Reason}, S=#state{test_refs=Refs, 
                                                  bad_commits=Bad}) ->
  Commit = maps:get(Ref, Refs),
  NewBad = [{Commit, Error} | Bad],
  NewRefs = maps:remove(Ref, Refs),
  S#state{bad_commits = NewBad, test_refs = NewRefs};
handle_down(Ref, {ok, {_Passed, []}}, S=#state{test_refs=Refs, 
                                            good_commits=Good}) ->
  Commit = maps:get(Ref, Refs),
  NewGood = [Commit | Good],
  NewRefs = maps:remove(Ref, Refs),
  S#state{good_commits = NewGood, test_refs = NewRefs};
handle_down(Ref, {ok, {_, Bad}}, S=#state{test_refs=Refs,
                                             bad_commits=Bads}) ->
  Commit = maps:get(Ref, Refs),
  NewBad = [{Commit, {error, {diffs_failed, Bad}}} | Bads],
  NewRefs = maps:remove(Ref, Refs),
  S#state{bad_commits = NewBad, test_refs = NewRefs}.

