-module(test_diff_analysis_project_manager). 
-export([create_test_project/1]).
-include_lib("apps/snapshot_gen/include/test_diff_analysis_project.hrl").
-define(PRETTY_NAME, "Test Project Manager").


create_test_project(ConfigPath) ->
  % parse config
  {ok, Config} = parse_config(ConfigPath),
  ok = initial_setup(Config),
  ok = create_new_project(Config),
  {ok, Project} = create_source_repo(Config),
  ok = init_local_repo(Config, Project),
  ok = initial_commit(Config),
  ok = initial_push_source_repo(Config),
  ok = create_tests(Config, Project),
  {ok, Config, Project}.

-spec parse_config(Filename :: string()) -> config().
parse_config(File) ->
  test_diff_analysis_project_parser:parse(File).

initial_setup(C=#config{}) ->
  ok = create_root(C),
  ok = delete_past(C).
  
create_root(#config{root_dir=RootDir}) ->
  info("Making sure ~p exists.", [RootDir]),
  case filelib:is_dir(RootDir) of
    true -> ok;
    false -> ok = file:make_dir(RootDir)
  end.

delete_past(#config{project_dir=ProjDir}) ->
  %% TODO: delete on GitLab as well
  case filelib:is_dir(ProjDir) of
    true ->
      info("Deleting old test project ~p.", [ProjDir]),
      os:cmd("rm -Rf " ++ ProjDir);
    _ -> ok
  end,
  info("Creating test project folder ~p.", [ProjDir]),
  ok = file:make_dir(ProjDir).

create_new_project(#config{project_dir=ProjDir}) ->
  ProjDir,
  ok.

create_source_repo(#config{title=LTitle, token=LToken, source=Source}) ->
  Title = list_to_binary(LTitle),
  Token = list_to_binary(LToken),
  Options = [{project_name, Title}, {namespace_id, <<"">>}],
  delete_existing_source_repo(Title, Token, Source),
  info("Creating test project ~p on ~p.", [LTitle, Source]),
  {ok, _Proj} = source:create_project(Source, Token, Options).

delete_existing_source_repo(BTitle, BToken, Source) ->
  try
    {ok, Projects} = source:projects(Source, BToken),
    [ExistingProj] = lists:filter(fun(Project) ->
      BTitle == source_project:name(Project)
    end, Projects),
    info("Project ~s already exists. Deleting.", [binary_to_list(BTitle)]),
    {ok, done} = source:delete_project(Source, BToken, [
      {project_id, source_project:id(ExistingProj)}
    ]),
    timer:sleep(5000),
    {ok, done}
  catch
    M:E -> {error, {M, E}}
  end.

init_local_repo(C=#config{}, Project) ->
  Origin = binary_to_list(source_project:clone_url(Project)),
  git("init", C),
  git(["remote add origin ", Origin], C),
  ok.

initial_commit(C=#config{project_dir=ProjDir, file_location=FileLoc, 
                         initial_commit=Filenames}) ->
  CopyFiles = [{env:join([FileLoc, Filename]), env:join([ProjDir, Filename])}
                || Filename <- Filenames],
  lists:foreach(fun({Source, Target}) ->
    {done, _, _} = sh:oneliner(["cp", Source, Target])
  end, CopyFiles),
  info("Creating initial commit of ~p files.", [length(CopyFiles)]),
  git_add(C),
  git_commit(C, "Initial Commit", ""),
  ok.

initial_push_source_repo(C=#config{}) ->
  info("First push to origin.", []),
  git("push -u origin master", C),
  ok.

git(Command, #config{project_dir=ProjDir}) ->
  {done, _, Out} = sh:oneliner(lists:flatten(["git ", Command]), ProjDir),
  Out.

git_add(C=#config{}) ->
  git("add .", C).

git_commit(#config{project_dir=ProjDir}, Title, Message) ->
  Command = lists:flatten(io_lib:format("printf '~s~n~n~s' | git commit -F -", [Title, Message])),
  info("Committing: ~n\t~p~n\t~p.", [Title, Message]),
  {done, _, Out} = sh:oneliner(Command, ProjDir),
  Out.

git_new_branch(C=#config{}, #merge_request{title=Title}) ->
  {ok, AlphaNumeric} = re:compile("[^a-zA-Z0-9]"),
  BranchName = string:to_lower(re:replace(Title, AlphaNumeric, "-", 
                                          [global, {return, list}])),
  git(["checkout -b ", BranchName], C),
  BranchName.

git_delete_branch(C=#config{}, Branch) ->
  info("Deleting Branch ~s", [Branch]),
  git(["branch -D ", Branch], C),
  ok.

git_checkout_master(C=#config{}) ->
  info("Returning to branch master", []),
  git("checkout master", C),
  ok.

git_push_new_branch(C=#config{}, Branch) ->
  info("Pushing new branch: ~p", [Branch]),
  git(["push --set-upstream origin ", Branch], C),
  ok.

git_pull(C=#config{}) ->
  info("Pulling latest changes.", []),
  git("pull", C),
  ok.

create_tests(C=#config{actions=Actions}, Project) ->
  lists:foreach(fun(Action) -> 
    create_test_action(C, Action, Project) 
  end, Actions).

create_test_action(C=#config{}, M=#merge_request{commits=Commits}, Project) ->
  info("Creating Merge Request ~p", [M#merge_request.title]),
  Branch = git_new_branch(C, M),
  lists:foreach(fun(Commit) -> 
    ok = create_test_commit(C, Commit) 
  end, Commits),
  git_push_new_branch(C, Branch),
  Title = M#merge_request.title,
  Mr = create_merge_request(Title, Branch, C, Project),
  accept_merge_request(C, Project, Mr),
  git_checkout_master(C),
  git_pull(C),
  git_delete_branch(C, Branch),
  ok.

create_test_commit(Con=#config{}, #commit{title=Title, changes=Changes}) ->
  info("Creating Commit ~p", [Title]),
  Messages = lists:flatten(lists:map(fun(Change) ->
    create_test_change(Con, Change) 
  end, Changes)),
  git_add(Con),
  Message = lists:flatten(io_lib:format("~p.", [Messages])),
  git_commit(Con, Title, Message),
  ok.

create_test_change(C=#config{}, U=#update{}) ->
  Target = U#update.initial,
  Source = U#update.new,
  Scores = U#update.scores,
  info("Creating Update to ~p", [Target]),
  ok = create_test_update(C, Source, Target),
  {Target, Scores}.

create_test_update(C=#config{project_dir=ProjDir}, Source,
       Target) ->
  FileLoc = C#config.file_location,
  From = env:join([FileLoc, Source]),
  To = env:join([ProjDir, Target]),
  {done, 0, _} = sh:oneliner(["cp", From, To]),
  ok.

create_merge_request(LTitle, LBranch, Config, Project) ->
  info("Pushing Merge Request '~s'", [LTitle]),
  Title = list_to_binary(LTitle),
  Branch = list_to_binary(LBranch),
  Target = <<"master">>,
  ProjId = source_project:id(Project),
  Source = Config#config.source,
  Token = list_to_binary(Config#config.token),
  {ok, Mr} = source:create_merge_request(Source, Token, [
    {project_id, ProjId},
    {source_branch, Branch},
    {target_branch, Target},
    {title, Title}
  ]),
  Mr.

accept_merge_request(#config{source=Source, token=LToken}, Project, Mr) ->
  Title = binary_to_list(source_mr:title(Mr)),
  info("Accepting Merge Request '~s'", [Title]),
  Token = list_to_binary(LToken),
  ProjId = source_project:id(Project),
  MrId = source_mr:id(Mr),
  {ok, Mr2} = source:accept_merge_request(Source, Token, [
    {project_id, ProjId},
    {mr_id, MrId}
  ]),
  Mr2.

info(Message, Args) ->
  Pre = io_lib:format("~s: ", [?PRETTY_NAME]),
  Msg = io_lib:format(Message, Args),
  error_logger:info_msg(lists:flatten([Pre, Msg]), []).
