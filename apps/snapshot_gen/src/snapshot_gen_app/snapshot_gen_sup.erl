-module(snapshot_gen_sup).
-behaviour(supervisor).
%% Supervisor callbacks
-export([start_link/0, stop/0, init/1, handle_info/2]).
-export([start_snapshot_gen/6, cancel_snapshot_gen/4]).
-export([snapshot_pid/3]).

% will start the whole application
start_link() ->
  supervisor:start_link({local, snapshot_gen}, ?MODULE, []).

% will stop all sub processes too. We don't actually need this...
stop() ->
  case whereis(snapshot_gen) of
    Pid when is_pid(Pid) ->
      exit(Pid, kill);
    _ -> ok
  end.

init([]) ->
  MaxRestart = 0,
  MaxTime = 3600,
  {ok, {{one_for_one, MaxRestart, MaxTime}, []}}.

start_snapshot_gen(ProjId, Endpoint, Start, End, CreatorUsername, Title) ->
  Name = {ProjId, Start, End},  
  Args = #{
    proj_id   => ProjId,
    endpoint  => Endpoint,
    start     => Start,
    'end'     => End,
    creator   => CreatorUsername,
    title     => Title,
    name      => Name
  },
  ChildSpec = {Name,
                {snapshot_server, start_link, [Args]},
                 temporary, 10500, worker, [snapshot_server]},
  supervisor:start_child(snapshot_gen, ChildSpec).

cancel_snapshot_gen(ProjIdAtSource, Start, End, CreatorUsername) ->
  Name = {ProjIdAtSource, Start, End, CreatorUsername},
  supervisor:terminate_child(snapshot_gen, Name),
  supervisor:delete_child(snapshot_gen, Name).

handle_info(Msg, State) ->
  io:fwrite("received ~p with state ~p", [Msg, State]),
  {noreply, State}.

snapshot_pid(ProjId, Start, End) ->
  Name = {ProjId, Start, End},
  Children = supervisor:which_children(snapshot_gen),
  try
    Child = get_child(Name, Children),
    pid_from_child(Child)
  catch _:_ -> noproc
  end.

% exits if child doesn't exist
get_child(Name, Children) ->
  hd(lists:filter(fun({CName, _, _, _}) -> CName == Name end, Children)).

pid_from_child({_, Pid, _, _}) -> Pid.
  
