%%%-------------------------------------------------------------------
%%% Alexei Popov Jr <hello@alexpopov.ca>
%%% Tool for getting information from GitLab and wrapping it for analysis.
%%%-------------------------------------------------------------------
-module(gitlab_analysis).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/api/include/api.hrl").
-include_lib("apps/api/include/gitlab.hrl").
-include_lib("apps/db/include/db.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([members/2, merge_requests/3, merge_request_commits/3,
        filter_snapshot_merge_requests/4, merge_request_notes/4,
        merge_request_diffs/3, merge_request_issues/3]).


-export_type([error/0]).

-type error() ::
  {?MODULE, 
    {api, 
     failed_read, 
     members|merge_requests|merge_request_commits|issues|merge_request_notes, 
     any() 
    }
    | {dmap, 
       filter_merge_requests|merge_request_notes|merge_request_diffs
       |merge_request_commits,
       any()
      }
     
  }.

%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%

%%--------------------------------------------------------------------
%% Get all Members in a GitLab project.
%%--------------------------------------------------------------------
-spec members(
  Endpoint :: endpoint(), 
  Project :: db_project:self()
) -> result:either([db_member:self()], any()).
members(Endpoint, Project) ->
  IdAtSource = db_project:id_at_source(Project),
  MaybeJson = api:read(members, Endpoint, [{project_id, IdAtSource}]),
  MaybeJson2 = result:mapError(MaybeJson, fun(Reason) ->
    {error, {?MODULE, {api, failed_read, members, Reason}}}
  end),
  unpack_members_json(MaybeJson2, Project).

  
%%--------------------------------------------------------------------
%% Get Merge Requests in a GitLab project.
%% Filters Merge Requests so that only those merged into master 
%% are returned.
%%--------------------------------------------------------------------
-spec merge_requests(
  Endpoint :: endpoint(),
  Project :: db_project:self(), 
  Members :: [db_member:self()]
) -> result:either([db_merge_request:self()], any()).
merge_requests(Endpoint, Project, Members) ->
  IdAtSource = db_project:id_at_source(Project),
  MaybeJson = api:read(merge_requests, Endpoint, [
    {project_id, IdAtSource},
    {state, ?STATE_MERGED}
  ]),
  MaybeJson2 = result:mapError(MaybeJson, fun(Reason) ->
    {?MODULE, {api, failed_read, merge_requests, Reason}}
  end),
  unpack_merge_requests_json(MaybeJson2, Project, Members).

%%--------------------------------------------------------------------
%% Get all commits for all Merge Requests passed in. 
%% It is possible that not all commits represented in Merge Requests
%% will be available through git.
%%--------------------------------------------------------------------
-spec merge_request_commits(
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MergeRequest :: [db_merge_request:self()]
) -> result:either([{db_merge_request:self(), [binary()]}], any()).
merge_request_commits(Endpoint, Project, MergeRequests) ->
  IdAtSource = db_project:id_at_source(Project),
  RawMRComms = raw_merge_request_commits(Endpoint, IdAtSource, MergeRequests),
  MrCommits = unpack_merge_requests_commits(RawMRComms),
  MrCommits.

%%--------------------------------------------------------------------
%% Filter Merge Requests to the Specified Snapshot range. 
%% Can fail even if Merge Requests are valid if GitLab fails to return a commit.
%%--------------------------------------------------------------------
-spec filter_snapshot_merge_requests(
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  Snapshot :: db_snapshot:self(),
  MergeRequestCommits :: [{db_merge_request:self(), [binary()]}]
) -> result:either([{db_merge_request:self(), [binary()]}], any()).
filter_snapshot_merge_requests(Endpoint, Snapshot, Project, MergeRequestCommits) ->
  MaybeMRCs = notmyjob:dmap(fun(MRC) ->
    validate_merge_request(Endpoint, Project, Snapshot, MRC)
  end, MergeRequestCommits),
  case MaybeMRCs of
    {error, Reason} -> 
      {error, {?MODULE, {dmap, filter_merge_requests, Reason}}};
    List ->
      % we ignore all the not_in_snapshot errors we got.
      GoodMRCs = result:values(List),
      {ok, GoodMRCs}
  end.    

validate_merge_request(Endpoint, Project, Snapshot, {MR, Shas}) ->
  Date = find_likeliest_merge_date(Endpoint, Project, Shas),
  case db_snapshot:contains_datetime(Date, Snapshot) of
    true ->
      DatedMR = db_merge_request:set_merge_date(Date, MR),
      {ok, {DatedMR, Shas}};
    false ->
      {error, {not_in_snapshot, {MR, Shas, Snapshot}}}
  end.
      
%% -- ? Was needed for older versions of GitLab where MR's were (more) shoddy
%% --   Now the list of commits associated with the MR is (more) reliable.
%% filter_remaining_commits([], _, _, GoodShas) -> GoodShas;
%% filter_remaining_commits([Sha|T], Date, Project, GoodShas) ->
%%   ProjId = db_project:id(Project),
%%   case git_core:first_merge_commit(ProjId, Sha) of
%%     {ok, #{date := MDate}} when MDate == Date -> 
%%       filter_remaining_commits(T, Date, Project, [Sha|GoodShas]);
%%     _ -> 
%%       filter_remaining_commits(T, Date, Project, GoodShas)
%%   end.

find_likeliest_merge_date(Endpoint, Project, Shas) ->
  case find_merge_date_helper(Shas, Project) of
    {error, _} -> find_latest_commit_date(Endpoint, Project, Shas);
    Date -> Date
  end.

find_merge_date_helper([], _Project) -> {error, no_reachable_commits};
find_merge_date_helper([Sha|T], Project) ->
  ProjId = db_project:id(Project),
  case git_core:first_merge_commit(ProjId, Sha) of
    {ok, #{date := Date}} -> Date;
    _ -> find_merge_date_helper(T, Project)
  end.

find_latest_commit_date([], _, _) -> {error, gitlab_commit_error};
find_latest_commit_date(Endpoint, Proj, [Sha|T]) ->
  MaybeCommit = commit_from_gitlab(Endpoint, Proj, Sha),
  case MaybeCommit of
    {error, _} -> find_latest_commit_date(Endpoint, Proj, T);
    {ok, Commit} ->
      db_commit:create_date(Commit)
  end.

commit_from_gitlab(Endpoint, Proj, Sha) ->
  SourceProjId = db_project:id_at_source(Proj),
  ProjId = db_project:id(Proj),
  MaybeCommit = api:read(commit, Endpoint, [
    {project_id, SourceProjId},
    {commit_sha, Sha}
  ]),
  result:map(MaybeCommit, fun(Inner) -> 
    {Commit, _} = gitlab_convert:unpack_commit(Inner, ProjId),
    Commit
  end).

%%--------------------------------------------------------------------
%% Get all Notes for all Merge Requests Passed in.
%%--------------------------------------------------------------------
-spec merge_request_notes(
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MergeRequests :: [db_merge_request:self()],
  Members :: [db_member:self()]
) -> result:either(
       [{db_merge_request:self(), [db_note:self()]}],
       any()
      ).
merge_request_notes(Endpoint, Project, MergeRequests, Members) ->
  MaybeNotes = notmyjob:dmap(fun(MergeRequest) ->
    case merge_request_note(Endpoint, Project, MergeRequest, Members) of
      {ok, Notes} -> {MergeRequest, Notes};
      {error, Reason} -> {error, Reason}
    end
  end, MergeRequests), 
  case MaybeNotes of
    {error, Reason} -> 
      {error, {?MODULE, {dmap, merge_request_notes, Reason}}};
    NestedNotes -> {ok, NestedNotes}
  end.

merge_request_issues(Endpoint, Project, Members) ->
  IdAtSource = db_project:id_at_source(Project),
  MaybeJsonIssues = api:read(issues, Endpoint, [
    {project_id, IdAtSource},
    {state, <<"all">>}]),
  case MaybeJsonIssues of
    {error, Reason} ->
      {error, {?MODULE, {api, failed_read, issues, Reason}}};
    {ok, JsonList} ->
      ProjId = db_project:id(Project),
      MaybeIssues = gitlab_convert:unpack_issues(JsonList, ProjId, Members),
      result:mapError(MaybeIssues, fun(Reason) ->
        {error, {?MODULE, Reason}}
      end)
  end.

-spec merge_request_diffs(
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MergeRequests :: [db_merge_request:self()]
) -> result:either([db_merge_request_diff:self()], any()).
merge_request_diffs(Endpoint, Project, MergeRequests) ->
  MaybeDiffs = notmyjob:dmap(fun(MR) ->
    merge_request_diff(Endpoint, Project, MR)
  end, MergeRequests),
  case MaybeDiffs of
    {error, Reason} -> 
      {error, {?MODULE, {dmap, merge_request_diffs, Reason}}};
    DiffLists ->
      NestedDiffs = result:values(DiffLists),
      {ok, lists:flatten(NestedDiffs)}
  end.

%%%  _____      _            _                  _____ _____
%%% |  __ \    (_)          | |           /\   |  __ \_   _|
%%% | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |
%%% |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |
%%% | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_
%%% |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____|
%%%

%%--------------------------------------------------------------------
%% JSON -> Members conversion.
%%--------------------------------------------------------------------
-spec unpack_members_json(
  JsonList :: result:either([map()], any()),
  Project :: db_project:self()
) -> result:either([db_member:self()], any()).
unpack_members_json({error, Reason}, _Project) ->
  {error, Reason};
unpack_members_json({ok, JsonList}, Project) ->
  ProjId = db_project:id(Project),
  MaybeMembers = gitlab_convert:unpack_members(JsonList, ProjId),
  result:mapError(MaybeMembers, fun(Reason) -> {?MODULE, Reason} end).

%%--------------------------------------------------------------------
%% JSON -> Merge Requests conversion.
%%--------------------------------------------------------------------
-spec unpack_merge_requests_json(
  JsonList :: result:either([map()], any()),
  Project :: db_project:self(),
  Members :: [db_member:self()]
) -> result:either([db_merge_request:self()], any()).
unpack_merge_requests_json({error, Reason}, _Project, _Members) ->
  {error, Reason};
unpack_merge_requests_json({ok, JsonList}, Project, Members) ->
  ProjectId = db_project:id(Project),
  MaybeMRs = gitlab_convert:unpack_merge_requests(JsonList, ProjectId, Members),
  MaybeFiltered = result:map(MaybeMRs, fun filter_merged_mrs/1),
  result:mapError(MaybeFiltered, fun(Reason) -> {?MODULE, Reason} end).
  
%%--------------------------------------------------------------------
%% Filters merge requests by only those merged into the master branch
%%--------------------------------------------------------------------
-spec filter_merged_mrs(MergeRequests :: [db_merge_request:self()]) 
  -> [db_merge_request:self()].
filter_merged_mrs(MergeRequests) ->
  lists:filter(fun(MergeRequest) -> 
    db_merge_request:state(MergeRequest) == <<"merged">> andalso
    db_merge_request:target_branch(MergeRequest) == <<"master">>
  end, MergeRequests).
  
%%--------------------------------------------------------------------
%% Returns the JSON for the Commits corresponding to a Merge Request.
%% Extremely convoluted return because of the way dmap works.
%%--------------------------------------------------------------------
-spec raw_merge_request_commits(
  Endpoint :: endpoint(),
  ProjectId :: db_project:id(),
  MergeRequests :: [db_merge_request:self()]
) -> result:either(
  [{ok, [map()], db_merge_request:self()} | {error, any()}]
  , any()
).
raw_merge_request_commits(Endpoint, ProjectId, MergeRequests) ->
  MaybeMergeRequestCommits = notmyjob:dmap(fun(MR) ->
    Id = db_merge_request:id_at_source(MR),
    Commits = api:read(merge_request_commits, Endpoint, [
      {project_id, ProjectId},
      {merge_request_id, Id}
    ]),
    case Commits of
      {ok, InnerCommits} ->
        {ok, InnerCommits, MR};
      {error, Reason} ->
        {error, {?MODULE, {api, failed_read, merge_request_commits, Reason}}}
    end
  end, MergeRequests),
  % check if it suceeded
  case MaybeMergeRequestCommits of
    {error, Reason} -> 
      {error, {?MODULE, {dmap, merge_request_commits, Reason}}};
    List ->
      {ok, List}
  end.
  
%%--------------------------------------------------------------------
%% Unpacks the Commit JSON and returns a list of Shas with each merge request
%% Extremely convoluted input because of the way dmap works
%% (see `raw_merge_request_commits/3`)
%%--------------------------------------------------------------------
-spec unpack_merge_requests_commits(
  result:either(
    [{CommitsJson :: [map()], MergeRequest :: db_merge_request:self()}],
    any()
  )
) -> result:either([{db_merge_request:self(), [binary()]}], any()).
unpack_merge_requests_commits({ok, MRCsJson}) ->
  Unpacked = notmyjob:dmap(fun(MergeRequestCommits) ->
    do_unpack_merge_request_commits(MergeRequestCommits)
  end, MRCsJson),
  case Unpacked of
    {error, Reason} ->
      {error, {?MODULE, {dmap, merge_request_commits, Reason}}};
    List -> 
      GoodList = result:values(List),
      {ok, GoodList}
  end;
unpack_merge_requests_commits({error, Reason}) ->
  {error, Reason}.
  
%%--------------------------------------------------------------------
%% Extract Shas for a set of commits for a merge request.
%%--------------------------------------------------------------------
-spec do_unpack_merge_request_commits(
   {error, Reason}) -> {error, Reason};
  ({ok, [Json :: map()], db_merge_request:self()}
) -> {db_merge_request:self(), [Sha :: binary()]}.
do_unpack_merge_request_commits({error, Reason}) ->
  {error, Reason};
do_unpack_merge_request_commits({ok, GitCommits, MR}) ->
  MaybeCommitShas = notmyjob:dmap(fun(GitCommit) ->
    #{
      sha := Sha
    } = GitCommit,
    Sha
  end, GitCommits),
  case MaybeCommitShas of
    {error, Reason} ->
      {error, {?MODULE, {dmap, merge_request_commits, Reason}}};
    CommitShas -> {ok, {MR, CommitShas}}
  end.

%%--------------------------------------------------------------------
%% Get all Notes for Merge Request Passed in.
%%--------------------------------------------------------------------
-spec merge_request_note(
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MergeRequest :: db_merge_request:self(),
  Members :: [db_member:self()]
) -> result:either(
       {db_merge_request:self(), [db_note:self()]},
       any()
      ).
merge_request_note(Endpoint, Project, MergeRequest, Members) ->
  {_, GitLabProjId} = db_project:sp_id(Project),
  MaybeJsonNotes = api:read(merge_request_notes, Endpoint,
    [
      {project_id, GitLabProjId},
      {merge_request_id, db_merge_request:id_at_source(MergeRequest)}
    ]),
  MaybeJson = result:mapError(MaybeJsonNotes, fun(Reason) ->
    {?MODULE, {api, failed_read, merge_request_notes, Reason}}
  end),
  unpack_merge_request_notes(MergeRequest, MaybeJson, Members).

%%--------------------------------------------------------------------
%% Unpack a List of JSON for Notes in a MR.
%%--------------------------------------------------------------------
-spec unpack_merge_request_notes(
  MergeRequest :: db_merge_request:self(),
  MaybeJson :: result:either([map()], any()),
  Members :: [db_member:self()]
) -> result:either([db_note:self()], any()).
unpack_merge_request_notes(MergeRequest, {ok, JsonNotes}, Members) ->
  Notes = gitlab_convert:unpack_mr_notes(JsonNotes, MergeRequest, Members), 
  {ok, Notes};
unpack_merge_request_notes(_MR, {error, Reason}, _Members) ->
  {error, Reason}.

%%--------------------------------------------------------------------
%% Get Cumulative Diffs for a Merge Request
%%--------------------------------------------------------------------
merge_request_diff(Endpoint, Project, MergeRequest) ->
  ProjIdSource = db_project:id_at_source(Project),
  MaybeDiffs = api:read(merge_request_changes, Endpoint, [
    {project_id, ProjIdSource},
    {merge_request_id, db_merge_request:id_at_source(MergeRequest)}
  ]),
  unpack_merge_request_diff(MaybeDiffs, MergeRequest).
  
%%--------------------------------------------------------------------
%% Unpack a List of JSON for Diffs in a MR.
%% --------------------------------------------------------------------
unpack_merge_request_diff({ok, JsonList}, MergeRequest) ->
  gitlab_convert:unpack_merge_request_diffs(JsonList, MergeRequest);
unpack_merge_request_diff({error, Reason}, _MR) ->
  {error, Reason}.

