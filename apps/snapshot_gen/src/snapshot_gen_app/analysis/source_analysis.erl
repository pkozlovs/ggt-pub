-module(source_analysis).
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/api/include/api.hrl").
-export([start/4]).
-export_type([error/0]).

%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%


-type error() ::  gitlab_analysis:error().
  
start(Project, Snapshot, Endpoint, Pid) ->
  Source = Endpoint#endpoint.name,
  Members = members(Source, Endpoint, Project),
  MergeRequests = merge_requests(Source, Endpoint, Project, Snapshot, Members),
  MRsCommits = merge_request_commits(Source, Endpoint, Project, 
                 MergeRequests),
  GoodMRsCommits = filtered_snapshot_merge_requests(Source, Endpoint,
                      Snapshot, Project, MRsCommits),
  Notes = merge_request_notes(Source, Endpoint, Project, GoodMRsCommits, 
                              Members),
  Issues = merge_request_issues(Source, Endpoint, Project, Members),
  Diffs = merge_request_diffs(Source, Endpoint, Project, GoodMRsCommits),
  FinalResult = result:zip_list([Members, Notes, Issues, Diffs, GoodMRsCommits]),
  complete_with_result(Pid, FinalResult).


%%%  _____      _            _                  _____ _____
%%% |  __ \    (_)          | |           /\   |  __ \_   _|
%%% | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |
%%% |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |
%%% | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_
%%% |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____|
%%%
-spec members(
  Source :: source(),
  Endpoint :: endpoint(),
  Project :: db_project:self()
) -> result:either([db_member:self()], source_analysis:error()).
members(gitlab, Endpoint, Project) ->
  gitlab_analysis:members(Endpoint, Project).

-spec merge_requests(
  Source :: source(),
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  Snapshot :: db_snapshot:self(),
  Members :: result:either([db_member:self()], any())
) -> result:either({[db_merge_request:self()], [db_member:self()]}, any()).
merge_requests(_Source, _Endpoint, _Project, _Snapshot, {error, Reason}) ->
  {error, Reason};
merge_requests(gitlab, Endpoint, Project,_Snapshot, {ok, Members}) ->
  gitlab_analysis:merge_requests(Endpoint, Project, Members).

-spec merge_request_commits(
  Source :: source(),
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MaybeMergeRequests :: result:either({[db_merge_request:self()], [db_member:self()]}, any())
) -> result:either([{db_merge_request:self(), [binary()]}], any()).
merge_request_commits(gitlab, Endpoint, Project, {ok, MRs}) ->
  gitlab_analysis:merge_request_commits(Endpoint, Project, MRs);
merge_request_commits(_Source, _Endpoint, _Project, {error, Reason}) ->
  {error, Reason}.

-spec filtered_snapshot_merge_requests(
  Source :: gitlab,
  Endpoint :: endpoint(),
  Snapshot :: db_snapshot:self(),
  Project :: db_project:self(),
  MRCs :: result:either([{db_merge_request:self(), [binary()]}], any())
) -> result:either([{db_merge_request:self(), [binary()]}], any()).
filtered_snapshot_merge_requests(gitlab, Endpoint, Snapshot, Project, {ok, MRCs}) ->
  gitlab_analysis:filter_snapshot_merge_requests(
    Endpoint, Snapshot, Project, MRCs
  );
filtered_snapshot_merge_requests(_, _, _, _, {error, Reason}) ->
  {error, Reason}.

-spec merge_request_notes(
  Source :: source(),
  Endpoint :: endpoint(),
  Project :: db_project:self(),
  MRShas :: result:either([{db_merge_request:self(), [binary()]}], any()),
  Members :: result:either([db_member:self()], any())
) -> result:either([db_note:self()], any()).
merge_request_notes(gitlab, Endpoint, Project, {ok, MRShas}, {ok, Members}) ->
  MergeRequests = [MR || {MR, _} <- MRShas],
  MaybeMRNotes = gitlab_analysis:merge_request_notes(Endpoint, Project, 
                                                MergeRequests, Members),
  case MaybeMRNotes of
    {ok, MRNotes} ->
      Notes = [Notes || {_, Notes} <- MRNotes],
      {ok, lists:flatten(Notes)};
    {error, Reason} ->
      {error, Reason}
  end;
merge_request_notes(gitlab, _, _, Left, Right) ->
  result:zip(Left, Right).

merge_request_issues(gitlab, Endpoint, Project, {ok, Members}) ->
  gitlab_analysis:merge_request_issues(Endpoint, Project, Members);
merge_request_issues(_, _, _, {error, Reason}) ->
  {error, Reason}.

merge_request_diffs(gitlab, Endpoint, Project, {ok, MRShas}) ->
  MRs = [MR || {MR, _} <- MRShas],
  gitlab_analysis:merge_request_diffs(Endpoint, Project, MRs);
merge_request_diffs(_, _, _, {error, Reason}) ->
  {error, Reason}.


-spec complete_with_result(
  Owner :: pid(),
  Result :: result:either({
              Members :: [db_member:self()], 
              Notes :: [db_note:self()],
              Issues :: [db_issue:self()],
              Diffs :: [db_merge_request_diff:self()],
              MRsAndCommits :: [{db_merge_request:self(), [binary()]}] 
            }, any())
) -> any().
complete_with_result(Pid, {ok, 
                    [Members, Notes, Issues, Diffs, MRsAndCommits]}) ->
  MRs = [MR || {MR, _} <- MRsAndCommits],
  Pid ! {ok, {?MODULE, {Members, MRs, MRsAndCommits, Notes, Issues, Diffs}}};
complete_with_result(Pid, {error, Reason}) ->
  Pid ! {error, Reason}.
