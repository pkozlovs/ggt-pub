-module(diff_analysis).
-include_lib("apps/snapshot_gen/include/diff_analysis.hrl").
-export([scores/1]). 

-type line_location_pair() :: {line_location(), line_location()}.

-type error_reason() :: {ignored, too_many_insertions 
                         | too_many_deletions}.

-spec scores(
  Diff :: binary()
) -> result:either(running_scores(), error_reason()).
scores(Diff) ->
  DirtyLines = split_lines(Diff),
  NumberedLines = list_utils:number_elements(DirtyLines),
  Lines = clean_lines(NumberedLines),
  Start = #running_scores{remaining = Lines},
  Deuselessed = extract_whitespace_syntax(Start),
  InsDels = preliminary_ins_dels(Deuselessed),
  case find_moves_refactors(InsDels) of
    {ignored, Reason} ->
      {error, {ignored, Reason}};
    Scores -> {ok, clean_up_scores(Scores)}
  end.

clean_up_scores(#running_scores{whitespace=Ws, syntax=Ss, moves=Ms, 
                                refactors=Rs, insertions=Is, deletions=Ds}) ->
  {
    [clean_up_helper(X) || X <- Ws],
    [clean_up_helper(X) || X <- Ss],
    [clean_up_helper(X) || X <- Ms],
    [clean_up_helper(X) || X <- Rs],
    [clean_up_helper(X) || X <- Is],
    [clean_up_helper(X) || X <- Ds]
  }.

clean_up_helper({N, _}) when is_integer(N) -> N * 1;
clean_up_helper({{N, _}, {M, _}}) when is_integer(N), is_integer(M) -> {N, M}.

-spec split_lines(
  Diff :: binary()
) -> [binary()].
split_lines(Diff) ->
  binary:split(Diff, <<"\n">>, [global]).

clean_lines(Lines) ->
  RemovingPreamble = diff_clean:strip_diff_garbage(Lines),
  RemovingContext = diff_clean:strip_context(RemovingPreamble),
  RemovingContext.

git_change_type(<<"+", Line/binary>>) -> {insertion, Line};
git_change_type(<<"-", Line/binary>>) -> {deletion, Line};
git_change_type(_) -> undefined.

extract_whitespace_syntax(R=#running_scores{remaining=Rem, whitespace=White,
                                            syntax=Syntax}) ->
  {NewWhite, NewSyntax, NewRest} = lists:foldl(
    fun({Loc, Line}, {Wht, Syn, Rest}) ->
      NoSpace = remove_whitespace(Line),
      case git_change_type(NoSpace) of
        {_, <<>>} -> {[{Loc, Line}|Wht], Syn, Rest};
        {_, Nonempty} ->
          NoSyntax = remove_syntax(Nonempty),
          case NoSyntax of
            <<>> -> {Wht, [{Loc, Line}|Syn], Rest};
            _ -> {Wht, Syn, [{Loc, Line}|Rest]}
          end
      end
    end, {[], [], []}, Rem),
  R#running_scores{whitespace = White ++ NewWhite, 
                   syntax = NewSyntax ++ Syntax,
                   remaining = NewRest}.


remove_whitespace(Line) -> 
  re:replace(Line, "\\s", "", [{return, binary}, global]).

remove_syntax(Line) -> 
  re:replace(Line, "[^0-9A-Za-z]", "", [{return, binary}, global]).

preliminary_ins_dels(RunningScores) ->
  LineList = RunningScores#running_scores.remaining,
  {Ins, Dels} = insertions_deletions(LineList),
  RunningScores#running_scores{
    insertions = Ins,
    deletions = Dels,
    remaining = []
  }.

insertions_deletions(Lines) ->
  {Ins, Dels} = lists:foldl(fun({Loc, Line}, {Ins, Dels}) ->
    case git_change_type(Line) of
      {insertion, Change} -> {[{make_ref(), {Loc, Change}}|Ins], Dels};
      {deletion, Change} -> {Ins, [{make_ref(), {Loc, Change}}|Dels]}
    end
  end, {[], []}, Lines),
  {lists:reverse(Ins), lists:reverse(Dels)}.

find_moves_refactors(RunningScores) ->
  case filter_absurd_sizes(RunningScores) of
    #running_scores{insertions=Ins, deletions=Dels} ->
      CrossProduct = [{In, Del} || In <- Ins, Del <- Dels],
      do_find_moves_syntax_refactors(CrossProduct, RunningScores);
    Error -> Error 
  end. 

filter_absurd_sizes(#running_scores{insertions=Insertions}) 
                    when length(Insertions) > 200 ->
  {ignored, too_many_insertions};
filter_absurd_sizes(#running_scores{deletions=Deletions}) 
                    when length(Deletions) > 200 ->
  {ignored, too_many_deletions};
filter_absurd_sizes(Scores) -> 
  case max_length_ins_or_del(Scores) > 10000 of
    true -> {ignored, insertion_or_deletion_line_length_too_long};
    false -> Scores 
  end.
 
%% Find the length of the longest line of text inserted or deleted.
max_length_ins_or_del(RunningScores) ->
  #running_scores{insertions=Ins, deletions=Dels} = RunningScores,
  MaxIns = max_length_ins_or_del(Ins, 0),
  MaxDels= max_length_ins_or_del(Dels, 0),
  max(MaxIns, MaxDels).

max_length_ins_or_del([Head | Tail], Max) ->
  % Parameter is an array of [ {_, {_, String}}, ....]
  {_, {_, UnicodeText}} = Head,
  % NOTE: string:length() seems to not exist in our version of the Erlang 
  % runtime system; using byte_size as backup.
  case byte_size(UnicodeText) > Max of 
    true  -> max_length_ins_or_del(Tail, byte_size(UnicodeText));
    false -> max_length_ins_or_del(Tail, Max)
  end;
max_length_ins_or_del([], Max) ->
  Max.
 
%% -------------------------------------------------------
%% Analyzes Insertions and Deletions to weed out potential moves
%% and refactors.
%%
%% Calling: this function must be called with a cross product of all
%% Insertions and Deletions that you want to consider in a diff. 
%% The arguments `Insertions` and `Deletions` must each be a list of all 
%% Insertions and Deletions of which Cross Product was formed. 
%%
%% As the Moves and Refactors are determined by the heuristic, they will be
%% removed from the Insertions and Deletions lists, since they are already
%% accounted for. 
%%
%% Algorithm in depth, iterating over all pairs of insertions and deletions:
%% 1. Eliminate pairs that are too dissimilar 
%% 2. Find the Levenshtein distance of a pair that could match
%% 3. If the distance is 0, i.e. the strings match, we deem that a 'Move'
%% 4. Else if the distance is greater than 0, we check if it's within
%%      our bounds of a "refactor" or "trivial change"; this is a computed
%%      heuristic that is defined by max_refactor_threshold/2.
%% 5. If the distance is within bounds, we deem that a refactor. 
%% 6. Else this pair presents no further interest to this analysis.
%% 7. We terminate when we've gone through all the pairs: what's left are
%%      Moves, Refactors, and unmatched Insertions and Deletions.
%%
%%  See also: eval_move_refactor/8.
%% -------------------------------------------------------
-spec do_find_moves_syntax_refactors(
  Pairs :: [line_location_pair()],
  Scores :: #running_scores{}
) -> {
  FinalScores :: #running_scores{}
}.
% base case; we remove all the references we had. 
do_find_moves_syntax_refactors([], R=#running_scores{insertions=Ins, 
                                                     deletions=Dels}) -> 
  RemoveRefs = fun({Ref, Rest}) when is_reference(Ref) -> Rest;
                  (Rest) -> Rest end,
  NewIns = lists:map(RemoveRefs, Ins),
  NewDels = lists:map(RemoveRefs, Dels),
  R#running_scores{insertions=NewIns, deletions=NewDels};
% if the lines are identical we have a move
do_find_moves_syntax_refactors([Pair={{_, {_, Same}}, {_, {_, Same}}} |Tail],
                               Scores) ->
  {NewTail, NewScores} = update_running(move, Pair, Tail, Scores),
  do_find_moves_syntax_refactors(NewTail, NewScores);
% skip case where lines are too dissimilar in size; speeds things up
do_find_moves_syntax_refactors([{{_, {_, In}}, {_, {_, Del}}}|Tail], Scores) 
                              when abs(byte_size(In) - byte_size(Del)) > 6 ->
  do_find_moves_syntax_refactors(Tail, Scores);
% case where lines are close in size
do_find_moves_syntax_refactors([Pair={{_, {ILoc, In}}, {_, {DLoc, Del}}}|Tail], 
                               Scores) ->
  LinePair = {{ILoc, In}, {DLoc, Del}},
  SameWithoutSpaces = are_identical_without_whitespace(LinePair),
  SameWithoutSyntax = are_identical_without_whitespace_syntax(LinePair),
  if
    SameWithoutSpaces ->
      {NewTail, NewScores} = update_running(whitespace, Pair, Tail, Scores),
      do_find_moves_syntax_refactors(NewTail, NewScores);
    SameWithoutSyntax ->
      {NewTail, NewScores} = update_running(syntax, Pair, Tail, Scores),
      do_find_moves_syntax_refactors(NewTail, NewScores);
    true->
      % we will need to do full-blown string similary metrics
      Lev = string_metrics:levenshtein(In, Del),
      Max = max_refactor_threshold(In, Del),
      %TODO: update this to use the new pair
      eval_move_refactor(Lev, Max, Pair, Tail, Scores) 
  end.

are_identical_without_whitespace({{_, In}, {_, Del}}) ->
  NoWhiteIn = remove_whitespace(In),
  NoWhiteDel = remove_whitespace(Del),
  NoWhiteIn == NoWhiteDel.

-spec are_identical_without_whitespace_syntax(line_location_pair()) 
  -> boolean().
are_identical_without_whitespace_syntax({{_, In}, {_, Del}}) ->
  NoSynIn = remove_whitespace(remove_syntax(In)),
  NoSynDel = remove_whitespace(remove_syntax(Del)),
  NoSynIn == NoSynDel.

%% -------------------------------------------------------
%% Mutually Recursive Helper for do_find_moves_syntax_refactors/5.
%%
%% As described in do_find_moves_syntax_refactors/5, this helper performs
%% the choice of whether a change was a move, a refactor, or neither.
%%
%% -------------------------------------------------------
-spec eval_move_refactor(
  LevenshteinDistance :: non_neg_integer(),
  RefactorThreshold :: non_neg_integer(),
  Pair :: line_location_pair(),
  Tail :: [line_location_pair()],
  Scores :: #running_scores{}
) -> {
  NewScores :: #running_scores{}
}.
% levenshtein of 0 means have a simple move
eval_move_refactor(0, _, Pair, Tail, Scores) ->
  {NewTail, NewScores} = update_running(move, Pair, Tail, Scores),
  do_find_moves_syntax_refactors(NewTail, NewScores);
% levenshtein of N < Thresh means we have a trivial change
eval_move_refactor(N, Max, Pair, Tail, Scores) when (N < Max) ->
  {NewTail, NewScores} = update_running(refactor, Pair, Tail, Scores),
  do_find_moves_syntax_refactors(NewTail, NewScores);
% larger distances don't mean anything in particular
eval_move_refactor(_, _, _Pair, Tail, Scores) ->
  do_find_moves_syntax_refactors(Tail, Scores).

-spec update_running(
  Type :: refactor | move | syntax,
  Pair :: unique_line_location(),
  Tail :: [unique_line_location()],
  Scores :: #running_scores{}
) -> {
  NewTail :: [unique_line_location()], 
  NewScores :: #running_scores{}  
}.
update_running(Type, {{InRef, In}, {DelRef, Del}}, Tail, Scores) 
  ->
  NewTail = lists:filter(fun({{LeftRef, {_, _}}, {RightRef, {_, _}}}) ->
    if
      LeftRef == InRef -> false;
      RightRef == DelRef -> false;
      true -> true
    end
  end, Tail),
  Ins = Scores#running_scores.insertions,
  Dels = Scores#running_scores.deletions,
  Scores2 = insert_scores(Type, {In, Del}, Scores),
  NewIns = lists:filter(fun(RefLineLoc) -> 
    not is_unique_line(RefLineLoc, InRef) end, Ins),
  NewDels = lists:filter(fun(RefLineLoc) ->
    not is_unique_line(RefLineLoc, DelRef) end, Dels),       
  Scores3 = Scores2#running_scores{insertions = NewIns, deletions = NewDels},
  {NewTail, Scores3}.

is_unique_line({Ref, _}, Ref) -> true;
is_unique_line(_, _) -> false.

-spec max_refactor_threshold(binary(), binary()) -> non_neg_integer().
max_refactor_threshold(Left, Right) when is_binary(Left), is_binary(Right) ->
  LargerSize = max(byte_size(Left), byte_size(Right)),
  max(LargerSize div 6, 3).

insert_scores(refactor, X, R) -> insert_scores_refactor(X, R);
insert_scores(move, X, R) -> insert_scores_move(X, R);
insert_scores(syntax, X, R) -> insert_scores_syntax(X, R);
insert_scores(whitespace, X, R) -> insert_scores_whitespace(X, R).

insert_scores_whitespace(X, R=#running_scores{whitespace=Whites}) ->
  R#running_scores{whitespace = [X|Whites]}.

insert_scores_syntax(X, R=#running_scores{syntax=Syntax}) ->
  R#running_scores{syntax = [X|Syntax]}.

insert_scores_move(X, R=#running_scores{moves=Moves}) ->
  R#running_scores{moves = [X|Moves]}.

insert_scores_refactor(X, R=#running_scores{refactors=Refs}) ->
  R#running_scores{refactors = [X|Refs]}.

