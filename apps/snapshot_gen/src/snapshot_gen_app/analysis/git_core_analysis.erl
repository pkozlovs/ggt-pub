-module(git_core_analysis).
-export([start/5]).
-include_lib("apps/db/include/db.hrl").

%%% ======================================================
%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%
%%% =======================================================
-export_type([error/0]).

-type error() ::
  {?MODULE,
    {git_core, failed, clone|pull, binary()}
    | {git_core, failed, get_commits, any()}
    | git_core_convert:error()
  }.

start(Project, Snapshot, Username, Token, Pid) ->
  io:format("git_core_analysis: Starting analysis~n", []),
  % Value Definitions
  ProjId = db_project:id(Project),
  {Start, End} = db_snapshot:date_range(Snapshot),
  % Start:
  io:format("git_core_analysis: Cloning...~n", []),
  CloneRes = clone_project(Project, ProjId, Username, Token),
  io:format("git_core_analysis: Getting Commits...~n", []),
  CommitRes = get_commits(CloneRes, Project, Start, End),
  io:format("git_core_analysis: Filtering Commits...~n", []),
  FilteredRes = filter_commits(CommitRes, ProjId, Snapshot),
  io:format("git_core_analysis: Complete with Results...~n", []),
  complete_with_results(FilteredRes, Pid),
  ok.
    
%%% ======================================================
%%%  _____      _            _                  _____ _____
%%% |  __ \    (_)          | |           /\   |  __ \_   _|
%%% | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |
%%% |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |
%%% | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_
%%% |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____|
%%%
%%% ======================================================

clone_project(Project, ProjectID, Username, Token) ->
  CloneUrl = db_project:clone_url(Project),
  case git_core:clone_project(ProjectID, CloneUrl, Username, Token) of
    {ok, StdOut} -> {ok, StdOut};
    {error, Reason} ->
      error_logger:error_msg("Failed cloning: ~p~n", [Reason]),
      {error, {?MODULE, {git_core, Reason}}}
  end.

% get all commits within this time frame
get_commits({ok, _}, Project, Start, End) ->
  ProjId = db_project:id(Project),
  GitCommits = get_git_commits(ProjId, Start, End),
  extract_author_commit_diffs(GitCommits, Project);
get_commits({error, Reason}, _, _, _) ->
  {error, Reason}.

% filter commits by their first merge date; we ignore commits that were made
% during the snapshot window but not merged until later
filter_commits({ok, Commits}, ProjId, Snapshot) ->
  FilteredCommits = lists:filter(fun({_, Commit, _}) ->
    %io:format("git_core_analysis: This is what we're going to get the SHA out of:~p~n", [Commit]),
    case git_core:first_merge_commit(ProjId, db_commit:sha(Commit)) of
      {ok, #{date := MergeDate}} ->
        db_snapshot:contains_datetime(MergeDate, Snapshot);
      {error, _} -> false
    end
  end, Commits),
  {ok, FilteredCommits};
filter_commits(Error={error,_}, _, _) ->
  Error.

% gets a list of commits for a project in a time-frame. 
-spec get_git_commits(binary(), calendar:datetime(), calendar:datetime()) 
  -> result:either([map()], error()).
get_git_commits(ProjIdHex, Start, End) ->
  case git_core:commits(ProjIdHex, Start, End) of
    {error, Reason} ->
    {error, {?MODULE, {git_core, failed, git_commits, Reason}}};
    {ok, Maps} -> {ok, Maps}
  end.

% extract the authors and commits from the list of maps return by git
-spec extract_author_commit_diffs(
  {ok, [map()]},  db_project:self()
) -> {ok, [git_core_convert:unpacked_commit()]};
  ({error, any()}, db_project:self()) -> {error, any()}.
extract_author_commit_diffs({ok, GitCommits}, Project) ->
  ProjId = db_project:id(Project),
  MaybeCommits = git_core_convert:unpack_commits(GitCommits, ProjId),
  result:mapError(MaybeCommits, fun(Reason) -> {error, {?MODULE, Reason}} end);
extract_author_commit_diffs({error, Reason}, _Project) ->
  {error, Reason}.

-spec unique_authors([db_author:self()]) -> [db_author:self()].
unique_authors(Authors) ->
  Set = sets:from_list(Authors),
  sets:to_list(Set).

-spec complete_with_results(result:either(
  [git_core_convert:unpacked_commit()], any()
), pid()) -> any().
complete_with_results({ok, UnpackedCommits}, Pid) ->
  Authors = [Author || {Author, _, _} <- UnpackedCommits],
  UniqueAuthors = unique_authors(Authors),
  Commits = [Commit || {_, Commit, _} <- UnpackedCommits],
  FileDiffs = [FileDiffs || {_, _, FileDiffs} <- UnpackedCommits],
  Pid ! {ok, {?MODULE, {UniqueAuthors, Commits, FileDiffs}}};
complete_with_results({error, {?MODULE, Reason}}, Pid) ->
  Pid ! {error, {?MODULE, Reason}}.
