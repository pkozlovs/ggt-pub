-module(meta_analysis).
-export([start/1]).
-include_lib("apps/snapshot_gen/include/snapshot_generator.hrl").
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/snapshot_gen/include/diff_analysis.hrl").

%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%

%%--------------------------------------------------------------------
%% Start the Meta analyses, wherein we combine GitLab and Git Core
%% data and finally store it all in the database.
%%--------------------------------------------------------------------
-export_type([error/0]).
-type error() :: 
  {?MODULE,
    {dmap, commit_diff_scores, any()}
    | {dmap, merge_request_diff_scores, any()}
    | {db, error_storing, any()}
  }.

-spec start(
  State :: analyses_state()
) -> result:either(analyses_state(), error()).
start(State=#analyses_state{}) ->
  ConsistentState = verify_all_mr_commits(State),
  GenAuthorCommitsState = generate_author_commits(ConsistentState),  
  GoodDiffState = flatten_commit_file_diffs(GenAuthorCommitsState),
  ScoresCommitState = gen_commit_diff_scores(GoodDiffState),
  ScoresMRState = gen_merge_request_diff_scores(ScoresCommitState),
  FileScoringState = extract_file_types(ScoresMRState),
  case store_state(FileScoringState) of
    {ok, Result} ->
      {ok, Result};
    {error, Reason} ->
      Snapshot = State#analyses_state.snapshot,
      SnapshotId = db_snapshot:id(Snapshot),
      db:update_snapshot_state(SnapshotId, {failed, Reason}),
      {error, {snapshot_creation_failed, Reason}}
  end.
  
%%%  _____      _            _                  _____ _____
%%% |  __ \    (_)          | |           /\   |  __ \_   _|
%%% | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |
%%% |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |
%%% | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_
%%% |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____|
%%%

%%--------------------------------------------------------------------
%% Makes sure all MRs have Commits pulled in
%%--------------------------------------------------------------------
-spec verify_all_mr_commits(
  State :: analyses_state()
) -> NewState :: {ok, analyses_state()}.
verify_all_mr_commits(State) ->
  % value definitions
  MRsCommits = State#analyses_state.merge_request_commits,
  Commits = State#analyses_state.commits,
  Project = State#analyses_state.project,
  ShasToVerify = lists:flatten([Shas || {_, Shas} <- MRsCommits]),
  CommitMap = maps:from_list(lists:map(fun(Commit) ->
    Sha = db_commit:sha(Commit),
    {Sha, Commit}
  end, Commits)),
  % check if there are any commits missing in snapshot window 
  % that are in the MR
  case find_missing_commits(ShasToVerify, CommitMap, []) of
    % all commits present, replace with actual merge request commits
    % and return updated state
    [] ->
      NewMRCs = construct_merge_request_commits(State),
      NewState = State#analyses_state{merge_request_commits = NewMRCs},
      {ok, NewState};
    % if we have commits missing, get them. :eye_roll:
    MissingShas ->
      {NewAuthors, NewCommits, NewDiffs} = get_missing_commits(MissingShas, 
                                                               Project),
      ExistingFileDiffs = State#analyses_state.commit_file_diffs,
      ExistingAuthors = State#analyses_state.authors,
      NewState = State#analyses_state{
        commits = (NewCommits ++ Commits),
        authors = (NewAuthors ++ ExistingAuthors),
        commit_file_diffs = (NewDiffs ++ ExistingFileDiffs)
      },
      % cannot get to this point twice in a row; we would error when
      % getting missing commits
      verify_all_mr_commits(NewState)
  end.

flatten_commit_file_diffs({ok, State}) ->
  FileDiffResults = State#analyses_state.commit_file_diffs,
  GoodDiffs = extract_good_diffs(FileDiffResults),
  NewState = State#analyses_state{
    commit_file_diffs = GoodDiffs
  },
  {ok, NewState}.


gen_commit_diff_scores({ok, State}) ->
  FileDiffs = State#analyses_state.commit_file_diffs,
  case notmyjob:dmap(fun score_commit_diff/1, FileDiffs) of
    {error, Reason} ->
      {error, {?MODULE, {dmap, commit_diff_scores, Reason}}};
    Values ->
      {DiffScores, ScoreLocations} = list_utils:split_pairs(Values),
      NewState = State#analyses_state{
        commit_diff_scores = DiffScores,
        diff_score_locations = ScoreLocations
      },
      {ok, NewState}
  end.

extract_file_types({ok, State}) ->
  FileDiffs = State#analyses_state.commit_file_diffs,
  ExtensionSet = sets:from_list([db_commit_diff:file_extension(Diff) ||
    Diff <- FileDiffs
  ]),
  ProjId = db_project:id(State#analyses_state.project),
  ScoringMatrices = lists:map(fun(Ext) ->
    db:construct_diff_scores_multipliers(ProjId, Ext)
  end, sets:to_list(ExtensionSet)),
  NewState = State#analyses_state{
    diff_score_multipliers = ScoringMatrices
  },
  {ok, NewState};
extract_file_types({error, Reason}) ->
  {error, Reason}.

score_commit_diff(Diff) ->
  Bin = db_commit_diff:diff(Diff),
  DiffId = db_commit_diff:id(Diff),  
  CommitId = db_commit_diff:commit_id(Diff),
  try
    case validate_filetype(db_commit_diff:file_extension(Diff)) of
      error -> throw(file_extension);
      ok -> ok
    end,
    {ok, Scores={Whites, Syntax, Moves, Refs, Ins, Dels}}
      = diff_analysis:scores(Bin),
    DiffScore = db:construct_commit_diff_scores(
      CommitId, 
      DiffId,
      db_commit_diff:file_extension(Diff),
      length(Whites), 
      length(Syntax),
      length(Moves),
      length(Refs),
      length(Ins),
      length(Dels)
    ),
    Locations = db_diff_score_locations:new({DiffId, Scores}),
    {DiffScore, Locations}
  catch
    throw:file_extension ->
      Score = db:construct_commit_diff_scores(CommitId, DiffId, 
                                              {ignored, file_extension}),
      Loc = db_diff_score_locations:new_fake(DiffId),
      {Score, Loc};
    error:{badmatch, {error, Reason}} ->
      Score = db:construct_commit_diff_scores(CommitId, DiffId, Reason),
      Loc = db_diff_score_locations:new_fake(DiffId),
      {Score, Loc}
  end.

gen_merge_request_diff_scores({ok, State}) ->
  MRDiffs = State#analyses_state.merge_request_diffs,
  case notmyjob:dmap(fun score_merge_request_diff/1, MRDiffs) of
    {error, Reason} ->
      {error, {?MODULE, {dmap, merge_request_diff_scores, Reason}}};
    Values ->
      {DiffScores, Locations} = list_utils:split_pairs(Values),
      OldLocations = State#analyses_state.diff_score_locations,
      NewState = State#analyses_state{
        merge_request_diff_scores = DiffScores,
        diff_score_locations = OldLocations ++ Locations 
      },
      {ok, NewState}
  end.

score_merge_request_diff(Diff) ->
  Bin = db_merge_request_diff:diff(Diff),
  MrDiffId = db_merge_request_diff:id(Diff),
  MrId = db_merge_request_diff:merge_request_id(Diff),
  Ext = db_merge_request_diff:file_extension(Diff),
  try 
     case validate_filetype(db_merge_request_diff:file_extension(Diff)) of
      error -> throw(file_extension);
      ok -> ok
    end,
    {ok, Scores={Whites, Syntax, Moves, Refs, Ins, Dels}} 
      = diff_analysis:scores(Bin),
    DiffScores = db:construct_merge_request_diff_scores(
      MrId,
      MrDiffId,
      Ext,
      length(Whites),
      length(Syntax),
      length(Moves),
      length(Refs),
      length(Ins),
      length(Dels)
    ),
    Locations = db_diff_score_locations:new({MrDiffId, Scores}),
    {DiffScores, Locations}
  catch
    throw:file_extension ->
      Score = db:construct_merge_request_diff_scores(MrId, MrDiffId, 
                                                     {ignored, file_extension}),
      Loc = db_diff_score_locations:new_fake(MrDiffId),
      {Score, Loc};
    error:{badmatch, {error, Reason}} ->
      Score = db:construct_commit_diff_scores(MrId, MrDiffId, Reason),
      Loc = db_diff_score_locations:new_fake(MrDiffId),
      {Score, Loc}
  end.


%%--------------------------------------------------------------------
%% Finds all SHAs that are in MRs but not in our existing Commits list.
%%--------------------------------------------------------------------
-spec find_missing_commits(
  Shas :: [binary()],
  CommitMap :: map(),
  Missing :: [binary()]
) -> [MissingShas :: binary()].
find_missing_commits([], _CommitSet, Missing) ->
  lists:reverse(Missing);
find_missing_commits([Sha | Tail], CommitMap, Missing) ->
  case maps:find(Sha, CommitMap) of
    error -> 
      find_missing_commits(Tail, CommitMap, [Sha | Missing]);
    {ok, _} ->
      find_missing_commits(Tail, CommitMap, Missing)
  end.

%%--------------------------------------------------------------------
%% Finds all SHAs that are in MRs but not in our existing Commits list.
%%--------------------------------------------------------------------    
-spec get_missing_commits([binary()], db_project:self()) 
  -> {[db_author:self()], [db_commit:self()], [[db_commit_diff:self()]]}.
get_missing_commits(Shas, Project) ->
  MissingAuthorCommits = lists:map(fun(Sha) ->
    get_missing_commit(Sha, Project)
  end, Shas),
  list_utils:split_triplets(MissingAuthorCommits).

-spec get_missing_commit(binary(), db_project:self()) 
  -> git_core_convert:unpacked_commit().
get_missing_commit(Sha, Project) ->
  ProjectId = db_project:id(Project),
  case git_core:commit(ProjectId, Sha) of
    {ok, GitCommit} ->
      git_core_convert:unpack_commit(GitCommit, ProjectId);
    {error, _Reason} -> % TODO: log this reason eventually
      FakeCommit = db:construct_fake_commit(ProjectId, Sha),
      FakeAuthor = db:construct_fake_commit_author(),
      {FakeAuthor, FakeCommit, [{error, _Reason}]}
  end.

%%
%%  Creates author commits from existing authors and commits
%%  and any new ones found during pair programming parsing
%%
generate_author_commits({ok, S=#analyses_state{commits=Commits, 
                                               authors=Authors}}) ->
  GitAuthorCommits = lists:map(fun(Commit) ->
    Ids = {db_commit:author_id(Commit), db_commit:id(Commit)},
    db_author_commit:new(Ids)
  end, Commits),
  {NewAuthors, NewAuthorCommits} = add_pair_programmers(Commits, Authors,
                                                        GitAuthorCommits),
  S1 = S#analyses_state{author_commits=NewAuthorCommits, authors=NewAuthors},
  {ok, S1};
generate_author_commits(Error={error, _}) -> 
  Error.

%%
%%  Takes in existing authors and author_commits
%%  and adds all pair programmers to both lists
%%
-spec add_pair_programmers(
  Commits :: [db_commit:self()],
  ExistingAuthors :: [db_author:self()],
  ExistingAuthorCommits :: [db_author_commit:self()]
) -> {AllAuthors :: [db_author:self()], 
      AllAuthorCommits :: [db_author_commit:self()]
     }.
add_pair_programmers([], Authors, AuthorCommits) ->
  % base-case: flatten and remove dupes
  {lists:usort(lists:flatten(Authors)), 
   lists:usort(lists:flatten(AuthorCommits))};
add_pair_programmers([Commit|Commits], Authors, AuthorCommits) ->
  Msg = db_commit:message(Commit),
  CommitId = db_commit:id(Commit),
  case pair_programming_parser:parse(Msg) of
    {error, _} -> 
      % no pair programming info; carry on
      add_pair_programmers(Commits, Authors, AuthorCommits);
    {ok, []} ->
      % no pair programming info either; carry on
      add_pair_programmers(Commits, Authors, AuthorCommits);
    {ok, Names} ->
      % we generate new authors for those mentioned
      NewAuthors = [db_author:new({Name, Name}) || Name <- Names],
      % we generate new author commits for them too
      NewAuthorCommits = lists:map(fun(Author) ->
        Ids = {db_author:id(Author), CommitId},
        db_author_commit:new(Ids)
      end, NewAuthors),
      % recurse
      add_pair_programmers(Commits, [NewAuthors|Authors], 
                                    [NewAuthorCommits|AuthorCommits])
  end.
  
%%--------------------------------------------------------------------
%% Takes a state with MR Commits in the form [{db_merge_request:self(), [binary()]}]
%% and returns a list of Merge Request Commits.
%%--------------------------------------------------------------------
-spec construct_merge_request_commits(
  State :: analyses_state()
) -> [db_merge_request_commit:self()].
construct_merge_request_commits(State) ->
  MRsCommits = State#analyses_state.merge_request_commits,
  Commits = State#analyses_state.commits,
  % make a dictionary of commits
  CommitMap = maps:from_list(lists:map(fun(Commit) ->
    Sha = db_commit:sha(Commit),
    {Sha, Commit}
  end, Commits)),
  % construct merge_request_commits by matching shas
  MergeRequestCommits = lists:map(fun(MRCommits) ->
    {MR, Shas} = MRCommits,
    construct_merge_request_commits(MR, Shas, CommitMap)
  end, MRsCommits),
  lists:flatten(MergeRequestCommits).
  
%%--------------------------------------------------------------------
%% Actual construction of merge request commits.
%% Maps MR Shas to Commits and outputs a corresponding merge_request_commit
%% for each
%%--------------------------------------------------------------------
-spec construct_merge_request_commits(
  MergeRequest :: db_merge_request:self(),
  Shas :: [binary()],
  CommitMap :: map()
)  -> [db_merge_request_commit:self()].
construct_merge_request_commits(MergeRequest, Shas, CommitMap) ->
  MRId = db_merge_request:id(MergeRequest),
  lists:map(fun(Sha) ->
    Commit = maps:get(Sha, CommitMap),
    CommitId = db_commit:id(Commit),
    db:construct_merge_request_commit(CommitId, MRId)
  end, Shas).
    
%%--------------------------------------------------------------------
%% Store everything atomically; a single failure will fail everything.
%%--------------------------------------------------------------------
store_state({ok, State}) ->
  % project already stored in DB
  % snapshot needs to be updated as complete if this succeeds
  % save commits, authors, file diffs, MRs, MR commits, and members.
  Project = State#analyses_state.project,
  ProjectId = db_project:id(Project),
  Snapshot = State#analyses_state.snapshot,
  SnapshotId = db_snapshot:id(Snapshot),
  Commits = State#analyses_state.commits,
  CommitDiffs = State#analyses_state.commit_file_diffs,
  CommitDiffScores = State#analyses_state.commit_diff_scores,
  Authors = State#analyses_state.authors,
  AuthorCommits = State#analyses_state.author_commits,
  Notes = State#analyses_state.notes,
  Issues = State#analyses_state.issues,
  MergeRequestDiffs = State#analyses_state.merge_request_diffs,
  MergeRequests = State#analyses_state.merge_requests,
  MergeRequestCommits = State#analyses_state.merge_request_commits,
  MRDiffScores = State#analyses_state.merge_request_diff_scores,
  ScoreMultipliers = State#analyses_state.diff_score_multipliers,
  ScoreLocations = State#analyses_state.diff_score_locations,
  Members =  State#analyses_state.members,
  case db:atomically_do(fun() ->
    {ok, _} = db:update_snapshot_state(SnapshotId, completed),
    {ok, _} = db:update_commits(Commits, SnapshotId),
    {ok, _} = db:update_commit_authors(Authors, ProjectId),
    {ok, _} = db:update_author_commits(AuthorCommits),
    {ok, _} = db:update_commit_file_diffs(CommitDiffs),
    {ok, _} = db:update_commit_diff_scores(CommitDiffScores),
    {ok, _} = db:update_diff_score_multipliers(ScoreMultipliers),
    {ok, _} = db:update_merge_requests(MergeRequests, ProjectId),
    {ok, _} = db:update_merge_request_commits(MergeRequestCommits),
    {ok, _} = db:update_diff_score_locations(ScoreLocations),
    {ok, _} = db:update_members(Members),
    {ok, _} = db:update_notes(Notes),
    {ok, _} = db:update_issues(Issues),
    {ok, _} = db:update_merge_request_diffs(MergeRequestDiffs),
    {ok, _} = db:update_merge_request_diff_scores(MRDiffScores)
  end) of
    {error, Reason} ->
      {error, {?MODULE, {db, error_storing, Reason}}};
    {ok, _} ->
      Snapshot = State#analyses_state.snapshot,
      SuccessState = State#analyses_state{
        snapshot = db_snapshot:set_state(completed, Snapshot)
      },
      {ok, SuccessState}
  end.
  
extract_good_diffs(FileDiffs) ->
  lists:flatten(result:values(FileDiffs)).

validate_filetype(<<".csv">>) -> error;
validate_filetype(<<".bin">>) -> error;
validate_filetype(<<".swp">>) -> error;
validate_filetype(<<".config">>) -> error;
validate_filetype(<<".jpg">>) -> error;
validate_filetype(<<".jpeg">>) -> error;
validate_filetype(<<".png">>) -> error;
validate_filetype(<<".exe">>) -> error;
validate_filetype(<<".o">>) -> error;
validate_filetype(<<".so">>) -> error;
validate_filetype(<<".7z">>) -> error;
validate_filetype(<<".gz">>) -> error;
validate_filetype(<<".jar">>) -> error;
validate_filetype(<<".rar">>) -> error;
validate_filetype(<<".tar">>) -> error;
validate_filetype(<<".zip">>) -> error;
validate_filetype(<<".log">>) -> error;
validate_filetype(<<".DS_Store">>) -> error;
validate_filetype(<<".d">>) -> error;
validate_filetype(<<".lo">>) -> error;
validate_filetype(<<".dylib">>) -> error;
validate_filetype(<<".la">>) -> error;
validate_filetype(<<".a">>) -> error;
validate_filetype(<<".out">>) -> error;
validate_filetype(<<".app">>) -> error;
validate_filetype(<<".dll">>) -> error;
validate_filetype(<<".pdf">>) -> error;
validate_filetype(<<".elf">>) -> error;
validate_filetype(<<".class">>) -> error;
validate_filetype(<<".war">>) -> error;
validate_filetype(<<".ear">>) -> error;
validate_filetype(<<".pid">>) -> error;
validate_filetype(<<".grunt">>) -> error;
validate_filetype(<<".env">>) -> error;
validate_filetype(<<".egg">>) -> error;
validate_filetype(<<".txt">>) -> error;
validate_filetype(<<".xml">>) -> error;
validate_filetype(<<>>) -> error;
validate_filetype(_) -> ok.
