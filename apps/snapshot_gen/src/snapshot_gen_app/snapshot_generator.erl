-module(snapshot_generator).
-include_lib("apps/api/include/api.hrl").
% snapshot generation
-export([gen_snapshot/6, test_gen_snapshot/0, test_multi_snapshot/0]).
% snapshot access
-export([snapshot_pid/1, snapshot_pid/3]).
% snapshot observing
-export([add_observer/3]).

gen_snapshot(ProjId, Endpoint, Start, End, CreatorUsername, Title) ->
  snapshot_gen_sup:start_snapshot_gen(ProjId, Endpoint, Start, End,
                                     CreatorUsername, Title).

test_gen_snapshot() ->
  ProjID = <<"4359">>, % giant project
  Endpoint = #endpoint{
    name = gitlab,
    url = <<"https://csil-git1.cs.surrey.sfu.ca/api/v3/projects">>,
    token = <<"NQNz53HZx2or7PCPngzh">>
  },
  Start = {{2015, 6, 1}, {0, 0, 0}},
  End = {{2017, 7, 1}, {23, 59, 58}},
  gen_snapshot(ProjID, Endpoint, Start, End, <<"apa53">>, 
               <<"My Test Snapshot">>).

test_multi_snapshot() ->
  {ok, Pid1} = test_gen_snapshot(),
  ProjID = <<"4359">>, % giant project
  Endpoint = #endpoint{
    name = gitlab,
    url = <<"https://csil-git1.cs.surrey.sfu.ca/api/v3/projects">>,
    token = <<"NQNz53HZx2or7PCPngzh">>
  },
  Start = {{2016, 6, 1}, {0, 0, 0}},
  End = {{2016, 7, 2}, {23, 59, 58}},
  {ok, Pid2} = gen_snapshot(ProjID, Endpoint, Start, End, <<"apa53">>, 
               <<"My Test Snapshot 2">>),
  {ok, {Pid1, Pid2}}.

snapshot_pid({snapshot, Snapshot}) -> 
  try 
    ProjId = db_snapshot:project_id(Snapshot),
    {ok, Proj} = db:get_project({project_id, ProjId}),
    ProjIdAtSource = db_project:id_at_source(Proj),
    Start = db_snapshot:start_date(Snapshot),
    End = db_snapshot:end_date(Snapshot),
    snapshot_gen_sup:snapshot_pid(ProjIdAtSource, Start, End)
  catch _:_ -> noproc
  end.

snapshot_pid(ProjIdAtSource, Start, End) -> 
  snapshot_gen_sup:snapshot_pid(ProjIdAtSource, Start, End). 

%% see snapshot_server:add_observer/3 for details
add_observer(SnapshotPid, Type, ObserverPid) ->
  snapshot_server:add_observer(SnapshotPid, Type, ObserverPid).
