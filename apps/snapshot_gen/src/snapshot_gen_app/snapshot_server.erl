-module(snapshot_server).
-behaviour(gen_server).
%% behaviour exports
-export([start_link/1, init/1, terminate/2]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([code_change/3]).
-export([update_async/2, add_observer/3]).

-export_type([state_update/0, observer_update_function/0]).

-define(MAX_CRASH, 2).

%% internal state
-record(state, {
  snapshot_ref :: reference() | undefined,
  times_crashed = 0 :: non_neg_integer(),
  args = #{} :: map(),
  snapshot_id :: undefined | binary(),
  observers = [] :: [{gen_server, pid()}] 
}).

%% public entry points
start_link(Args) ->
  gen_server:start_link(?MODULE, Args, []).

init(Args) ->
  self() ! start_snapshot_gen,
  Args1 = maps:put(server, self(), Args),
  State = #state{args=Args1},
  {ok, State}.

%% Updates from Snapshot Gen
update_async(Server, Call = {snapshot_id, _}) ->
  gen_server:cast(Server, Call).

% syncronous calls
handle_call(_Call, _Sender, State) ->
  error_logger:info_msg("~p: Unhandled Call: ~p~n", [?MODULE, _Call]),
  {reply, ok, State}.

% asyncronous calls
handle_cast({snapshot_id, SnapId}, S) ->
  {noreply, S#state{snapshot_id=SnapId}};
handle_cast({add_observer, Type, Pid}, S=#state{observers=Observers}) ->
  S1 = S#state{observers = [{Type, Pid}|Observers]},
  {noreply, S1};
handle_cast(_Call, State) ->
  {noreply, State}.

% start up our snapshot gen
handle_info(start_snapshot_gen, S=#state{}) ->
  S1 = launch_snapshot(S),
  {noreply, S1};
% handle snapshot dying on us
handle_info({'DOWN', Ref, process, _, Reason}, S=#state{snapshot_ref=Ref}) ->
  handle_down(Reason, S);
handle_info(_Msg, S) ->
  error_logger:info_msg("~p: Unhandled message ~p~n", [?MODULE, _Msg]),
  {noreply, S}.

terminate(normal, _State) ->
  ok;
terminate(Reason, State) ->
  #{title := SnapName} = State#state.args,
  error_logger:error_report([{snapshot_gen, SnapName}, {error, Reason}]), 
  ok.

code_change(_Vsn, State, _Extra) ->
  {ok, State}.

%% handle success
handle_down({ok, Snapshot}, S=#state{args=Args}) ->
  #{title := Title} = Args,
  error_logger:info_msg("~p: Snapshot \"~p\" Completed Successfully~n", 
                        [?MODULE, Title]),
  notify_observers({complete, Snapshot}, S),
  {stop, normal, S};
handle_down(normal, S=#state{}) -> % TODO: figure out if we even need this
  {stop, normal, S};
handle_down(Error={error, Reason}, S=#state{times_crashed=T, args=Args}) ->
  case T < ?MAX_CRASH of
    true -> 
      notify_observers({restart, Error}, S),
      NewState = relaunch_snapshot(S),
      #{title := Title} = Args,
      error_logger:info_msg("~p: Restarting Snapshot ~p for Reason: ~p~n",
                            [?MODULE, Title, Reason]),
      {noreply, NewState};
    false ->
      notify_observers({failed, Error}, S),
      error_logger:info_msg("~p: Snapshot crashed too many times.~n", [?MODULE]),
      clean_up(S, Error),
      % crash if we exceed the number 
      {stop, Error, S}
  end;
handle_down(Error, S) ->
  io:fwrite("Unhandled down: ~p with state ~p~n", [Error, S]),
  handle_down({error, Error}, S).

launch_snapshot(S=#state{args=Args}) ->
  {_Pid, Ref} = erlang:spawn_monitor(snapshot_gen_worker, init, [Args]),
  S#state{snapshot_ref=Ref}.

regen_snapshot(S=#state{snapshot_id=SnapId, args=Args}) ->
  #{creator := Username, endpoint := Endpoint} = Args,
  {_Pid, Ref} = erlang:spawn_monitor(
    snapshot_gen_worker, regen_snapshot_analysis, 
    [SnapId, Username, Endpoint, self()]),
  S#state{snapshot_ref=Ref}.

% we didn't even make it to getting the snapshot id
relaunch_snapshot(S=#state{times_crashed=T, snapshot_id=undefined}) ->
  % exponential back-off to avoid transient errors
  back_off(T),
  S1 = launch_snapshot(S),
  S1#state{times_crashed=T+1};
relaunch_snapshot(S=#state{times_crashed=T}) ->
  back_off(T),
  S1 = regen_snapshot(S),
  S1#state{times_crashed=T+1}.
  
back_off(TimesFailed) ->
  Rand = trunc(rand:uniform() * 1000),
  Max = round(math:pow(2, TimesFailed)),
  Period = Rand rem Max,
  timer:sleep(Period * 4000).

clean_up(#state{snapshot_id=undefined}, _) ->
  ok;
clean_up(#state{snapshot_id=SnapId}, Error) ->
  db:update_snapshot_state(SnapId, {failed, Error}),
  ok.


%%% 
%%% Observers
%%%

%%
%%  A process can choose to observe updates in Snapshot Generation, reacting
%%  to them as desired. Currently three updates are available:
%%    - restart, if snapshot_gen encounted an error and attempted a restart
%%    - complete, for a successfully analyzed snapshot
%%    - failed, when we've restarted too many times and the error does not
%%        appear to be transient.
%%
%%
%%  Observers are added through `snapshot_server:add_observer/3` and its
%%  wrapping function `snapshot_generator:add_observer/3`.
%% 
%%  The first arg is the PID of a Snapshot Server. You get this PID 
%%  from starting snapshot_generation, but it can be accessed at any time    
%%  (provided snapshot_gen is still running) from:
%%  snapshot_generator:snapshot_pid/1,3.
%%
%%  The second argument is the type of observer. Three types are supported:
%%    - `gen_server`: other gen_servers, to whom the message will be cast
%%    - `process`: any process, to whom the message will be sent as usual
%%    - `function`: for processes that cannot or will not check their mailbox
%%
%%  The final argument is the receiving PID, in the case of type `gen_server`
%%  and `process`, or an `observer_update_function()` which takes the message
%%  that would ordinarily gets sent and reacts to it.
%%
-type state_update() :: {?MODULE, 
                            {restart, Error :: any()}
                          | {complete, db_snapshot:self()}
                          | {failed, Error :: any()}
                        }.
-type observer_update_function() :: fun((state_update()) -> any()).
-spec add_observer(pid(), gen_server, pid()) -> ok;
                  (pid(), process, pid()) -> ok;
                  (pid(), function, observer_update_function()) -> ok.
add_observer(Server, Type, ObserverPidOrFun) ->
  gen_server:cast(Server, {add_observer, Type, ObserverPidOrFun}).

-spec notify_observers(state_update(), #state{}) -> ok.
notify_observers({restart, Error}, S=#state{}) ->
  notify_observers_helper({restart, Error}, S);
notify_observers({complete, Snapshot}, S=#state{}) ->
  notify_observers_helper({complete, Snapshot}, S);
notify_observers({failed, Error}, S=#state{}) ->
  notify_observers_helper({failed, Error}, S).

-spec notify_observers_helper(state_update(), #state{}) -> ok.
notify_observers_helper(Value, #state{observers=Observers}) ->
  Message = {?MODULE, Value},
  lists:foreach(fun
    ({gen_server, Pid}) ->
      gen_server:cast(Pid, Message);
    ({process, Pid}) ->
        Pid ! Message;
    ({function, Fun}) ->
        Fun(Message)
  end, Observers).

