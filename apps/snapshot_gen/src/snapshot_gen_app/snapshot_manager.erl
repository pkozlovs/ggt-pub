-module(snapshot_manager).
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/api/include/api.hrl").

-export([test_edit_snapshot/0, edit_snapshot/7, create_snapshot/6,
        delete_snapshots/1, test_delete_snapshot/0, rename_snapshot/2]).

%% Edits snapshot generated in snapshot_generator:test_gen_snapshot/0
test_edit_snapshot() ->
  ProjID = <<"4359">>, % giant project
    Endpoint = #endpoint{
      name = gitlab,
      url = <<"https://cs-gitlab-db.cs.surrey.sfu.ca/api/v3/projects">>,
      token = <<"8HBrEVrzPcFEfj6NSLzM">>
    },
    %Start = {{2016, 6, 1}, {0, 0, 0}},
    %End = {{2016, 7, 1}, {23, 59, 58}},
    Start = {{2016, 6, 1}, {0, 0, 1}},
    End = {{2016, 7, 1}, {24, 59, 59}},
    SnapshotId = 
    <<12,30,128,220,13,215,101,61,91,99,86,153,220,230,157,51,120,36,189,194>>,
    edit_snapshot({snapshot_id, SnapshotId}, ProjID, Endpoint, Start, 
                  End, <<"apa53">>, <<"full edit using gen">>).

test_delete_snapshot() ->
  SnapshotId = 
  <<12,30,128,220,13,215,101,61,91,99,86,153,220,230,157,51,120,36,189,194>>,
  delete_snapshots({snapshot_ids, [SnapshotId]}).

-spec delete_snapshots({snapshot_ids, [db_snapshot:id()]}
) -> result:either(Count :: non_neg_integer(), any()).
delete_snapshots({snapshot_ids, SnapshotIDs}) ->
  db:delete_snapshots({snapshot_ids, SnapshotIDs}).

-spec rename_snapshot(
  {snapshot_id, db_snapshot:id()}, 
  binary()
) -> result:either(db_snapshot:self(), any()).
rename_snapshot({snapshot_id, SnapshotID}, NewName) ->
  db:rename_snapshot({snapshot_id, SnapshotID}, NewName).

-spec create_snapshot(
  SourceProjId :: binary(),
  Endpoint :: #endpoint{},
  Start :: calendar:datetime(),
  End :: calendar:datetime(),
  CreatorUsername :: binary(),
  Title :: binary()
) -> result:either(db_snapshot:self(), snapshot_generator:error()).
create_snapshot(SourceProjId, Endpoint, Start, End, CreatorUsername, Title) ->
  snapshot_generator:gen_snapshot(SourceProjId, Endpoint, Start, End,
                                  CreatorUsername, Title).

-spec edit_snapshot(
  SnapshotID :: {snapshot_id, db_snapshot:id()},
  ProjID :: binary(),
  Endpoint :: #endpoint{},
  NewStart :: calendar:datetime(),
  NewEnd :: calendar:datetime(),
  CreatorUsername :: binary(),
  NewTitle :: binary()
) -> result:either(db_snapshot:self(), snapshot_generator:error()).
 edit_snapshot(
   {snapshot_id, SnapshotID}, ProjID, Endpoint, NewStart, 
   NewEnd, CreatorUsername, NewTitle
) ->
  case edit_type(
    {snapshot_id, SnapshotID}, NewTitle, NewStart, NewEnd) of 
    rename ->
      db:rename_snapshot({snapshot_id, SnapshotID}, NewTitle);
    full_edit ->
      db:delete_snapshot({snapshot_id, SnapshotID}),
      MaybeSnapshot = snapshot_generator:gen_snapshot(ProjID, Endpoint, 
                                                     NewStart, NewEnd, 
                                                     CreatorUsername, NewTitle),
      handle_edit_result(MaybeSnapshot);
    {error, Reason} ->
      {error, {?MODULE, {db, failed_edit, snapshot, Reason}}}
  end.

%% Determine type of edit of the snapshot to be made
-spec edit_type(
  {snapshot_id, db_snapshot:id()},
  NewTitle :: binary(),
  NewStartDate :: calendar:datetime(),
  NewEndDate :: calendar:datetime()
) -> result:either(full_edit, rename, any()).
edit_type({snapshot_id, SnapshotId}, _NewTitle, NewStartDate, NewEndDate) ->
  case db:get_snapshot({snapshot_id, SnapshotId}) of
    {error, none} -> erlang:error({none, {snapshot, SnapshotId}});
    {ok, Snapshot} -> 
      {OldStartDateSec, OldEndDateSec} = db_snapshot:date_range_sec(Snapshot),
      NewStartDateSec = 
        calendar:datetime_to_gregorian_seconds(NewStartDate),
      NewEndDateSec = 
        calendar:datetime_to_gregorian_seconds(NewEndDate),
      if OldStartDateSec =/= NewStartDateSec;
        OldEndDateSec =/= NewEndDateSec ->
          full_edit;
        true -> 
          rename
      end
  end.

handle_edit_result({error, Reason}) ->
  {error, {?MODULE, {db, failed_edit_recreation, snapshot, Reason}}};
handle_edit_result({ok, Snapshot}) ->
  {ok,{edit_success, Snapshot}}.
