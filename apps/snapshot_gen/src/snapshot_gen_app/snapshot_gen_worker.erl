-module(snapshot_gen_worker).
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("apps/snapshot_gen/include/snapshot_generator.hrl").
-export([init/1, gen_snapshot/7, regen_snapshot_analysis/4]).
-export_type([error/0]).

%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%

-type error() ::
  {?MODULE,
    {api, failed_read, project_info, any()}
    | {db, failed_create, snapshot, any()}
    | {analysis_crashed, any()}
    | {unexpected_message, any()}
    | git_core_analysis:error()
    | source_analysis:error()
    | meta_analysis:error()
  }.

init(Args) ->
   #{
    proj_id := ProjId,
    endpoint := Endpoint,
    start := Start,
    'end' := End,
    creator := UserName,
    title := Title,
    server := Server
  } = Args,
  gen_snapshot(ProjId, Endpoint, Start, End, UserName, Title, Server).

-spec gen_snapshot(
  SourceProjId :: binary(),
  Endpoint :: #endpoint{},
  Start :: calendar:datetime(),
  End :: calendar:datetime(),
  CreatorUsername :: binary(),
  Title :: binary(),
  Server :: pid()
) -> result:either(db_snapshot:self(), error()).
gen_snapshot(ProjID, Endpoint, Start, End, CreatorUsername, Title, Server) ->
  %% Basic Repo Info
  Source = Endpoint#endpoint.name,
  JSON = project_info(Source, ProjID, Endpoint),
  Project = project_from_json(JSON, Source, ProjID),
  create_or_retrieve_project(Project),
  ProjSnap = create_snapshot(Project, Start, End, CreatorUsername, Title),
  update_server_snapshot(ProjSnap, Server),
  proc_bus:notify_send(snapshots),  %% a sneaky bus
  AnalysesResult = start_analyses(ProjSnap, CreatorUsername, Endpoint),
  CombinedAnalysis = combined_analysis(AnalysesResult),
  FinalResult = handle_final_result(CombinedAnalysis, ProjSnap),
  proc_bus:notify_send(snapshots),  %% a sneaky bus
  exit(FinalResult).

regen_snapshot_analysis(SnapshotID, CreatorUsername, Endpoint, _Server) ->
  MaybeSnapshot = db:get_snapshot({snapshot_id, SnapshotID}),
  MaybeProject = result:flatMap(MaybeSnapshot,
    fun(Snap) -> db:get_project({snapshot_id, db_snapshot:id(Snap)}) end 
  ),
  ProjSnap = result:zip(MaybeProject, MaybeSnapshot),
  AnalysesResult = start_analyses(ProjSnap, CreatorUsername, Endpoint),
  CombinedAnalysis = combined_analysis(AnalysesResult),
  FinalResult = handle_final_result(CombinedAnalysis, ProjSnap),
  proc_bus:notify_send(snapshots),  %% a sneaky bus
  exit(FinalResult).
  
%%%  _____      _            _                  _____ _____
%%% |  __ \    (_)          | |           /\   |  __ \_   _|
%%% | |__) | __ ___   ____ _| |_ ___     /  \  | |__) || |
%%% |  ___/ '__| \ \ / / _` | __/ _ \   / /\ \ |  ___/ | |
%%% | |   | |  | |\ V / (_| | ||  __/  / ____ \| |    _| |_
%%% |_|   |_|  |_| \_/ \__,_|\__\___| /_/    \_\_|   |_____|
%%%
%
% +-----------------+
% | Basic Repo Info |
% +-----------------+------------+-----------------+--------------------+
%         Git Core: | Clone Repo | Analyze Commits | Users from Commits |
%                   +------------+--------+--------+----------+---------+
%           Source: | Get MRs from GitLab | Users from GitLab |         |
%                   +---------------------+-------------------+ - - - ->v
%                   |            |        |        |          |         |
%                   |            |        |        |          |         |
%                   v------------v--------v--------v----------v---------v
%                   |                   Datebase                        |
%                   +---------------------------------------------------+-----------------+
%                                                                       | Merged Analysis |
%                                                                       +-----------------+


%% Project Info

% Get JSON for a given project, or an error on failure.
project_info(_Source, ProjID, Endpoint) -> % TODO: remove Source
  Result = api:read(project, Endpoint, [{project_id, ProjID}]),
  case Result of
    {ok, _Json} -> 
      Result;
    {error, Reason} -> 
      {error, 
        {?MODULE, {api, failed_read, project_info, Reason}}}
  end.
  
% get project fields 
project_from_json({ok, Json}, Source, ProjID) ->
  #{
    name := Name,
    web_url := WebUrl,
    http_url := GitUrl
  } = Json,
  Project = db:construct_project({Source, ProjID}, Name, WebUrl, GitUrl),
  {ok, Project};
project_from_json({error, Reason}, _Source, _ProjID) ->
  {error, Reason}.

%% gets project, or creates it if none exists.
create_or_retrieve_project({ok, Project}) ->
  {Source, ProjID} = db_project:sp_id(Project),
  case db:get_project(Source, ProjID) of
    {error, none} ->
      Result = db:create_project(Project),
      Result;
    {ok, Project} -> 
      {ok, Project}
  end;
create_or_retrieve_project({error, Reason}) ->
  {error, Reason}.

%% creates a snapshot a snapshot, errors if such snapshot exists.
create_snapshot({ok, Project}, Start, End, Creator, Title) ->
  User = db:construct_user(Creator),
  ProjId = db_project:id(Project),
  Snapshot = db:construct_snapshot(ProjId, Start, End, 
                                   now, db_user:id(User), Title, in_progress),
  case db:create_snapshot(Snapshot) of
    {ok, Snapshot} -> {ok, {Project, Snapshot}};
    {error, Reason} 
      -> {error, 
            {?MODULE, {db, failed_create, snapshot, Reason}}}
  end;
create_snapshot({error, Reason}, _Start, _End, _Username, _Title) ->
  {error, Reason}.

update_server_snapshot({ok, {_P, Snapshot}}, Pid) ->
  Id = db_snapshot:id(Snapshot),
  snapshot_server:update_async(Pid, {snapshot_id, Id});
update_server_snapshot(Error={error, _Reason}, _) ->
  Error.
      
%% Analysis
-spec start_analyses(
  result:either({db_project:self(), db_snapshot:self()}, any()),
  Username :: binary(),
  Endpoint :: endpoint()
) -> result:either(any(), any()). % TODO: make this spec more specific.
start_analyses({ok, {Project, Snapshot}}, Username, Endpoint) ->
  process_flag(trap_exit, true),
  start_git_core(Project, Snapshot, Username, Endpoint),
  StartState = #analyses_state{
    project = Project,
    snapshot = Snapshot
  },
  case receive_git_core(StartState) of
    {ok, NewState} ->
      start_source_analysis(Project, Snapshot, Endpoint),
      receive_source_analysis(NewState);
    Error = {error, _} -> Error
  end;
start_analyses({error, Reason}, _Username, _Endpoint) ->
  {error, Reason}.

start_git_core(Project, Snapshot, Username, Endpoint) ->
  Self = self(),
  GitCoreAnalysis = fun() ->
    Token = Endpoint#endpoint.token,
    git_core_analysis:start(Project, Snapshot, Username, Token, Self) 
  end,
  spawn_link(GitCoreAnalysis),
  ok.

receive_git_core(State) ->
  receive
    {ok, {git_core_analysis, {Authors, Commits, Diffs}}} ->
      {ok, State#analyses_state{
        commits = Commits,
        authors = Authors,
        commit_file_diffs = Diffs
      }};
    {error, Reason} ->
      {error, {?MODULE, Reason}};
    {'EXIT', _Pid, normal} ->
      receive_git_core(State);
    {'EXIT', _Pid, Reason} ->
      Snapshot = State#analyses_state.snapshot,
      SnapshotId = db_snapshot:id(Snapshot),
      db:update_snapshot_state(SnapshotId, {failed, Reason}),
      {error, {?MODULE, {analysis_crashed, Reason}}};
    Unexpected ->
      Error = {error, {?MODULE, {unexpected_message, Unexpected}}},
      Snapshot = State#analyses_state.snapshot,
      SnapshotId = db_snapshot:id(Snapshot),
      db:update_snapshot_state(SnapshotId, {failed, Error}),
      Error
  end.

start_source_analysis(Project, Snapshot, Endpoint) ->
  Self = self(),
  SourceAnalysis = fun() -> 
    source_analysis:start(Project, Snapshot, Endpoint, Self)
  end,
  spawn_link(SourceAnalysis),
  ok.
  
receive_source_analysis(State) ->
  receive 
    {ok, {source_analysis, 
          {Members, MergeRequests, MRsAndCommits, Notes, Issues, Diffs}
         }} ->
      NewState = State#analyses_state{
        merge_requests = MergeRequests,
        members = Members,
        merge_request_commits = MRsAndCommits,
        notes = Notes,
        merge_request_diffs = Diffs,
        issues = Issues
      },
      {ok, NewState};
    {error, Reason} ->
      {error, {?MODULE, Reason}};
    %% link error handling
    {'EXIT', _Pid, normal} ->
      receive_source_analysis(State);
    {'EXIT', _Pid, Reason} ->
      Snapshot = State#analyses_state.snapshot,
      SnapshotId = db_snapshot:id(Snapshot),
      db:update_snapshot_state(SnapshotId, {failed, Reason}),
      {error, {?MODULE, {analysis_crashed, Reason}}};
    Unexpected ->
      Error = {error, {?MODULE, {unexpected_message, Unexpected}}},
      Snapshot = State#analyses_state.snapshot,
      SnapshotId = db_snapshot:id(Snapshot),
      db:update_snapshot_state(SnapshotId, {failed, Error}),
      Error
  end.

%-spec receive_analyses(
  %Received :: [git_core_analysis | source_analysis], 
  %State :: analyses_state()
%) -> result:either(analyses_state(), any()).
%receive_analyses(Received, State) ->
  %case are_all_received(Received) of
    %true ->
      %{ok, State};
    %false ->
      %receive 
        %{ok, {git_core_analysis, {Authors, Commits, Diffs}}} ->
          %NewState = State#analyses_state{
            %commits = Commits,
            %authors = Authors,
            %commit_file_diffs = Diffs
          %},
          %receive_analyses([git_core_analysis | Received], NewState);
        %{ok, {source_analysis, 
              %{Members, MergeRequests, MRsAndCommits, Notes, Issues, Diffs}
             %}} ->
          %NewState = State#analyses_state{
            %merge_requests = MergeRequests,
            %members = Members,
            %merge_request_commits = MRsAndCommits,
            %notes = Notes,
            %merge_request_diffs = Diffs,
            %issues = Issues
          %},
          %receive_analyses([source_analysis | Received], NewState);
        %{error, Reason} ->
          %{error, {?MODULE, Reason}};
        %%% link error handling
        %{'EXIT', _Pid, normal} ->
          %receive_analyses(Received, State);
        %{'EXIT', _Pid, Reason} ->
          %Snapshot = State#analyses_state.snapshot,
          %SnapshotId = db_snapshot:id(Snapshot),
          %db:update_snapshot_state(SnapshotId, {failed, Reason}),
          %{error, {?MODULE, {analysis_crashed, Reason}}};
        %Unexpected ->
          %Error = {error, {?MODULE, {unexpected_message, Unexpected}}},
          %Snapshot = State#analyses_state.snapshot,
          %SnapshotId = db_snapshot:id(Snapshot),
          %db:update_snapshot_state(SnapshotId, {failed, Error}),
          %Error
      %end
  %end.
      
%are_all_received(Received) ->
  %Processes = [git_core_analysis, source_analysis],
  %IsReceived = fun(Process, Acc) ->
    %Acc andalso lists:member(Process, Received)
  %end,
  %lists:foldl(IsReceived, true, Processes).  

-spec combined_analysis(
  State :: result:either(analyses_state(), any())
) -> result:either(analyses_state(), any()).
combined_analysis({ok, State}) ->
  meta_analysis:start(State);
combined_analysis({error, Reason}) ->
  {error, Reason}.

%% everything is okay
handle_final_result({ok, State}, _ProjSnap) ->
  {ok, State#analyses_state.snapshot};
%% we failed during analysis, set snapshot to "failed"
handle_final_result({error, Reason}, {ok, {_Proj, Snapshot}}) ->
  SnapshotId = db_snapshot:id(Snapshot),
  db:update_snapshot_state(SnapshotId, {failed, Reason}),
  proc_bus:notify_send(snapshots),  
  {error, Reason};
%% we failed somewhere earlier. This sucks.
handle_final_result({error, Reason}, _) ->
  proc_bus:notify_send(snapshots),
  {error, Reason}.

