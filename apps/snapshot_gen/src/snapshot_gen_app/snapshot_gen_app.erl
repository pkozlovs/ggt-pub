-module(snapshot_gen_app).
-behaviour(application).
-export([start/2, stop/1, start/0]).

start() ->
  snapshot_gen_sup:start_link().

start(_StartType, _StartArgs) ->
  start().

stop(_State) ->
  ok.
