-module(gitlab_convert).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/db/include/db.hrl").
%%--------------------------------------------------------------------
%% Exports
%%--------------------------------------------------------------------
-export([unpack_members/2, unpack_member/2]).
-export([unpack_merge_requests/3, unpack_merge_request/3]).
-export([unpack_commit/2]).
-export([unpack_mr_notes/3, unpack_mr_note/3]).
-export([unpack_merge_request_diff/2, unpack_merge_request_diffs/2]).
-export([unpack_issue/3, unpack_issues/3]).

-type error() ::
  {?MODULE, 
    {dmap_failed, 
      members|merge_requests|issues|merge_request_notes|merge_reqest_diffs,
      any()
    }
  }.

                

%%--------------------------------------------------------------------
%% JSON List -> Members
%%--------------------------------------------------------------------
-spec unpack_members(
  Jsons :: [map()],
  ProjectId :: db_project:id()
) -> result:either([db_member:self()], error()).
unpack_members(Jsons, ProjectId) ->
  case notmyjob:dmap(fun(Json) -> unpack_member(Json, ProjectId) end, Jsons) of
    {error, Reason} 
      -> {error, {?MODULE, {dmap_failed, members, Reason}}};
    Members -> {ok, Members}
  end.

%%--------------------------------------------------------------------
%% JSON -> Member
%%--------------------------------------------------------------------
-spec unpack_member(
  Json :: map(),
  ProjectId :: db_project:id()
) -> db_member:self().
unpack_member(Json, ProjectId) ->
  #{
    username := Username,
    name := Name,
    url := WebUrl
  } = Json,
  db:construct_member(Username, Name, WebUrl, ProjectId).

%%--------------------------------------------------------------------
%% JSON List -> Merge Requests
%%--------------------------------------------------------------------
-spec unpack_merge_requests(
  JsonList :: [map()],
  ProjectId :: db_project:id(),
  Members :: [db_member:self()]
) -> result:either([db_merge_request:self()], error()).
unpack_merge_requests(JsonList, ProjectId, Members) ->
  MemberMap = maps:from_list(lists:map(fun(Member) ->
    {db_member:username(Member), Member}
  end, Members)),
  MaybeMergeRequests = notmyjob:dmap(fun(Json) ->
    unpack_merge_request(Json, ProjectId, MemberMap)
  end, JsonList),
  case MaybeMergeRequests of
    {error, Reason} 
      -> {error, {?MODULE, {dmap_failed, merge_requests, Reason}}};
    MergeRequests -> {ok, MergeRequests}
  end.

%%--------------------------------------------------------------------
%% JSON -> Merge Request
%%--------------------------------------------------------------------
-spec unpack_merge_request(
  Json :: map(),
  ProjectId :: db_project:id(),
  MemberMap :: map() % {username => Member}
) -> db_merge_request:self().
unpack_merge_request(Json, ProjectId, MemberMap) ->
  #{
    id := IdAtSource,
    iid := IidAtSource,
    title := Title,
    message := Message,
    auname := AuthorName,
    asname := AssigneeName,
    state := State,
    tbranch := TargetBranch,
    sbranch := SourceBranch,
    sha := Sha,
    merge_sha := MergeSha,
    url := Url
  } = Json,
  FakeMember = db:construct_fake_member(ProjectId),
  Author = maps:get(AuthorName, MemberMap, FakeMember),
  AuthorId = safe_member_id(Author),
  Assignee = maps:get(AssigneeName, MemberMap, none),
  AssigneeId = safe_member_id(Assignee),
  MergeRequest = db:construct_merge_request(IdAtSource, IidAtSource, ProjectId, Title, 
    Message, AuthorId, AssigneeId, State, TargetBranch, SourceBranch, Sha, 
    MergeSha, Url),
  MergeRequest.

-spec unpack_issues(
  JsonList :: [map()],
  db_project:id(),
  [db_member:self()]
) -> result:either([db_issue:self()], error()).
unpack_issues(JsonList, ProjectId, Members) ->
  MemberMap = maps:from_list(lists:map(fun(Member) ->
    {db_member:username(Member), Member}
  end, Members)),
  MaybeIssues = notmyjob:dmap(fun(Json) ->
    unpack_issue(Json, ProjectId, MemberMap)
  end, JsonList),
  case MaybeIssues of 
    {error, Reason} 
      -> {error, {?MODULE, {dmap_failed, issues, Reason}}};
    Issues -> {ok, Issues}
  end.

unpack_issue(Json, ProjectId, MemberMap) ->
  #{
    id := IdAtSource,
    title := Title,
    message := Message,
    state := State,
    date := Date,
    auname := AuthorName,
    asname := AssigneeName,
    url := Url
  } = Json,
  FakeMember = db:construct_fake_member(ProjectId),
  Author = maps:get(AuthorName, MemberMap, FakeMember),
  AuthorId = safe_member_id(Author),
  Assignee = maps:get(AssigneeName, MemberMap, none),
  AssigneeId = safe_member_id(Assignee),
  db:construct_issue(ProjectId, IdAtSource, Title, Message, State,
                             Date, AuthorId, AssigneeId, Url).

%%--------------------------------------------------------------------
%% Safely get the ID of a member, or `none` if none passed in.
%%-------------------------------------------------------------------- 
-spec safe_member_id(none) -> none; (Member :: db_member:self()) -> db_member:id(). 
safe_member_id(none) -> 
  none;
safe_member_id(Member) ->
  db_member:id(Member).

%%--------------------------------------------------------------------
%% JSON -> Commit
%%--------------------------------------------------------------------
-spec unpack_commit(
  Json :: map(),
  ProjId :: db_project:id()
) -> {db_commit:self(), db_author:self()}.
unpack_commit(Json, ProjId) ->
  #{
      sha := Sha,
      aname := Name,
      aemail := Email,
      date := Date,
      pshas := ParentShas,
      title := Title,
      message := Message
  } = Json,
  Author = db:construct_commit_author(Name, Email),
  Commit = db:construct_commit(ProjId, Title, Message, Sha, ParentShas,
            db_author:id(Author), Date),
  {Commit, Author}.

%%--------------------------------------------------------------------
%% JSON List -> Merge Request Notes
%%--------------------------------------------------------------------
-spec unpack_mr_notes(
  JsonList :: [map()],
  MergeRequest :: db_merge_request:self(),
  Members :: [db_member:self()]
) -> [db_note:self()].
unpack_mr_notes(JsonList, MergeRequest, Members) ->
  MemberMap = maps:from_list(lists:map(fun(Member) ->
    {db_member:username(Member), Member}
  end, Members)),
  lists:filtermap(fun(Json) -> 
    case unpack_mr_note(Json, MergeRequest, MemberMap) of
      {ok, Note} -> {true, Note};
      {error, _} -> false
    end
  end, JsonList).

%%--------------------------------------------------------------------
%% JSON -> Merge Request Note
%%--------------------------------------------------------------------
-spec unpack_mr_note(
  Json :: map(),
  MergeRequest :: db_merge_request:self(),
  Members :: map() % {username => db_member:self()}
) -> result:either(db_note:self(), system_note).
unpack_mr_note(Json, MergeRequest, MemberMap) ->
  #{
      auname := Name,
      date := Date,
      body := Body,
      system := System,
      id := IdAtSource
  } = Json,
  case System of
    true ->
      {error, system_note};
    false ->
      MergeRequestId = db_merge_request:id(MergeRequest),
      ProjId = db_merge_request:project_id(MergeRequest),
      FakeMember = db:construct_fake_member(ProjId),
      Member = maps:get(Name, MemberMap, FakeMember),
      Note = db:construct_note(db_member:id(Member), MergeRequestId, Date,
                               Body, merge_request, IdAtSource),
      {ok, Note}
  end.

unpack_merge_request_diffs(JsonList, MergeRequest) ->
  MaybeMRDs = notmyjob:dmap(fun(Json) ->
    unpack_merge_request_diff(Json, MergeRequest)
  end, JsonList),
  case MaybeMRDs of
    {error, Reason} 
      -> {error, {?MODULE, {dmap_failed, merge_request_diffs, Reason}}};
    List -> {ok, List}
  end.

unpack_merge_request_diff(Json, MergeRequest) ->
  #{
    name := Name,
    diff := Diff
  } = Json,
  MergeRequestId = db_merge_request:id(MergeRequest),
  db:construct_merge_request_diff(MergeRequestId, Name, Diff).


