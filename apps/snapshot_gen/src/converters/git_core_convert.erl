%%%-------------------------------------------------------------------
%%% Alexei Popov Jr <hello@alexpopov.ca>
%%% Tool for getting information from GitLab and wrapping it for analysis.
%%%-------------------------------------------------------------------
-module(git_core_convert).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/db/include/db.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([unpack_commits/2, unpack_commit/2]).
-export([unpack_diffs/2, unpack_diff/2]).

-export_type([unpacked_commit/0]).
-export_type([error/0]).
%%--------------------------------------------------------------------
%% Type definitions
%%--------------------------------------------------------------------
-type unpacked_commit() :: 
  {db_author:self(), db_commit:self(), result:either([db_commit_diff:self()], any())}.

-type error() :: 
  {?MODULE, 
    {dmap, commits|commit_file_diffs, any()}
  }.

%%% ======================================================
%%%  _____       _     _ _                 _____ _____
%%% |  __ \     | |   | (_)          /\   |  __ \_   _|
%%% | |__) |   _| |__ | |_  ___     /  \  | |__) || |
%%% |  ___/ | | | '_ \| | |/ __|   / /\ \ |  ___/ | |
%%% | |   | |_| | |_) | | | (__   / ____ \| |    _| |_
%%% |_|    \__,_|_.__/|_|_|\___| /_/    \_\_|   |_____|
%%%
%%% =======================================================

%%--------------------------------------------------------------------
%% JSON List -> Commits
%%--------------------------------------------------------------------
-spec unpack_commits([map()], key()) 
  -> result:either([unpacked_commit()], error()).
unpack_commits(GitCommits, ProjectId) ->
  MaybeAuthorCommits = notmyjob:dmap(fun(GitCommit) ->
    unpack_commit(GitCommit, ProjectId)
  end, GitCommits),
  case MaybeAuthorCommits of
    {error, Reason} -> {error, {?MODULE, dmap, commits, Reason}};
    AuthorCommits -> 
      case lists:filter(fun(UnpackedCommit) -> 
        is_list(UnpackedCommit) end, AuthorCommits
      ) of
        [] -> ok;
        Else ->
          erlang:exit({'for fucks sake', Else})
      end,
      {ok, AuthorCommits}
  end.

%%--------------------------------------------------------------------
%% JSON -> Commit
%%--------------------------------------------------------------------  
-spec unpack_commit(map(), key()) -> unpacked_commit().
unpack_commit(GitCommit, ProjectId) ->
  #{
    sha := Sha,
    aname := Name,
    aemail := Email,
    date := Date,
    pshas := ParentShas,
    title := Title,
    message := Message,
    files := GitDiffs
  } = GitCommit,
  Author = db:construct_commit_author(Name, Email),
  AuthorId = db_author:id(Author),
  Commit = db:construct_commit(
    ProjectId, Title, Message, Sha, ParentShas, AuthorId, Date
  ),
  Diffs = unpack_diffs(GitDiffs, db_commit:id(Commit)),
  {Author, Commit, Diffs}.

%%--------------------------------------------------------------------
%% JSON List -> Commit File Diffs
%%--------------------------------------------------------------------
-spec unpack_diffs([map()], key()) 
  -> {ok, [db_commit_diff:self()]} | {error, error()}.
unpack_diffs(GitDiffs, CommitId) ->
  case notmyjob:dmap(fun(Diff) -> unpack_diff(Diff, CommitId) end, GitDiffs) of
    {error, Reason} -> {error, {?MODULE, dmap, commit_file_diffs, Reason}};
    Diffs -> {ok, Diffs}
  end.

%%--------------------------------------------------------------------
%% JSON -> Commit File Diff
%%--------------------------------------------------------------------
-spec unpack_diff(map(), key()) -> db_commit_diff:self().
unpack_diff(GitDiff, CommitId) ->
  #{
    name := Name,
    insertions := Ins,
    deletions := Dels,
    net := Net,
    diff := Diff
  } = GitDiff,
  db:construct_commit_file_diff(CommitId, Name, Ins, Dels, Net, Diff).
 

