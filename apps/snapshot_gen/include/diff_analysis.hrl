-ifndef(DIFF_ANALYSIS).
-define(DIFF_ANALYSIS, true).

-type location() :: non_neg_integer().
-type line_location() :: {location(), binary()}.
-type unique_line_location() :: {reference(), line_location()}.
-record(running_scores,
  {
    whitespace  = [] :: [line_location()],
    syntax      = [] :: [line_location()],
    moves       = [] :: [line_location()],
    refactors   = [] :: [line_location()],
    insertions  = [] :: [line_location()],
    deletions   = [] :: [line_location()],
    remaining   = [] :: [line_location()]
  }).

-type running_scores() :: #running_scores{}.

-endif.

