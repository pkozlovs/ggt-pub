-ifndef(TEST_DIFF_ANALYSIS).
-define(TEST_DIFF_ANALYSIS, true).
-define(HOME, hd(hd(element(2, init:get_argument(home))))).
-define(ROOT_DIR, env:config(test_diff_analysis, root_dir, error)).
-define(TEST_DIR, env:config(test_diff_analysis, test_dir, error)).
-define(PROJ_DIR(ProjDir), env:join([?HOME, ?ROOT_DIR, ?TEST_DIR, ProjDir])).

-type path() :: string().
-type score() :: non_neg_integer().
-type scores() :: {score(), score(), score(), score(), score(), score()}.

-record(update, {
  initial :: path(),
  new :: path(),
  scores :: scores()
}).
-type update() :: #update{}.
-type change() :: update().

-record(commit, {
  title :: string(),
  changes :: [change()]
}).
-type commit() :: #commit{}.

-record(merge_request, {
  title :: string(),
  commits :: [commit()]
}).
-type merge_request() :: #merge_request{}.

-type action() :: merge_request().

-record(config, {
  title :: string(),
  username :: string(),
  token :: string(),
  source :: source:source(),
  file_location :: path(),
  root_dir = env:join([?HOME, ?ROOT_DIR, ?TEST_DIR]),
  project_dir :: path(),
  initial_commit :: [path()],
  actions :: [action()]
}).
-type config() :: #config{}.

-endif.
