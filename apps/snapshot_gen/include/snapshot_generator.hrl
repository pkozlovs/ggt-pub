-ifndef(SNAPSHOT_GENERATOR_RECORDS).
-define(SNAPSHOT_GENERATOR_RECORDS, true).

-record(analyses_state, 
  {
    project :: db_project:self() | undefined,
    snapshot :: db_snapshot:self() | undefined,
    commits = [] :: [db_commit:self()],
    commit_diff_scores = [] :: [db_commit_diff_scores:self()],
    diff_score_locations = [] :: [db_diff_score_locations:self()],
    commit_file_diffs = [] :: [db_commit_diff:self()],
    authors = [] :: [db_author:self()],
    author_commits = [] :: [db_author_commit:self()],
    notes = [] :: [db_note:self()] | undefined,
    merge_request_diffs = [] :: [db_merge_request_diff:self()],
    merge_requests = [] :: [db_merge_request:self()] | undefined,
    merge_request_commits = [] :: [{db_merge_request:self(), [binary()]}] 
      | [db_merge_request_commit:self()],
    members = [] :: [db_member:self()],
    merge_request_diff_scores = [] :: [db_merge_request_scores:self()],
    diff_score_multipliers = [] :: [db_score_multipliers:self()],
    issues = [] :: [db_issue:self()]
  }).
-type analyses_state() :: #analyses_state{}.

-endif.
