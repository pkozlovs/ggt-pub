-module(ggs).
-behaviour(supervisor).
-behaviour(application).
-export([init/1, start/2, stop/1, main/1]).
-compile(export_all).

main(A)    -> mad_repl:sh(A).
start(_,_) -> supervisor:start_link({local,ggs},ggs,[]).
stop(_)    -> ok.
init([])   ->
  load_modules(),
  PrivDir = code:priv_dir(server),
  MaybeStarted = cowboy:start_https(
    https, % Protocol
    200, % NBAcceptors
    [ % TransOpts
      {port, wf:config(n2o, port, 443)},
      {certfile, env:join([PrivDir, "ssl/server.crt"])},
      {keyfile, env:join([PrivDir, "ssl/server.key"])},
      {password, "sfuggs"}
    ],
    [ % ProtoOpts
      {env, [
        {dispatch, points()}
      ]}
    ]
  ),
  verify_start(MaybeStarted),
  sup().

load_modules() ->
  [code:ensure_loaded(M) || M <- [
    page_projects,
    page_project,
    page_snapshot,
    page_graphs,
    page_member_mr,
    page_notes,
    page_score,
    
    env,
    hash,
    notmyjob,
    proc_bus,
    time
  ]].

verify_start({ok, _}) ->
  cowboy:start_http(http, 200, [
    {port, wf:config(n2o, port_redirect, 80)}
  ],
  [
    {env, [{dispatch, redirect_point()}]}
  ]);
verify_start({error, _}) -> halt(abort, []).

redirect_point() ->
  cowboy_router:compile([
    {'_', [
      {'_', http_redirect_handler, []}
    ]}
  ]).

sup()    -> { ok, { { one_for_one, 5, 100 }, [] } }.
env()    -> [ { env, [ { dispatch, points() } ] } ].
static() ->   { dir, "apps/client/static", mime() }.
n2o()    ->   { dir, "deps/n2o/priv",      mime() }.
mime()   -> [ { mimetypes, cow_mimetypes, all   } ].
port()   -> [ { port, wf:config(n2o,port,9999)  } ].
points() ->
  cowboy_router:compile( [ { '_', [
    { "/static/[...]",              n2o_static, static() },
    { "/n2o/[...]",                 n2o_static, n2o()    },
    { "/ws/[...]",                  n2o_stream, []       },
    { '_',                          n2o_cowboy, []       }
  ] } ] ).
