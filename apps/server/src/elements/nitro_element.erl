-module(nitro_element).

-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/server/include/elements.hrl").

-opaque dtl() :: #dtl{}.
-export_type([dtl/0]).

-opaque dtl_pure() :: #dtl_pure{}.
-export_type([dtl_pure/0]).
