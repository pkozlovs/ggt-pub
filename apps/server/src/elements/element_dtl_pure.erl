-module(element_dtl_pure).
-author('Maxim Sokhatsky').
-author("Pavel Kozlovsky").
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/server/include/elements.hrl").
-compile(export_all).

%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A modified version of dtl nitrogen element that utilizes pure
%%% Erlydtl rendering bindings as opposed to nitrogen html rendering
%%% @end
%%%-------------------------------------------------------------------

render_element(Record=#dtl_pure{}) ->
  M = list_to_atom(nitro:to_list(Record#dtl_pure.file) ++ "_view"),
  {ok,R} = render(
    M,
    Record#dtl_pure.js_escape,
    Record#dtl_pure.bindings ++ 
    if
      Record#dtl_pure.bind_script==true -> [{script,nitro:script()}];
      true -> []
    end
  ),
  R.

render(M, true, Args) ->
  {ok, R} = M:render(Args),
  {ok, nitro:js_escape(R)};
render(M, _, Args) -> M:render(Args).
