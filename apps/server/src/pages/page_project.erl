%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_project).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/server/include/elements.hrl").
-include_lib("deps/n2o/include/wf.hrl").

-include_lib("apps/api/include/api.hrl").
-include_lib("apps/db/include/db.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).

-compile(export_all).
-compile([{parse_transform, lager_transform}]).
%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  %TO-DO: check for logged in user,
  ProjectHash = project_hash(),
  MaybeData = wireframe:dehash_url(ProjectHash),
  MaybeProject = project(MaybeData),
  dtl(MaybeProject, MaybeData).
%%  main(wireframe:dehash_url(ProjectHash)).

%%--------------------------------------------------------------------
%% @doc Standard websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  load_page_state(),
  #{source := SourceName, proj_id := ProjId} = wf:state(state),
  Token = local:api_token(wf:user(), <<"gitlab">>),
  ws_async:spawn(fun() -> update_projects(Token) end),
  proc_bus:register_process(snapshots, self(), fun() ->
    update_snapshots(SourceName, ProjId), wireframe:actions()
  end),
  client_event:trigger(ready),
  client_event:trigger(list_update);
  % client:event(init);
%%  wf:wire("$(wsWatcher).trigger('init');");
event(terminate) ->
  % client:event(terminate),
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).

%%--------------------------------------------------------------------
%% @doc API websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
api_event(request_createSnapshot, Body, _) ->
  DecBody = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
  wf:info(?MODULE, "~p", [DecBody]),
  #{
    <<"name">>           := Name,
    <<"timestamp_from">> := TimestampFrom,
    <<"timestamp_to">>   := TimestampTo
  } = DecBody,
  CleanName = trim(Name),
  MaybeProjectData = wireframe:dehash_url(project_hash()),
  generate_snapshot(MaybeProjectData, CleanName, TimestampFrom, TimestampTo);

api_event(request_editSnapshot, Body, _) -> 
  DecBody = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
  wf:info(?MODULE, "~p", [DecBody]),
  #{
    <<"name">>           := Name,
    <<"timestamp_from">> := TimestampFrom,
    <<"timestamp_to">>   := TimestampTo,
    <<"snapshot_id">>    := SnapshotID
  } = DecBody,
  BinSnapshotID = hash:hex_to_bin(SnapshotID),
  MaybeProjectData = wireframe:dehash_url(project_hash()),
  edit_snapshot(MaybeProjectData, Name, 
                TimestampFrom, TimestampTo, BinSnapshotID);

api_event(request_deleteSnapshots, Body, _) ->
  DecBody = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
  wf:info(?MODULE, "~p", [DecBody]),
  #{
    <<"snapshot_id">>           := SnapshotIDs
  } = DecBody,
  BinSnapshotIds = lists:map(fun(SnapshotID) ->
    hash:hex_to_bin(SnapshotID)
  end, SnapshotIDs),
  delete_snapshots(BinSnapshotIds).


%%====================================================================
%% Private functions
%%====================================================================
project_hash() -> wireframe:qb(project_id).

wire_api_events() ->
  wf:wire(#api{name = request_createSnapshot, tag = api3}),
  wf:wire(#api{name = request_deleteSnapshots, tag = api3}),
  wf:wire(#api{name = request_editSnapshot, tag = api3}).

user_api_key(SourceName) ->
  Username = wf:user(),
  local:api_token(Username, SourceName).

%%--------------------------------------------------------------------
%% Get project information from remote source
%%--------------------------------------------------------------------
project({ok, {SourceName, ProjectId}}) ->
  Endpoint = api:get_endpoint(SourceName, user_api_key(SourceName)),
  Params = [{project_id, ProjectId}],
  api:read(project, Endpoint, Params);
project({error, Reason}) ->
  {error, Reason}.

%%--------------------------------------------------------------------
%% Generate dtl for specific project
%%--------------------------------------------------------------------
dtl({ok, Project}, {ok, {SourceName, ProjectId}}) ->
  wire_api_events(),
  #{name := ProjectName, namespace := Namespace, web_url := WebUrl} = Project,
  Snapshots = local:snapshots(SourceName, ProjectId),
  TransformedSnapshots = snapshots_to_dtl_pure(Snapshots),
  {LastSnapshotName, 
   LastSnapshotStartDate, 
   LastSnapshotEndDate} = get_last_snapshot_data(wf:user()),
  #dtl_pure{
    file 		= "project",
    ext 		= "html",
    app         = "ggs",
    bindings 	= [
      {username, wf:user()},
      {menu_item_projects, "select"},
      {project_url,        wf:path(?REQ)},
      {project_name,       wf:to_list(ProjectName)},
      {project_namespace,  wf:to_list(Namespace)},
      {project_rurl,       wf:to_list(WebUrl)},
      {snapshots,          TransformedSnapshots},
      {last_snapshot_name, LastSnapshotName},
      {last_snapshot_start_date, LastSnapshotStartDate},
      {last_snapshot_end_date, LastSnapshotEndDate}
    ] 
  };
dtl({error, Reason}, _) ->
  wf:error(?MODULE, "~p", [Reason]),
  #dtl{file = "fourohfour", ext	= "html", app = "ggs"}.

%%--------------------------------------------------------------------
%% Get snapshot list
%%--------------------------------------------------------------------
snapshots(SourceName, ProjectId) ->
  Source = binary_to_atom(SourceName, unicode),
  MaybeProject = db:get_project(Source, ProjectId),
  query_snapshots(MaybeProject).

query_snapshots({ok, Project}) ->
  case db:get_snapshots({project_id, db_project:id(Project)}) of
    {error, _Reason}  -> [];
    {ok, Snapshots}   -> Snapshots
  end;
query_snapshots({error, _Reason}) ->
  [].

%%--------------------------------------------------------------------
%% Transform snapshot list
%%--------------------------------------------------------------------
snapshots_to_dtl_pure(Snapshots) ->
  lists:map(fun(Snapshot) ->
    SnapshotName = db_snapshot:title(Snapshot),
    SnapshotId = hash:bin_to_hex(db_snapshot:id(Snapshot)),
    Url = hackney_url:make_url("", ["snapshots", wf:to_list(SnapshotId)], []),
    Datetime = db_snapshot:create_date(Snapshot),
    Date = time:pretty(Datetime),
    {StartDatetime, EndDatetime} = db_snapshot:date_range(Snapshot),
    StartDatetime_string = time:pretty_date(
      time:change_timezone(StartDatetime, "GMT", "PST")
    ),
    EndDatetime_string = time:pretty_date(
      time:change_timezone(EndDatetime, "GMT", "PST")
    ),
    {ok, Author} = db:get_user({user_id, db_snapshot:creator_id(Snapshot)}),
    AuthorName = db_user:username(Author),
    State = snapshot_state(Snapshot),
    [
      {name,        SnapshotName},
      {url,         Url},
      {date,        Date},
      {author,      AuthorName},
      {state,       State},
      {snapshot_id, SnapshotId},
      {start_date,  StartDatetime_string},
      {end_date,    EndDatetime_string}
    ]
  end, Snapshots).

snapshots_to_dtl(Snapshots) ->
  lists:map(fun(Snapshot) ->
    SnapshotName = db_snapshot:title(Snapshot),
    SnapshotId = hash:bin_to_hex(db_snapshot:id(Snapshot)),
    Url = hackney_url:make_url("", ["snapshots", wf:to_list(SnapshotId)], []),
    Datetime = db_snapshot:create_date(Snapshot),
    Date = time:pretty(Datetime),
    {StartDatetime, EndDatetime} = db_snapshot:date_range(Snapshot),
    StartDatetime_string = time:pretty_date(
      time:change_timezone(StartDatetime, "GMT", "PST")
    ),
    EndDatetime_string = time:pretty_date(
      time:change_timezone(EndDatetime, "GMT", "PST")
    ),
    {ok, Author} = db:get_user({user_id, db_snapshot:creator_id(Snapshot)}),
    AuthorName = db_user:username(Author),
    State = snapshot_state(Snapshot),
    #dtl_pure{
      file = snapshots_item,
      ext = "html",
      bind_script = false,
      bindings = [
        {snapshot, [
          {name,        SnapshotName},
          {url,         Url},
          {date,        Date},
          {author,      AuthorName},
          {state,       State},
          {snapshot_id, SnapshotId},
          {start_date,  StartDatetime_string},
          {end_date,    EndDatetime_string}
        ]}
      ]
    }
  end, Snapshots).

%%--------------------------------------------------------------------
%% Get snapshot data
%%--------------------------------------------------------------------
snapshot_state(Snapshot) ->
  case db_snapshot:simple_state(Snapshot) of
    in_progress -> "processing";
    completed   -> "completed";
    failed -> "failed"
  end.

get_last_snapshot_data(Username) ->
  case db:get_last_snapshot({username, Username}) of
    {ok, Snapshot} -> 
      {
        db_snapshot:title(Snapshot),
        time:datetime_to_timestamp(db_snapshot:start_date(Snapshot)),
        time:datetime_to_timestamp(db_snapshot:end_date(Snapshot))
      };
    _ -> {<<>>, <<>>, <<>>}
  end.

%%--------------------------------------------------------------------
%% Snapshot generation
%%--------------------------------------------------------------------
trim(<<>>) ->
  <<>>;
trim(Bin= <<C,BinTail/binary>>) ->
    case is_whitespace(C) of
        true -> trim(BinTail);
        false -> trim_tail(Bin)
    end.

trim_tail(<<C>>) ->
    case is_whitespace(C) of
        true -> false;
        false -> <<C>>
    end;
trim_tail(<<C,Bin/binary>>) ->
    case trim_tail(Bin) of
        false -> trim_tail(<<C>>);
        BinTail -> <<C,BinTail/binary>>
    end.

is_whitespace(C) -> lists:member(C, "\s\t\r\n").

generate_snapshot(_, <<>>, _, _) ->
  Message = io_lib:format("I'm sorry, ~s. I'm afraid I can't do that.
    This is not a valid snapshot name.", [wf:user()]),
  client_modal:show_present("modal_create_snapshot"),
  client_modal:show_error("Error", Message);
generate_snapshot(_, _, <<>>, _) ->
  Message = io_lib:format("I'm sorry, ~s. I'm afraid I can't do that.
    There is no \"from\" time.", [wf:user()]),
  client_modal:show_present("modal_create_snapshot"),
  client_modal:show_error("Error", Message);
generate_snapshot(_, _, _, <<>>) ->
  Message = io_lib:format("I'm sorry, ~s. I'm afraid I can't do that.
    There is no \"to\" time.", [wf:user()]),
  client_modal:show_present("modal_create_snapshot"),
  client_modal:show_error("Error", Message);
generate_snapshot(
  {ok, {SourceName, ProjectId}},
  Title, TimestampFrom, TimestampTo
) ->
  DatetimeFrom = time:timestamp_to_datetime(TimestampFrom),
  DatetimeTo = time:timestamp_to_datetime(TimestampTo),
  Token = user_api_key(SourceName),
  Username = wf:user(),
  ws_async:spawn(fun() ->
    Endpoint = api:get_endpoint(SourceName, Token),
    Result = snapshot_manager:create_snapshot(
      ProjectId, Endpoint, DatetimeFrom, DatetimeTo, Username, Title
    ),
    verify_snapshot_creation(Result)
  end).

% verify_snapshot_creation({ok, _Debug}) ->
%   proc_bus:notify_send(snapshots),
%   client_modal:show_ok("Success", "Snapshot has been created.");
verify_snapshot_creation({error, {exists, _Debug}}) ->
  client_modal:show_error("Error",
    "Snapshot with this timeframe already exists!");
verify_snapshot_creation({error, Reason}) ->
  io:format("**error: ~p~n", [Reason]),
  {Localized, _Extra} = localizable_error:pretty({error, Reason}),
  wf:wire(#alert{text = Localized}).

delete_snapshots(SnapshotIDs) ->
  ws_async:spawn(fun() ->
    Result = snapshot_manager:delete_snapshots({snapshot_ids, SnapshotIDs}),
    verify_deletion(Result)
  end).

edit_snapshot(_, Name, <<>>, <<>>, SnapshotID) -> 
  ws_async:spawn(fun() ->
    Result = snapshot_manager:rename_snapshot({snapshot_id, SnapshotID}, 
                                                                          Name),
    verify_edit(Result)
  end);

edit_snapshot(ProjectData, Name, <<>>, EndDate, SnapshotID) -> 
  case db:get_snapshot({snapshot_id, SnapshotID}) of 
    {error, Reason} -> {error, {?MODULE, {Reason}}};
    {ok, Snapshot} ->
      SnapshotStartDate = db_snapshot:start_date(Snapshot),
      SnapshotEndDate = time:timestamp_to_datetime(EndDate),
      edit_snapshot({ProjectData, Name, 
                    SnapshotStartDate, SnapshotEndDate, SnapshotID})
  end;

edit_snapshot(ProjectData, Name, StartDate, <<>>, SnapshotID) -> 
  case db:get_snapshot({snapshot_id, SnapshotID}) of 
    {error, Reason} -> {error, {?MODULE, {Reason}}};
    {ok, Snapshot} ->
      SnapshotStartDate = time:timestamp_to_datetime(StartDate),
      SnapshotEndDate = db_snapshot:end_date(Snapshot),
      edit_snapshot({ProjectData, Name, 
                    SnapshotStartDate, SnapshotEndDate, SnapshotID})
  end;

edit_snapshot(ProjectData, Name, StartDate, EndDate, SnapshotID) -> 
  SnapshotStartDate = time:timestamp_to_datetime(StartDate),
  SnapshotEndDate = time:timestamp_to_datetime(EndDate),
  edit_snapshot({ProjectData, Name, 
                SnapshotStartDate, SnapshotEndDate, SnapshotID}).

edit_snapshot(
  {{ok, {SourceName, ProjectID}}, Name, StartDate, EndDate, SnapshotID}
) -> 
  Token = user_api_key(SourceName),
  Username = wf:user(),
  ws_async:spawn(fun() ->
    Endpoint = api:get_endpoint(SourceName, Token),
    Result = snapshot_manager:edit_snapshot(
      {snapshot_id, SnapshotID}, ProjectID, Endpoint, 
      StartDate, EndDate, Username, Name),
      verify_edit(Result)
  end).

verify_deletion({ok, _}) ->
  proc_bus:notify_send(snapshots),
  client_modal:show_ok("Success", "Snapshots have been deleted.");
verify_deletion({error, Reason}) ->
  io:format("**error: ~p~n", [Reason]),
  {Localized, _Extra} = localizable_error:pretty({error, Reason}),
  wf:wire(#alert{text = Localized}).

verify_edit({ok, _}) ->
  proc_bus:notify_send(snapshots),
  client_modal:show_ok("Success", "Snapshot has been edited.");
verify_edit({error, Reason}) ->
  io:format("**error: ~p~n", [Reason]),
  {Localized, _Extra} = localizable_error:pretty({error, Reason}),
  wf:wire(#alert{text = Localized}).
%%--------------------------------------------------------------------
%% WS Process state
%%--------------------------------------------------------------------
load_page_state() ->
  {ok, {SourceName, ProjId}} = wireframe:dehash_url(project_hash()),
  State = #{
    source  => SourceName,
    proj_id => ProjId
  },
  io:format("~p~n", [State]),
  wf:state(state, State).

%%--------------------------------------------------------------------
%% Snapshot update watcher
%%--------------------------------------------------------------------
update_snapshots(SourceName, ProjId) ->
  Snapshots = snapshots(SourceName, ProjId),
  TransformedSnapshots = snapshots_to_dtl(Snapshots),
  client_dom:inner_html(list_snapshots, TransformedSnapshots),
  client_event:trigger(list_update).

%%--------------------------------------------------------------------
%% Retrieves a list of available projects and sends it to the client
%%--------------------------------------------------------------------
update_projects(Token) ->
  % Token will be a session variable
  % Endpoint = ?EP_GITLAB_SFU(Token),
  case remote:projects(<<"gitlab">>, Token) of
    {ok, Projects} ->
      ProjectList = dtl_transform_projects(Projects),
      wireframe:update(dd_project_list, ProjectList);
    {error, Reason} ->
      wf:wire(#alert{text = wf:to_list(Reason)})
  end.

%%--------------------------------------------------------------------
%% Transforms a list of project maps to nitrogeb templates
%%--------------------------------------------------------------------
dtl_transform_projects(Projects) ->
  notmyjob:dmap(fun(Project)->
    #{id := Id, name := Name} = Project,
    Url = [
      "/projects/", 
      wf:to_list(wireframe:enhash_url(<<"gitlab">>, wf:to_binary(Id)))
    ],
    #dtl{
      file = projects_item_dropdown,
      ext = "html",
      bind_script = false,
      bindings = [
        {name, wf:to_list(Name)}, {url, Url}
      ]
    }
  end, Projects).
