%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc /snapshots/:id/:username/merge_requests
%%% @end
%%%-------------------------------------------------------------------
-module(page_member_notes).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
-include_lib("apps/db/include/db.hrl").

-include_lib("apps/server/include/elements.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  MaybeSnapshotId = hash:hex_to_bin(snapshot_hash()),
  MaybeSnapshot = local:snapshot(MaybeSnapshotId),
  MaybeProject = local:project(MaybeSnapshot),
  MaybeMember = local:member(MaybeProject, member_username()),
  main_dtl(MaybeProject, MaybeSnapshot, MaybeMember).

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  client:event(init);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Query cowboy bindings
%%--------------------------------------------------------------------
snapshot_hash() -> wireframe:qb(snapshot_hash).
member_username() -> wireframe:qb(username).

main_dtl({ok, Project}, {ok, Snapshot}, {ok, Member}) ->
  LoggedUser = wf:user(),
  ProjectName = db_project:name(Project),
  SnapshotName = db_snapshot:title(Snapshot),
  MemberName = db_member:name(Member),
  MemberUsername = db_member:username(Member),

  {Source, SourceId} = db_project:sp_id(Project),
  ProjectUrl = hackney_url:make_url(<<"">>, [
    <<"projects">>, wireframe:enhash_url(wf:to_binary(Source), SourceId)
  ], []),
  SnapshotUrl = hackney_url:make_url(<<"">>, [ 
    <<"snapshots">>, snapshot_hash()
  ], []),

  OtherMembers = dtl_transform_members(local:members(db_project:id(Project))),

  Notes = local:merge_request_notes(
            db_snapshot:id(Snapshot), 
            db_member:id(Member)
  ),
  NotesTransformed = dtl_transform_notes(
    Notes,
    db_project:web_url(Project)
  ),
  
  Score = db_diff_scores:flatten(
    result:value(db:get_total_commit_score(
      {snapshot_id, db_snapshot:id(Snapshot)},
      {member_id, db_member:id(Member)}
    ))
  ),
  WordCount = local:count_words_in_notes(Notes),
  
  #dtl_pure{
    file 		  = "member_notes",
    ext 		  = "html",
    app       = "ggs",
    bindings 	= [
      {username,            LoggedUser},

      {menu_item_snapshots, "select"},
      {select_notes,        "select"},

      {project_name,        ProjectName},
      {snapshot_name,       SnapshotName},
      {member_name,         MemberName},
      {member_username,     MemberUsername},
      {members,             OtherMembers},

      {project_url,         ProjectUrl},
      {snapshot_url,        SnapshotUrl},
      {snapshot_hash,       snapshot_hash()},
      {score,               Score},
      {word_count,          WordCount},  

      {notes,               NotesTransformed}
    ] 
  };
main_dtl(_, _, _) ->
  #dtl{file = "fourohfour", ext	= "html", app = "ggs"}.

dtl_transform_members(Members) ->
  lists:map(fun(Member) ->
    Url = hackney_url:make_url("", [
      <<"snapshots">>,
      snapshot_hash(),
      db_member:username(Member),
      <<"notes">> 
    ], []),
    [
      {name,  db_member:username(Member)},
      {url,   Url}
    ]
  end, Members).

dtl_transform_notes(Notes, ProjectUrl) ->
  lists:map(fun(Note) ->
    Words = binary:split(db_note:body(Note), [<<" ">>, <<"\n">>], [global, trim_all]),
    WordCount = erlang:length(Words),
    {ok, MergeRequest} = db:get_merge_request(
      {merge_request_id, db_note:merge_request_id(Note)}
    ),
    Url = hackney_url:make_url(ProjectUrl, [
      <<"merge_requests">>,
      db_merge_request:iid_at_source(MergeRequest),
      <<"#note_", (db_note:id_at_source(Note))/binary>>
    ], []),
    AuthorId = db_note:creator_id(Note),
    Direction = case db_merge_request:creator_id(MergeRequest) of
      AuthorId -> "Self";
      _        -> "Others"
    end,
    [
      {url,  Url},
      {body, web_convert:html_encode(wf:to_list(db_note:body(Note)))},
      {date, time:pretty_date(db_note:create_date(Note))},
      {type, db_note:type(Note)},
      {direction, Direction},
      {word_count, WordCount}
    ]
  end, Notes).
