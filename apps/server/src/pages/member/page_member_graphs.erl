%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc /snapshots/:id/:username/graphs
%%% @end
%%%-------------------------------------------------------------------
-module(page_member_graphs).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
-include_lib("apps/db/include/db.hrl").

-include_lib("apps/server/include/elements.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).

-spec main() -> #dtl{}|#dtl_pure{}.
main() ->
  SnapshotId_hex = wireframe:qb(snapshot_hash),
  MemberUsername = wireframe:qb(username),
  MaybeSnapshotId = hash:hex_to_bin(SnapshotId_hex),

  MaybeSnapshot = local:snapshot(MaybeSnapshotId),
  MaybeProject = local:project(MaybeSnapshot),
  MaybeMember = local:member(MaybeProject, MemberUsername),
  main_dtl(MaybeProject, MaybeSnapshot, MaybeMember).

main_dtl({ok, Project}, {ok, Snapshot}, {ok, Member}) ->
  Username = wf:user(),
  ws_state:create_state([
    {username, Username},
    {project, Project},
    {snapshot, Snapshot},
    {member, Member}
  ]),

  ProjectName = db_project:name(Project), 
  SnapshotName = db_snapshot:title(Snapshot),
  MemberName = db_member:name(Member),
  MemberUsername = db_member:username(Member),
  MemberId = db_member:id(Member),

  SnapshotId = db_snapshot:id(Snapshot),
  SnapshotId_hex = wireframe:qb(snapshot_hash),
  {Source, SourceId} = db_project:sp_id(Project),
  ProjectUrl = hackney_url:make_url(<<"">>, [
    <<"projects">>, wireframe:enhash_url(wf:to_binary(Source), SourceId)
  ], []),
  SnapshotUrl = hackney_url:make_url(<<"">>, [
    <<"snapshots">>, SnapshotId_hex
  ], []),
  
  Score = db_diff_scores:flatten(
    result:value(db:get_total_commit_score(
      {snapshot_id, SnapshotId},
      {member_id, MemberId}
    ))
  ),
%%   ScoreString = float_to_list(float(Score), [{decimals,2}]),
  
  Notes = local:merge_request_notes(SnapshotId, MemberId),
  WordCount = local:count_words_in_notes(Notes),

  wrapper_dtl:construct_page(
    "member_graphs",
    [
      {username, Username},

      {menu_item_snapshots, "select"},
      {select_graphs, "select"},

      {project_name,        ProjectName},
      {snapshot_name,       SnapshotName},
      {member_name,         MemberName},
      {member_username,     MemberUsername},

      {project_url,         ProjectUrl},
      {snapshot_url,        SnapshotUrl},
      {snapshot_hash,       SnapshotId_hex},
      {score,               Score},
      {word_count,          WordCount}	
    ]
  );
main_dtl(_, _, _) ->
  #dtl{file = "fourohfour", ext = "html", app = "ggs"}.

-spec event(atom()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  Project = ws_state:get_state(project),
  Snapshot = ws_state:get_state(snapshot),
  ws_async:spawn(fun() ->
    update_members(Project, Snapshot)
  end),
  Member = ws_state:get_state(member),
  ws_async:spawn(fun() ->
    update_graphs(Snapshot, Member)
  end),
  ok;
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).

update_members(Project, Snapshot) ->
  Members = local:members(db_project:id(Project)),
  SnapshotId_hex = hash:bin_to_hex(db_snapshot:id(Snapshot)),
  Url = hackney_url:make_url("", [
    <<"snapshots">>, SnapshotId_hex
  ], []),
  Members_DTL = lists:map(fun(Member) ->
    wrapper_dtl:construct_snippet(
      "li_dd_member",
      [
        {name, db_member:name(Member)},
        {url, [Url, "/", db_member:username(Member)]}
      ]
    )
  end, Members),
  client_dom:inner_html(dd_member_list, Members_DTL).


update_graphs(Snapshot, Member) ->
  {{StartDate, _}, {EndDate, _}} = db_snapshot:date_range(Snapshot),
  DateRange = [pre|date_range(StartDate, EndDate)],
  DateRange_JSON = convert_json:convert_date_range(DateRange),
  SnapshotId = db_snapshot:id(Snapshot),
  Commits = local:commits(SnapshotId, db_member:id(Member)),
  CommitBuckets = commit_buckets(Commits, DateRange),
  CommitBuckets_JSON = convert_json:convert_buckets(CommitBuckets),

  MRs = local:merge_requests(Snapshot, Member),
  MRBuckets = mr_buckets(MRs, DateRange),
  MRBuckets_JSON = convert_json:convert_buckets(MRBuckets),

  Notes = local:merge_request_notes(SnapshotId, db_member:id(Member)),
  NoteBuckets = note_buckets(Notes, DateRange),
  NoteBuckets_JSON = convert_json:convert_buckets(NoteBuckets),

  UnifiedData = #{
    date_range => DateRange_JSON,
    commits => CommitBuckets_JSON,
    merge_requests => MRBuckets_JSON,
    notes => NoteBuckets_JSON
  },
  UnifiedData_JSON = jsone:encode(UnifiedData),
  client_event:trigger(update_graphs, UnifiedData_JSON).

date_range(SameDate, SameDate) ->
  [SameDate];
date_range(HeadDate, EndDate) ->
  [HeadDate|date_range(edate:shift(HeadDate, 1, days), EndDate)].

commit_buckets([], _) ->
  #{};
commit_buckets([Commit|Rest], DateRange) ->
  {CommitDate, _} = db_commit:create_date(Commit),
  Map = commit_buckets(Rest, DateRange),
  case lists:member(CommitDate, DateRange) of
    true ->
      insert_count(CommitDate, Map);
    false ->
      insert_count(pre, Map)
  end.

mr_buckets([], _) ->
  #{};
mr_buckets([MR|Rest], DateRange) ->
  {MRDate, _} = db_merge_request:merge_date(MR),
  Map = mr_buckets(Rest, DateRange),
  case lists:member(MRDate, DateRange) of
    true ->
      insert_count(MRDate, Map);
    false ->
      insert_count(pre, Map)
  end.

note_buckets([], _) ->
  #{};
note_buckets([Note|Rest], DateRange) ->
  {NoteDate, _} = db_note:create_date(Note),
  Map = note_buckets(Rest, DateRange),
  case lists:member(NoteDate, DateRange) of
    true ->
      insert_count(NoteDate, Map);
    false ->
      insert_count(pre, Map)
  end.

insert_count(Date, Map) ->
  case maps:find(Date, Map) of
    {ok, Count} ->
      Map#{Date := Count + 1};
    error ->
      Map#{Date => 1}
  end.
