%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc /snapshots/:id/:username/merge_requests
%%% @end
%%%-------------------------------------------------------------------
-module(page_member_scores).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
-include_lib("apps/db/include/db.hrl").
-include_lib("apps/db/include/frontend_structs.hrl").
-include_lib("apps/server/include/elements.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-export([api_event/3]).
-compile([{parse_transform, lager_transform}]).
%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  MaybeSnapshotId = hash:hex_to_bin(snapshot_hash()),
  MaybeSnapshot = local:snapshot(MaybeSnapshotId),
  MaybeProject = local:project(MaybeSnapshot),
  MaybeMember = local:member(MaybeProject, member_username()),
  main_dtl(MaybeProject, MaybeSnapshot, MaybeMember).

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(atom()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  Project = ws_state:get_state(project),
  Snapshot = ws_state:get_state(snapshot),
  Member = ws_state:get_state(member),
  ws_async:spawn(fun() ->
    update_members(Project, Snapshot)
  end),
  OwnedMRs = local:merge_requests(Snapshot, Member),
  ContrMRs = local:unauthored_merge_requests(Snapshot, Member),
  AllMRs = lists:merge(OwnedMRs, ContrMRs),
  AllMRsJson = convert_json:convert_merge_requests(
                 AllMRs, owned,
                 db_project:id(Project),
                 db_snapshot:id(Snapshot),
                 db_member:id(Member)),

  % FirstMRId = case AllMRs of [] -> <<>>; [MR|_] -> MR#merge_request.id end,
  % Commits = local:commits(FirstMRId),
  % CommitsJson = 
  
  %%% NORMAL MR FUNCTIONS
  %client_event:trigger(update_mr, AllMRsJson),
  %client_event:trigger(ready);

  SnapshotId = db_snapshot:id(Snapshot),
  MemberId   = db_member:id(Member),
  MemberEmails = local:member_emails(
    db_project:id(Project), db_member:id(Member)
  ),
  AllCommits = local:commits(SnapshotId, MemberId),
  AllCommitsJSON = convert_json:convert_commits(AllCommits, MemberEmails), 

  client_event:trigger(update_commits, AllCommitsJSON),
  client_event:trigger(ready);

  
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).

%%====================================================================
%% API Events, sepearete block 
%%====================================================================
api_event(Event, Body, _) ->
  Data = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
  request(Event, Data).

request(request_select_mr, #{<<"id">> := <<"not_mr">>}) ->
  Project = ws_state:get_state(project),
  Member = ws_state:get_state(member),
  MemberEmails = local:member_emails(
    db_project:id(Project), db_member:id(Member)
  ),
  Snapshot = ws_state:get_state(snapshot),
  {ok, OrphanedCommits} = db:get_commits_without_mr(db_snapshot:id(Snapshot)),
  OrphanedCommitsFiltered = lists:filter(fun(Commit) ->
    AuthorId = db_commit:author_id(Commit),
    {ok, Author} = db:get_author({author_id, AuthorId}),
    AuthorEmail = db_author:email(Author),
    lists:member(AuthorEmail, MemberEmails)
  end, OrphanedCommits),
  CommitsJSON =
    convert_json:convert_commits(OrphanedCommitsFiltered, MemberEmails),
  client_event:trigger(replace_commits, CommitsJSON),
  update_mr_files(
    <<"This container does not provide cumulative file changes.">>
  ),
  Title = <<"Mergeless Commits">>,
  Message = <<"These commits were pushed directly to master branch.">>,
  DtlScore = #dtl_pure{
    file =        div_total_score,
    ext =         "html",
    bind_script = false,
    bindings = [
      {total_score, [
        {whitespace, <<"N/A">>},
        {syntax, <<"N/A">>},
        {moves, <<"N/A">>},
        {refactor, <<"N/A">>},
        {insertions, <<"N/A">>},
        {deletions, <<"N/A">>},
        {sum, <<"N/A">>},
        {title, Title},
        {message, Message}
      ]}
    ]
  },
  client_dom:inner_html(total_score, DtlScore);
request(request_select_mr, #{<<"id">> := Id}) ->
  BinId = hash:hex_to_bin(Id),
  Project = ws_state:get_state(project),
  Member = ws_state:get_state(member),
  Commits = local:commits(BinId),
  MemberEmails = local:member_emails(
    db_project:id(Project), db_member:id(Member)
  ),
  CommitsJSON = convert_json:convert_commits(Commits, MemberEmails),
  client_event:trigger(replace_commits, CommitsJSON),
  Files = local:merge_request_files(BinId),
  update_mr_files(Files),
  MergeRequest = local:merge_request(BinId),
  Title = db_merge_request:title(MergeRequest),
  Message = db_merge_request:message(MergeRequest),
  Scores = local:merge_request_score(BinId, db_project:id(Project)),
  update_total_from_mr(Scores, Title, Message),
  client_event:trigger(highlight_files);
request(request_select_commit, #{<<"id">> := Id}) ->
  BinId = hash:hex_to_bin(Id),
  Files = local:files(BinId),
  update_commit_files(Files),
  Commit = local:commit(BinId),
  ProjId = db_commit:project_id(Commit),
  Title = db_commit:title(Commit),
  Message = db_commit:message(Commit),
  CommitCumScore = local:score(BinId, ProjId),
  update_total_from_commit(CommitCumScore, Title, Message),
  client_event:trigger(highlight_files);
request(Event, Data) ->
  error_logger:error_msg("~p: Unrecognized api event: ~p - ~p~n",
    [?MODULE, Event, Data]).

update_commit_files(Message) when is_binary(Message) ->
  wireframe:update(files_list, Message);
update_commit_files(Files) ->
  Files_DTL = notmyjob:dmap(fun(File) ->
    Diff = erlang:binary_to_list(db_commit_diff:diff(File)),
    Diff_esc = erlang:list_to_binary(web_convert:html_encode(Diff)),
    AugmentedDiff = augdiff:parse(Diff_esc),
    Lines_DTL = transform_lines(AugmentedDiff),
    Filename = db_commit_diff:filename(File),
    FileDiffId = db_commit_diff:id(File),
    {Whitespace, Syntax, Moves, Refactor, Insertions, Deletions, Sum} =
      handle_commit_diff_scores(FileDiffId),
    Extension = case filename:extension(Filename) of
      <<".", Ext/binary>> -> Ext;
      Any -> Any
    end,
    #dtl_pure{
      file =        li_file,
      ext =         "html",
      bind_script = false,
      bindings = [
        {file, [
          {title, Filename},
          {lines, Lines_DTL},
          {language, Extension},
          {whitespace, Whitespace},
          {syntax, Syntax},
          {moves, Moves},
          {refactor, Refactor},
          {insertions, Insertions},
          {deletions, Deletions},
          {sum, Sum}
        ]}
      ]
    }
  end, Files),
  wireframe:update(files_list, Files_DTL).

handle_commit_diff_scores(FileDiffId) ->
  case local:commit_file_diff_scores(FileDiffId) of
    {ok, FileDiffScores} -> 
      Scores = FileDiffScores#cumulative_commit_score.raw_scores,
      {Whitespace, Syntax, Moves, Refactor, Insertions, Deletions} =
        db_diff_scores:all(Scores),
      Sum = FileDiffScores#cumulative_commit_score.sum,
      {Whitespace, Syntax, Moves, Refactor,
       Insertions, Deletions, format_score(Sum, 2)};
    {error, not_available} -> 
      % lager:notice("commit diff scores unavailable"),
      {'N/A','N/A','N/A','N/A','N/A','N/A','N/A'}
  end.

update_mr_files(Message) when is_binary(Message) ->
  wireframe:update(files_list, Message);
update_mr_files(Files) ->
  DtlCommits = lists:map(fun(File) ->
    Diff = wf:to_binary(
      web_convert:html_encode(wf:to_list(db_merge_request_diff:diff(File)))
    ),
    BetterDiff = augdiff:parse(Diff),
    LinesDtl = transform_lines(BetterDiff),
    Filename = db_merge_request_diff:filename(File),
    FileDiffId = db_merge_request_diff:id(File),
    {Whitespace, Syntax, Moves, Refactor, Insertions, Deletions, Sum} =
      handle_mr_diff_scores(FileDiffId),
    Extension = case filename:extension(Filename) of
      <<".", Ext/binary>> -> Ext;
      Any -> Any
    end,
    #dtl_pure{
      file =        li_file,
      ext =         "html",
      bind_script = false,
      bindings = [
        {file, [
          {title, Filename},
          {lines, LinesDtl},
          {language, Extension},
          {whitespace, Whitespace},
          {syntax, Syntax},
          {moves, Moves},
          {refactor, Refactor},
          {insertions, Insertions},
          {deletions, Deletions},
          {sum, Sum}
        ]}
      ]
    }
  end, Files),
  wireframe:update(files_list, DtlCommits).

handle_mr_diff_scores(FileDiffId) ->
  case local:merge_request_file_diff_scores(FileDiffId) of
    {ok, FileDiffScores} ->
      Scores = FileDiffScores#cumulative_merge_request_score.raw_scores,
      {Whitespace, Syntax, Moves, Refactor, Insertions, Deletions} =
        db_diff_scores:all(Scores),
      Sum = FileDiffScores#cumulative_merge_request_score.sum,
      {Whitespace, Syntax, Moves, Refactor, 
       Insertions, Deletions, format_score(Sum, 2)};
    {error, not_available} ->
      % lager:notice("mr diff scores unavailable"),
     {'N/A','N/A','N/A','N/A','N/A','N/A','N/A'}
  end.

update_total_from_mr(MRCumScore, Title, Message) ->
  Sum = MRCumScore#cumulative_merge_request_score.sum,
  Scores = MRCumScore#cumulative_merge_request_score.raw_scores,
  update_total_score(Scores, Title, Sum, Message).

update_total_from_commit(CommitCumScore, Title, Message) -> 
  Sum = CommitCumScore#cumulative_commit_score.sum,
  Scores = CommitCumScore#cumulative_commit_score.raw_scores,
  update_total_score(Scores, Title, Sum, Message).

update_total_score(Scores, Title, Sum, Message) ->
  {Whitespace, Syntax, Moves, Refactor, Insertions, Deletions} =
    db_diff_scores:all(Scores),
  DtlScore = #dtl_pure{
      file =        div_total_score,
      ext =         "html",
      bind_script = false,
      bindings = [
        {total_score, [
          {whitespace, format_score(Whitespace, 0)},
          {syntax, format_score(Syntax, 0)},
          {moves, format_score(Moves, 0)},
          {refactor, format_score(Refactor, 0)},
          {insertions, format_score(Insertions, 0)},
          {deletions, format_score(Deletions, 0)},
          {sum, format_score(Sum, 2)},
          {title, Title},
          {message, Message}
        ]}
      ]
    },
  client_dom:inner_html(total_score, DtlScore).

format_score(Value, NearestDegree) -> 
  float_to_list(float(Value),[{decimals,NearestDegree}]).


transform_lines(Lines) ->
  lists:map(fun(Line) ->
    [
      {old, augdiff:old(Line)},
      {new, augdiff:new(Line)},
      {data, augdiff:data(Line)},
      {type, augdiff:type(Line)},
      {augment, augdiff:augment(Line)}
    ]
  end, Lines).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Query cowboy bindings
%%--------------------------------------------------------------------
snapshot_hash() -> wireframe:qb(snapshot_hash).
member_username() -> wireframe:qb(username).

main_dtl({ok, Project}, {ok, Snapshot}, {ok, Member}) ->
  Username = wf:user(),
  ws_state:create_state([
    {username, Username},
    {project, Project},
    {snapshot, Snapshot},
    {member, Member}
  ]),

  client:api(request_select_mr),
  client:api(request_select_commit),

  LoggedUser = wf:user(),
  ProjectName = db_project:name(Project),
  SnapshotName = db_snapshot:title(Snapshot),
  MemberName = db_member:name(Member),
  MemberUsername = db_member:username(Member),

  {Source, SourceId} = db_project:sp_id(Project),
  ProjectUrl = hackney_url:make_url(<<"">>, [
    <<"projects">>, wireframe:enhash_url(wf:to_binary(Source), SourceId)
  ], []),
  SnapshotUrl = hackney_url:make_url(<<"">>, [
    <<"snapshots">>, snapshot_hash()
  ], []),

  OtherMembers = dtl_transform_members(
    local:members(db_project:sp_id(Project))
  ),
  
  Score = db_diff_scores:flatten(
    result:value(db:get_total_commit_score(
      {snapshot_id, db_snapshot:id(Snapshot)}, 
      {member_id, db_member:id(Member)}
    ))
  ),
  Notes = local:merge_request_notes(
            db_snapshot:id(Snapshot), 
            db_member:id(Member)
  ),
  WordCount = local:count_words_in_notes(Notes),

  #dtl_pure{
    file 		  = "member_scores",
    ext 		  = "html",
    app       = "ggs",
    bindings 	= [
      {username,            LoggedUser},

      {menu_item_snapshots, "select"},
      {select_scores,       "select"},

      {project_name,        ProjectName},
      {snapshot_name,       SnapshotName},
      {member_name,         MemberName},
      {member_username,     MemberUsername},
      {members,             OtherMembers},

      {project_url,         ProjectUrl},
      {snapshot_url,        SnapshotUrl},
      {snapshot_hash,       snapshot_hash()},
      {score,               Score},
      {word_count,          WordCount}  
    ] 
  };
main_dtl(_, _, _) ->
  #dtl{file = "fourohfour", ext	= "html", app = "ggs"}.

dtl_transform_members(Members) ->
  lists:map(fun(Member) ->
    Url = hackney_url:make_url("", [
      <<"snapshots">>,
      snapshot_hash(),
      db_member:username(Member),
      <<"merge_requests">> 
    ], []),
    [
      {name,  db_member:username(Member)},
      {url,   Url}
    ]
  end, Members).

update_members(Project, Snapshot) ->
  Members = local:members(db_project:id(Project)),
  SnapshotId_hex = hash:bin_to_hex(db_snapshot:id(Snapshot)),
  Url = hackney_url:make_url("", [
    <<"snapshots">>, SnapshotId_hex
  ], []),
  Members_DTL = lists:map(fun(Member) ->
    wrapper_dtl:construct_snippet(
      "li_dd_member",
      [
        {name, db_member:name(Member)},
        {url, [Url, "/", db_member:username(Member), "/merge_requests"]}
      ]
    )
  end, Members),
  client_dom:inner_html(dd_member_list, Members_DTL).