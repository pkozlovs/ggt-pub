%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_snapshot).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/server/include/elements.hrl").
-include_lib("deps/n2o/include/wf.hrl").

-include_lib("apps/api/include/api.hrl").
-include_lib("apps/db/include/db.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-export([api_event/3]).

-compile(export_all).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  SnapshotHash = snapshot_hash(),
  MaybeSnapshotId = hash:hex_to_bin(SnapshotHash),
  MaybeSnapshot = local:snapshot(MaybeSnapshotId),
  MaybeProject = local:project(MaybeSnapshot),
  client:api(request_merge),
  dtl(MaybeProject, MaybeSnapshot).
  % main_id(MaybeSnapshotId).

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  Project = ws_state:get_state(project),
  ProjectId = db_project:id(Project),
  Snapshot = ws_state:get_state(snapshot),
  SnapshotId = db_snapshot:id(Snapshot),
  update_members(ProjectId, SnapshotId),
  update_authors(ProjectId),
  update_snapshots(Snapshot),
  client_event:trigger(ready);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).


%%--------------------------------------------------------------------
%% @doc API websocket events
%% @spec event(Identifier, Body, Tag) -> any()
%% @end
%%--------------------------------------------------------------------
api_event(request_merge, Body, _) ->
  DecBody = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
  wf:info(?MODULE, "~p", [DecBody]),
  #{<<"pairs">> := Pairs} = DecBody,
  client_dom:inner_html(list_members, 
    <<"<tr><td colspan=\"3\" class=\"item-loading\"></td></tr>">>
  ),
  SnapshotHash = snapshot_hash(),
  MaybeSnapshotId = hash:hex_to_bin(SnapshotHash),
  MaybeSnapshot = local:snapshot(MaybeSnapshotId),
  {ok, Project} = local:project(MaybeSnapshot),
  ProjectId = db_project:id(Project),
  db:dissociate_member_authors({project_id, ProjectId}),
  lists:map(fun(#{<<"author">> := Author, <<"member">> := Member}) ->
    MemberId = hash:hex_to_bin(Member),
    AuthorId = hash:hex_to_bin(Author),
    db:associate_member_author(
      {project_id, ProjectId},
      {member_id, MemberId},
      {author_id, AuthorId}
    )
  end, Pairs),
  ws_async:spawn(fun() ->
    update_members(ProjectId, MaybeSnapshotId),
    client_modal:show_ok("Success",
      "Contributors have been assigned to participants!"
    )
  end).

%%====================================================================
%% Private functions
%%====================================================================
snapshot_hash() -> wireframe:qb(snapshot_name).

dtl({ok, Project}, {ok, Snapshot}) ->
  {Source, Id} = db_project:sp_id(Project),
  ProjectHash = wireframe:enhash_url(wf:to_binary(Source), Id),
  ProjectUrl = ["/projects/", ProjectHash],
  ProjectName = db_project:name(Project),
  SnapshotName = db_snapshot:title(Snapshot),
  Username = wf:user(),
  ws_state:create_state([
    {username, Username},
    {project, Project},
    {snapshot, Snapshot}
  ]),
  #dtl_pure{
    file = "snapshot",
    ext = "html",
    app = "ggs",
    bindings 	= [
      {username, Username},
      {menu_item_snapshots, "select"},
      {project_url, ProjectUrl},
      {project_name, ProjectName},
      {snapshot_name, SnapshotName}
    ] 
  };
dtl(MaybeProject, MaybeSnapshot) ->
  wf:error(?MODULE, "~p~n~p", [
    wf:to_list(MaybeProject), wf:to_list(MaybeSnapshot)
  ]),
  #dtl{file = "fourohfour", ext	= "html", app = "ggs"}.

update_members(ProjectId, SnapshotId) ->
  Members = local:members(ProjectId),
  Members_DTL = notmyjob:dmap(fun(Member) ->
    wrapper_dtl:construct_snapshot_member(Member, SnapshotId)
  end, Members),
  client_dom:inner_html(list_members, Members_DTL).

update_authors(ProjectId) ->
  Authors = local:authors(ProjectId),
  Members = local:members(ProjectId),
  Authors_DTL = notmyjob:dmap(fun(Author) ->
    AuthorId = db_author:id(Author),
    AuthorEmail = db_author:email(Author),
    MaybeLinkedMember = db:get_member(
      {project_id, ProjectId},
      {author_id, AuthorId}
    ),
    LinkedMemberId = case MaybeLinkedMember of
      {ok, LinkedMember} -> db_member:id(LinkedMember);
      {error, _} -> <<>>
    end,
    SelectMembers = lists:map(fun(Member) ->
      MemberId = db_member:id(Member),
      Selected = case MemberId of
        LinkedMemberId -> <<"selected">>;
        _ -> guess_member(Member, AuthorEmail)
      end,
      wrapper_dtl:construct_option_member(Member, Selected)
    end, Members),
    wrapper_dtl:construct_snapshot_author(Author, SelectMembers)
  end, Authors),
  client_dom:inner_html(list_authors, Authors_DTL).

guess_member(Member, AuthorEmail) ->
  MemberUsername = db_member:username(Member),
  case binary:split(AuthorEmail, <<"@">>, [global, trim_all]) of
    [MemberUsername|_] ->
      <<"selected">>;
    _ ->
      <<>>
  end.
  
%%--------------------------------------------------------------------
%% Snapshot update watcher
%%--------------------------------------------------------------------
update_snapshots(Snapshot) ->
  Snapshots = query_snapshots(Snapshot),
  TransformedSnapshots = snapshots_to_dtl(Snapshots),
  wireframe:update(dd_snapshot_list, TransformedSnapshots).
  
%%--------------------------------------------------------------------
%% Get snapshot list
%%--------------------------------------------------------------------
query_snapshots(Snapshot) ->
  ProjId = db_snapshot:project_id(Snapshot),
  case db:get_snapshots({project_id, ProjId}) of
    {error, _Reason}  -> [];
    {ok, Snapshots}   -> Snapshots
  end.
  
%%--------------------------------------------------------------------
%% Transform snapshot list
%%--------------------------------------------------------------------
snapshots_to_dtl(Snapshots) ->
  lists:map(fun(Snapshot) ->
    SnapshotId = hash:bin_to_hex(db_snapshot:id(Snapshot)),
    Url = hackney_url:make_url("", ["snapshots", wf:to_list(SnapshotId)], []),
    #dtl_pure{
      file = snapshots_item_dropdown,
      ext = "html",
      bind_script = false,
      bindings = [
        {snapshot, [
          {name, db_snapshot:title(Snapshot)},
          {url, Url}
        ]}
      ]
    }
  end, Snapshots).
