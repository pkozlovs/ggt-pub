%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_manager_new).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-export([api_event/3]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> nitro_element:dtl_pure().
main() ->
  Username = wf:user(),
  ws_state:create_state([
    {username, Username}
  ]),
  client_event:create([
    request_preview,
    request_create
  ]),
  wrapper_dtl:construct_page("manager_new", [
    {head_title, <<"Manager - Create">>},
    {username, Username},
    {menu_item_manager, <<"select">>}
  ]).

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  client_event:trigger(ready);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).


api_event(request_preview, Data, _) ->
  User = ws_state:get_state(username),
  ws_async:spawn(fun() ->
    MaybeData_map = jsone:decode(wf:to_binary(Data), [{object_format, map}]),
    io:format("MAP: ~p~n", [MaybeData_map]),
    MaybeProjects = prepare_creation_state(MaybeData_map, User),
    case MaybeProjects of
      {error, Reason} ->
        client_modal:close_current(),
        report_error(Reason);
      Projects ->
        erlang:put(project_creation_state, Projects),
        Token = local:api_token(User, <<"gitlab">>),
        Status = project_manager:post_projects(Projects, gitlab, Token),
        io:format("STATUS: ~p~n", [Status]),
        client_modal:close_current(),
        notify_client(Status, [])
    end
  end).

prepare_creation_state(#{
  <<"group_prefix">> := GroupPrefix,
  <<"group_suffix">> := GroupSuffix,
  <<"group_names">> := GroupNames,
  <<"project_names">> := ProjectNames,
  <<"grouped_emails">> := GroupedEmails
}, User) ->
  GroupPrefix_invalid = bin_utils:contains_unsafe(GroupPrefix),
  GroupSuffix_invalid = bin_utils:contains_unsafe(GroupSuffix),
  GroupNames_clean = bin_utils:strip_blanks(GroupNames),
  GroupNames_invalid = bin_utils:contains_unsafe(GroupNames_clean),
  ProjectName_clean = bin_utils:strip_blanks(ProjectNames),
  ProjectName_invalid = bin_utils:contains_unsafe(ProjectName_clean),
  GroupedEmails_clean = bin_utils:strip_blanks(GroupedEmails),
  create_state(
    {GroupPrefix, GroupPrefix_invalid},
    {GroupSuffix, GroupSuffix_invalid},
    {GroupNames_clean, GroupNames_invalid},
    {ProjectName_clean, ProjectName_invalid},
    GroupedEmails_clean,
    User
  ).

create_state({_, {match, _}}, {_, _}, {_, _}, {_, _}, _, _) ->
  {error, <<"Group prefix field contains invalid characters">>};
create_state({_, _}, {_, {match, _}}, {_, _}, {_, _}, _, _) ->
  {error, <<"Group suffix field contains invalid characters">>};
create_state({_, _}, {_, _}, {_, {match, _}}, {_, _}, _, _) ->
  {error, <<"Group name field contains invalid characters">>};
create_state({_, _}, {_, _}, {_, _}, {_, {match, _}}, _, _) ->
  {error, <<"Project name field contains invalid characters">>};
create_state(_, _, {<<"">>, _}, _, _, _) ->
  {error, <<"Group names field cannot be empty">>};
create_state(_, _, _, {<<"">>, _}, _, _) ->
  {error, <<"Project name field cannot be empty">>};
create_state(_, _, _, _, <<"">>, _) ->
  {error, <<"User emails field cannot be empty">>};
create_state(
  {GroupPrefix, nomatch},
  {GroupSuffix, nomatch},
  {GroupNames, nomatch},
  {ProjectNames, nomatch},
  GroupedEmails,
  User
) ->
  Groups = project_manager:create_groups(GroupPrefix, GroupSuffix, GroupNames),
  UserGroups = project_manager:batch_users(GroupedEmails),
  io:format("GROUPS: ~p~n", [Groups]),
  io:format("GROUPS: ~p~n", [UserGroups]),
  Token = local:api_token(User, <<"gitlab">>),
  MaybeSourceUsers = source:users(gitlab, Token),
  MaybeSourceGroups = source:groups(gitlab, Token),
  MaybeProjects = project_manager:create_projects(
    Groups,
    UserGroups,
    ProjectNames,
    MaybeSourceUsers,
    MaybeSourceGroups
  ),
  io:format("PROJECTS: ~p~n", [MaybeProjects]),  
  MaybeProjects.

report_error(group_mismatch) ->
  client_modal:show_error("Error",
    "The number of group names does not match the number of email groups;<br>"
    "Nothing was created"
  );
report_error({user_not_found, User}) ->
  client_modal:show_error("Error",
    io_lib:format(
      "The following user does not exist in the system: ~s;<br>"
      "Nothing was created", [User]
    )
  );
report_error({group_exists, Group}) ->
  client_modal:show_error("Error",
    io_lib:format(
      "The following group already exists: ~s;<br>"
      "Nothing was created", [Group]
    )
  );
report_error(Other) ->
  client_modal:show_error("Error",
    io_lib:format("~s;<br>Nothing was created", [Other])
  ).

notify_client([], []) ->
  client_modal:show_ok("Success",
    "Projects were successfuly generated!"
  );
notify_client([], ErrorBuffer) ->
  Errors = lists:map(fun
    ({error, {group_not_created, Name, Reason}}) ->
      io_lib:format("Group for project ~s could not be created: ~p<br>", [
        Name, Reason
      ]);
    ({error, {project_not_created, Reason}}) ->
      io_lib:format("A project could not be created: ~p<br>", [
        Reason
      ]);
    ({error, {user_not_added_project, UserName, Reason}}) ->
      io_lib:format("User ~s could not be added to a project: ~p<br>", [
        UserName, Reason
      ]);
    ({error, {user_not_added_group, UserName, Reason}}) ->
      io_lib:format("User ~s could not be added to a group: ~p<br>", [
        UserName, Reason
      ]);
    (SomethingElse) ->
      io_lib:format("Unrecognized error: ~p<br>", [
        SomethingElse
      ])
  end, ErrorBuffer),
  ErrorString = io_lib:format("~s", [Errors]),
  client_modal:show_ok("Ok, but something went wrong...",
    "There were errors during creation:<br><br>" ++
    ErrorString
  );
notify_client([{error, Reason}|Rest], ErrorBuffer) ->
  notify_client(Rest, [{error, Reason}|ErrorBuffer]);
notify_client([UserAddStatus|Rest], ErrorBuffer) ->
  UserAddErrors = lists:filtermap(fun
    (ok) ->
      false;
    ({error, Reason}) ->
      {true, {error, Reason}}
  end, UserAddStatus),
  notify_client(Rest, UserAddErrors ++ ErrorBuffer).


