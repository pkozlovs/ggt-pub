%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_manager_projects).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-export([api_event/3]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> nitro_element:dtl_pure().
main() ->
  Username = wf:user(),
  Visibility = wireframe:qb(visibility),
  ws_state:create_state([
    {username, Username},
    {visibility, Visibility}
  ]),
  client_event:create([
    request_leave_projects,
    request_delete_projects,
    request_hide_projects,
    request_show_projects
  ]),
  {NavTab, BtnName, BtnId} = case Visibility of
    visible -> {nav_tab_visible, <<"hide">>, <<"hide">>};
    hidden -> {nav_tab_hidden, <<"show">>, <<"show">>};
    all -> {nav_tab_all, <<"hide">>, <<"hide">>}
  end,
  wrapper_dtl:construct_page("manager_projects", [
    {head_title, <<"Manager - Projects">>},
    {username, Username},
    {menu_item_manager, <<"select">>},
    {NavTab, <<"select">>},
    {btn_name, BtnName},
    {btn_id, BtnId}
  ]).

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  User = ws_state:get_state(username),
  Visibility = ws_state:get_state(visibility),
  ws_async:spawn(fun() ->
    Token = local:api_token(User, <<"gitlab">>),
    update_projects(Visibility, Token),
    client_event:trigger(projects_ready)
  end),
  client_event:trigger(ready);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).

update_projects(Visibility, Token) ->
  case source:projects(gitlab, Token) of
    {ok, Projects} ->
      Projects_DTL = wrap_projects(Visibility, Projects),
      client_dom:inner_html(list_projects, Projects_DTL);
    {error, unauthorized} ->
      client_modal:show_error("Error",
        "You are unauthorized to access projects on GitLab. Please ensure " 
        "you set a correct personal access token. To set your personal "
        "token, go to Profile Settings > Personal Access Tokens"
      );
    {error, Reason} ->
      client_modal:show_error("Error",
        io_lib:format("An internal error occurred: ~p", [Reason])
      )
  end.

wrap_projects(visible, Projects) ->
  lists:filtermap(fun(Project) ->
    ProjectId = source_project:id(Project),
    case db:get_bl_project({source, gitlab}, {id, ProjectId}) of
      {ok, _} ->
        false;
      {error, none} ->
        {true, wrapper_dtl:construct_manager_project(Project)}
    end
  end, Projects);
wrap_projects(hidden, Projects) ->
  lists:filtermap(fun(Project) ->
    ProjectId = source_project:id(Project),
    case db:get_bl_project({source, gitlab}, {id, ProjectId}) of
      {ok, _} ->
        {true, wrapper_dtl:construct_manager_project(Project)};
      {error, none} ->
        false
    end
  end, Projects);
wrap_projects(all, Projects) ->
  lists:map(fun(Project) ->
    wrapper_dtl:construct_manager_project(Project)
  end, Projects).

api_event(request_leave_projects, Data, _) ->
  User = ws_state:get_state(username),
  % Visibility = ws_state:get_state(visibility),
  ws_async:spawn(fun() ->
    Token = local:api_token(User, <<"gitlab">>),
    MaybeData_map = jsone:decode(wf:to_binary(Data), [{object_format, map}]),
    leave_projects(MaybeData_map, User, Token)
  end);
api_event(request_delete_projects, Data, _) ->
  User = ws_state:get_state(username),
  Visibility = ws_state:get_state(visibility),
  ws_async:spawn(fun() ->
    Token = local:api_token(User, <<"gitlab">>),
    MaybeData_map = jsone:decode(wf:to_binary(Data), [{object_format, map}]),
    delete_projects(MaybeData_map, Token, Visibility)
  end);
api_event(request_hide_projects, Data, _) ->
  User = ws_state:get_state(username),
  Visibility = ws_state:get_state(visibility),
  ws_async:spawn(fun() ->
    Token = local:api_token(User, <<"gitlab">>),
    MaybeData_map = jsone:decode(wf:to_binary(Data), [{object_format, map}]),
    hide_projects(MaybeData_map, Token, Visibility)
  end);
api_event(request_show_projects, Data, _) ->
  User = ws_state:get_state(username),
  Visibility = ws_state:get_state(visibility),
  ws_async:spawn(fun() ->
    Token = local:api_token(User, <<"gitlab">>),
    MaybeData_map = jsone:decode(wf:to_binary(Data), [{object_format, map}]),
    show_projects(MaybeData_map, Token, Visibility)
  end).

%%====================================================================
%% Leave helpers
%%====================================================================

leave_projects(#{<<"project_ids">> := ProjectIds}, User, Token) ->
  lists:foreach(fun
    (#{<<"id">> := Id}) ->
      Status = project_manager:leave_project(Id, User, Token),
      verify_removal(Status);
    (_) ->
      client_modal:show_error("Internal API Error",
        "Cannot process a project. Please contact system admins."
      )
  end, ProjectIds);
leave_projects(_, _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process this request. Please contact system admins."
  ).

verify_removal({ok, _}) ->
  client_modal:show_ok("Success!",
    "You have successfully left the selected projects."
  );
verify_removal({error, Error}) ->
  client_modal:show_error("Error",
    io_lib:format("Unable to leave: ~p", [Error])
  ).

%%====================================================================
%% Delete helpers
%%====================================================================

delete_projects(#{<<"projects">> := ProjectMaps}, Token, Visibility) ->
  case delete_projects_helper(ProjectMaps, Token, []) of
    {error, Reason} ->
      client_modal:close_current(),
      client_modal:show_error("Error",
        io_lib:format("Unable to deleted projects: ~p", [Reason])
      );
    [] ->
      client_modal:close_current(),
      client_modal:show_ok("Success",
        "Projects were successfully deleted"
      );
    Errors ->
      Text = parse_deletion_errors(Errors, []),
      client_modal:close_current(),
      client_modal:show_error("Ok, but something went wrong...",
        io_lib:format("There were errors during deletion:<br><br>~s", [Text])
      )
  end,
  update_projects(Visibility, Token),
  client_event:trigger(projects_update);
delete_projects(_, _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process this request. Please contact system admins."
  ).

delete_projects_helper([], _, Acc) ->
  Acc;
delete_projects_helper(
  [#{<<"id">> := Id, <<"name">> := Name}|Rest], Token, Acc
) ->
  case project_manager:delete_project(Id, Token) of
    {error, Reason} ->
      delete_projects_helper(Rest, Token, [{error, {Name, Reason}}|Acc]);
    {ok, _} ->
      delete_projects_helper(Rest, Token, Acc)
  end;
delete_projects_helper([_|_], _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process a project. Please contact system admins."
  ).

parse_deletion_errors([], Acc) ->
  io_lib:format("~s", [lists:reverse(Acc)]);
parse_deletion_errors([{error, {Name, Reason}}|Rest], Acc) ->
  Line = io_lib:format("Project \"~s\": ~p<br>", [Name, Reason]),
  parse_deletion_errors(Rest, [Line|Acc]).


%%====================================================================
%% Hide helpers
%%====================================================================

hide_projects(#{<<"projects">> := ProjectMaps}, Token, Visibility) ->
  case hide_projects_helper(ProjectMaps, Token, []) of
    {error, Reason} ->
      client_modal:close_current(),
      client_modal:show_error("Error",
        io_lib:format("Unable to hide projects: ~p", [Reason])
      );
    [] ->
      client_modal:close_current(),
      client_modal:show_ok("Success",
        "Projects were successfully hidden."
      );
    Errors ->
      Text = parse_deletion_errors(Errors, []),
      client_modal:close_current(),
      client_modal:show_error("Partial Success",
        io_lib:format("There were errors during hiding:<br><br>~s", [Text])
      )
  end,
  update_projects(Visibility, Token),
  client_event:trigger(projects_update);
hide_projects(_, _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process this request. Please contact system admins."
  ).

hide_projects_helper([], _, Acc) ->
  Acc;
hide_projects_helper(
  [#{<<"id">> := Id, <<"name">> := Name}|Rest], Token, Acc
) ->
  case project_manager:hide_project(Id, gitlab) of
    {error, {exists, _}} ->
      hide_projects_helper(Rest, Token, [{error, {Name, already_hidden}}|Acc]);
    {error, Reason} ->
      hide_projects_helper(Rest, Token, [{error, {Name, Reason}}|Acc]);
    {ok, _} ->
      hide_projects_helper(Rest, Token, Acc)
  end;
hide_projects_helper([_|_], _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process a project. Please contact system admins."
  ).

%%====================================================================
%% Show helpers
%%====================================================================

show_projects(#{<<"projects">> := ProjectMaps}, Token, Visibility) ->
  case show_projects_helper(ProjectMaps, Token, []) of
    {error, Reason} ->
      client_modal:close_current(),
      client_modal:show_error("Error",
        io_lib:format("Unable to show projects: ~p", [Reason])
      );
    [] ->
      client_modal:close_current(),
      client_modal:show_ok("Success",
        "Projects were successfully shown."
      );
    Errors ->
      Text = parse_deletion_errors(Errors, []),
      client_modal:close_current(),
      client_modal:show_error("Partial Success",
        io_lib:format("There were errors during showing:<br><br>~s", [Text])
      )
  end,
  update_projects(Visibility, Token),
  client_event:trigger(projects_update);
show_projects(_, _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process this request. Please contact system admins."
  ).

show_projects_helper([], _, Acc) ->
  Acc;
show_projects_helper(
  [#{<<"id">> := Id, <<"name">> := Name}|Rest], Token, Acc
) ->
  case project_manager:show_project(Id, gitlab) of
    {error, Reason} ->
      show_projects_helper(Rest, Token, [{error, {Name, Reason}}|Acc]);
    {ok, _} ->
      show_projects_helper(Rest, Token, Acc)
  end;
show_projects_helper([_|_], _, _) ->
  client_modal:show_error("Internal API Error",
    "Cannot process a project. Please contact system admins."
  ).