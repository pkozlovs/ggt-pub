%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A universal authenticaton module
%%% @end
%%%-------------------------------------------------------------------
-module(page_auth).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).

-include_lib("xmerl/include/xmerl.hrl").

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  MaybeAction = wireframe:qb(action),
  perform_action(MaybeAction),

  % MaybeTicket = wf:qp(ticket, ?REQ),
  % redirect(MaybeTicket),
  #dtl{
    file 		  = "login",
    ext 		  = "html",
    app       = "ggs",
    bindings 	= []
  }.

event(init) -> ok.

%%====================================================================
%% Private functions
%%====================================================================
perform_action(sign_in) ->
  request_cas_ticket();
perform_action(verify_ticket) ->
  MaybeTicket = wf:qp(ticket, ?REQ),
  MaybeReturnUrl = wf:qp(user_url, ?REQ),
  Verification = verify_cas_ticket(MaybeTicket, MaybeReturnUrl),
  react_to_cas_verification(Verification, MaybeReturnUrl);
perform_action(sign_out) ->
  wf:logout(),
  redirect("https://cas.sfu.ca/cas/logout");
perform_action(Action) ->
  error_logger:error_msg("Invalid action: ~p", [Action]).
  %%TO-DO: recover from this, but should never happen

request_cas_ticket() ->
  {UserUrl, _} = cowboy_req:url(?REQ),
  {HostUrl, _} = cowboy_req:host_url(?REQ),
  ReturnUrl = hackney_url:make_url(HostUrl, 
    [<<"sign_in">>], [{<<"user_url">>, UserUrl}]
  ),
  CasUrl = hackney_url:make_url(<<"https://cas.sfu.ca">>,
    [<<"cas">>, <<"login">>], [{<<"service">>, ReturnUrl}]
  ),
  client_window:redirect_replace(CasUrl).

verify_cas_ticket(Ticket, ReturnUrl) ->
  {HostUrl, _} = cowboy_req:host_url(?REQ),
  ConfirmUrl = hackney_url:make_url(HostUrl, 
    [<<"sign_in">>], [{<<"user_url">>, ReturnUrl}]
  ),
  Response = cas_verify(Ticket, ConfirmUrl),
  parse_cas_response(Response).

cas_verify(Ticket, ServiceUrl) ->
  VerifyUrl = hackney_url:make_url("https://cas.sfu.ca", [
    "cas", "serviceValidate"
  ], [
    {<<"ticket">>, Ticket},
    {<<"service">>, ServiceUrl}
  ]),
  hackney:request(get, VerifyUrl, [], <<>>, [insecure]).

parse_cas_response({ok, _, _, ClientRef}) ->
  hackney:body(ClientRef);
parse_cas_response({error, Reason}) ->
  {error, Reason}.

react_to_cas_verification({ok, Response}, ReturnUrl) ->
  MaybeUsername = username(Response),
  login_user(MaybeUsername, ReturnUrl);
react_to_cas_verification({error, _Reason}, _) ->
  ok.

username(RawXml) ->
  {Xml, _} = xmerl_scan:string(wf:to_list(RawXml)),
  [_, StatusXml, _] = Xml#xmlElement.content,
  xml_parse_auth(StatusXml#xmlElement.nsinfo, StatusXml#xmlElement.content).

xml_parse_auth({"cas", "authenticationFailure"}, Content) ->
  error_logger:error_msg("CAS Error ~p~n", [Content]),
  {error, {auth_fail, Content}};
xml_parse_auth({"cas", "authenticationSuccess"}, Content) ->
  [_, UsernameXml, _, _, _] = Content,
  xml_parse_username(
    UsernameXml#xmlElement.nsinfo,
    UsernameXml#xmlElement.content
  ).

xml_parse_username({"cas", "user"}, [UsernameXml]) ->
  {ok, wf:to_binary(UsernameXml#xmlText.value)};
xml_parse_username(_, Content) ->
  error_logger:error_msg("CAS Error ~p~n", [Content]),
  {error, {unexpected_content, Content}}.

login_user({ok, Username}, ReturnUrl) ->
  session:login(Username),
  local:create_user(Username),
  redirect(ReturnUrl);
login_user({error, Reason}, _) ->
  error_logger:error_msg("CAS Error ~p~n", [Reason]).

redirect(undefined) ->
  ok;
redirect(Url) ->
  client_window:redirect_replace(Url).
