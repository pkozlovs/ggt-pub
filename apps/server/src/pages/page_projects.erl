%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Project list page.
%%% @end
%%%-------------------------------------------------------------------
-module(page_projects).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("deps/n2o/include/wf.hrl").

-include_lib("apps/api/include/api.hrl").
-include_lib("apps/db/include/db.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-compile([{parse_transform, lager_transform}]).
%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  %TO-DO: check for logged in user,
  #dtl{
    file 		= "projects",
    ext 		= "html",
    app         = "ggs",
    bindings 	= [
      {page_title, "Projects"},
      {username, wf:user()},
      {menu_item_projects, "select"}
    ] 
  }.

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  User = wf:user(),
  {Query, _} = cowboy_req:qs(?REQ),
  SelectedPage = query_string_to_number(Query),
  ws_async:spawn(fun() ->
    {Num, _} = string:to_integer(SelectedPage),
    Token = local:api_token(User, <<"gitlab">>),
    {MaybeProjects, MaybeHeaders} = case Token of
      <<>> -> {[], []};
      _ -> source:projects(gitlab, Token, [{page_num, Num}])
    end,
    update_projects(MaybeProjects),
    update_project_pages(MaybeHeaders)
  end);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).


%%====================================================================
%% Private functions
%%====================================================================

query_string_to_number(Query) ->
  PageQuery = binary_to_list(Query),
  case PageQuery of 
    [] -> "1";
    _ -> 
      QuerySegments = string:tokens(PageQuery, "="),
      lists:nth(length(QuerySegments), QuerySegments)
  end.

%%--------------------------------------------------------------------
%% Transforms a list of project maps to nitrogeb templates
%%--------------------------------------------------------------------
update_projects([]) ->
  wireframe:update(list_projects, [
    #p{body = <<"You don't seem to be a member of any projects, or have not yet 
      set your personal access token. To set your personal token, go to Profile
      Settings > Personal Access Tokens">>, style = "margin-left:16px;"}
  ]);
update_projects({error, Reason}) ->
  client_modal:show_error(<<"Unable to access projects">>, Reason);
update_projects({ok, Projects}) ->
  ProjectDtls = lists:filtermap(fun(Project)->
    SourceId = source_project:id(Project),
    case db:get_bl_project({source, gitlab}, {id, SourceId}) of
      {error, none} ->
        Name = source_project:special_name(Project),
        ProjectHash_hex = wireframe:enhash_url(<<"gitlab">>, SourceId),
        URL = hackney_url:make_url(<<>>, [<<"projects">>, ProjectHash_hex], []),
        <<FirstLetter/utf8, _/binary>> = Name,
        NumSnapshots = count_snapshots(<<"gitlab">>, SourceId),
        DTL = #dtl{
          file = projects_item,
          ext = "html",
          bind_script = false,
          bindings = [
            {first_letter, [FirstLetter]},
            {name, Name}, {url, URL},
            {count, NumSnapshots}
          ]
        },
        {true, DTL};
      {ok, _} ->
        false
    end
  end, Projects),
  wireframe:update(list_projects, ProjectDtls).

%%--------------------------------------------------------------------
%% Count snapshots for project
%%--------------------------------------------------------------------
%% TO-DO
count_snapshots(Source, ProjectId) ->
  length(local:snapshots(Source, ProjectId)).

update_project_pages([]) ->
  io:format("Error getting projects headers! Was token set?~n");
update_project_pages({error, Reason}) ->
 io:format("Unable to access projects! ~p~n", [Reason]);
%Where the second tuple element is a tuple of {ResponseCode, [HeaderData], Ref} we don't care about either the ResponseCode or Ref at this point
update_project_pages({ok, {_, Headers, _}}) ->
  NumOfPages = lists:filtermap(fun(Elem) ->
    {First, Second} = Elem,
    case First of
      <<"X-Total-Pages">> -> 
        {Int, _} = string:to_integer(binary_to_list(Second)),
        {true, Int};
      _ -> false
    end
  end, Headers),
  PageRange = lists:seq(1, lists:nth(1, NumOfPages)),
  DTLs = lists:map(fun(PageNum) -> 
      PageNumStr = integer_to_list(PageNum),
      URL = hackney_url:make_url(<<>>, [<<"projects/">>], 
        [{<<"page">>, list_to_binary(PageNumStr)}]),
      DTL = #dtl{
      file = projects_page_item,
      ext = "html",
      bind_script = false,
      bindings = [
        {url, URL},
        {page_num, PageNumStr}
      ]
    }

  end, PageRange),
  wireframe:update(footer, DTLs).