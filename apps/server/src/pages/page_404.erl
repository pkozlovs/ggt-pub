%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_404).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  #dtl{
    file 		= "fourohfour",
    ext 		= "html",
    app         = "ggs",
    bindings 	= [
      {username, wf:user()}
    ] 
  }.

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).


%%====================================================================
%% Private functions
%%====================================================================
