%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_profile_score_weight).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  client:api(request_updateTokens),
  % FyBcq52kJXGky9sSrqd4
  TokenGitlabSfu = local:api_token(wf:user(), <<"gitlab">>),
  #dtl{
    file 		= "profile_score_weight",
    ext 		= "html",
    app         = "ggs",
    bindings 	= [
      {menu_item_profile, "select"},
      {username, wf:user()},
      {gitlabsfu_token, TokenGitlabSfu}
    ] 
  }.
