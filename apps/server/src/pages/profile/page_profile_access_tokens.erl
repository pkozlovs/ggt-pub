%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A template page to be used for creation of other pages.
%%% @end
%%%-------------------------------------------------------------------
-module(page_profile_access_tokens).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/api/include/api.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([main/0]).
-export([event/1]).
-export([api_event/3]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starting point of an http request
%% @spec main() -> Dtl
%% @end
%%--------------------------------------------------------------------
-spec main() -> #dtl{}.
main() ->
  client:api(request_updateTokens),
  % FyBcq52kJXGky9sSrqd4
  TokenGitlabSfu = local:api_token(wf:user(), <<"gitlab">>),
  #dtl{
    file 		= "profile_access_tokens",
    ext 		= "html",
    app         = "ggs",
    bindings 	= [
      {menu_item_profile, "select"},
      {username, wf:user()},
      {gitlabsfu_token, TokenGitlabSfu}
    ] 
  }.

%%--------------------------------------------------------------------
%% @doc Websocket events
%% @spec event(Identifier)
%% @end
%%--------------------------------------------------------------------
-spec event(any()) -> any().
event(init) ->
  wf:info(?MODULE, "Websocket process is running [~p]", [self()]),
  client:event(init);
event(terminate) ->
  wf:info(?MODULE, "Websocket process terminated [~p]", [self()]).


api_event(request_updateTokens, Body, _) ->
  User = wf:user(),
  ws_async:spawn(fun() ->
    DecBody = jsone:decode(wf:to_binary(Body), [{object_format, map}]),
    wf:info(?MODULE, "~p", [DecBody]),
    #{<<"tokens">> := Tokens} = DecBody,
    lists:foreach(fun(#{<<"source">> := Source, <<"value">> := Value}) ->
      local:api_token(User, Source, Value)
    end, Tokens),
    client_modal:show_ok("Success", "Your tokens have been updated!")
  end).
  

%%====================================================================
%% Private functions
%%====================================================================
