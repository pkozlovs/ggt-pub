-module(project_manager).

-export([create_groups/3]).
-export([batch_users/1]).
-export([create_projects/5]).

-export([post_projects/3]).

-export([leave_project/3]).
-export([delete_project/2]).
-export([hide_project/2]).
-export([show_project/2]).

-record(project, {
  name :: binary(),
  namespace :: binary(),
  users :: [source:user()]
}).
-opaque project() :: #project{}.
-export_type([project/0]).

-spec create_groups(binary(), binary(), binary()) -> [binary()].
create_groups(Prefix, Suffix, Names) ->
  NameList = binary:split(
    Names,
    [<<",">>, <<", ">>],
    [global, trim_all]
  ),
  io:format("NAMES: ~p~n", [NameList]),
  lists:map(fun(Name) ->
    <<Prefix/binary, Name/binary, Suffix/binary>>
  end, NameList).

-spec batch_users(binary()) -> [binary()].
batch_users(Emails) ->
  Batches = binary:split(
    Emails,
    <<"\n\n">>,
    [global, trim_all]
  ),
  lists:map(fun(Batch) ->
    Users = binary:split(Batch, <<"\n">>, [global, trim_all]),
    lists:map(fun(User) -> bin_utils:strip_blanks(User) end, Users)
  end, Batches).

-spec create_projects([binary()], [binary()], binary(), any(), any()) -> 
  [project()] | {error, term()}.
create_projects(Groups, UserGroups, ProjectName,
  {ok, SourceUsers}, {ok, SourceGroups}) ->
  construct_projects(Groups, UserGroups, ProjectName,
    SourceUsers, SourceGroups, []);
create_projects(_, _, _, {error, Reason}, _) ->
  {error, Reason};
create_projects(_, _, _, _, {error, Reason}) ->
  {error, Reason}.

construct_projects([], [], _, _, _, Acc) ->
  Acc;
construct_projects([], _, _, _, _, _) ->
  {error, group_mismatch};
construct_projects(
  [Group|Groups],
  [UserGroup|UserGroups],
  ProjectName,
  SourceUsers,
  SourceGroups,
  Acc
) ->
  MaybeGroup = verify_group(Group, SourceGroups),
  MaybeUsers = verify_users(UserGroup, SourceUsers, []),
  MaybeProject = construct_project(ProjectName, MaybeGroup, MaybeUsers),
  case MaybeProject of
    {error, Reason} ->
      {error, Reason};
    Project ->
      construct_projects(Groups, UserGroups, ProjectName,
        SourceUsers, SourceGroups, [Project|Acc])
  end.

verify_group(Group, SourceGroups) ->
  case lists:keyfind(Group, 3, SourceGroups) of
    false ->
      {ok, Group};
    _SourceGroup ->
      {error, {group_exists, Group}}
  end.

verify_users([], _, Acc) ->
  {ok, Acc};
verify_users([User|Rest], SourceUsers, Acc) ->
  [Username|_] = binary:split(User, <<"@">>, [global, trim_all]),
  case lists:keyfind(Username, 4, SourceUsers) of
    false ->
      {error, {user_not_found, User}};
    SourceUser ->
      verify_users(Rest, SourceUsers, [SourceUser|Acc])
  end.

construct_project(_, {error, Reason}, _) ->
  {error, Reason};
construct_project(_, _, {error, Reason}) ->
  {error, Reason};
construct_project(ProjectName, {ok, Group}, {ok, Users}) ->
  #project{name = ProjectName, namespace = Group, users = Users}.

-spec post_projects([project()], source:source(), source:token()) -> 
  {ok, term()} | {error, term()}.
post_projects([], _, _) ->
  [];
post_projects([#project{
  name = Name,
  namespace = GroupName,
  users = Users
}|Rest], Source, Token) ->
  MaybeGroup = post_group(GroupName, Source, Token),
  MaybeProject = post_project(Name, MaybeGroup, Source, Token),
  FinalStatus = add_users(Users, MaybeGroup, MaybeProject, Source, Token),
  [FinalStatus|post_projects(Rest, Source, Token)].

post_group(Group, Source, Token) ->
  Path = web_convert:quote_dash(Group),
  source:create_group(Source, Token, [
    {group_name, Group},
    {group_path, Path}
  ]).

post_project(Name, {error, Reason}, _, _) ->
  {error, {group_not_created, Name, Reason}};
post_project(Name, {ok, Group}, Source, Token) ->
  source:create_project(Source, Token, [
    {project_name, Name},
    {namespace_id, source_group:id(Group)}
  ]).

add_users(_, {error, Reason}, _, _, _) ->
  {error, {group_not_created, Reason}};
add_users(_, _, {error, Reason}, _, _) ->
  {error, {project_not_created, Reason}};
add_users([], _, _, _, _) ->
  [];
add_users([User|Rest], {ok, Group}, {ok, Project}, Source, Token) ->
  UserId = source_user:id(User),
  MaybeUser1 = source:add_user(Source, Token, [
    {project_id, source_project:id(Project)},
    {user_id, UserId}
  ]),
  MaybeUser2 = source:add_user(Source, Token, [
    {group_id, source_group:id(Group)},
    {user_id, UserId}
  ]),
  UserName = source_user:name(User),
  MaybeAdd = verify_add(MaybeUser1, MaybeUser2, UserName),
  [MaybeAdd|add_users(Rest, {ok, Group}, {ok, Project}, Source, Token)].

verify_add({error, Reason}, _, UserName) ->
  {error, {user_not_added_project, UserName, Reason}};
verify_add(_, {error, Reason}, UserName) ->
  {error, {user_not_added_group, UserName, Reason}};
verify_add({ok, User}, {ok, User}, _) ->
  ok.

-spec leave_project(binary(), binary(), binary()) ->
  {ok, any()} | {error, any()}.
leave_project(Id, User, Token) ->
  io:format("Trying to leave project: ~p~n", [Id]),
  MaybeSourceUsers = source:users(gitlab, Token, [{project_id, Id}]),
  remove_user_from_project(Id, User, Token, MaybeSourceUsers).

remove_user_from_project(Id, User, Token, {ok, SourceUsers}) ->
  case lists:keyfind(User, 4, SourceUsers) of
    false ->
      {error, {not_a_member, User}};
    SourceUser ->
      UserId = source_user:id(SourceUser),
      source:remove_user(gitlab, Token, [{project_id, Id}, {user_id, UserId}])
  end;
remove_user_from_project(_, _, _, {error, Reason}) ->
  {error, Reason}.

%% END OF DISASTER

-spec delete_project(binary(), binary()) -> {ok, term()} | {error, term()}.
delete_project(Id, Token) ->
  case source:project(gitlab, Token, [{project_id, Id}]) of
    {ok, SourceProject} ->
      OwnerId = source_project:owner_id(SourceProject),
      NamespaceId = source_project:namespace_id(SourceProject),
      delete_project(Id, Token, OwnerId, NamespaceId);
    {error, Reason} ->
      {error, Reason}
  end.

delete_project(Id, Token, null, GroupId) ->
  MaybeDeleted = source:delete_project(gitlab, Token, [{project_id, Id}]),
  delete_group(GroupId, Token, MaybeDeleted);
delete_project(Id, Token, _, _) ->
  source:delete_project(gitlab, Token, [{project_id, Id}]).

delete_group(GroupId, Token, {ok, _}) ->
  case source:projects(gitlab, Token, [{group_id, GroupId}]) of
    {ok, []} ->
      source:delete_group(gitlab, Token, [{group_id, GroupId}]);
    {ok, _SourceProjects} ->
      {ok, deleted};
    {error, Reason} ->
      {error, Reason}
  end;
delete_group(_, _, {error, Reason}) ->
  {error, Reason}.

-spec hide_project(binary(), atom()) -> {ok, term()} | {error, term()}.
hide_project(Id, Source) ->
  Project = db:construct_bl_project({Source, Id}),
  db:create_bl_project(Project).

-spec show_project(binary(), atom()) -> {ok, term()} | {error, term()}.
show_project(Id, Source) ->
  db:delete_bl_project({sp_id, {Source, Id}}).