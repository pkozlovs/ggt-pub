-module(http_redirect_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).


init(_Type, Req, _Options) ->
  {ok, Req, undefined_state}.

handle(Req, State) ->
  {Host, _} = cowboy_req:host(Req),
  {Path, _} = cowboy_req:path(Req),
  {Qs, _} = cowboy_req:qs(Req),
  NewURL = hackney_url:make_url(<<"https://", Host/binary>>, Path, Qs),  
  {ok, Reply} = cowboy_req:reply(
    303,
    [{<<"Location">>, NewURL}],
    <<"Redirecting to secure...">>,
    Req
  ),
  {ok, Reply, State}.

terminate(_Reason, _Req, _State) ->
  ok.