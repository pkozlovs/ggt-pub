-module(config).
-compile(export_all).

log_level() -> info.
log_modules() -> % any
  [
    %    wf_convert,
    %    n2o_file,
    n2o_async,
    n2o_proto,
    %    n2o_client,
    % n2o_static,
      %  n2o_static,
    n2o_stream,
    n2o_nitrogen,
    n2o_session,
    store_mnesia,
    page_index,
    page_login,
    page_project,
    page_projects,
    page_graphs,
    page_member_mr,
    page_profile_access_tokens,
    page_profile_score_weight,
    page_snapshot
  ].
