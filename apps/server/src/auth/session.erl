-module(session).

-compile(export_all).

route(Route) ->
  case wf:user() of
    undefined -> {page_auth, [{action, sign_in}]};
    _         -> Route
  end.

login(Username) -> wf:user(Username).
logout() -> wf:logout().
