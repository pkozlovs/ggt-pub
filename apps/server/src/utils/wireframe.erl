%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A set of custom tailored wf functions
%%% @end
%%%-------------------------------------------------------------------
-module(wireframe).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([async/1]).
-export([actions/0]).
-export([update/2]).
-export([qb/1]).
-export([enhash_url/2, dehash_url/1]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Executes Fun asynchronously and flushes nitrogen actions back
%% to the parent process.
%% @spec async(Fun) -> {ok, Pid}.
%% @end
%%--------------------------------------------------------------------
-spec async(function()) -> {ok, pid()}.
async(Fun) ->
  OwnId = self(),
  {ok, spawn(fun() -> async_run(Fun, OwnId) end)}.

%%--------------------------------------------------------------------
%% @doc Get nitro actions (removing them) from process dictionary.
%% @spec projects(Endpoint) -> {ok, Projects} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec actions() -> list().
actions() ->
  Actions = wf:actions(),
  wf:actions([]),
  {flush, Actions}.

%%--------------------------------------------------------------------
%% @doc Update element's inner HTML with nitro elements
%% @spec update(Target, Elements) -> any()
%% @end
%%--------------------------------------------------------------------
-spec update(atom(), list()) -> any().
update(Target, Elements) ->
  wf:wire(#jq{
    target = Target,
    property = innerHTML,
    right = wf:js_escape(wf:render(Elements)),
    format = "'~s'"
  }).

%%--------------------------------------------------------------------
%% @doc Retrieve REQ's binding by key
%% @spec qb(Key) -> any()
%% @end
%%--------------------------------------------------------------------
-spec qb(atom()) -> any().
qb(Key) -> {Value, _Req} = cowboy_req:binding(Key, ?REQ), Value.

%%--------------------------------------------------------------------
%% @doc Transform data {Source, Id} to hexadecimal binary
%% @spec enhash_url(Source, Id) -> HexBin
%% @end
%%--------------------------------------------------------------------
-spec enhash_url(binary(), binary()) -> binary().
enhash_url(Source, Id) ->
  JoinedData = <<Source/binary, "|", Id/binary>>,
  hash:bin_to_hex(JoinedData).

%%--------------------------------------------------------------------
%% @doc Transform hexadecimal binary to data {Source, Id}
%% @spec dehash_url(BinHex) -> {ok, Source, Id} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec dehash_url(binary()) ->
        {ok, {binary(), binary()}} | {error, {atom(), any()}}.
dehash_url(Url) ->
  parse_hash(hash:hex_to_bin(Url)).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Execute fun, and send n2o-type flush to the parent
%%--------------------------------------------------------------------
async_run(Fun, ParentId) ->
  Fun(),
  Actions = wf:actions(),
  wf:actions([]),
  try ParentId ! {flush, Actions} catch _:_ -> ok end.

%%--------------------------------------------------------------------
%% Transform project hash to source and id
%%--------------------------------------------------------------------
parse_hash({error, Reason}) ->
  {error, Reason};
parse_hash(Binary) ->
  case binary:split(Binary, <<"|">>) of
    [Provider, Id] -> {ok, {Provider, Id}};
    _Other         -> {error, {bad_format, Binary}}
  end.
