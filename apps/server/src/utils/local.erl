-module(local).

-export([project/1]).
-export([snapshots/2, snapshot/1]).
-export([member/2, members/1, authors/1]).
-export([
  merge_requests/2, unauthored_merge_requests/2, merge_request_files/1,
  merge_request_notes/2, count_words_in_notes/1, merge_request/1
]).
-export([
  commit/1, commits/1, commits/2, count_net_lines/1
]).
-export([
  files/1, commit_file_diff_scores/1, merge_request_file_diff_scores/1
]).
-export([
  score/2, merge_request_score/2, 
  merge_request_commit_score/3, 
  commits_without_mr_score/3
]).
-export([create_user/1]).
-export([api_token/3, api_token/2]).
-export([member_emails/2]).

-include_lib("apps/db/include/db.hrl").
-include_lib("apps/db/include/frontend_structs.hrl").

project({ok, Snapshot}) ->
  db:get_project({project_id, db_snapshot:project_id(Snapshot)});
project({error, Reason}) ->
  {error, Reason}.

-spec snapshots(binary(), binary()) -> any().
snapshots(Source, ProjectId) ->
  SourceAtom = binary_to_atom(Source, unicode),
  MaybeProject = db:get_project(SourceAtom, ProjectId),
  query_snapshots(MaybeProject).

snapshot({error, Reason}) ->
  {error, Reason};
snapshot(SnapshotId) ->
  db:get_snapshot({snapshot_id, SnapshotId}).

member({ok, Project}, Username) ->
  db:get_member(
    {project_id, db_project:id(Project)},
    {username, Username}
  );
member({error, Reason}, _) ->
  {error, Reason}.

members(ProjectId) -> query_members(ProjectId).

authors(ProjectId) -> query_authors(ProjectId).

-spec merge_requests(db:snapshot(), db:member()) -> [db:merge_request()].
merge_requests(Snapshot, Member) ->
  case db:get_merge_requests(
    {member_id, db_member:id(Member)},
    {snapshot_id, db_snapshot:id(Snapshot)}
  ) of
    {ok, MergeRequests} ->  MergeRequests;
    {error, _Reason}    ->  []
  end.

merge_request(MergeRequestId) ->
  case db:get_merge_request({merge_request_id, MergeRequestId}) of
    {ok, MergeRequest} -> MergeRequest;
    {error, Reason}                  -> {error, Reason}
  end.

unauthored_merge_requests(Snapshot, Member) ->
  MemberId = db_member:id(Member),
  MaybeMergeRequests = db:get_merge_requests_contributed(
    {member_id, MemberId}, 
    {snapshot_id, db_snapshot:id(Snapshot)}
  ),
  MergeRequests = result:value(MaybeMergeRequests, []),
  lists:filter(fun(MergeRequest) ->
    case db_merge_request:creator_id(MergeRequest) of
      MemberId -> false;
      _        -> true
    end
  end, MergeRequests).

commit(CommitId) ->
  case db:get_commit({commit_id, CommitId}) of 
    {ok, Commit} -> Commit;
    {error, Reason} -> {error, Reason}
  end.


commits(MergeRequestId) ->
  case db:get_sorted_commits({merge_request_id, MergeRequestId}) of
    {ok, Commits} -> Commits;
    _             -> []
  end.

commits(SnapshotId, MemberId) ->
  case db:get_commits(
    {snapshot_id, SnapshotId},
    {member_id, MemberId}
  ) of
    {ok, Commits} -> Commits;
    _             -> []
  end.

count_net_lines(Commits) ->
  count_net_lines(Commits, 0).

count_net_lines([], Count) -> Count;
count_net_lines([Commit|Rest], Count) ->
  {ok, Stats} = db:get_commit_stats({commit_id, db_commit:id(Commit)}),
  Net = db_commit_stats:net(Stats),
  count_net_lines(Rest, Count + Net).

files(CommitId) ->
  case db:get_augmented_diffs({commit_id, CommitId}) of
    {ok, Files} ->
      Files;
    What ->
      error_logger:error_msg("Commit Request Files: ~p~n", [What]),
      erlang:list_to_binary(io_lib:format("Unable to load files: ~p", [What]))
  end.

commit_file_diff_scores(FileDiffId) ->
  case db:get_commit_diff_scores({commit_file_diff_id, FileDiffId}) of
    {ok, FileDiffScores} -> {ok, FileDiffScores};
    _                    -> {error, not_available}
  end.

merge_request_file_diff_scores(MrDiffId) ->
  case db:get_merge_request_diff_scores({merge_request_diff_id, MrDiffId}) of
    {ok, FileDiffScores} -> {ok, FileDiffScores};
    _                    -> {error, not_available}
  end.

merge_request_files(MergeRequestId) ->
  case db:get_augmented_diffs({merge_request_id, MergeRequestId}) of
    {ok, Files} ->
      Files;
    What ->
      error_logger:error_msg("Merge Request Files: ~p~n", [What]),
      erlang:list_to_binary(io_lib:format("Unable to load files: ~p", [What]))
  end.


merge_request_notes(SnapshotId, MemberId) ->
  case db:get_notes({snapshot_id, SnapshotId}, {member_id, MemberId}) of
    {ok, Notes} -> Notes;
    _           -> []
  end.

merge_request_score(MergeRequestId, ProjectId) ->
  case db:get_merge_request_score({merge_request_id, MergeRequestId},
                                   {project_id, ProjectId}) of 
    {ok, Scores} -> Scores;
    _            -> not_available
  end.                                 

score(CommitId, ProjectId) ->
  case db:get_commit_score({commit_id, CommitId}, {project_id, ProjectId}) of
    {ok, Scores} -> Scores;
    _             -> not_available
  end.



% Sum of commit-scores for commits in MergeRequestId where commit was made by MemberId
is_author_for_member(ProjectId, AuthorEmail, MemberId) ->
  MemberEmails = member_emails(ProjectId, MemberId),
  lists:member(AuthorEmail, MemberEmails).

merge_request_commit_score(ProjectId, MergeRequestId, MemberId) ->
  CommitsForMergeRequest = commits(MergeRequestId),
  total_commit_score(CommitsForMergeRequest, ProjectId, MemberId, 0).

commits_without_mr_score(ProjectId, SnapshotId, MemberId) ->
  case db:get_commits_without_mr(SnapshotId) of
    {ok, Commits} ->
      total_commit_score(Commits, ProjectId, MemberId, 0);
    Result ->
      io:format("WARNING: commits_without_mr_score failed to get OK from db:get_commits_without_mr.~n"),
      io:format("   SnapshotId: ~p~n", [SnapshotId]),
      io:format("   db:get_commits_without_mr returns: ~p~n", [Result]),
      <<"N/A">>
  end.

total_commit_score([], ProjectId, MemberId, Score) ->  Score;
total_commit_score([Commit|Rest], ProjectId, MemberId, Score) ->
  CommitId = db_commit:id(Commit),
  CommitScore = score(CommitId, ProjectId),
  
  case CommitScore of
    not_available -> 
      io:format("ERROR: ~p - CommitScore -- not_available~n", [?FUNCTION_NAME]),
      not_available;
    (_) ->
      CommitScoreInt = CommitScore#cumulative_commit_score.sum, 
      AuthorId = db_commit:author_id(Commit),
      Author = db:get_author({author_id, AuthorId}),
      AuthorEmail = transform_author(Author),

      IsCommitForMember = is_author_for_member(ProjectId, AuthorEmail, MemberId),
      case IsCommitForMember of
        true  -> total_commit_score(Rest, ProjectId, MemberId, Score + CommitScoreInt);
        false -> total_commit_score(Rest, ProjectId, MemberId, Score)
      end
  end.





count_words_in_notes(Notes) ->
  count_words_in_notes(Notes, 0).

count_words_in_notes([], Count) -> Count;
count_words_in_notes([Note|Rest], Count) ->
  Words = binary:split(db_note:body(Note), [<<" ">>, <<"\n">>], [global, trim_all]),
  count_words_in_notes(Rest, Count + erlang:length(Words)).

-spec create_user(binary()) -> any().
create_user(Username) -> db:update_user(Username).

-spec api_token(binary(), binary(), binary()) -> any().
api_token(Username, Source, Token) ->
  Source_a = binary_to_atom(Source, utf8),
  db:update_api_token(Username, Source_a, Token).

-spec member_emails(binary(), binary()) -> [db_author:self()].
member_emails(ProjectId, MemberId) ->
  MaybeAuthors = db:get_authors_for_member(
    {project_id, ProjectId},
    {member_id, MemberId}
  ),
  transform_authors(MaybeAuthors).

-spec api_token(binary(), binary()) -> any().
api_token(Username, Source) ->
  Source_a = binary_to_atom(Source, utf8),
  MaybeToken = db:get_api_token(Username, Source_a),
  transform_token(MaybeToken).



query_snapshots({ok, Project}) ->
  ProjId = db_project:id(Project),
  case db:get_snapshots({project_id, ProjId}) of
    {ok, Snapshots}   -> Snapshots;
    {error, _Reason}  -> []
  end;
query_snapshots({error, _Reason}) ->
  [].

query_members(ProjectId) ->
  case db:get_members({project_id, ProjectId}) of
    {ok, Members}     -> Members;
    {error, _Reason}  -> []
  end.

query_authors(ProjectId) ->
  case db:get_authors({project_id, ProjectId}) of
    {ok, Authors}     -> Authors;
    {error, _Reason}  -> []
  end.


transform_token({ok, Token}) ->
  Token#api_token.value;
transform_token({error, _}) ->
  <<"">>.

transform_authors({error, _}) ->
  [];
transform_authors({ok, Authors}) ->
  lists:map(fun(Author) -> db_author:email(Author) end, Authors).

% Just one Author
transform_author({error, _}) ->
  [];
transform_author({ok, Author}) ->
  db_author:email(Author).

