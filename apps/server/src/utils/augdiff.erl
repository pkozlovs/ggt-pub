-module(augdiff).

-export([parse/1]).
-export([old/1]).
-export([new/1]).
-export([data/1]).
-export([type/1]).
-export([augment/1]).

-record(diff, {
  filename = undefined :: binary(),
  lines = [] :: [line()]
}).

-opaque diff() :: #diff{}.
-export_type([diff/0]).

-record(line, {
  type = undefined ::
    misc
    | new
    | old
    | hunk
    | unchanged,
  augment = undefined ::
    whitespace
    | syntax
    | moves
    | refactors
    | insertions
    | deletions
    | none,
  old = undefined :: integer() | undefined,
  new = undefined :: integer() | undefined,
  data = undefined :: binary() | undefined
}).

-opaque line() :: #line{}.
-export_type([line/0]).

-define(HUNK_PATTERN, <<"(@@ \\-[0-9]+,[0-9]+ \\+[0-9]+,[0-9]+ @@)">>).

%%====================================================================
%% External functions
%%====================================================================
-spec parse(binary()) -> [line()].
parse(Diff) ->
  RawLines = binary:split(Diff, <<"\n">>, [global]),
  parse_lines(RawLines).

old(#line{old = Old}) -> Old.
new(#line{new = New}) -> New.
data(#line{data = Data}) -> Data.
type(#line{type = Type}) -> Type.
augment(#line{augment = Augment}) -> Augment.


parse_lines(Lines) ->
  parse_augmented_syntax(Lines, 1, 1).

parse_augmented_syntax([], _, _) ->
  [];
parse_augmented_syntax([<<"%W", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, whitespace, Old, New, Rest);
parse_augmented_syntax([<<"%S", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, syntax, Old, New, Rest);
parse_augmented_syntax([<<"%M", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, moves, Old, New, Rest);
parse_augmented_syntax([<<"%R", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, refactors, Old, New, Rest);
parse_augmented_syntax([<<"%I", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, insertions, Old, New, Rest);
parse_augmented_syntax([<<"%D", Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, deletions, Old, New, Rest);
parse_augmented_syntax([<<Git/binary>>|Rest], Old, New) ->
  parse_git_and_next(Git, none, Old, New, Rest).

parse_git_and_next(Line, Augment, Old, New, Rest) ->
  case parse_git_syntax(Line, Augment, Old, New) of
    {ok, {NewLine, UpOld, UpNew}} ->
      [NewLine|parse_augmented_syntax(Rest, UpOld, UpNew)];
    {error, _} ->
      parse_augmented_syntax(Rest, Old, New)
  end.

parse_git_syntax(<<$@, _/binary>> = Line, _, Old, New) ->
  {ok, parse_hunk(Line, Old, New)};
parse_git_syntax(<<"+++", _/binary>>, _, _, _) ->
  {error, not_a_line};
parse_git_syntax(<<"---", _/binary>>, _, _, _) ->
  {error, not_a_line};
parse_git_syntax(<<$-, Real/binary>>, Augment, Old, New) ->
  {ok, {construct_line(old, Augment, Old, New, Real), Old + 1, New}};
parse_git_syntax(<<$+, Real/binary>>, Augment, Old, New) ->
  {ok, {construct_line(new, Augment, Old, New, Real), Old, New + 1}};
parse_git_syntax(<<" ", Real/binary>>, Augment, Old, New) ->
  {ok, {construct_line(unchanged, Augment, Old, New, Real), Old + 1, New + 1}};
parse_git_syntax(<<"\\ No newline at end of file">> = 
  Line, Augment, Old, New) ->
  {ok, {construct_line(unchanged, Augment, Old, New, Line), Old + 1, New + 1}};
parse_git_syntax(_, _, _, _) ->
  {error, unknown_format}.

parse_hunk(LineData, Old, New) ->
  MaybeHunkData = re:run(LineData, 
    <<"-[0-9]+|\\+[0-9]+|@@">>,
    [global, {capture, all, binary}]
  ),
  case MaybeHunkData of
    {match, [
      [<<"@@">>],
      [<<"-", UpOld/binary>>],
      [<<"+", UpNew/binary>>],
      [<<"@@">>]
    ]} ->
      {
        #line{type = hunk, data = LineData},
        binary_to_integer(UpOld),
        binary_to_integer(UpNew)
      };
    _ ->
      {
        #line{type = unchanged, old = Old, new = New, data = LineData},
        Old,
        New
      }
    end.

construct_line(Type, Augment, Old, New, Data) ->
  #line{
    type = Type,
    augment = Augment,
    old = Old,
    new = New,
    data = <<" <code>", Data/binary, "</code>">>
  }.