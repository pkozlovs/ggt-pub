%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Wrapper for interaction with the client side
%%% @end
%%%-------------------------------------------------------------------
-module(client).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([info/2, source_error/1, error/2]).
-export([api/1]).
-export([event/1, event/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Pop info modal
%% @spec info(Title, Message) -> any()
%% @end
%%--------------------------------------------------------------------
-spec info(string(), string()) -> any().
info(Title, Message) ->
  wf:wire(["$('#modal_info .title').html('", Title ,"');"]),
  wf:wire(["$('#modal_info .content').html('", Message ,"');"]),
  wf:wire("Modal_openDefault('#modal_info');").

%%--------------------------------------------------------------------
%% @doc Pop error modal with formatted message for source errors
%% @spec source_error(Errors) -> any()
%% @end
%%--------------------------------------------------------------------
-spec source_error([{any(), string(), string()}]) -> any().
source_error([]) -> ok;
source_error(Errors) ->
  ErrorTexts = lists:map(fun({Reason, Name, _}) ->
    [Name, "<br/><i>", remote:parse_error(Reason), "</i>"]
  end, Errors),
  Message = [
    "Unable to retrieve projects for these services:<br/><br/>",
    ErrorTexts
  ],
  client:error("Retrieval Error", lists:flatten(Message)).

%%--------------------------------------------------------------------
%% @doc Pop error modal
%% @spec error(Title, Message) -> any()
%% @end
%%--------------------------------------------------------------------
-spec error(string(), string()) -> any().
error(Title, Message) ->
  wf:wire(["$('#modal_error .title').html('", Title ,"');"]),
  wf:wire(["$('#modal_error .content').html('", Message ,"');"]),
  wf:wire("Modal_openDefault('#modal_error');").

%%--------------------------------------------------------------------
%% @doc Wire API3 event
%% @spec api(Callback) -> any()
%% @end
%%--------------------------------------------------------------------
-spec api(atom()) -> any().
api(Callback) -> wf:wire(#api{name = Callback, tag = api3}).

%%--------------------------------------------------------------------
%% @doc Trigger a client-side event listener
%% @spec event(Trigger) -> any()
%% @end
%%--------------------------------------------------------------------
-spec event(atom() | string()) -> any().
event(init) -> event("init");
event(terminate) -> event("terminate");
event(Trigger) when is_list(Trigger) ->
  wf:wire(["$(wsWatcher).trigger('", Trigger, "');"]);
event(_) ->
  ok.

%%--------------------------------------------------------------------
%% @doc Trigger a client-side event listener sending data in JSON format
%% @spec event(Trigger, Map) -> any()
%% @end
%%--------------------------------------------------------------------
-spec event(string(), map()) -> any().
event(Trigger, Data) ->
  Json = jsone:encode(Data),
  wf:wire(["$(wsWatcher).trigger('", Trigger, "', ['", Json, "']);"]).
