%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Wrapper for interaction with the source API and data
%%% @end
%%%-------------------------------------------------------------------
-module(remote).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("apps/api/include/api.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([projects/2, project/3]).
-export([parse_error/1]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Retrieve a set of projects with a set of source errors
%% @spec projects(Token) -> {Projects, Errors}
%% @end
%%--------------------------------------------------------------------
% -spec projects(binary()) -> {[map()], [{error, any()}]}.
% projects(<<"">>) ->
%   {[], []};
% projects(Token) ->
%   ProjectBatches =
%     notmyjob:dmap(fun(#{name := Name, url := Url, endpoint_ref := Ref}) ->
%     Endpoint = api:get_endpoint(Ref, Token),
%     MaybeProjects = api:read(projects, Endpoint, []),
%     unwrap_projects(MaybeProjects, Name, Url)
%   end, ?SOURCES),
%   Errors = proplists:get_all_values(error, ProjectBatches),
%   Projects = lists:flatten(proplists:delete(error, ProjectBatches)),
%   {Projects, Errors}.

-spec project(binary(), binary(), binary()) -> {ok, map()} | {error, any()}.
project(Token, Source, ProjectId) ->
  Endpoint = api:get_endpoint(Source, Token),
  Params = [{project_id, ProjectId}],
  api:read(project, Endpoint, Params).

projects(_, <<"">>) ->
  [];
projects(SourceName, Token) ->
  Endpoint = api:get_endpoint(SourceName, Token),
  MaybeProjects = api:read(projects, Endpoint, []),
  verify_projects(MaybeProjects).
%%--------------------------------------------------------------------
%% @doc Transform source errors to text strings
%% @spec parse_error(ErrorTuple) -> ErrorText
%% @end
%%--------------------------------------------------------------------
-spec parse_error(tuple()) -> string().
parse_error({bad_status_code, 401, _}) ->
  <<"You are not authorized. Please verify your personal access token.">>;
parse_error({server_error, connect_timeout}) ->
  <<"This service took too long to respond">>;
parse_error(Other) ->
  wf:to_binary(Other).

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Unwrap source's return tuple
%%--------------------------------------------------------------------
verify_projects({error, Reason}) ->
  {error, parse_error(Reason)};
verify_projects(Projects) ->
  Projects.