-module(web_convert).
-author('Maxim Sokhatsky').
-author('Pave Kozlovsky').
-compile(export_all).
-include_lib("deps/n2o/include/wf.hrl").

% WF to_atom to_list to_binary

-define(IS_STRING(Term), (is_list(Term) andalso Term /= [] andalso is_integer(hd(Term)))).

-ifndef(N2O_JSON).
-define(N2O_JSON, (application:get_env(n2o,json,jsone))).
-endif.

html_encode(L,Fun) when is_function(Fun) -> Fun(L);
html_encode(L,EncType) when is_atom(L) -> html_encode(wf:to_list(L),EncType);
html_encode(L,EncType) when is_integer(L) -> html_encode(integer_to_list(L),EncType);
html_encode(L,EncType) when is_float(L) -> html_encode(float_to_list(L,[{decimals,9},compact]),EncType);
html_encode(L, false) -> L;
html_encode(L, true) -> L;
html_encode(L, whites) -> html_encode_whites(wf:to_list(lists:flatten([L]))).
html_encode(<<>>) -> [];
html_encode([]) -> [];
html_encode([H|T]) ->
	case H of
		$< -> "&lt;" ++ html_encode(T);
		$> -> "&gt;" ++ html_encode(T);
		$" -> "&quot;" ++ html_encode(T);
		$' -> "&#39;" ++ html_encode(T);
		$& -> "&amp;" ++ html_encode(T);
		BigNum when is_integer(BigNum) andalso BigNum > 255 ->
			%% Any integers above 255 are converted to their HTML encode equivilant,
			%% Example: 7534 gets turned into &#7534;
			[$&,$# | wf:to_list(BigNum)] ++ ";" ++ html_encode(T);
		Tup when is_tuple(Tup) -> 
			throw({html_encode,encountered_tuple,Tup});
		_ -> [H|html_encode(T)]
	end.

html_encode_whites([]) -> [];
html_encode_whites([H|T]) ->
	case H of
		$\s -> "&nbsp;" ++ html_encode_whites(T);
		$\t -> "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ++ html_encode_whites(T);
		$< -> "&lt;" ++ html_encode_whites(T);
		$> -> "&gt;" ++ html_encode_whites(T);
		$" -> "&quot;" ++ html_encode_whites(T);
		$' -> "&#39;" ++ html_encode_whites(T);
		$& -> "&amp;" ++ html_encode_whites(T);
		_ -> [H|html_encode_whites(T)]
	end.

-define(PERCENT, 37).  % $\%
-define(FULLSTOP, 46). % $\.
-define(IS_HEX(C), ((C >= $0 andalso C =< $9) orelse
    (C >= $a andalso C =< $f) orelse
    (C >= $A andalso C =< $F))).
-define(QS_SAFE(C), ((C >= $a andalso C =< $z) orelse
    (C >= $A andalso C =< $Z) orelse
    (C >= $0 andalso C =< $9) orelse
    (C =:= ?FULLSTOP orelse C =:= $- orelse C =:= $~ orelse
        C =:= $_))).

quote_dash(Atom) when is_atom(Atom) -> quote_dash(atom_to_list(Atom));
quote_dash(Int) when is_integer(Int) -> quote_dash(integer_to_list(Int));
quote_dash(Bin) when is_binary(Bin) -> quote_dash(binary_to_list(Bin));
quote_dash(String) -> quote_dash(String, []).

quote_dash([], Acc) -> lists:reverse(Acc);
quote_dash([C | Rest], Acc) when ?QS_SAFE(C) -> quote_dash(Rest, [C | Acc]);
quote_dash([$\s | Rest], Acc) -> quote_dash(Rest, [$- | Acc]);
quote_dash([C | Rest], Acc) -> <<Hi:4, Lo:4>> = <<C>>, quote_dash(Rest, [digit(Lo), digit(Hi), ?PERCENT | Acc]).

digit(0) -> $0;
digit(1) -> $1;
digit(2) -> $2;
digit(3) -> $3;
digit(4) -> $4;
digit(5) -> $5;
digit(6) -> $6;
digit(7) -> $7;
digit(8) -> $8;
digit(9) -> $9;
digit(10) -> $a;
digit(11) -> $b;
digit(12) -> $c;
digit(13) -> $d;
digit(14) -> $e;
digit(15) -> $f.