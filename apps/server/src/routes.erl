

%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A router invoked by server via http and ws requests
%%% @end
%%%-------------------------------------------------------------------
-module(routes).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
-include_lib("deps/n2o/include/wf.hrl").
%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([init/2, finish/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Router-specific callback function
%% @spec finish(State, Context) -> {ok, State, Context}
%% @end
%%--------------------------------------------------------------------
finish(State, Ctx) -> {ok, State, Ctx}.
%%--------------------------------------------------------------------
%% @doc Router-specific callback function
%% @spec init(State, Context) -> {ok, State, NewContext}
%% @end
%%--------------------------------------------------------------------
init(State, Ctx) ->
  Req = Ctx#cx.req,
  Path = wf:path(Req),
  wf:info(?MODULE,"Route: ~p~n",[Path]),
  {Module, Bindings} = route_prefix(Path),
  NewReq = cowboy_req:set([{bindings, Bindings}], Req),
  {ok, State, Ctx#cx{path = Path, module = Module, req = NewReq}}.

%%====================================================================
%% Private functions
%%====================================================================
route_prefix(<<"/ws/",P/binary>>) -> route_partition(P);
route_prefix(<<"/",P/binary>>) -> route_partition(P);
route_prefix(P) -> route_partition(P).

route_partition(Path) -> route(binary:split(Path, <<"/">>, [global, trim])).

%%--------------------------------------------------------------------
%% Routes by dynamic path segments
%%--------------------------------------------------------------------
%% MANAGER
% projects
route([<<"manager">>]) ->
  session:route({page_manager_projects, [
    {visibility, visible}
  ]});
route([<<"manager">>, <<"projects">>]) ->
  session:route({page_manager_projects, [
    {visibility, visible}
  ]});
route([<<"manager">>, <<"projects">>, <<"visible">>]) ->
  session:route({page_manager_projects, [
    {visibility, visible}
  ]});
route([<<"manager">>, <<"projects">>, <<"hidden">>]) ->
  session:route({page_manager_projects, [
    {visibility, hidden}
  ]});
route([<<"manager">>, <<"projects">>, <<"all">>]) ->
  session:route({page_manager_projects, [
    {visibility, all}
  ]});
% create
route([<<"manager">>, <<"projects">>, <<"new">>]) ->
  session:route({page_manager_new, []});

%% PROFILE
% tokens
route([<<"profile">>]) ->
  session:route({page_profile_access_tokens, []});

%% ANALYZER
%% projects
route([<<"projects">>]) ->
  session:route({page_projects, []});
%% projects/:Id
route([<<"projects">>, Id]) ->
  session:route({page_project, [
    {project_id, Id}
  ]});
%% snapshots/:Name
route([<<"snapshots">>, Hash]) ->
  session:route({page_snapshot, [
    {snapshot_name, Hash}
  ]});
%% snapshots/:Name/:Username
route([<<"snapshots">>, Hash, Username]) ->
  session:route({page_member_graphs, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% snapshots/:Hash/:Username/graphs
route([<<"snapshots">>, Hash, Username, <<"graphs">>]) ->
  session:route({page_member_graphs, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% snapshots/:Hash/:Username/merge_requests
route([<<"snapshots">>, Hash, Username, <<"merge_requests">>]) ->
  session:route({page_member_mr, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% snapshots/:Hash/:Username/commits
route([<<"snapshots">>, Hash, Username, <<"commits">>]) ->
  session:route({page_commits, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% snapshots/:Hash/:Username/notes
route([<<"snapshots">>, Hash, Username, <<"notes">>]) ->
  session:route({page_member_notes, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% snapshots/:Hash/:Username/scores
route([<<"snapshots">>, Hash, Username, <<"scores">>]) ->
  session:route({page_member_scores, [
    {snapshot_hash, Hash},
    {username, Username}
  ]});
%% sign_in
route([<<"sign_in">>]) ->
  {page_auth, [{action, verify_ticket}]};
%% sign_out
route([<<"sign_out">>]) ->
  {page_auth, [{action, sign_out}]};
%% 
route([]) ->
  session:route({page_projects, []});
%% *
route(_) ->
  session:route({page_404, []}).

