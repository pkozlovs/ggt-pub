-module(client_ws_api).

-include_lib("deps/nitro/include/nitro.hrl").

-export([create_api/1]).

-spec create_api(atom()|[atom()]) -> any().
create_api(Callbacks) when erlang:is_list(Callbacks) ->
  [wire_api(Callback) || Callback <- Callbacks];
create_api(Callback) ->
  wire_api(Callback).

wire_api(Callback) ->
  wf:wire(#api{name = Callback, tag = api3}).
