-module(client_window).

-export([redirect_replace/1]).

redirect_replace(Url) ->
  Query = io_lib:format("window.location.replace(\"~s\");", [Url]),
  wf:wire(Query).
