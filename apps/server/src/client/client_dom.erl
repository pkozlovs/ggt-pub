-module(client_dom).

-include_lib("deps/nitro/include/nitro.hrl").

-export([inner_html/2]).
-export([outer_html/2]).

-spec inner_html(atom(), list()) -> any().
inner_html(TargetId, Elements) ->
  update(innerHTML, TargetId, Elements).

-spec outer_html(atom(), list()) -> any().
outer_html(TargetId, Elements) ->
  update(outerHTML, TargetId, Elements).

update(Property, TargetId, Elements) ->
  wf:wire(#jq{
    target = TargetId,
    property = Property,
    right = wf:js_escape(wf:render(Elements)),
    format = "'~s'"
  }).
