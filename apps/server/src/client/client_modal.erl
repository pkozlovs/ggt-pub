-module(client_modal).

-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("deps/n2o/include/wf.hrl").

-export([show_ok/2]).
-export([show_error/2]).
-export([show_present/1]).
-export([close_current/0]).

show_ok(Title, Message) ->
  show(modal_ok, Title, Message).

show_error(Title, Message) ->
  show(modal_error, Title, Message).

show_present(Id) ->
  wf:wire(io_lib:format("window.modal.showPersistent(\"#~s\");", [Id])).

show(File, Title, Message) ->
  Id = wf:temp_id(),
  Dtl = #dtl{
    file = File,
    ext = "html",
    bind_script = false,
    bindings = [
      {id, Id},
      {title, Title},
      {message, Message}
    ]
  },
  Render = wf:js_escape(wf:render(Dtl)),
  wf:insert_bottom("modal_cache", Render),
  Query = io_lib:format("window.modal.show(\"#~s\");", [Id]),
  wf:wire(Query).

-spec close_current() -> any().
close_current() ->
  client_js:execute("$.modal.close();", []).