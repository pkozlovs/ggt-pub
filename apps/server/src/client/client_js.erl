-module(client_js).

-export([execute/2]).

-spec execute(io:format(), [term()]) -> ok.
execute(Format, Data) ->
  Query = io_lib:format(Format, Data),
  wf:wire(Query).
