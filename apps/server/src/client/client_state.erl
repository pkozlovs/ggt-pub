-module(client_state).

-include_lib("deps/nitro/include/nitro.hrl").

-export([create_state/1]).
-export([get_state/0]).
-export([get_state/1]).
-export([get_state/2]).

-spec create_state([{atom(), any()}]) -> ok.
create_state(StateData) ->
  State = #transfer{state = StateData},
  wf:wire(State).

-spec get_state() -> term() | undefined.
get_state() ->
  get_state(state).

-spec get_state(atom()) -> term() | undefined.
get_state(Key) ->
  get_state(Key, undefined).

-spec get_state(atom(), term()) -> term() | undefined.
get_state(Key, Default) ->
  case erlang:get(Key) of
    undefined -> Default;
    Value -> Value
  end.
