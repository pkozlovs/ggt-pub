-module(client_event).

-export([create/1]).
-export([trigger/1]).
-export([trigger/2]).

-include_lib("deps/nitro/include/nitro.hrl").

-define(EVENT_READY, "window.websocket.READY").
-define(EVENT_TERMINATED, "window.websocket.TERMINATED").

-spec create(atom()|[atom()]) -> any().
create(Callbacks) when erlang:is_list(Callbacks) ->
  [wire_api(Callback) || Callback <- Callbacks];
create(Callback) ->
  wire_api(Callback).

wire_api(Callback) ->
  wf:wire(#api{name = Callback, tag = api3}).

-spec trigger(atom()) -> ok.
trigger(ready) ->
  wire(?EVENT_READY);
trigger(terminated) ->
  wire(?EVENT_TERMINATED);
trigger(EventName) ->
  EventString = io_lib:format("\"~s\"", [erlang:atom_to_list(EventName)]),
  wire(EventString).

-spec trigger(atom(), binary()|string()) -> ok.
trigger(EventName, Data) ->
  wire(EventName, Data).

wire(EventString) ->
  client_js:execute("window.websocket.trigger(~s);", [EventString]).

wire(EventName, Data) ->
  client_js:execute("window.websocket.trigger(\"~s\", ~s);", [
    EventName, Data
  ]).