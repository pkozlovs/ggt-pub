-module(ws_async).

-export([spawn/1]).

-spec spawn(function()) -> {ok, pid()}.
spawn(Fun) ->
  Self = self(),
  AsyncFun = fun() ->
    Fun(),
    Actions = wf:actions(),
    wf:actions([]),
    try Self ! {flush, Actions} catch 
      _:_ ->
        error_logger:error_msg("Websocket no longer reachable: ~p~n", [Self])
    end
  end,
  {ok, erlang:spawn(AsyncFun)}.
