-module(convert_json).

-include_lib("apps/db/include/db.hrl").
-include_lib("apps/db/include/frontend_structs.hrl").

-export([convert_merge_requests/5]).
-export([convert_commits/2]).
-export([convert_date_range/1]).
-export([convert_buckets/1]).
-export([format_score/2]).

-spec convert_merge_requests(
        [db_merge_request:self()], 
        owned|contributed,
        ProjectId :: db_projet:id(),
        SnapshotId :: db_snapshot:id(),
        MemberId :: db_member:id()
) -> binary().

convert_merge_requests(MergeRequests, Type, ProjectId, SnapshotId, MemberId) ->
   
  MergeRequestsMaps = lists:map(fun(MergeRequest) ->
    {ok, Stats} = db:get_merge_request_stats(
      {merge_request_id, db_merge_request:id(MergeRequest)}
    ),
    {ok, Diffs} = db:get_merge_request_diffs(
      {merge_request_id, db_merge_request:id(MergeRequest)}
    ),
    FileNum = erlang:length(Diffs),
    Loc = db_merge_request_stats:net(Stats),
    Date = date_output(MergeRequest),
    Scores = local:merge_request_score(db_merge_request:id(MergeRequest), 
                db_merge_request:project_id(MergeRequest)),
    RawScores = Scores#cumulative_merge_request_score.raw_scores,

    % Compute the current user's score for just merge requests
    CommitScore = local:merge_request_commit_score(
                    db_merge_request:project_id(MergeRequest),
                    db_merge_request:id(MergeRequest), 
                    MemberId),
    io:format("~p - CommitScore: ~p *********************************!!~n", [?FUNCTION_NAME, CommitScore]),
    
    {Whitespace, Syntax, Moves, Refactors, Insertions, Deletions} =
      db_diff_scores:all(RawScores),
    % Sum = Scores#cumulative_merge_request_score.sum,
    Url = list_to_binary(io_lib:format(
      "<a href=\"~s\" target=\"_blank\" >"
        "<i class=\"material-icons\">&#xE895;</i>"
      "</a>",
      [db_merge_request:web_url(MergeRequest)])),
      #{
        <<"DT_RowId">> => hash:bin_to_hex(db_merge_request:id(MergeRequest)),
        title_msg => combine_title_message(db_merge_request:title(MergeRequest),
                                      db_merge_request:message(MergeRequest)),
        source_branch => db_merge_request:source_branch(MergeRequest),
        loc => Loc,
        file_num => FileNum,
        url => Url,
        type => Type,
        date => Date,
        whitespace => Whitespace,
        syntax => Syntax,
        moves => Moves,
        refactors => Refactors,
        insertions => Insertions,
        deletions => Deletions,
        sum => format_score(Scores#cumulative_merge_request_score.sum, 0),
        commitsum => format_score(CommitScore, 0)
      }
    end, MergeRequests),
  OrphanCommitHeader = commit_orphanage(
                         ProjectId,
                         SnapshotId,
                         MemberId),
  jsone:encode([OrphanCommitHeader|MergeRequestsMaps]).

commit_orphanage(ProjectId, SnapshotId, MemberId) ->
  OrphanedCommitScore = local:commits_without_mr_score(ProjectId, SnapshotId, MemberId),
  #{
    <<"DT_RowId">> => <<"not_mr">>,
    <<"DT_RowClass">> => <<"orphanage">>,
    title_msg => <<"Mergeless Commits">>,
    source_branch => <<"master">>,
    loc => <<"N/A">>,
    file_num => <<"N/A">>,
    url => <<"#">>,
    type => <<"N/A">>,
    date => <<"N/A">>,
    whitespace => <<"N/A">>,
    syntax => <<"N/A">>,
    moves => <<"N/A">>,
    refactors => <<"N/A">>,
    insertions => <<"N/A">>,
    deletions => <<"N/A">>,
    sum => <<"N/A">>,
    commitsum => format_score(OrphanedCommitScore, 0)
  }.

convert_commits(Commits, MemberEmails) ->
  CommitsMap = convert_commits_helper(Commits, MemberEmails),
  jsone:encode(CommitsMap).

convert_commits_helper([], _) ->
  [];
convert_commits_helper([H|T], MemberEmails) ->
  case is_integer(db_commit:id(H)) of
    true ->
      convert_commits_helper(T, MemberEmails);
    false ->
      {ok, Author} = db:get_author({author_id, db_commit:author_id(H)}),
      AuthorEmail = db_author:email(Author),
      Type = case lists:member(AuthorEmail, MemberEmails) of
        true  -> <<"owned">>;
        false -> <<"unowned">>
      end,
      {ok, Stats} = db:get_commit_stats({commit_id, db_commit:id(H)}),
      {Ins, Del, Net} = db_commit_stats:counts(Stats),
      ProjId = db_commit:project_id(H),
      Scores = local:score(db_commit:id(H), ProjId),
      RawScores = Scores#cumulative_commit_score.raw_scores,
      {Whitespace, Syntax, Moves, Refactors, Insertions, Deletions} =
        db_diff_scores:all(RawScores),
      Map = #{
        <<"DT_RowId">> => hash:bin_to_hex(db_commit:id(H)),
        <<"DT_RowClass">> => Type,
        title_msg => combine_title_message(db_commit:title(H),
                                           db_commit:message(H)),
        author => AuthorEmail,
        date => time:pretty_date(db_commit:create_date(H)),
        add => Ins,
        del => Del,
        net => Net,
        whitespace => Whitespace,
        syntax => Syntax,
        moves => Moves,
        refactors => Refactors,
        insertions => Insertions,
        deletions => Deletions,
        sum => format_score(Scores#cumulative_commit_score.sum, 0)
      },
      [Map|convert_commits_helper(T, MemberEmails)]
  end.

date_output(MergeRequest) ->
  case db_merge_request:merge_date(MergeRequest) of
    undefined -> undefined;
    Datetime -> time:pretty_date(Datetime)
  end.

-spec convert_date_range([calendar:date()|pre]) -> binary().
convert_date_range(DateRange) ->
  List = lists:map(fun(pre) ->
    <<"pre">>;
  (Date) ->
    time:pretty_date(Date)
  end, DateRange),
  List.
  % jsone:encode(List).

-spec convert_buckets(map()) -> list().
convert_buckets(Buckets) ->
  List = maps:to_list(Buckets),
  NewList = lists:map(fun({pre, Count}) ->
    [<<"pre">>, Count];
  ({Date, Count}) ->
    [time:pretty_date(Date), Count]
  end, List),
  NewList.

combine_title_message(Title, null) ->
  % edge case for merge requests originating from API call
  combine_title_message(Title, <<>>);
combine_title_message(Title, Message) -> 
  TitleTemp = binary_to_list(Title),
  MessageTemp = binary_to_list(Message),
  String = io_lib:format("~s: ~s", [TitleTemp, MessageTemp]),
  erlang:list_to_binary(String).

format_score(Value, NearestDegree) -> 
  float_to_binary(float(Value),[{decimals,NearestDegree}]).
