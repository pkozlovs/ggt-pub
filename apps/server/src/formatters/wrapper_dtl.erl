-module(wrapper_dtl).

-include_lib("deps/nitro/include/nitro.hrl").
-include_lib("apps/server/include/elements.hrl").

-export([construct_page/2]).
-export([construct_snippet/2]).
-export([construct_manager_projects/1]).
-export([construct_manager_project/1]).
-export([construct_snapshot_member/2]).
-export([construct_snapshot_author/2]).
-export([construct_option_member/2]).

-opaque dtl_pure() :: #dtl_pure{}.
-export_type([dtl_pure/0]).

-spec construct_page(string(), [{atom(), term()}]) -> dtl_pure().
construct_page(TemplateFileName, Bindings) ->
  #dtl_pure{
    file = TemplateFileName,
    ext = "html",
    app = "ggs",
    bindings = Bindings
  }.

-spec construct_snippet(string(), [{atom(), term()}]) -> dtl_pure().
construct_snippet(TemplateFileName, Bindings) ->
  #dtl_pure{
    file = TemplateFileName,
    ext = "html",
    bind_script = false,
    bindings = Bindings
  }.

-spec construct_manager_projects([source:project()]) -> [dtl_pure()].
construct_manager_projects([]) ->
  [];
construct_manager_projects([Project|Rest]) ->
  [construct_manager_project(Project)|construct_manager_projects(Rest)].

-spec construct_manager_project(source:project()) -> dtl_pure().
construct_manager_project(Project) ->
  construct_snippet("li_manager_project", [
    {name, source_project:special_name(Project)},
    {id, source_project:id(Project)},
    {url, source_project:web_url(Project)}
  ]).

-spec construct_snapshot_member(db:member(), binary()) -> dtl_pure().
construct_snapshot_member(Member, SnapshotId) ->
  MemberId = db_member:id(Member),
  SnapshotId_hex = hash:bin_to_hex(SnapshotId),
  URL = hackney_url:make_url(<<"">>, [
      <<"snapshots">>, SnapshotId_hex, db_member:username(Member)
  ], []),
  Notes = local:merge_request_notes(SnapshotId, MemberId),
  WordCount = local:count_words_in_notes(Notes),
  Score = db_diff_scores:flatten(
    result:value(db:get_total_commit_score(
      {snapshot_id, SnapshotId},
      {member_id, MemberId}
    ))
  ),
  ScoreString = float_to_list(float(Score), [{decimals,2}]),
  MemberEmails = local:member_emails(
    db_member:project_id(Member), db_member:id(Member)
  ),
  construct_snippet("tr_snapshot_member", [
    {name, db_member:name(Member)},
    {username, db_member:username(Member)},
    {url, URL},
    {id, hash:bin_to_hex(db_member:id(Member))},
    {emails, MemberEmails},
    {score, ScoreString},
    {word_count, WordCount}
  ]).

-spec construct_snapshot_author(db:author(), any()) -> dtl_pure().
construct_snapshot_author(Author, Members_DTL) ->
  AuthorEmail = db_author:email(Author),
  AuthorName = db_author:name(Author),
  AuthorId_hex = hash:bin_to_hex(db_author:id(Author)),
  construct_snippet("li_snapshot_author", [
    {email, AuthorEmail},
    {username, AuthorName},
    {id, AuthorId_hex},
    {members, Members_DTL}
  ]).

-spec construct_option_member(db:member(), binary()) -> any().
construct_option_member(Member, Selected) ->
  MemberName = db_member:name(Member),
  MemberId_hex = hash:bin_to_hex(db_member:id(Member)),
  [
    {name, MemberName},
    {select, Selected},
    {id, MemberId_hex}
  ].