-module(diff_clean).
-export([strip_diff_garbage/1, strip_context/1, is_change/1,
        remove_all_whitespace/1]).

% strips garbage until after the first @@ marker},
strip_diff_garbage([]) -> []; 
strip_diff_garbage([{_, <<"@@", _/binary>>}|Tail]) -> Tail;
strip_diff_garbage([_|Tail]) ->
  strip_diff_garbage(Tail).

strip_context(LinesList) ->
  lists:filter(fun is_change/1, LinesList).

is_change({_Loc, <<"+", _/binary>>}) -> true;
is_change({_Loc, <<"-", _/binary>>}) -> true;
is_change(_) -> false.

remove_all_whitespace(LineLocations) ->
  lists:map(fun({Loc, Line}) ->
    NewLine = re:replace(Line, "\\s", "", [{return, binary}, global]),
    {Loc, NewLine}
  end, LineLocations).


