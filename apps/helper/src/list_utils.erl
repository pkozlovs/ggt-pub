-module(list_utils).
-export([split_pairs/1, split_triplets/1, head/1, count/1, identicals/2]).
-export([number_elements/1]).
-export_type([one_or_none/1]).
-type one_or_none(Type) :: Type | none.

% Maps a list of homogenous pairs into 
% a pair of independently homogenous lists.
-spec split_pairs([{A, B}])    -> {[A], [B]}.
split_pairs([]) -> {[], []};
split_pairs([{One, Two} | Tail]) ->
  List = [{One, Two} | Tail],
  lists:foldl(fun({X, Y}, {Xs, Ys}) -> {[X | Xs], [Y | Ys]} 
  end, {[], []}, List).

% Maps a list of homogenous triplets into 
% a triplet of independently homogenous lists.
-spec split_triplets([{X, Y, Z}]) -> {[X], [Y], [Z]}.
split_triplets([]) -> {[], [], []};
split_triplets([{One, Two, Three} | Tail]) ->
  List = [{One, Two, Three} | Tail],
  lists:foldl(fun({X, Y, Z}, {Xs, Ys, Zs}) ->
    {[X | Xs], [Y | Ys], [Z | Zs]} 
  end, {[], [], []}, List).
    
-spec head([Type]) -> list_utils:one_or_none(Type).
head(List) -> 
  case List of 
    [H|_] -> H;
    [] -> none
  end.

-spec count(
  List :: list()
) -> non_neg_integer().
count(List) ->
  lists:foldl(fun(_, X) -> X+1 end, 0, List). 

identicals(As, Bs) -> [A || A <- As, B <- Bs, A == B].

number_elements(List) when is_list(List) ->
  {_Count, Numbered} = lists:foldl(fun(Elem, {N, Acc}) -> 
    {N+1, [{N, Elem}|Acc]} end, {0, []}, List),
  lists:reverse(Numbered).
