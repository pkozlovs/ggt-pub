%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Time formatting and conversions.
%%% @end
%%%-------------------------------------------------------------------
-module(time).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([datetime_to_iso8601/1]).
-export([iso8601_to_datetime/1]).
-export([timestamp_to_datetime/1]).
-export([date_to_timestamp/1]).
-export([pretty/1]).
-export([pretty_simple/1]).
-export([pretty_date/1]).
-export([datetime_to_timestamp/1]).

-export([change_timezone/3]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc datetime() to bin iso8601
%% @spec datetime_to_iso8601(DateTime) -> Parseable
%% @end
%%--------------------------------------------------------------------
-spec datetime_to_iso8601(calendar:datetime()) -> binary().
datetime_to_iso8601(DateTime) -> iso8601:format(DateTime).

%%--------------------------------------------------------------------
%% @doc bin iso8601 to datetime()
%% @spec iso8601_to_datetime(Parseable) -> DateTime
%% @end
%%--------------------------------------------------------------------
-spec iso8601_to_datetime(binary()) -> calendar:datetime().
iso8601_to_datetime(Parseable) -> iso8601:parse(Parseable).

%%--------------------------------------------------------------------
%% @doc bin unix timestamp to datetime()
%% @spec iso8601_to_datetime(Parseable) -> DateTime
%% @end
%%--------------------------------------------------------------------
-spec timestamp_to_datetime(binary()) -> calendar:datetime().
timestamp_to_datetime(Timestamp) ->
  Timestamp_int = erlang:binary_to_integer(Timestamp),
  qdate:to_date(Timestamp_int).

-spec date_to_timestamp(calendar:date()) -> integer().
date_to_timestamp({Y, M, D}) ->
  calendar:datetime_to_gregorian_seconds({{Y, M, D}, {0, 0, 0}}) - 62167219200.

datetime_to_timestamp(Datetime) ->
  {Date, {H, M, S}} = Datetime,
  calendar:datetime_to_gregorian_seconds({Date, {H, M, S}}) - 62167219200.

-spec pretty(calendar:datetime()) -> binary().
pretty(Datetime) -> 
  {Date, {H, M, S}} = Datetime,
  NewS = case S of 60 -> 59; _ -> S end,
  list_to_binary(httpd_util:rfc1123_date({Date, {H, M, NewS}})).

-spec pretty_simple(calendar:datetime()) -> binary().
pretty_simple({{Y, M, D}, {H, Min, S}}) ->
  list_to_binary(io_lib:format(
    "~4..0w/~2..0w/~2..0w ~2..0w:~2..0w:~2..0w",
    [Y, M, D, H, Min, S]
  )).

-spec pretty_date(calendar:date()) -> binary();
                (calendar:datetime()) -> binary().
pretty_date({Y, M, D}) ->
  String = io_lib:format("~s. ~B, ~B", [
    httpd_util:month(M),
    D,
    Y
  ]),
  erlang:list_to_binary(String);

pretty_date({{Y, M, D}, _}) ->
  list_to_binary(io_lib:format(
    "~4..0w/~2..0w/~2..0w",
    [Y, M, D]
  )).

-spec change_timezone(calendar:datetime(), string(), string()) ->
  calendar:datetime().
change_timezone(Datetime, FromTZ, ToTZ) ->
  Self = self(),
  erlang:spawn(fun() ->
    qdate:set_timezone(FromTZ),
    Self ! {time_convert, qdate:to_date(ToTZ, prefer_standard, Datetime)}
  end),
  receive
    {time_convert, NewDateTime} -> NewDateTime
  end.

