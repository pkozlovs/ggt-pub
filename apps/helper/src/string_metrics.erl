%%%=============================================================================
%%% @author Adam Lindberg <adam@erlang-consulting.com>
%%%         Fredrik Svensson
%%% @doc This module contains string metric operations.
%%%
%%% Currently only the function {@link levenshtein/2}.
%%% @end
%%%=============================================================================
-module(string_metrics).
 
-export([levenshtein/2]).
 
%%------------------------------------------------------------------------------
%% @spec levenshtein(StringA, StringB) -> integer()
%%      StringA = StringB = string()
%% @doc Calculates the Levenshtein distance between two strings
%% @end
%%------------------------------------------------------------------------------
levenshtein(Samestring, Samestring) -> 0;
levenshtein(String, []) -> length(String);
levenshtein([], String) -> length(String);
levenshtein(Source, Target) when is_list(Source) andalso is_list(Target) ->
    levenshtein_rec(Source, Target, lists:seq(0, length(Target)), 1);
levenshtein(Source, Target) when is_binary(Source) andalso is_binary(Target) ->
    bin_levenshtein_rec(Source, Target, lists:seq(0, byte_size(Target)), 1).

bin_levenshtein_rec(<<H:8, Tail/binary>>, Target, DistList, Step) ->
  bin_levenshtein_rec(Tail, Target, bin_levenshtein_distlist(Target, DistList, H, [Step], Step), Step + 1);
bin_levenshtein_rec(<<>>, _, DistList, _) ->
  lists:last(DistList).

bin_levenshtein_distlist(<<H:8,T/binary>>, [DLH|DLT], SourceChar, NewDistList, 
                        LastDist) when length(DLT) > 0 ->
  Min = lists:min([LastDist + 1, hd(DLT) + 1, DLH + dif(H, SourceChar)]),
  bin_levenshtein_distlist(T, DLT, SourceChar, NewDistList ++ [Min], Min);
bin_levenshtein_distlist(<<>>, _, _, NewDistList, _) ->
  NewDistList.
 
%% Recurses over every character in the source string and calculates a list of distances
levenshtein_rec([SrcHead|SrcTail], Target, DistList, Step) ->
    levenshtein_rec(SrcTail, Target, levenshtein_distlist(Target, DistList, SrcHead, [Step], Step), Step + 1);
levenshtein_rec([], _, DistList, _) ->
    lists:last(DistList).
 
%% Generates a distance list with distance values for every character in the target string
levenshtein_distlist([TargetHead|TargetTail], [DLH|DLT], SourceChar, NewDistList, LastDist) when length(DLT) > 0 ->
    Min = lists:min([LastDist + 1, hd(DLT) + 1, DLH + dif(TargetHead, SourceChar)]),
    levenshtein_distlist(TargetTail, DLT, SourceChar, NewDistList ++ [Min], Min);
levenshtein_distlist([], _, _, NewDistList, _) ->
    NewDistList.
 
% Calculates the difference between two characters or other values
dif(C, C) -> 0;
dif(_, _) -> 1.
