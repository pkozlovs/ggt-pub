-module(augmented_diff).
-export([augment/2]).

-define(RECORD(Rec, Name), element(1, Rec) == Name).

augment(CommitDiff, Locations) when ?RECORD(CommitDiff, db_commit_diff) ->
  Diff = db_commit_diff:diff(CommitDiff),
  augment_diff(Diff, Locations);
augment(MrDiff, Locations) when ?RECORD(MrDiff, db_merge_request_diff) ->
  Diff = db_merge_request_diff:diff(MrDiff),
  augment_diff(Diff, Locations).

augment_diff(Diff, Locations) when is_binary(Diff) ->
  Lines = split_diff(Diff),
  NumberedLines = list_utils:number_elements(Lines),
  LineMap = maps:from_list(NumberedLines),
  FinalMap = lists:foldl(fun(Type, Map) -> 
    augment_type(Type, Locations, Map)
  end, LineMap, [whitespace, syntax, moves, refactors, insertions, deletions]),
  reconstruct_diff(FinalMap). % TODO
  
split_diff(Bin) ->
  binary:split(Bin, <<"\n">>, [global]).

augment_type(Type, Locations, LineMap) ->
  LocationList = db_diff_score_locations:Type(Locations),
  Prefix = prefix_for_type(Type),
  recursively_augment(LocationList, Prefix, LineMap).

prefix_for_type(whitespace) -> <<"%W">>;
prefix_for_type(syntax) -> <<"%S">>;
prefix_for_type(moves) -> <<"%M">>;
prefix_for_type(refactors) -> <<"%R">>;
prefix_for_type(insertions) -> <<"%I">>;
prefix_for_type(deletions) -> <<"%D">>.

recursively_augment([], _Prefix, LineMap) ->
  LineMap;
recursively_augment([{N, M}|Tail], Prefix, LineMap) 
                    when is_integer(N), is_integer(M) ->
  Map1 = prefix_at(N, Prefix, LineMap),
  Map2 = prefix_at(M, Prefix, Map1),
  recursively_augment(Tail, Prefix, Map2);
recursively_augment([N|Tail], Prefix, LineMap) when is_integer(N) ->
  NewMap = prefix_at(N, Prefix, LineMap),
  recursively_augment(Tail, Prefix, NewMap).
  
prefix_at(N, Prefix, LineMap) ->
  case maps:get(N, LineMap) of
    {badkey, _} -> LineMap;
    Line ->
      maps:put(N, <<Prefix/binary, Line/binary>>, LineMap)
  end.

reconstruct_diff(ListMap) ->
  List = maps:to_list(ListMap),
  Sorted = lists:sort(fun first_element_sort/2, List),
  Denumbered = [L || {_, L} <- Sorted],
  NewLined = [<<L/binary, <<"\n">>/binary>> || L <- Denumbered],
  list_to_binary(NewLined).



first_element_sort({N, _}, {M, _}) ->
  N =< M.


  
