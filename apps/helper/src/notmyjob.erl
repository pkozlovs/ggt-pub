%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Provides utility functions inteded to make one's life eaier.
%%% @end
%%%-------------------------------------------------------------------
-module(notmyjob).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([dmap/2]).
%%====================================================================
%% Macros
%%====================================================================
-define(NUM_PROC_MAX, 2048).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Maps function Fun to each element of the list List1 in parallel.
%% It is recommended to use this instead of lists:map/2 when Fun does
%% a lot of work.
%% @spec dmap(fun((T) -> U), [T]) -> [U]
%% @end
%%--------------------------------------------------------------------
-spec dmap(fun((T) -> U), [T]) -> [U] | {error, any()}.
dmap(Fun, List) ->
  Parent = self(),
  {Pid, MRef} = spawn_monitor(fun() ->
    Result = split_spawn(Fun, List, [], 0),
    Parent ! {self(), Result}
  end),
  receive
    {Pid, Result}                       -> demonitor(MRef, [flush]), Result;
    {'DOWN', MRef, process, Pid, Other} -> {error, Other}
  end.

%%====================================================================
%% Private functions
%%====================================================================
%%--------------------------------------------------------------------
%% Safety mechanism that partitions concurrent processes into
%% sequential batches (related to port_limit)
%%--------------------------------------------------------------------
split_spawn(_, [], Buffer, _) ->
  OrderedBuffer = lists:reverse(Buffer),
  rebuild_list(OrderedBuffer);
split_spawn(Fun, List, Buffer, ?NUM_PROC_MAX) ->
  case rebuild_list(Buffer) of
    {error, Reason} ->
      {error, Reason};
    CompletedList   ->
      lists:reverse(CompletedList) ++ split_spawn(Fun, List, [], 0)
  end;
split_spawn(Fun, [Head|Tail], Buffer, Count) ->
  Parent = self(),
  Reference = spawn_monitor(fun() -> async_transform(Parent, Fun, Head) end),
  split_spawn(Fun, Tail, [Reference|Buffer], Count + 1).

%%--------------------------------------------------------------------
%% Applies Fun to Item and sends the result to the supervising process
%%--------------------------------------------------------------------
async_transform(Parent, Fun, Item) -> Parent ! {self(), Fun(Item)}.

%%--------------------------------------------------------------------
%% Monitors map processes, and collects their responses to rebuild a new list
%%--------------------------------------------------------------------
rebuild_list([]) -> [];
rebuild_list([{Pid, MRef}|Rest]) ->
  receive
    {Pid, Result} ->
      demonitor(MRef, [flush]),
      case rebuild_list(Rest) of
        {error, Reason} -> {error, Reason};
        List            -> [Result|List]
      end;
    {'DOWN', MRef, process, Pid, Reason} ->
      {error, {Pid, Reason}}
  end.
