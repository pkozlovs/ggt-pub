%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc Module for extracting information from server's environment
%%% @end
%%%-------------------------------------------------------------------
-module(env).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([config/3]).
-export([arg/1, home_dir/0]).
-export([join/1]).
-export([chmod/2]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Returns environment variables defined in sys.config
%% @spec config(App, Par, Def) -> Val
%% @end
%%--------------------------------------------------------------------
-spec config(atom(), atom(), term()) -> term().
config(App, Par, Def) -> application:get_env(App, Par, Def).

%%--------------------------------------------------------------------
%% @doc Returns all values associated with the command-line user flag Flag.
%% @spec arg(Flag) -> {ok, Arg} | error
%% @end
%%--------------------------------------------------------------------
-spec arg(atom()) -> {ok, [[string()]]} | error.
arg(Flag) -> init:get_argument(Flag).

%%--------------------------------------------------------------------
%% @doc get $HOME.
%% @spec home_dir(Flag) -> Path
%% @end
%%--------------------------------------------------------------------
-spec home_dir() -> string().
home_dir() ->
  case env:arg(home) of
    {ok, [[Path]]} -> Path;
    error -> error
  end.

%%--------------------------------------------------------------------
%% @doc Joins a list of filenames/dirs with a '/'
%% @spec join(List) -> Path
%% @end
%%--------------------------------------------------------------------
-spec join(list()) -> file:filename_all().
join(List) -> filename:join(List).

%%--------------------------------------------------------------------
%% @doc Changes file's permissions
%% @spec chmod(Filename, Mode) -> ok | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec chmod(string(), integer()) -> ok | {error, any()}.
chmod(Filename, Mode) -> file:change_mode(Filename, Mode).
