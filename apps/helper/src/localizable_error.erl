-module(localizable_error).
-export([pretty/1]).
% debug
-export([pretty/2, message/1]).

-spec pretty({error, any()}) 
  -> {iolist(), UnhandledReason :: any()}.
pretty({error, Reason}) ->
  {List, Unhandled} = pretty({error, Reason}, []),
  {lists:reverse(List), Unhandled}.

pretty(Reason, Acc) ->
  case message(Reason) of
    {done, Description, Unhandled} ->
      {[Description|Acc], Unhandled};
    {more, Description, MoreError} ->
      pretty(MoreError, [Description|Acc]);
    OtherBullshit ->
      erlang:error({'some other bullshit', OtherBullshit})
  end.

message({error, More}) ->
  {more, "Error in ", More};
% Snapshot Generator
message({snapshot_generator, More}) ->
  {more, "Snapshot Generator: ", More};
message({api, Error, Call, Reason}) ->
  api_message(Error, Call, Reason);
message({db, failed_create, snapshot, Reason}) ->
  {done, "database failed to create snapshot.", Reason};
message({analysis_crashed, Reason}) ->
  {done, "analysis crashed due to internal error.", Reason};
message({unexpected_message, Reason}) ->
  {done, "analysis stopped due to internal error.", Reason};
% Git Core Analysis
message({git_core_analysis, More}) ->
  {more, "Git Analysis: ", More};
message({git_core, failed, clone, Reason}) ->
  {done, "git core failed clone.", Reason};
message({git_core, failed, pull, Reason}) ->
  {done, "git core failed pull.", Reason};
message({git_core, failed, get_commits, Reason}) ->
  {done, "git core failed extracting commits.", Reason};
% Git Core Convert
message({git_core_convert, {dmap, commits, Reason}}) ->
  {done, "dmap crashed processing git commits.", Reason};
message({git_core_convert, {dmap, commit_file_diffs, Reason}}) ->
  {done, "dmap crashed processing git file diffs.", Reason};
% GitLab Analysis
message({gitlab_analysis, More}) ->
  {more, "GitLab: ", More};
% Meta Analysis
message({meta_analysis, More}) ->
  {more, "Combined Analysis: ", More};
message({db, error_storing, Reason}) ->
  {done, "database failed to store all results.", Reason};
message({dmap, Object, Reason}) ->
  dmap_message(Object, Reason);
% Result joining
message({{left_error, Reason1}, {right_error, Reason2}}) ->
  {Pretty1, Extra1} = pretty(Reason1, []),
  {Pretty2, Extra2} = pretty(Reason2, []),
  {done, 
    ["Two Reasons: ", 
      lists:reverse(Pretty1), " and ", 
      lists:reverse(Pretty2)
    ], 
    {{left, Extra1}, {right, Extra2}}
  };
message({{errors, [Error|_]}, {values, _}}) ->
  {Pretty, Extra} =  pretty(Error, []),
  {done, ["Multiple Errors. First: ", lists:reverse(Pretty)], Extra};
message(Unknown) ->
  {done, "unknown error.", Unknown}.

dmap_message(filter_merge_requests, Reason) ->
  {done, "dmap crashed filtering merge requests to snapshot.", Reason};
dmap_message(merge_request_notes, Reason) ->
  {done, "dmap crashed processing merge request notes.", Reason};
dmap_message(merge_request_diffs, Reason) ->
  {done, "dmap crashed processing merge_request_diffs.", Reason};
dmap_message(merge_request_diff_scores, Reason) ->
  {done, "dmap crashed while processing merge request scores", Reason};
dmap_message(merge_request_commits, Reason) ->
  {done, "dmap crashed processing merge_request_commits.", Reason}.

api_message(failed_read, project_info, Reason) ->
  {done, "failed to get project info.", Reason};
api_message(failed_read, members, Reason) ->
  {done, "failed to get members", Reason};
api_message(failed_read, merge_requests, Reason) ->
  {done, "failed to get merge requests.", Reason};
api_message(failed_read, merge_request_commits, Reason) ->
  {done, "failed to get commits for merge requests.", Reason};
api_message(failed_read, issues, Reason) ->
  {done, "failed to get project issues.", Reason};
api_message(failed_read, merge_request_notes, Reason) ->
  {done, "failed to get merge request notes.", Reason}.



