%%%-------------------------------------------------------------------
%%% Author : Pavel Kozlovsky <pkozlovs@sfu.ca>
%%% @doc A message bus that pings processes registered under pools.
%%% @end
%%%-------------------------------------------------------------------
-module(proc_bus).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([register_process/3, deregister_process/2]).
-export([notify_send/1, notify/1]).
%%--------------------------------------------------------------------
%% Macro definitions
%%--------------------------------------------------------------------
-define(PROC_GROUP, process_bus_group).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Registers process to receive notifications
%% @spec register_process(Pool, Pid, Fun) -> ok
%% @end
%%--------------------------------------------------------------------
-spec register_process(atom(), pid(), function()) -> ok.
register_process(Pool, Pid, Fun) ->
  syn:join({?PROC_GROUP, Pool}, Pid, #{exec => Fun}).

%%--------------------------------------------------------------------
%% @doc Removes process from notification registry
%% @spec deregister_process(Pool, Pid) -> ok | {error, Reason}
%% @end
%%--------------------------------------------------------------------
-spec deregister_process(atom(), pid()) -> ok | {error, any()}.
deregister_process(Pool, Pid) ->
  syn:leave({?PROC_GROUP, Pool}, Pid).

%%--------------------------------------------------------------------
%% @doc Notifies all processes registered under Pool
%% @spec notify(Pool) -> [NotifyWorkerPid]
%% @end
%%--------------------------------------------------------------------
-spec notify(atom()) -> [pid()].
notify(Pool) ->
  RegProcs = syn:get_members({?PROC_GROUP, Pool}, with_meta),
  lists:map(fun({_, #{exec := Fun}}) ->
    spawn(fun() -> Fun() end)
  end, RegProcs).

%%--------------------------------------------------------------------
%% @doc Notifies all processes registered under Pool and send response to them
%% @spec notify(Pool) -> [NotifyWorkerPid]
%% @end
%%--------------------------------------------------------------------
-spec notify_send(atom()) -> [pid()].
notify_send(Pool) ->
  RegProcs = syn:get_members({?PROC_GROUP, Pool}, with_meta),
  lists:map(fun({Pid, #{exec := Fun}}) ->
    spawn(fun() -> Result = Fun(), Pid ! Result end)
  end, RegProcs).
