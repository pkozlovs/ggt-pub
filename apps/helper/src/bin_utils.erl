-module(bin_utils).

-export([strip_blanks/1]).
-export([contains_unsafe/1]).

-spec strip_blanks(binary()) -> binary().
strip_blanks(Binary) ->
  re:replace(
    re:replace(Binary, <<"\\s+$">>, <<"">>, [global,{return,binary}]),
    <<"^\\s+">>, <<"">>, [global,{return,binary}]
  ).

-spec contains_unsafe(binary()) -> nomatch | {match, any()}.
contains_unsafe(Binary) ->
  re:run(
    Binary,
    <<"[^a-zA-Z 0-9-_,]+">>,
    [global, {capture, all, binary}]
  ).