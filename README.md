# Great Git Stats

To familiarize yourself with the project, please peruse `docs/`, particularly [`GettingStarted.md`](docs/GettingStarted.md)

## Installation and Building The Project
Run from the root of the project:
```
sudo ./build.sh
```
This will take a while (this is everything you will ever need for development, really), so go watch [Silicon Valley](http://www.imdb.com/title/tt2575988/) in the meantime.


## Manually Installing, Building and Running This Project
Everything is done from the project's root dir.

### Installation
This installs all of your development requirements
```
sudo ./bootstrap.sh
```

### Get Dependencies
Has to be done only once, unless the dependencies change.
```
$ sudo mad deps
```
When running `mad`, if you get the error `Loading of /home/brian/bin/erlang/mad/mad.beam failed: badfile` then copy `cp scripts/pre-built-binaries/mad /usr/local/bin`.
### Compile
Can be done in a different terminal window/tab while the server is running. The recompiled modules will be automatically picked up and reloaded.
```
$ mad comp
```
### Run the Server
(sudo is important as we are overwriting the default number of allowed open ports in the current shell)
```
$ sudo mad plan rep
```
Website should be accessible at `localhost:9999`<br>
**Note:** The server can be stopped with `Ctrl-C`; `Ctrl-C` may cause graphical bugs in current terminal session.
#### Recommended way to have server running detached:
1. Install `screen`
```
$ apt-get install screen
```
2. Start screen, run the server in it
```
$ screen
$ sudo mad plan rep
```
3. To detach: `Ctrl+A+D`. To return to the screen session, find yours with `screen ls` and reconnect `screen -r <session_id>`

### Clean the Project
Cleaning erlang, sass and js
```
$ mad clean
```
Removing storage items
```
$ sudo rm -rf Mnesia.*
$ sudo rm -rf ~/.ggs
```

## On Database

### Summary

All database will be local to your machine for now. The database will be called `Mnesia.whatever` in the project root. It is already in the `.gitignore`. Any changes you make to the database are local to your machine. If you mess up or change record definitions, remove the entire folder and rerun the server. check `db.[erl|hrl]` for Mnesia access API.

### Usage

Basic CRU is fully supported by the current API. All functions return either `{ok, Result}` or `{error, Reason}`. The most common error you'll see is `{error, none}`, meaning no record in the DB matched your access parameters. 

Currently there are four generic functions that can get, create, and update records in the DB.

- `get_record_by_id(ID, Table)` where `ID` is the primary ID of the record you're looking for, and `Table` is the name of the table—and, consequently, record—that you're wishing to access. 
- `get_record_with_pred(Predicate, Table)` where `Predicate` is a function with type `(#record-type) -> bool`. The _first_ record where this predicate returns `true` will be returned. 
- `update_record(Record)` simply updates the record in the DB by its primary ID. *Warning:* this method does not check if the record already exists and _will_ succeed if it does not, effectively acting as a `create` but with none of the checks.
- `create_record(Table, Fields)` will safely create a record in the DB. `Table` is the name of the table/record, and `Fields` is a tuple of all parameters in the record _except_ its ID (since that is assigned by the database itself). An important caveat right now is that `create_record/2` is extremely naive, and does allow for creating a new project record, even if one already exists with an identical `source()` and `project_id`. To this end, creating records should be used with care until independent creation functions are written, at which point this method will be removed. Consider it deprecated.

### Logging

This project uses the Lager library for all logging purposes. Lager has an `error_logger` handler which reformats the logs it receives nicer. Should you want to disable this feature, add the tuple `{error_logger_redirect, false}` before the `handler` tuple
in `sys.config`. Note that error_logger calls will not be logged anymore, so you should not push this change in the config file to live servers. When wanting to use lager functions, add the compile flag: `-compile({parse_transform, lager_transform}).` 
Logs are found under the `log` folder in the root directory. Currently, there are 4 log files, debug, console, error, and crash. Files can be rotated externally, or internally via options in `sys.config` in our root directory. `console.log`, `debug.log`, and `error.log` are rotated when either 10mb file size is reached or at midnight. The latter 2 are rotated within 5 files, while `console.log` deletes itself once either conditions are met. More information and guides can be found at https://github.com/erlang-lager/lager.

### Contributing

There are some simple rules to follow when contributing to the database:

- When defining records, always have the first field be `id` with type `non_neg_integer()`. 
- Records must have the same name as the table they're in. 
- Do not add lists as property that could conceivably be used to search on: such many-to-one relations are better kept in a new record type.
- Try to use `binary()` fields in place of strings.
- If you feel the need to write a custom db accessor, first make sure that the generic `get_record_with_pred/2` does not fit your use-case (see usage, above).
- Look at the project CRUD functions for examples of how to interact with the DB. 
- Write/use `dirty` methods when composing complex queries and then run all of them in a single transaction. 
