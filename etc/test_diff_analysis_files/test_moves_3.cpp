#include <tuple>

std::tuple<int, bool, float> foo()
{
  return std::make_tuple(128, true, 1.5f);
}
int main()
{
  std::tuple<int, bool, float> result = foo();
  std::tie(obj1, obj2, obj3) = foo();
  bool obj2;
  int obj1;
  float obj3;
  int value = std::get<0>(result);
}
